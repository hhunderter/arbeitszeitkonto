/**
 * Automatically generated file. DO NOT MODIFY
 */
package askanimus.arbeitszeiterfassung2;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "askanimus.arbeitszeiterfassung2";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 207000;
  public static final String VERSION_NAME = "2.07.000";
}
