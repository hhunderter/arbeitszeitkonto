/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.appwidget.AppWidgetManager;
import android.content.ActivityNotFoundException;

import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;

import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.ActionBar;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.SearchView;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

//import org.apache.commons.io.IOUtils;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import askanimus.arbeitszeiterfassung2.arbeitsjahr.ArbeitsjahrPager;
import askanimus.arbeitszeiterfassung2.arbeitsmonat.ArbeitsmonatPager;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.ArbeitsplatzExpandFragment;
import askanimus.arbeitszeiterfassung2.arbeitstag.ArbeitstagExpandListAdapter;
import askanimus.arbeitszeiterfassung2.arbeitstag.ArbeitstagFragment;
import askanimus.arbeitszeiterfassung2.arbeitstag.ArbeitstagPager;
import askanimus.arbeitszeiterfassung2.arbeitswoche.ArbeitswochePager;
import askanimus.arbeitszeiterfassung2.charts.ChartsFragment;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.datensicherung.AAutoBackup;
import askanimus.arbeitszeiterfassung2.datensicherung.Datensicherung_Activity;
import askanimus.arbeitszeiterfassung2.export.AExportBasis;
import askanimus.arbeitszeiterfassung2.export.IExport_Basis;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;
import askanimus.arbeitszeiterfassung2.setup.InitAssistent;
import askanimus.arbeitszeiterfassung2.setup.LocaleHelper;
import askanimus.arbeitszeiterfassung2.setup.SettingsActivity;
import askanimus.arbeitszeiterfassung2.export.ExportActivity;
import askanimus.arbeitszeiterfassung2.suche.Suche_Activity;
import askanimus.arbeitszeiterfassung2.widget.Stempeluhr;

import static askanimus.arbeitszeiterfassung2.setup.ASetup.*;

public class MainActivity
        extends 
        AppCompatActivity 
        implements
        NavigationDrawerFragment.NavigationDrawerCallbacks,
        ArbeitstagExpandListAdapter.ArbeitstagListeCallbacks,
        ArbeitstagFragment.ArbeitstagMainCallbacks,
        ISetup{


    //private Context mContext = this;
    private StorageHelper mStorageHelper;
    //private String TUTORIAL_PATH;

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /*
        wird zum anpassen der App Sprache benötigt, wenn diese von der Systemsprache abweicht
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

  @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(
                PreferenceManager
                        .getDefaultSharedPreferences(getApplicationContext())
                        .getBoolean(ASetup.KEY_THEMA_DUNKEL, false) ?
                        R.style.MyAppTheme :
                        R.style.MyAppTheme_Light
        );

        setContentView(R.layout.activity_main);

        // allgem. Steuerung und Navigation
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        // Set up the drawer.
        assert mNavigationDrawerFragment != null;
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                findViewById(R.id.drawer_layout));
    }

    @Override
    public void onResume() {
        super.onResume();
        String mAction = getIntent().getAction();
        if(mAction!= null && mAction.contains(APP_RESET)){
            zustand=INIT_ZUSTAND_UNGELADEN;
        }
        ASetup.init(this, this::resume);
    }

    private void resume() {
        // die Werte des aktuellen Arbeitsplatzes einlesen
        // Beim ersten Start der App neuen Job mit Defaultwerten anlegen
        if (mPreferenzen.contains(KEY_INIT_FINISH)) {
            // Aktuelles Datum setzen und Zeitwerte auf 0 setzen
            aktDatum.set(new Date());

            // den letzten Tag auf den aktuellen Tag setzen
            letzterAnzeigeTag.set(getLetzterAnzeigeTag(aktJob).getCalendar());

            SharedPreferences.Editor mEdit = mPreferenzen.edit();

            //
            // In Version 1.02.93 ist die Option für Dezimalminuten von den allgem. Einstellungen in
            // die Einstellungen des arbeitsplatz gewandert
            // Alle Arbeitsplätze inkl. den schon geladenen aktuellen müssen nun aktuallisiert werden
            //
            if (mPreferenzen.contains(KEY_ANZEIGE_DEZIMAL)) {
                boolean dezimal = mPreferenzen.getBoolean(KEY_ANZEIGE_DEZIMAL, true);
                //ArbeitsplatzListe aListe = new ArbeitsplatzListe(Einstellungen.aktJob);
                ArrayList<Arbeitsplatz> al = jobListe.getListe();
                if (al != null) {
                    for (Arbeitsplatz a : al) {
                        a.setOption(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL, dezimal);
                        a.schreibeJob();
                    }
                    mEdit.remove(KEY_ANZEIGE_DEZIMAL).apply();
                }
            }
            // Widgets neu zeichnen wenn die App von den Einstelluneg zurückkehrt
            if (mPreferenzen.getBoolean(KEY_RESUME_VIEW, false)) {
                AppWidgetManager wm = AppWidgetManager.getInstance(this);
                ComponentName cn = new ComponentName(getPackageName(), Stempeluhr.class.getName());
                    /*int[] wi = wm.getAppWidgetIds(cn);
                    for (int appWidgetId : wi) {
                        Stempeluhr.updateAppWidget(this, wm, appWidgetId);
                    }*/
                wm.notifyAppWidgetViewDataChanged(wm.getAppWidgetIds(cn), R.layout.stempeluhr2x4);
            }

            // den Pfad für den Speicherort des Handbuchs setzen
            //TUTORIAL_PATH = Environment.getExternalStorageDirectory().toString();
            //TUTORIAL_PATH += File.separator + res.getString(R.string.app_verzeichnis);

            // wenn die App mit einer bestimmten Ansicht und einen bestimmten Datum geöffnet werden soll
            Bundle extras = getIntent().getExtras();

            if (extras != null && extras.containsKey(KEY_ANZEIGE_VIEW)) {
                // die Activity wurde mit einen konkreten Datum und arbeitsplatz aufgerufen
                // z.B.: von der suche Activity oder Stempeluhr
                if (extras.getLong(KEY_JOBID) != aktJob.getId()) {
                    setAktivJob(extras.getLong(KEY_JOBID));
                    setAnzeigeOptionen();
                    letzterAnzeigeTag.set(getLetzterAnzeigeTag(aktJob).getCalendar());
                }
                long d = extras.getLong(KEY_ANZEIGE_DATUM);
                int v = extras.getInt(KEY_ANZEIGE_VIEW);

                if (v == VIEW_TAG) {
                    if (aktJob.isEndeAufzeichnung(aktDatum)) {
                        OpenView(VIEW_WOCHE, d);
                    } else {
                        OpenView(VIEW_TAG, d);
                    }
                } else
                    OpenView(v, d);

            } else {
                // Bevorzugte Ansicht öffnen
                if (mPreferenzen.getInt(KEY_ANZEIGE_VIEW, VIEW_TAG) == VIEW_LETZTER ||
                        mPreferenzen.getBoolean(KEY_RESUME_VIEW, false)) {
                    // Nach Rückkehr aus anderer Activity z.B.: Datenesicherung, export, Einstellungen
                    // oder wenn als bevorzugte Ansicht die letzte offene Ansicht gewählt ist
                    // zur letzten Ansicht (Tag oder Woche etc. und Datum) springen
                    mEdit.putBoolean(KEY_RESUME_VIEW, false).apply();

                    OpenView(
                            mPreferenzen.getInt(
                                    KEY_ANZEIGE_LETZTER, VIEW_TAG),
                            mPreferenzen.getLong(KEY_ANZEIGE_DATUM,
                                    aktDatum.getTimeInMillis())
                    );

                } else {
                    // Nach Rückkehr aus der Versenkeung die bevorzugte Ansicht öffnen
                    // also den Aktullen Tag oder Woche etc.
                    int v = mPreferenzen.getInt(KEY_ANZEIGE_VIEW, VIEW_TAG);
                    if (aktJob.isEndeAufzeichnung(aktDatum)) {
                        if (v == VIEW_TAG) {
                            v = VIEW_MONAT;
                        }
                    }
                    OpenView(v, aktDatum.getTimeInMillis());
                }
            }
            //}

            //
            // Fehlerkorrekturen und Anzeige der Versionshinweise
            //
            long altVersion = mPreferenzen.getLong(KEY_VERSION_APP, 0);
            if ( !mPreferenzen.contains(KEY_VERSION_APP) || altVersion < BuildConfig.VERSION_CODE){
                if(altVersion <= 205018){
                    /*
                     * alle Einstellungen zu Zeilen und Spalten der PDF Berichte werden zurück gesetzt
                     * und ab sofort im Arbeitsplatz gespeichert weil es zu Darstellungsproblemen gekommet
                     * wenn in den Arbeitsplätzen unterschiedliche Zusatzspalten vorliegen
                    */
                    // Monatsberichte
                    mEdit.remove(KEY_EXP_M_SPALTEN_LGAV);
                    mEdit.remove(KEY_EXP_M_ZEILEN_LGAV);
                    mEdit.remove(KEY_EXP_M_ZUSATZ_LGAV);
                    mEdit.remove(KEY_EXP_M_SPALTEN_KURZ);
                    mEdit.remove(KEY_EXP_M_ZEILEN_KURZ);
                    mEdit.remove(KEY_EXP_M_ZUSATZ_KURZ);
                    mEdit.remove(KEY_EXP_M_SPALTEN_AUSF);
                    mEdit.remove(KEY_EXP_M_ZEILEN_AUSF);
                    mEdit.remove(KEY_EXP_M_ZUSATZ_AUSF);
                    // Einsatzortberichte
                    mEdit.remove(KEY_EXP_EO_SPALTEN);
                    mEdit.remove(KEY_EXP_EO_ZEILEN);
                    mEdit.remove(KEY_EXP_EO_ZUSATZ);
                    // Wochenberichte
                    mEdit.remove(KEY_EXP_W_SPALTEN);
                    mEdit.remove(KEY_EXP_W_ZEILEN);
                    mEdit.remove(KEY_EXP_W_ZUSATZ);
                    // Zeitraumberichte
                    mEdit.remove(KEY_EXP_ZR_SPALTEN);
                    mEdit.remove(KEY_EXP_ZR_ZEILEN);
                    mEdit.remove(KEY_EXP_ZR_ZUSATZ).apply();
                }

                if (altVersion < 202009) {
                    /*
                     * Ein Fehler beim setzen dieser Variablen trat in der Vorversion ein Fehler auf.
                     * Desshalb wird die Userentscheidung wieder gelöscht
                     */
                    mPreferenzen.edit().remove(ISetup.KEY_ANTWORT_DEZ).apply();
                }

                // Überzählige Einträge in Einstellungen Tabelle löschen
                //if(altVersion < 205001) {
                    if (!mDatenbank.isOpen())
                        mDatenbank = ASetup.stundenDB.getWritableDatabase();

                    String count = "SELECT "
                            + Datenbank.DB_F_ID
                            + " FROM "
                            + Datenbank.DB_T_SETTINGS;
                    Cursor result = mDatenbank.rawQuery(count, null);

                    while (result.moveToNext()) {
                        @SuppressLint("Range")
                        long id = result.getLong(result.getColumnIndex(Datenbank.DB_F_ID));
                        if (id != 1) {
                            mDatenbank.delete(
                                    Datenbank.DB_T_SETTINGS,
                                    Datenbank.DB_F_ID + "=?",
                                    new String[]{Long.toString(id)}
                            );
                        }
                    }
                    result.close();


                    // Anzeige Versionsdialog
                    new AlertDialog.Builder(this)
                            .setTitle(getString(R.string.version_info_titel, BuildConfig.VERSION_NAME))
                            .setPositiveButton(android.R.string.ok, null)
                            .setView(R.layout.fragment_version_info)
                            .show();
                //}

                // neue Version in die Einsatellungen schreiben
                mEdit.putLong(KEY_VERSION_APP, BuildConfig.VERSION_CODE).apply();
            }

            /*
             * nur für Debugging im emulator notwendig
             */
            if (BuildConfig.BUILD_TYPE.contains("debug")) {
                // Autobabackup konfigurieren
                AAutoBackup.updateTimer(this);
                // neu Zeichnen der Stempeluhren anstoßen
                Stempeluhr.updateAllWidgets(this);
            }


        } else {
            // Version der App in die Einstellungen schreiben
            mPreferenzen.edit().putLong(KEY_VERSION_APP, BuildConfig.VERSION_CODE).apply();

            // Tabelle mit App Einstellungen initialisieren
            if(!mDatenbank.isOpen())
               mDatenbank = ASetup.stundenDB.getWritableDatabase();

            String count = "SELECT count(*) FROM " + Datenbank.DB_T_SETTINGS;
            Cursor result = mDatenbank.rawQuery(count, null);
            result.moveToFirst();

            if(result.getInt(0) <= 0) {
                ContentValues mWerte = new ContentValues();
                mWerte.put(Datenbank.DB_F_VERSION, BuildConfig.VERSION_CODE);
                mWerte.put(Datenbank.DB_F_ID, 1);
                mDatenbank.insert(Datenbank.DB_T_SETTINGS, null, mWerte);
            }
            result.close();

            // den Assistenten zur Ersteinrichtung aufrufen
            Intent iAssistent = new Intent();
            iAssistent.setClass(this, InitAssistent.class);
            iAssistent.putExtra(KEY_EDIT_JOB, aktJob.getId());
            iAssistent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(iAssistent);
        }

    }


    @Override
    public void onNavigationDrawerItemSelected(int position, Datum datum) {
        if(datum.liegtVor(aktJob.getStartDatum())){
           OpenView(position, aktJob.getStartDatum().getTimeInMillis());
           return;
        }
        if(datum.liegtNach(letzterAnzeigeTag)){
            OpenView(position, letzterAnzeigeTag.getTimeInMillis());
            return;
        }
        OpenView(position, datum.getTimeInMillis());
    }

    @Override
    public void onNavigationDrawerIconSelected(int icon) {
        if(icon == R.id.N_icon_hilfe)
            OpenTurorial();
    }

    // Bestimmtes Fragment öffnen
    void OpenView(int view, final long datum) {
        final Context mContext = this;

        // Den Inhalt der gewählten Seite anzeigen und den Titel anpassen
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fInhalt = null;
        int mTitelID = 0;

        switch (view) {
            case  VIEW_JOB:
                mTitelID = R.string.arbeitsplatz;
                fInhalt = ArbeitsplatzExpandFragment.newInstance();
                break;
            case  VIEW_JAHR:
                mTitelID = R.string.jahr;
                fInhalt = ArbeitsjahrPager.newInstance(datum);
                break;
            case  VIEW_MONAT:
                mTitelID = R.string.monat;
                fInhalt = ArbeitsmonatPager.newInstance(datum);
                break;
            case  VIEW_WOCHE:
                mTitelID = R.string.woche;
                fInhalt = ArbeitswochePager.newInstance(datum);
                break;
            case  VIEW_TAG:
                if (ASetup.aktJob.isEndeAufzeichnung(ASetup.aktDatum)) {
                    new AlertDialog.Builder(this)
                            .setTitle(this.getString(R.string.ende_titel))
                            .setMessage(getString(R.string.dialog_open_tag,
                                    ASetup.aktJob.getEndDatum().getString_Datum(mContext)
                                    /*Einstellungen.datumsformat.format(Einstellungen.aktJob.getEndDatum().getTime())*/))
                            .setPositiveButton(this.getString(android.R.string.ok), (dialog, whichButton) -> {
                                //OpenView(Einstellungen.VIEW_MONAT, datum);
                            })
                            .setNeutralButton(this.getString(R.string.einstellungen), (dialog, whichButton) -> {
                                // Einstellungen öffnen
                                Intent iSettings = new Intent();
                                iSettings.setClass(mContext, SettingsActivity.class);
                                iSettings.putExtra( KEY_EDIT_JOB, ASetup.aktJob.getId());
                                iSettings.putExtra( KEY_INIT_SEITE, 2);
                                startActivity(iSettings);
                                finish();
                            }).show();
                } else {
                    mTitelID = R.string.tag;
                    fInhalt = ArbeitstagPager.newInstance(datum, this);
                }
                break;
            case  VIEW_CHARTS:
                mTitelID = R.string.charts;
                int periode;
                if (ASetup.mPreferenzen.getInt( KEY_ANZEIGE_LETZTER,  VIEW_MONAT) ==  VIEW_WOCHE) {
                    periode = ChartsFragment.PERIODE_WOCHE;
                } else {
                    periode = ChartsFragment.PERIODE_MONAT;
                }
                fInhalt = ChartsFragment.newInstance(
                    ASetup.mPreferenzen.getLong(
                             KEY_ANZEIGE_DATUM,
                            ASetup.aktDatum.getTimeInMillis()),
                    periode,
                    ChartsFragment.TYP_ABWESENHEITEN);
                break;
            default:
                mTitelID = R.string.about;
                fInhalt = new AboutApp();
                //fInhalt = null;
        }

        // die neue Ansicht als letzte markieren
        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
        mEdit.putInt( KEY_ANZEIGE_LETZTER, view);
        mEdit.putLong( KEY_ANZEIGE_DATUM, datum);
        mEdit.apply();

        if (fInhalt != null) {
            ActionBar actionBar = getSupportActionBar();

            if (actionBar != null) {
                actionBar.setTitle(mTitelID);
            }

            fragmentManager.beginTransaction()
                    .replace(R.id.container, fInhalt)
                    .commitAllowingStateLoss();
        }

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            boolean app_schliessen = false;
            int bevorzugte_Ansicht = ASetup.mPreferenzen.getInt(
                     KEY_ANZEIGE_VIEW,
                     VIEW_TAG
            );
            int aktuelle_Ansicht = ASetup.mPreferenzen.getInt(
                     KEY_ANZEIGE_LETZTER,
                     VIEW_TAG
            );

            Datum angezeigtes_Datum = new Datum(
                    ASetup.mPreferenzen.getLong(
                             KEY_ANZEIGE_DATUM,
                            ASetup.aktDatum.getTimeInMillis()),
                    ASetup.aktJob.getWochenbeginn());

            if (bevorzugte_Ansicht !=  VIEW_LETZTER) {
                if(bevorzugte_Ansicht == aktuelle_Ansicht) {
                    int periode;
                    switch (bevorzugte_Ansicht) {
                        case  VIEW_JOB:
                            periode = -1;
                            break;
                        case  VIEW_JAHR:
                            periode = Calendar.YEAR;
                            break;
                        case  VIEW_WOCHE:
                            periode = Calendar.WEEK_OF_YEAR;
                            break;
                        case  VIEW_MONAT:
                            periode = Calendar.MONTH;
                            break;
                        default:
                            periode = Calendar.DAY_OF_MONTH;
                    }

                    app_schliessen = ( periode < 0 || angezeigtes_Datum.istGleich(
                            ASetup.aktDatum, periode
                    ));
                }
            } else
                app_schliessen = true;

            if(app_schliessen) {
                if (!ASetup.mPreferenzen.getBoolean( KEY_ANTWORT_BEENDEN, false)) {
                    final View vFrage = LayoutInflater.from(this).inflate(R.layout.fragment_dialog_frage_beenden, null);
                    AlertDialog.Builder aFrage = new AlertDialog.Builder(this);
                    //aFrage.setMessage(R.string.dialog_frage_dezimal);
                    aFrage.setView(vFrage);
                    aFrage.setPositiveButton(R.string.beenden, (dialog, which) -> {
                        // Antwort des Users merken
                        AppCompatCheckBox mMerken = vFrage.findViewById(R.id.F_button_merken);
                        if (mMerken.isChecked()) {
                            SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
                            mEdit.putBoolean( KEY_ANTWORT_BEENDEN, true).apply();
                        }
                        finish();
                    });
                    aFrage.setNegativeButton(R.string.offen, (dialog, which) -> {
                        // nichts machen, nur den Dialog schliessen
                    });
                    aFrage.create().show();
                } else {
                    super.onBackPressed();
                }
            } else
                OpenView(bevorzugte_Ansicht, ASetup.aktDatum.getTimeInMillis());

        } else {
            mNavigationDrawerFragment.CloseDrawer();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASetup.mPreferenzen.edit()
                    .putBoolean( KEY_RESUME_VIEW, isChangingConfigurations()).apply();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mNavigationDrawerFragment != null && !mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            //restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent iSettings = new Intent();
            iSettings.setClass(this, SettingsActivity.class);
            iSettings.putExtra(KEY_EDIT_JOB, ASetup.aktJob.getId());
            iSettings.putExtra(KEY_INIT_SEITE, 0);
            //iSettings.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(iSettings);
        } else if (id == R.id.action_export) {
            Intent iExport = new Intent();
            iExport.setClass(this, ExportActivity.class);
            iExport.putExtra(KEY_JOBID, ASetup.aktJob.getId());
            int seite = mPreferenzen.getInt(ISetup.KEY_ANZEIGE_LETZTER, ISetup.VIEW_WOCHE);
            seite = Math.min(-(seite - 3), VIEW_MONAT);
            iExport.putExtra(
                    KEY_ANZEIGE_VIEW,
                    seite
            );
            finish();
            startActivity(iExport);
        } else if (id == R.id.action_backup) {
            Intent iBackup = new Intent();
            iBackup.setClass(this, Datensicherung_Activity.class);
            finish();
            startActivity(iBackup);
        } else if (id == R.id.action_about) {
            OpenView(VIEW_ABOUT, ASetup.mPreferenzen.getLong(KEY_ANZEIGE_DATUM, ASetup.aktDatum.getTimeInMillis()));
        } else if (id == R.id.action_suche) {
            OpenSuche();
        } else if (id == R.id.action_hilfe) {
            OpenTurorial();
        }

        return super.onOptionsItemSelected(item);
    }

    protected void OpenSuche() {
       final Dialog mDialog = new Dialog(this);

        mDialog.setTitle(R.string.suche);

        mDialog.setContentView(R.layout.fragment_suche);
        final SearchView mSuche = mDialog.findViewById(R.id.SU_suche);
        mSuche.setQueryHint(getString(R.string.suche));
        mSuche.setIconified(false);
        mSuche.setSubmitButtonEnabled(true);
        mSuche.setOnCloseListener(() -> {
            if(mSuche.getQuery().length() > 0)
                mSuche.clearFocus();
            else
                mDialog.dismiss();
            return true;
        });
        mSuche.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String mSuchstring = mSuche.getQuery().toString();
                mSuche.clearFocus();
                mDialog.cancel();
                Intent  iSuche = new Intent();
                iSuche.setClass(getApplicationContext(), Suche_Activity.class);
                iSuche.putExtra( KEY_SUCHE_STRING, mSuchstring);
                startActivity(iSuche);
                finish();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        mDialog.show();
    }

    private void OpenTurorial() {
        // das Verzeichnis für das Handbuch prüfen/anlegen
        if (mStorageHelper == null) {
            String handbuchPfad;
            handbuchPfad = ASetup.mPreferenzen.getString(
                    ASetup.KEY_HANDBUCH_PFAD,
                    ASetup.mPreferenzen.getString(
                            ASetup.KEY_DATEN_DIR, "")
            );

            mStorageHelper = new StorageHelper(
                    this,
                    handbuchPfad,
                    null,
                    ISetup.KEY_HANDBUCH_PFAD,
                    true,
                    REQ_FOLDER_PICKER_WRITE_EXPORT/*,
                    this::OpenTurorial*/
            );
        }

        // ist das Verzeichnis vorhanden und beschreibbar
        if (mStorageHelper.isStorageMounted() && mStorageHelper.isWritheable()) {
            DocumentFile zielFile = mStorageHelper.getVerzeichnisFile().findFile(
                    res.getString(R.string.app_name) + "_Tutorial.pdf");

            Datum dFirst = new Datum(2016, 8, 4, aktJob.getWochenbeginn());

            // ist das Handbuch noch nicht am vorgesehnen Platz, dann dorthin kopieren
            if (zielFile == null || !zielFile.exists() || dFirst.liegtNach(new Datum(zielFile.lastModified(), aktJob.getWochenbeginn()))) {
                AssetManager assetManager = getAssets();
                InputStream in;
                OutputStream out;

                zielFile = mStorageHelper.getVerzeichnisFile().createFile(
                        IExport_Basis.DATEI_TYP_PDF,
                        res.getString(R.string.app_name) + "_Tutorial");

                if(zielFile != null) {
                    try {
                        out = getContentResolver().openOutputStream(zielFile.getUri());
                        if(out != null) {
                            in = assetManager.open("tutorial.pdf");

                            byte[] buffer = new byte[1024];
                            int read;
                            while ((read = in.read(buffer)) != -1) {
                                out.write(buffer, 0, read);
                            }
                            in.close();
                            out.flush();
                            out.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            // ist das Handbuch am vorgesehenen Platz
            if (zielFile != null && zielFile.exists() && zielFile.canRead()) {
                Intent intent = new Intent(Intent.ACTION_VIEW);

                Uri mURI = zielFile.getUri();//mStorageHelper.getDateiUri(zielFile.getName());
                //if (mURI != null) {
                intent.setDataAndType(mURI, IExport_Basis.DATEI_TYP_PDF);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                try {
                    PackageManager pm = getPackageManager();
                    if (intent.resolveActivity(pm) != null) {
                        startActivity(intent);
                    } else {
                        Toast.makeText(this, R.string.no_pdf_reader, Toast.LENGTH_LONG).show();
                    }
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(this, R.string.no_pdf_reader, Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(this, getString(R.string.no_handbuch_copy, mStorageHelper.getPfadSubtree()), Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        requestCode = requestCode & 0x0000ffff;

        if (data != null && resultCode == RESULT_OK && requestCode == REQ_FOLDER_PICKER_WRITE_EXPORT) {
            if (data.getData() != null) {
                getContentResolver().takePersistableUriPermission(data.getData(),
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                mStorageHelper.setPfad(data.getDataString());
                OpenTurorial();
            }
        }
    }


    /*
     * Rückmeldung vom Rechtemanagment nach Rechteanfrage
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
            requestCode = requestCode & 0x0000ffff;

            if (requestCode == REQ_DEMAND_WRITE) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mStorageHelper.setPfad(mStorageHelper.getPfad());
                } else {
                    // Recht verweigert, kein Handbuch anzeigen
                    Toast.makeText(
                            this,
                            getString(R.string.err_keine_berechtigung),
                            Toast.LENGTH_LONG
                    ).show();
                }
            }
        }
    }


    /*
     * Ist der angeforderte Speicher eingebunden?

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }*/

    @Override
    public void onEditArbeitstag(Datum datum) {
        OpenView( VIEW_TAG, datum.getTimeInMillis());
    }

    // Rückruf vom arbeitstag zum Wechsel in den Monat oder die Woche
    @Override
    public void onArbeitstagGoback(int ansicht, Datum datum) {
        OpenView(ansicht, datum.getTimeInMillis());
    }
}
