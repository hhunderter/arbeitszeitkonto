/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.widget;

import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

/**
 * @author askanimus@gmail.com on 05.07.16.
 */
public class StempeluhrStatus {
    protected static final String PREFS_NAME = "askanimus.arbeitszeiterfassung2.widget.Stempeluhr";
    protected static final String KEY_WIDGET_ID ="widgetid";
    protected static final String KEY_JOB = "job_";
    protected static final String KEY_STATUS = "status_";
    protected static final String KEY_DATUM = "datum_";
    protected static final String KEY_ZEIT = "zeit_";
    protected static final String KEY_PAUSE_BEGINN_OLD = "pause_";
    protected static final String KEY_PAUSE_BEGINN = "_pause_";
    protected static final String KEY_PAUSE_LAENGE_OLD = "pauselaenge_";
    protected static final String KEY_PAUSE_LAENGE = "_pauselaenge_";
    protected static final String KEY_PAUSESUMME = "pausesumme_";
    protected static final String KEY_TIMER_MIN = "timermin_";
    protected static final String KEY_TIMER_TEXT = "timertext_";
    protected static final String KEY_TIMER_SOUND = "timersound_";
    protected static final String KEY_NEUSTART = "neustart_";

    protected static final String KEY_OPT_RUNDEN = "opt_runden_";
    protected static final String KEY_OPT_RUNDEN_MINUTEN = "opt_runden_minuten_";
    protected static final String KEY_OPT_OPTIONEN = "opt_optionen_";
    //
    // Zustände des Stempeluhrwidgets
    //
    static final int WIDGET_STATUS_NEU = 0;
    static final int WIDGET_STATUS_RUN = 1;
    static final int WIDGET_STATUS_PAUSE = 3;
    static final int WIDGET_STATUS_STOP = 4;

    //
    // Rundungseinstellungen
    //
    final static int AUFRUNDEN = 1;
    final static int ABRUNDEN = 2;
    final static int KAUFRUNDEN = 3;
    final static int NICHTRUNDEN = 0;

    //
    // Widgetoptionen
    //
    final static int OPT_TAG_OEFFEN = 1;
    final static int OPT_ERKENNE_SCHICHT = 2;
    final static int OPT_SET_VON_REAL = 4;
    final static int OPT_SET_BIS_REAL = 8;
    final static int OPT_SET_PAUSE_REAL = 16;
    final static int OPT_EXTRASCHICHT = 32;

    //
    // Widget Werte
    //
    int mID;
    protected Arbeitsplatz mJob;
    protected Datum mTag;
    protected Uhrzeit mBeginn;
    protected boolean mNeustart;
    protected ArrayList <Uhrzeit> mPauseBeginn;
    protected ArrayList <Uhrzeit> mPauseLaenge;
    Uhrzeit mPauseSumme;
    protected long mAbwesenheit;
    int mStatus;

    //
    // die Optionen
    //
    int mRunden= NICHTRUNDEN;
    int mRunden_minuten = 1;

    int mOptionen = 28; //  011100;

    ArrayList<TimerEintrag> mTimerListe;

    StempeluhrStatus(Arbeitsplatz job, int id) {
        mJob = job;
        mID = id;
        mTag = new Datum(new Date(), mJob.getWochenbeginn());
        mBeginn = new Uhrzeit(0);
        mPauseBeginn = new ArrayList<>();//new Uhrzeit(0);
        mPauseLaenge = new ArrayList<>();
        mPauseSumme = new Uhrzeit(0);
        mAbwesenheit = mJob.getAbwesenheiten().getAktive(Abwesenheit.ARBEITSZEIT).getID();
        mStatus = WIDGET_STATUS_NEU;
        mTimerListe = new ArrayList<>();
    }

    Boolean getOption(int option){
        return (mOptionen & option) != 0;
    }

    void setOption(int option, Boolean wert){
        if(wert)
            mOptionen = mOptionen | option;
        else
            mOptionen = mOptionen & ~option;

    }

    void startWidget() {
        mBeginn.set();
        mTag.set(new Date());
        mAbwesenheit = Abwesenheit.ARBEITSZEIT;
        mStatus = WIDGET_STATUS_RUN;
        mNeustart = true;
    }

    void stopWidget(){
        if (mStatus == WIDGET_STATUS_PAUSE){
            stopPause();
        }
        mStatus = StempeluhrStatus.WIDGET_STATUS_STOP;
    }

    void resetWidget(){
        mBeginn.set(0);
        mPauseSumme.set(0);
        mPauseBeginn = new ArrayList<>();
        mPauseLaenge = new ArrayList<>();

        mStatus = WIDGET_STATUS_NEU;
    }

    TimerEintrag getNextTimer(int aktMinten){
        if (mTimerListe.isEmpty()) {
            return  null;
        } else {
            int m = aktMinten - mBeginn.getAlsMinuten();
            for (TimerEintrag te : mTimerListe) {
                if (te.minuten > m) {
                    return te;
                }
            }
            return  null;
        }
    }

    /*
     * Pausenmanagement
     */
    void startPause(){
        mPauseBeginn.add(new Uhrzeit());
        mPauseLaenge.add(new Uhrzeit(0));
        mStatus = WIDGET_STATUS_PAUSE;
    }

    void stopPause(){
        Uhrzeit t = new Uhrzeit();
        Uhrzeit laenge = getAktPauseLaenge();
        if(laenge != null){
            int p = t.getAlsMinuten() - getAktPauseBeginn();
            if (p < 0){
                p += ISetup.Minuten_TAG;
            }
            laenge.set(p);
        }
        mStatus = WIDGET_STATUS_RUN;
        updatePauseSumme();
    }

    void updatePause(int minuten){
        Uhrzeit l = getAktPauseLaenge();
        if(l != null){
            int laenge = minuten - getAktPauseBeginn();
            if (laenge < 0){
                laenge += ISetup.Minuten_TAG;
            }
            l.set(laenge);

            updatePauseSumme();
        }
    }

    private void updatePauseSumme(){
        mPauseSumme.set(0);
        for (Uhrzeit p : mPauseLaenge) {
            mPauseSumme.add(p.getAlsMinuten());
        }
    }

    int getAktPauseBeginn(){
        int s = mPauseBeginn.size();
        if(s > 0) {
            return mPauseBeginn.get(s - 1).getAlsMinuten();
        }
        return 0;
    }

    Uhrzeit getAktPauseLaenge(){
        int s = mPauseLaenge.size();
        if(s > 0) {
            return mPauseLaenge.get(s - 1);
        }
        return null;
    }

    void load(SharedPreferences prefs) {
        mStatus = prefs.getInt(KEY_STATUS + mID, StempeluhrStatus.WIDGET_STATUS_NEU);
        mTag = new Datum(prefs.getLong(KEY_DATUM + mID, new Date().getTime()), mJob.getWochenbeginn());
        mBeginn.set(prefs.getInt(KEY_ZEIT + mID,
                (mTag.get(Calendar.HOUR_OF_DAY) * 60) + mTag.get(Calendar.MINUTE)));
        int i = 0;
        Uhrzeit z;
        if (prefs.contains(KEY_PAUSE_BEGINN_OLD + mID)) {
            // Nach Update der App alte Pauseneinstellungen lesen und löschen
            z = new Uhrzeit(prefs.getInt(KEY_PAUSE_BEGINN_OLD + mID, 0));
            mPauseBeginn.add(z);
            z = new Uhrzeit(prefs.getInt(KEY_PAUSE_LAENGE_OLD + mID, 0));
            mPauseLaenge.add(z);

            prefs.edit().remove(KEY_PAUSE_BEGINN_OLD + mID).apply();
            prefs.edit().remove(KEY_PAUSE_LAENGE_OLD + mID).apply();
        } else {
            while (prefs.contains(i + KEY_PAUSE_BEGINN + mID)) {
                z = new Uhrzeit(prefs.getInt(i + KEY_PAUSE_BEGINN + mID, 0));
                mPauseBeginn.add(z);
                z = new Uhrzeit(prefs.getInt(i + KEY_PAUSE_LAENGE + mID, 0));
                mPauseLaenge.add(z);
                i++;
            }
        }
        mPauseSumme.set(prefs.getInt(KEY_PAUSESUMME + mID, 0));

        i = 0;
        while (prefs.contains(i + KEY_TIMER_MIN + mID)) {
            TimerEintrag e = new TimerEintrag();
            e.minuten = prefs.getInt(i + KEY_TIMER_MIN + mID, 0);
            e.meldung = prefs.getString(i + KEY_TIMER_TEXT + mID, "");
            String s = prefs.getString(i + KEY_TIMER_SOUND + mID, "");
            if (!s.isEmpty()){
                e.alarmton = Uri.parse(s);
            } else {
                e.alarmton = null;
            }
            mTimerListe.add(e);
            i++;
        }

        mOptionen = prefs.getInt(KEY_OPT_OPTIONEN + mID, mOptionen);
        mRunden = prefs.getInt(KEY_OPT_RUNDEN + mID, StempeluhrStatus.NICHTRUNDEN);
        mRunden_minuten = prefs.getInt(KEY_OPT_RUNDEN_MINUTEN + mID, 1);
        mNeustart = prefs.getBoolean(KEY_NEUSTART + mID, false);
    }
    
    void save(SharedPreferences prefs) {
        SharedPreferences.Editor mEditor = prefs.edit();

        mEditor.putLong(KEY_JOB + mID, mJob.getId());
        mEditor.putInt(KEY_STATUS + mID, mStatus);
        mEditor.putLong(KEY_DATUM + mID, mTag.getTimeInMillis());
        mEditor.putInt(KEY_ZEIT + mID, mBeginn.getAlsMinuten());
        int i = 0;
        while (i < mPauseBeginn.size()) {
            mEditor.putInt(i + KEY_PAUSE_BEGINN + mID, mPauseBeginn.get(i).getAlsMinuten());
            mEditor.putInt(i + KEY_PAUSE_LAENGE + mID, mPauseLaenge.get(i).getAlsMinuten());
            i++;
        }
        while (prefs.contains(i + KEY_PAUSE_BEGINN + mID)) {
            mEditor.remove(i + KEY_PAUSE_BEGINN + mID);
            mEditor.remove(i + KEY_PAUSE_LAENGE + mID);
            i++;
        }
        mEditor.putInt(KEY_PAUSESUMME + mID, mPauseSumme.getAlsMinuten());

        i = 0;
        for (TimerEintrag eintrag : mTimerListe) {
            mEditor.putInt(i + KEY_TIMER_MIN + mID, eintrag.minuten);
            mEditor.putString(i + KEY_TIMER_TEXT + mID, eintrag.meldung);
            if(eintrag.alarmton != null) {
                mEditor.putString(i + KEY_TIMER_SOUND + mID, eintrag.alarmton.toString());
            }
            i++;
        }
        while (prefs.contains(i + KEY_TIMER_MIN + mID)) {
            mEditor.remove(i + KEY_TIMER_MIN + mID);
            mEditor.remove(i + KEY_TIMER_TEXT + mID);
            mEditor.remove(i + KEY_TIMER_SOUND + mID);
            i++;
        }

        mEditor.putInt(KEY_OPT_OPTIONEN + mID, mOptionen);
        mEditor.putInt(KEY_OPT_RUNDEN + mID, mRunden);
        mEditor.putInt(KEY_OPT_RUNDEN_MINUTEN + mID, mRunden_minuten);
        mEditor.putBoolean(KEY_NEUSTART + mID, mNeustart);

        mEditor.apply();
    }


    void delete(SharedPreferences prefs) {
        SharedPreferences.Editor mPrefs = prefs.edit();
        mPrefs.remove(KEY_JOB + mID);
        mPrefs.remove(KEY_STATUS + mID);
        mPrefs.remove(KEY_DATUM + mID);
        mPrefs.remove(KEY_ZEIT + mID);
        mPrefs.remove(KEY_PAUSE_BEGINN + mID);
        mPrefs.remove(KEY_PAUSESUMME + mID);
        mPrefs.remove(KEY_OPT_OPTIONEN + mID);
        mPrefs.remove(KEY_OPT_RUNDEN + mID);
        mPrefs.remove(KEY_OPT_RUNDEN_MINUTEN + mID);

        int i = 0;
        while (prefs.contains(i + KEY_PAUSE_BEGINN + mID)) {
            mPrefs.remove(i + KEY_PAUSE_BEGINN + mID);
            mPrefs.remove(i + KEY_PAUSE_LAENGE + mID);
            i++;
        }

        i = 0;
        while (prefs.contains(i + KEY_TIMER_MIN + mID)) {
            mPrefs.remove(i + KEY_TIMER_MIN + mID);
            mPrefs.remove(i + KEY_TIMER_TEXT + mID);
            mPrefs.remove(i + KEY_TIMER_SOUND + mID);
            i++;
        }
        mPrefs.apply();
    }

    /*
     * Innere Klasse für eine Liste von Timern, die als Pausenerinnerung o.ä dienen
     */
    static class TimerEintrag {
        int minuten;        // Anzahl Minuten vom Start der Zeiterfassung des Widgets beginnend
        String meldung;     // Text der beim Erreichen angezeigt werden soll
        Uri alarmton;       // der Sound der asugegeben werden soll
    }
}
