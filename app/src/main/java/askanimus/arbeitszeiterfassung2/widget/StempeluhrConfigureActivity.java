/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.CompoundButtonCompat;

import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import com.codetroopers.betterpickers.numberpicker.NumberPickerBuilder;
import com.codetroopers.betterpickers.numberpicker.NumberPickerDialogFragment;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Locale;
import java.util.Objects;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.ArbeitsplatzListAdapter;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.ArbeitsplatzListe;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

/**
 * The configuration screen for the {@link Stempeluhr Stempeluhr} AppWidget.
 */
public class StempeluhrConfigureActivity
        extends
        AppCompatActivity
        implements
        ISetup,
        NumberPickerDialogFragment.NumberPickerDialogHandlerV2{

    private StempeluhrStatus wStatus;

    public StempeluhrConfigureActivity() {
        super();
    }

    // Zeigt ob es ein bestehendes Widget ist das konfiguriert wird
    private Boolean isNotNew = false;

    // Read the prefix from the SharedPreferences object for this widget.
    // If there is no preference saved, get the default from a resource
    static StempeluhrStatus loadPref(Context context, int appWidgetId, String action) {
        StempeluhrStatus mStatus = null;
        SharedPreferences mPrefs;
       if(ASetup.zustand == ISetup.INIT_ZUSTAND_GELADEN) {
           if(ASetup.mPreferenzen.contains(StempeluhrStatus.KEY_JOB + appWidgetId)){
               mPrefs = ASetup.mPreferenzen;
           } else {
               // vor Vers. 2.00.00 wurden die Einstellungen im anderen Context gespeichert
               mPrefs = context.getSharedPreferences(StempeluhrStatus.PREFS_NAME, MODE_PRIVATE);
           }

           // testen ob der arbeitsplatz noch existiert
           // SQL Anfrage zusammen bauen
           SQLiteDatabase mDatenbank = ASetup.mDatenbank;/*stundenDB.getWritableDatabase();*/
           String sql = "select " +
                   Datenbank.DB_F_ID +
                   " from " +
                   Datenbank.DB_T_JOB +
                   " where " +
                   Datenbank.DB_F_ID +
                   " = " +
                   mPrefs.getLong(StempeluhrStatus.KEY_JOB + appWidgetId, 0) +
                   " LIMIT 1 ";
           Cursor result = mDatenbank.rawQuery(sql, null);

           // Resultat der Anfrage auswerten
           if (result.getCount() > 0) {
               try {
                   mStatus = new StempeluhrStatus(
                           new Arbeitsplatz(mPrefs.getLong(StempeluhrStatus.KEY_JOB + appWidgetId, ASetup.aktJob.getId())),
                           appWidgetId);

                   mStatus.load(mPrefs);
               } catch (RuntimeException e) {
                   if (mStatus != null) {
                       mStatus.delete(ASetup.mPreferenzen);
                   }
                   mStatus = null;
                   result.close();
               }
               result.close();
           } else {
               result.close();
               deletePref(context, appWidgetId);
           }
       } else {
           ASetup.init(context, () -> {
            if(action != null) {
                try {
                    Stempeluhr.getPendingSelfIntent(context, action, appWidgetId).send();
                } catch (PendingIntent.CanceledException e) {
                    e.printStackTrace();
                }
            }

           });
       }
        return mStatus;
    }

    static void deletePref(Context context, int appWidgetId) {
        if (ASetup.zustand == ISetup.INIT_ZUSTAND_GELADEN) {
            SharedPreferences.Editor mPrefs = ASetup.mPreferenzen.edit();
            mPrefs.remove(StempeluhrStatus.KEY_JOB + appWidgetId);
            mPrefs.remove(StempeluhrStatus.KEY_STATUS + appWidgetId);
            mPrefs.remove(StempeluhrStatus.KEY_DATUM + appWidgetId);
            mPrefs.remove(StempeluhrStatus.KEY_ZEIT + appWidgetId);
            mPrefs.remove(StempeluhrStatus.KEY_PAUSE_BEGINN + appWidgetId);
            mPrefs.remove(StempeluhrStatus.KEY_PAUSESUMME + appWidgetId);
            mPrefs.remove(StempeluhrStatus.KEY_OPT_OPTIONEN + appWidgetId);
            mPrefs.remove(StempeluhrStatus.KEY_OPT_RUNDEN + appWidgetId);
            mPrefs.remove(StempeluhrStatus.KEY_OPT_RUNDEN_MINUTEN + appWidgetId);

            int i = 0;
            while (ASetup.mPreferenzen.contains(i + StempeluhrStatus.KEY_PAUSE_BEGINN + appWidgetId)) {
                mPrefs.remove(i + StempeluhrStatus.KEY_PAUSE_BEGINN + appWidgetId);
                mPrefs.remove(i + StempeluhrStatus.KEY_PAUSE_LAENGE + appWidgetId);
                i++;
            }

            i = 0;
            while (ASetup.mPreferenzen.contains(i + StempeluhrStatus.KEY_TIMER_MIN + appWidgetId)) {
                mPrefs.remove(i + StempeluhrStatus.KEY_TIMER_MIN + appWidgetId);
                mPrefs.remove(i + StempeluhrStatus.KEY_TIMER_TEXT + appWidgetId);
                i++;
            }

            mPrefs.apply();
        } else {
            ASetup.init(context, () -> deletePref(context, appWidgetId));
        }
    }


    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setTheme(
                PreferenceManager
                        .getDefaultSharedPreferences(getApplicationContext())
                        .getBoolean(ASetup.KEY_THEMA_DUNKEL, false) ?
                        R.style.MyFullscreenTheme :
                        R.style.MyFullscreenTheme_Light
        );
        ASetup.init(this, this::create);
    }

    private void create() {
        setTheme(ASetup.isThemaDunkel ? R.style.MyFullscreenTheme : R.style.MyFullscreenTheme_Light);

        final AppCompatSpinner mSpinnertJob;

        final ArbeitsplatzListe mJobs = ASetup.jobListe;

        int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;


        // die Widget Id auslesen
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
            isNotNew = extras.getBoolean(ISetup.KEY_WIDGET_CONFIG, false);
        }

        // Wurde die App ohne eine Appwidget ID aufgerufen?
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
            return;
        }

        SharedPreferences mPrefs = ASetup.mPreferenzen;
        if (!mPrefs.contains(StempeluhrStatus.KEY_JOB + mAppWidgetId)) {
            SharedPreferences.Editor pEdit = mPrefs.edit();
            pEdit.putLong(StempeluhrStatus.KEY_JOB + mAppWidgetId, ASetup.aktJob.getId()).apply();
        }

        wStatus = loadPref(this, mAppWidgetId, null);

        if (wStatus != null) {
            // Set the result to CANCELED.  This will cause the widget host to cancel
            // out of the widget placement if the user presses the back button.
            setResult(RESULT_CANCELED);

            setContentView(R.layout.stempeluhr_configure);


            mSpinnertJob = findViewById(R.id.SW_spinner_job);

            final ArbeitsplatzListAdapter jobListeAdapter = new ArbeitsplatzListAdapter(this);
            mSpinnertJob.setAdapter(jobListeAdapter);
            mSpinnertJob.setSelection(mJobs.getIndex(wStatus.mJob.getId()));
            mSpinnertJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    long mID = mJobs.getVonIndex(position).getId();
                    if (wStatus.mJob.getId() != mID/*mJobs.getID(position)*/) {
                        wStatus.mJob = mJobs.getVonID(mID);//new Arbeitsplatz(mJobs.getID(position));

                        wStatus.save(mPrefs);
                        updateButtons(true);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            updateButtons(false);
        }
    }

    private void updateButtons(boolean isChage) {
        // Farben setzen

        // Werte setzen und optionen ausblenden
        // Soll der Tag nach dem Ausstempeln geöffnet werden?
        SwitchCompat sOpenTag = findViewById(R.id.SW_switch_opentag);
        sOpenTag.setThumbTintList(wStatus.mJob.getFarbe_Thumb());
        sOpenTag.setTrackTintList(wStatus.mJob.getFarbe_Trak());

        // Soll immer eine Extraschicht angelegt werden?
        SwitchCompat sExtraSchicht = findViewById(R.id.SW_switch_extraschicht);
        sExtraSchicht.setThumbTintList(wStatus.mJob.getFarbe_Thumb());
        sExtraSchicht.setTrackTintList(wStatus.mJob.getFarbe_Trak());

        // soll eine definierte Schcht an Hand der Zeiten erkannt werden?
        SwitchCompat sErkenneSchicht = findViewById(R.id.SW_switch_erkenne_schicht);
        sErkenneSchicht.setThumbTintList(wStatus.mJob.getFarbe_Thumb());
        sErkenneSchicht.setTrackTintList(wStatus.mJob.getFarbe_Trak());

        // definierte Werte der erkannten Schicht überschreiben
        // von
        AppCompatCheckBox mBox_von = findViewById(R.id.SW_opt_vonreal);
        CompoundButtonCompat.setButtonTintList(
                mBox_von,
                wStatus.mJob.getFarbe_Radio());
        //bis
        AppCompatCheckBox mBox_bis = findViewById(R.id.SW_opt_bisreal);
        CompoundButtonCompat.setButtonTintList(
                mBox_bis,
                wStatus.mJob.getFarbe_Radio());
        // Pause
        AppCompatCheckBox mBox_pause = findViewById(R.id.SW_opt_pausereal);
        CompoundButtonCompat.setButtonTintList(
                mBox_pause,
                wStatus.mJob.getFarbe_Radio());


        // sollen die gestempelten Zeiten gerundet werden?
        RadioGroup gRunden = findViewById(R.id.SW_group_runden);
        for (View v : gRunden.getTouchables()) {
            CompoundButtonCompat.setButtonTintList(
                    ((AppCompatRadioButton) v),
                    wStatus.mJob.getFarbe_Radio());
        }

        // auf welchen Wert soll gerundet werden
        AppCompatEditText tRunden = findViewById(R.id.SW_wert_runden);
        SwitchCompat sRunden = findViewById(R.id.SW_switch_runden);
        sRunden.setThumbTintList(wStatus.mJob.getFarbe_Thumb());
        sRunden.setTrackTintList(wStatus.mJob.getFarbe_Trak());

        // der Button zum übernehmen der Einstellungen
        AppCompatButton bOK = findViewById(R.id.SW_button_add);
        ViewCompat.setBackgroundTintList(bOK, wStatus.mJob.getFarbe_Button());
        bOK.setTextColor(wStatus.mJob.getFarbe_Schrift_Button());


        // wenn kein Wechsel des Arbeitsplatzes dann die Handler und Werte setzen
        if(!isChage){
            // Tag nach ausstempeln öffnen
            sOpenTag.setChecked(wStatus.getOption(StempeluhrStatus.OPT_TAG_OEFFEN));
            // Extraschicht anlegen
            sExtraSchicht.setChecked(wStatus.getOption(StempeluhrStatus.OPT_EXTRASCHICHT));

            // Schichterkennung
            if (!wStatus.getOption(StempeluhrStatus.OPT_ERKENNE_SCHICHT)) {
                findViewById(R.id.SW_box_erkenne_schicht).setVisibility(View.GONE);
            } else {
                sErkenneSchicht.setChecked(true);
                if (wStatus.getOption(StempeluhrStatus.OPT_EXTRASCHICHT))
                    findViewById(R.id.SW_warnung).setVisibility(View.GONE);
            }

            // Stempelwerte
            mBox_von.setChecked(wStatus.getOption(StempeluhrStatus.OPT_SET_VON_REAL));
            mBox_bis.setChecked(wStatus.getOption(StempeluhrStatus.OPT_SET_BIS_REAL));
            mBox_pause.setChecked(wStatus.getOption(StempeluhrStatus.OPT_SET_PAUSE_REAL));
            // Runden
            tRunden.setText(String.valueOf(wStatus.mRunden_minuten));
            switch (wStatus.mRunden) {
                case StempeluhrStatus.AUFRUNDEN:
                    gRunden.check(R.id.SW_opt_aufrunden);
                    break;
                case StempeluhrStatus.ABRUNDEN:
                    gRunden.check(R.id.SW_opt_abrunden);
                    break;
                default:
                    gRunden.check(R.id.SW_opt_kaufrunden);
            }
            if (wStatus.mRunden == StempeluhrStatus.NICHTRUNDEN) {
                findViewById(R.id.SW_box_runden).setVisibility(View.GONE);
            } else {
                sRunden.setChecked(true);
            }

            // Handler zuweisen
            sExtraSchicht.setOnCheckedChangeListener(mExtraSchichtListener);
            sErkenneSchicht.setOnCheckedChangeListener(mErkenneSchichtListener);
            sRunden.setOnCheckedChangeListener(mRundenListener);
            tRunden.setOnClickListener(mRundenClickListener);

            // wenn es ein bestehendes Widget ist dann den Buttontext umstellen
            if (isNotNew) {
                bOK.setVisibility(View.GONE);
            } else {
                bOK.setOnClickListener(mOkClickListener);
            }

        }
    }

    /*
    * Handlerroutinen
    */
    View.OnClickListener mRundenClickListener = v -> {
        NumberPickerBuilder npb = new NumberPickerBuilder()
            .setFragmentManager(getSupportFragmentManager())
            .setMinNumber(BigDecimal.valueOf(1))
            .setMaxNumber(BigDecimal.valueOf(60))
            .setDecimalVisibility(View.INVISIBLE)
            .setPlusMinusVisibility(View.INVISIBLE)
            .setStyleResId(R.style.BetterPickersDialogFragment_Light)
            .setLabelText(getString(R.string.minutes_label));
    npb.show();
    };

    @Override
    public void onDialogNumberSet(int reference, BigInteger number, double decimal, boolean isNegative, BigDecimal fullNumber) {
        ((AppCompatEditText)findViewById(R.id.SW_wert_runden)).setText(String.format(Locale.getDefault(), "%d", number));
    }

     CompoundButton.OnCheckedChangeListener mErkenneSchichtListener =
             (buttonView, isChecked) -> findViewById(R.id.SW_box_erkenne_schicht).setVisibility( isChecked? View.VISIBLE: View.GONE);

     CompoundButton.OnCheckedChangeListener mExtraSchichtListener =
             (buttonView, isChecked) -> findViewById(R.id.SW_warnung).setVisibility( isChecked? View.GONE: View.VISIBLE);

    CompoundButton.OnCheckedChangeListener mRundenListener =
            (buttonView, isChecked) -> findViewById(R.id.SW_box_runden).setVisibility( isChecked? View.VISIBLE: View.GONE);

    View.OnClickListener mOkClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Context context = StempeluhrConfigureActivity.this;

            // Buttons auswerten
            wStatus.setOption(StempeluhrStatus.OPT_TAG_OEFFEN, ((SwitchCompat) findViewById(R.id.SW_switch_opentag)).isChecked());
            wStatus.setOption(StempeluhrStatus.OPT_EXTRASCHICHT, ((SwitchCompat) findViewById(R.id.SW_switch_extraschicht)).isChecked());
            wStatus.setOption(StempeluhrStatus.OPT_ERKENNE_SCHICHT, ((SwitchCompat) findViewById(R.id.SW_switch_erkenne_schicht)).isChecked());
            wStatus.setOption(StempeluhrStatus.OPT_SET_VON_REAL, ((AppCompatCheckBox) findViewById(R.id.SW_opt_vonreal)).isChecked());
            wStatus.setOption(StempeluhrStatus.OPT_SET_BIS_REAL, ((AppCompatCheckBox) findViewById(R.id.SW_opt_bisreal)).isChecked());
            wStatus.setOption(StempeluhrStatus.OPT_SET_PAUSE_REAL, ((AppCompatCheckBox) findViewById(R.id.SW_opt_pausereal)).isChecked());
            if (((SwitchCompat) findViewById(R.id.SW_switch_runden)).isChecked()) {
                AppCompatEditText tMinuten = findViewById(R.id.SW_wert_runden);
                String m;
                m = Objects.requireNonNull(tMinuten.getText()).toString();

                wStatus.mRunden_minuten = Integer.parseInt(m);
                RadioGroup gRunden = findViewById(R.id.SW_group_runden);
                int checkedRadioButtonId = gRunden.getCheckedRadioButtonId();
                if (checkedRadioButtonId == R.id.SW_opt_aufrunden) {
                    wStatus.mRunden = StempeluhrStatus.AUFRUNDEN;
                } else if (checkedRadioButtonId == R.id.SW_opt_abrunden) {
                    wStatus.mRunden = StempeluhrStatus.ABRUNDEN;
                } else {
                    wStatus.mRunden = StempeluhrStatus.KAUFRUNDEN;
                }
            } else {
                wStatus.mRunden = StempeluhrStatus.NICHTRUNDEN;
                wStatus.mRunden_minuten = 1;
            }

            // When the button is clicked, store the string locally
            wStatus.save(ASetup.mPreferenzen);

            // It is the responsibility of the configuration activity to update the app widget
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

            Stempeluhr.updateAppWidget(context, appWidgetManager, wStatus.mID);

            // Make sure we pass back the original appWidgetId
            Intent resultValue = new Intent();
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, wStatus.mID);
            setResult(RESULT_OK, resultValue);
            finish();
        }
    };


    @Override
    public void onBackPressed() {
        if (isNotNew) {
            mOkClickListener.onClick(findViewById(R.id.SW_button_add));
        } else {
            super.onBackPressed();
        }
    }
}

