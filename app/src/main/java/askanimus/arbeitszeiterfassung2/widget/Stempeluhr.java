/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import askanimus.arbeitszeiterfassung2.MainActivity;
import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.arbeitswoche.Arbeitswoche;
import askanimus.arbeitszeiterfassung2.arbeitsmonat.Arbeitsmonat;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitsschicht.Arbeitsschicht;
import askanimus.arbeitszeiterfassung2.arbeitstag.Arbeitstag;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsschicht.SchichtDefault;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;


/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link StempeluhrConfigureActivity StempeluhrConfigureActivity}
 */
public class Stempeluhr extends AppWidgetProvider {
    private static final int PUFFER = 60;

    private static final String START_CLICKED = "askanimus.StempeluhrStart";
    private static final String PAUSE_CLICKED = "askanimus.StempeluhrPause";
    private static final String STOP_CLICKED = "askanimus.StempeluhrStop";
    private static final String BACKGROUND_CLICKED = "askanimus.StempeluhrOpenApp";
    public static final String UPDATE = "askanimus.StempeluhrUpdate";
    public static final String TIMER = "askanimus.StempeluhrTIMER";


    private static AlarmManager mAlarmManager;

    public static void updateAppWidget(
            Context context,
            AppWidgetManager appWidgetManager,
            int addWidgetID) {

        // den momentanen Status, die Einstellungen des Widges lesen
        StempeluhrStatus wStatus = StempeluhrConfigureActivity.loadPref(context, addWidgetID, UPDATE);

        // das Widget Layout laden
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.stempeluhr2x4);

        // den Button zum Configurieren des Widget beleben
        Intent configurationIntent = new Intent(context, StempeluhrConfigureActivity.class);
        configurationIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, addWidgetID);
        configurationIntent.putExtra(ISetup.KEY_WIDGET_CONFIG, true);
        int flags = PendingIntent.FLAG_UPDATE_CURRENT;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            flags = flags | PendingIntent.FLAG_IMMUTABLE;
        }

        views.setOnClickPendingIntent(
                R.id.WS_button_einstellungen,
                PendingIntent.getActivity(
                        context,
                        addWidgetID,
                        configurationIntent,
                        flags));


        // Die Ansicht des Widgets aktuallisieren
        if (wStatus != null) {
            // beim Click auf den Widgethintergrund, die App öffnen
            // aktueller Tag des betreffenden Arbeitsplatzes
            views.setOnClickPendingIntent(
                    R.id.WS_box_widget,
                    getPendingSelfIntent(context, BACKGROUND_CLICKED, wStatus.mID)
            );

            Uhrzeit mNetto = new Uhrzeit(0);
            Datum mCal = new Datum(new Date(), wStatus.mJob.getWochenbeginn());
            Uhrzeit mEnde = new Uhrzeit();

            // Werte aktuallisieren und Buttons zeigen bzw. verbergen
            switch (wStatus.mStatus) {
                case StempeluhrStatus.WIDGET_STATUS_RUN:
                    // Bruttozeit berechnen = Laufzeit des Widgets
                    mNetto.set((mEnde.getAlsMinuten() - wStatus.mBeginn.getAlsMinuten()) + (wStatus.mTag.tageBis(mCal) * ISetup.Minuten_TAG));

                    // Wenn ein Timer gesetzt wurde, prüfen ob er schon erreicht ist
                    /*StempeluhrStatus.TimerEintrag mTimerEintrag = wStatus.getNextTimer(mNetto.getAlsMinuten());
                    if (mTimerEintrag != null && mNetto.getAlsMinuten() >= mTimerEintrag.minuten) {
                        // Alarm ausgeben
                        openTimerAlarm(context, wStatus);
                    }*/

                    // Wenn 24h erreicht oder überschritten wurden, ausstempeln
                    if (mNetto.getAlsMinuten() >= ISetup.Minuten_TAG) {
                        ausstempeln(context, wStatus);
                    } else {
                        if (!wStatus.mJob.isOptionSet(Arbeitsplatz.OPT_PAUSE_BEZAHLT))
                            mNetto.add(-wStatus.mPauseSumme.getAlsMinuten());

                        // Sichtbarkeiten einstellen
                        views.setViewVisibility(R.id.WS_button_pause, View.VISIBLE);
                        views.setViewVisibility(R.id.WS_button_stop, View.VISIBLE);
                        views.setViewVisibility(R.id.WS_button_start, View.INVISIBLE);

                        views.setViewVisibility(R.id.WS_box_zeiten, View.VISIBLE);

                        // Klickhandler registrieren bzw. erneuern
                        views.setOnClickPendingIntent(R.id.WS_button_pause, getPendingSelfIntent(context, PAUSE_CLICKED, wStatus.mID));
                        views.setOnClickPendingIntent(R.id.WS_button_stop, getPendingSelfIntent(context, STOP_CLICKED, wStatus.mID));

                    }
                    break;
                case StempeluhrStatus.WIDGET_STATUS_PAUSE:
                    wStatus.updatePause(mEnde.getAlsMinuten());

                    mNetto.set(mEnde.getAlsMinuten() - wStatus.mBeginn.getAlsMinuten() + (wStatus.mTag.tageBis(mCal) * ISetup.Minuten_TAG));

                    // Wenn 24h erreicht oder überschritten wurden ausstempeln
                    if (mNetto.getAlsMinuten() >= ISetup.Minuten_TAG) {
                        ausstempeln(context, wStatus);
                    } else {
                        if (!wStatus.mJob.isOptionSet(Arbeitsplatz.OPT_PAUSE_BEZAHLT))
                            mNetto.set(mNetto.getAlsMinuten() - wStatus.mPauseSumme.getAlsMinuten());

                        views.setViewVisibility(R.id.WS_button_pause, View.INVISIBLE);
                        views.setViewVisibility(R.id.WS_button_stop, View.VISIBLE);
                        views.setViewVisibility(R.id.WS_button_start, View.VISIBLE);

                        // Klickhandler registrieren bzw. erneuern
                        views.setOnClickPendingIntent(R.id.WS_button_start, getPendingSelfIntent(context, START_CLICKED, wStatus.mID));
                        views.setOnClickPendingIntent(R.id.WS_button_stop, getPendingSelfIntent(context, STOP_CLICKED, wStatus.mID));
                    }
                    break;
                default:
                    wStatus.mTag.set(mCal.getTime());
                    views.setViewVisibility(R.id.WS_button_pause, View.INVISIBLE);
                    views.setViewVisibility(R.id.WS_button_stop, View.INVISIBLE);
                    views.setViewVisibility(R.id.WS_button_start, View.VISIBLE);

                    views.setViewVisibility(R.id.WS_box_zeiten, View.INVISIBLE);
                    views.setViewVisibility(R.id.WS_wert_pause, View.INVISIBLE);

                    // Klickhandler registrieren bzw. erneuern
                    views.setOnClickPendingIntent(R.id.WS_button_start, getPendingSelfIntent(context, START_CLICKED, wStatus.mID));

                    // das aktuelle Datum anzeigen
                    views.setTextViewText(R.id.WS_wert_datum, mCal.getString_Datum(context));
                    break;
            }

            if (wStatus.mStatus != StempeluhrStatus.WIDGET_STATUS_NEU) {
                // Das Datum
                if (wStatus.mTag.tageBis(mCal) == 0) {
                    views.setTextViewText(R.id.WS_wert_datum, wStatus.mTag.getString_Datum(context));
                } else {
                    views.setTextViewText(
                            R.id.WS_wert_datum,
                            wStatus.mTag.getString_Datum_Bereich(
                                    context,
                                    0,
                                    wStatus.mTag.tageBis(mCal),
                                    Calendar.DAY_OF_MONTH));
                }

                // Die Zeitspanne zwichen Einstempeln und jetzt
                views.setTextViewText(R.id.WS_wert_von_bis, wStatus.mBeginn.getUhrzeitString() + " - " + mEnde.getUhrzeitString());

                // Die gesamte Pausenzeit
                if (wStatus.mPauseSumme.getAlsMinuten() > 0 || wStatus.mStatus == StempeluhrStatus.WIDGET_STATUS_PAUSE) {
                    views.setViewVisibility(R.id.WS_wert_pause, View.VISIBLE);
                    views.setTextViewText(R.id.WS_wert_pause, ASetup.res.getString(R.string.pause) + ": " + wStatus.mPauseSumme.getStundenString(true, wStatus.mJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
                } else {
                    views.setViewVisibility(R.id.WS_wert_pause, View.INVISIBLE);
                }
            }

            // die Netto Arbeitszeit
            views.setTextViewText(R.id.WS_wert_netto, mNetto.getStundenString(true, wStatus.mJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

            // der Arbeitsplatzname
            views.setTextViewText(R.id.WS_wert_job, wStatus.mJob.getName());
            views.setInt(R.id.WS_wert_job, "setBackgroundColor", wStatus.mJob.getFarbe_Widget_Titel_Background());
            views.setInt(R.id.WS_button_einstellungen, "setBackgroundColor", wStatus.mJob.getFarbe_Widget_Titel_Background());


            // der momentaner Wochhensaldo
            Arbeitswoche mWoche = new Arbeitswoche(wStatus.mTag.getTimeInMillis(), wStatus.mJob);
            views.setTextViewText(R.id.WS_titel_woche, ASetup.res.getString(R.string.woche_nummer, mWoche.getNummer()));
            Uhrzeit nettoWert = new Uhrzeit(mWoche.getIst() - mWoche.getSoll() + mNetto.getAlsMinuten());
            views.setTextViewText(R.id.WS_wert_woche, nettoWert.getStundenString(true, wStatus.mJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
            if (nettoWert.getAlsMinuten() < 0)
                views.setTextColor(R.id.WS_wert_woche, ASetup.res.getColor(R.color.negativ));
            else if (nettoWert.getAlsMinuten() > 0)
                views.setTextColor(R.id.WS_wert_woche, ASetup.res.getColor(R.color.positiv));

            // der momentane Monatssaldo
            String sMonat;
            Datum mDatum = wStatus.mJob.getAbrechnungsmonat(wStatus.mTag);
            Arbeitsmonat mMonat = new Arbeitsmonat(
                    wStatus.mJob,
                    mDatum.get(Calendar.YEAR),
                    mDatum.get(Calendar.MONTH),
                    true, false);
            if (wStatus.mJob.getMonatsbeginn() > 1) {
                SimpleDateFormat dFormat = new SimpleDateFormat("MMM", Locale.getDefault());
                sMonat = dFormat.format(mDatum.getTime());
                sMonat += "/";
                mDatum.add(Calendar.MONTH, 1);
                sMonat += dFormat.format(mDatum.getTime());
            } else {
                SimpleDateFormat dFormat = new SimpleDateFormat("MMMM", Locale.getDefault());
                sMonat = dFormat.format(mDatum.getTime());
            }
            views.setTextViewText(R.id.WS_titel_monat, sMonat);

            nettoWert.set(mMonat.getSaldo_Aktuell(wStatus.mTag.get(Calendar.DAY_OF_MONTH)) + mNetto.getAlsMinuten());
            views.setTextViewText(R.id.WS_wert_monat, nettoWert.getStundenString(true, wStatus.mJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
            if (nettoWert.getAlsMinuten() < 0)
                views.setTextColor(R.id.WS_wert_monat, ASetup.res.getColor(R.color.negativ));
            else if (nettoWert.getAlsMinuten() > 0)
                views.setTextColor(R.id.WS_wert_monat, ASetup.res.getColor(R.color.positiv));

            // sind die Aufzeichnungen Beendet oder haben noch nicht begonnen, dann das Widget sperren
            // die laufende Aufzeichnung läuft bis 24h nach Aufzeichnungsende weiter und wird dann beendet
            if (wStatus.mJob.isEndeAufzeichnung(mCal)) {
                // wenn Aufzeichnung noch läuft, dann das Sperrsymbol nicht anzeigen
                if (wStatus.mStatus == StempeluhrStatus.WIDGET_STATUS_NEU) {
                    views.setViewVisibility(R.id.WS_image_end, View.VISIBLE);
                    views.setViewVisibility(R.id.WS_button_start, View.GONE);
                }
            } else if (mCal.liegtVor(wStatus.mJob.getStartDatum())) {
                // wenn Aufzeichnung noch läuft, dann das Sperrsymbol nicht anzeigen
                if (wStatus.mStatus == StempeluhrStatus.WIDGET_STATUS_NEU) {
                    views.setViewVisibility(R.id.WS_image_end, View.VISIBLE);
                    views.setViewVisibility(R.id.WS_button_start, View.GONE);
                }
            } else {
                views.setViewVisibility(R.id.WS_image_end, View.GONE);
            }

            // Der arbeitsplatz ist nicht gelöscht
            views.setViewVisibility(R.id.WS_box_delete, View.GONE);
            views.setViewVisibility(R.id.WS_button_einstellungen, View.VISIBLE);
        } else {
            // Der arbeitsplatz ist gelöscht
            views.setViewVisibility(R.id.WS_box_delete, View.VISIBLE);
            views.setViewVisibility(R.id.WS_button_einstellungen, View.INVISIBLE);
        }
        // Mitteilen, dass der Widgetmanager alle Widgets neu zeichnen soll
        appWidgetManager.updateAppWidget(addWidgetID, views);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        String appAction = intent.getAction();
        Bundle extras = intent.getExtras();

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        StempeluhrStatus mStatus;

        if (extras != null && appAction != null) {
            if (appAction.equals(UPDATE)) {
                ComponentName cName = new ComponentName(context.getPackageName(), Stempeluhr.class.getName());
                int[] appWidgetIds = appWidgetManager.getAppWidgetIds(cName);
                for (int appWidgetId : appWidgetIds) {
                    updateAppWidget(context, appWidgetManager, appWidgetId);
                }
            } else {
                mStatus = StempeluhrConfigureActivity.loadPref(
                        context,
                        extras.getInt(
                                ISetup.KEY_WIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID),
                        intent.getAction());
                if (mStatus != null) {
                    switch (appAction) {
                        case START_CLICKED:
                            if (mStatus.mStatus == StempeluhrStatus.WIDGET_STATUS_NEU) {
                                mStatus.startWidget();
                            } else {
                                mStatus.stopPause();
                            }

                            // den Status speichern
                            mStatus.save(ASetup.mPreferenzen);
                            updateAppWidget(context, appWidgetManager, mStatus.mID);

                            break;
                        case PAUSE_CLICKED:
                            mStatus.startPause();
                            mStatus.save(ASetup.mPreferenzen);
                            updateAppWidget(context, appWidgetManager, mStatus.mID);
                            break;
                        case STOP_CLICKED:
                            mStatus.stopWidget();
                            ausstempeln(context, mStatus);
                            updateAppWidget(context, appWidgetManager, mStatus.mID);

                            break;
                        case BACKGROUND_CLICKED:
                            // den entsprechenden Tag öffnen
                            Intent iMain = new Intent();
                            iMain.putExtra(ISetup.KEY_ANZEIGE_VIEW, ISetup.VIEW_TAG);
                            iMain.putExtra(ISetup.KEY_ANZEIGE_DATUM, ASetup.aktDatum.getTimeInMillis());
                            iMain.putExtra(ISetup.KEY_JOBID, mStatus.mJob.getId());
                            iMain.setClass(context, MainActivity.class);
                            iMain.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            iMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(iMain);
                            break;
                    }
                }
            }
        }
    }

    protected static void ausstempeln(Context context, @NonNull StempeluhrStatus mStatus/*, Uhrzeit mEnde*/) {

        //StempeluhrStatus mStatus = status;
        Calendar zeitpunktEnde = Calendar.getInstance();
        zeitpunktEnde.setTime(new Date());
        Datum mKalender = new Datum(zeitpunktEnde, mStatus.mJob.getWochenbeginn());
        ASetup.aktDatum.set(mKalender.getTime());
        Uhrzeit mEnde = new Uhrzeit(zeitpunktEnde.get(Calendar.HOUR_OF_DAY), zeitpunktEnde.get(Calendar.MINUTE));


        int mZeitspanne = mEnde.getAlsMinuten() - mStatus.mBeginn.getAlsMinuten() + (mStatus.mTag.tageBis(mKalender) * ISetup.Minuten_TAG);
        // wenn der Timer >= 1 Minute in Betrieb war
        if (mZeitspanne > 0) {
            Arbeitstag mTag = new Arbeitstag(
                    mStatus.mTag.getCalendar(),
                    mStatus.mJob,
                    mStatus.mJob.getSollstundenTag(mStatus.mTag),
                    mStatus.mJob.getSollstundenTagPauschal(mStatus.mTag.get(Calendar.YEAR), mStatus.mTag.get(Calendar.MONTH))
            );
            Arbeitsschicht mSchicht;
            boolean istEingetragen = false;
            Uhrzeit mVon = new Uhrzeit(mStatus.mBeginn.getAlsMinuten());
            Uhrzeit mBis = new Uhrzeit((mZeitspanne > ISetup.Minuten_TAG) ? mStatus.mBeginn.getAlsMinuten() : mEnde.getAlsMinuten());
            Uhrzeit mPause = new Uhrzeit(mStatus.mPauseSumme.getAlsMinuten());
            int i;

            // Runden wenn gewünscht
            if (mStatus.mRunden != StempeluhrStatus.NICHTRUNDEN && mStatus.mRunden_minuten > 1) {
                int rVon = mVon.getAlsMinuten() % mStatus.mRunden_minuten;
                int rBis = mBis.getAlsMinuten() % mStatus.mRunden_minuten;
                int rPause = mPause.getAlsMinuten() % mStatus.mRunden_minuten;
                switch (mStatus.mRunden) {
                    case StempeluhrStatus.ABRUNDEN:
                        mVon.sub(rVon);
                        mBis.sub(rBis);
                        mPause.sub(rPause);
                        break;
                    case StempeluhrStatus.AUFRUNDEN:
                        mVon.add(mStatus.mRunden_minuten - rVon);
                        mBis.add(mStatus.mRunden_minuten - rBis);
                        if (mPause.getAlsMinuten() > 0)
                            mPause.add(mStatus.mRunden_minuten - rPause);
                        break;
                    case StempeluhrStatus.KAUFRUNDEN:
                        int rMinuten = Math.round((float) mStatus.mRunden_minuten / 2);
                        if (rVon > 0)
                            if (rVon > rMinuten) {
                                mVon.add(mStatus.mRunden_minuten - rVon);
                            } else {
                                mVon.sub(rVon);
                            }
                        if (rBis > 0)
                            if (rBis >= rMinuten) {
                                mBis.add(mStatus.mRunden_minuten - rBis);
                            } else {
                                mBis.sub(rBis);
                            }
                        if (rPause > 0)
                            if (rPause > rMinuten) {
                                mPause.add(mStatus.mRunden_minuten - rPause);
                            } else {
                                mPause.sub(rPause);
                            }
                        break;
                }
            }

            // Schicht erkennen wenn gewünscht
            if (mStatus.getOption(StempeluhrStatus.OPT_ERKENNE_SCHICHT)) {
                SchichtDefault defSchicht = null;

                if (mStatus.mJob.getDefaultSchichten().getSizeAktive() > 1) {
                    /* es gibt mehrere definierte Schichten*/
                    /* die Stempelzeiten müssen folgende Bedingungen erfüllen */
                    for (int s = 0; s < mStatus.mJob.getDefaultSchichten().getSizeAktive(); s++) {
                        defSchicht = mStatus.mJob.getDefaultSchichten().getAktive(s);
                        /* erkannt wenn Startzeit +/- puffer bei def. Startzeit liegt und Endzeit nach def. Endzeit - puffer*/
                        /* Überstunden durch später gehen*/
                        if ((mVon.getAlsMinuten() >= (defSchicht.getVon() - PUFFER)) && (mVon.getAlsMinuten() <= (defSchicht.getVon() + PUFFER))) {
                            if ((mBis.getAlsMinuten() >= (defSchicht.getBis() - PUFFER))) {
                                istEingetragen = true;
                                break;
                            }
                        }
                        /* erkannt wenn Endzeit +/- puffer bei def. Endzeit liegt und Startzeitzeit vor def. Endzeit + puffer*/
                        /* Überstunden durch früher beginnen*/
                        if (!istEingetragen) {
                            if ((mBis.getAlsMinuten() >= (defSchicht.getBis() - PUFFER)) && (mBis.getAlsMinuten() <= (defSchicht.getBis() + PUFFER))) {
                                if ((mVon.getAlsMinuten() <= (defSchicht.getVon() + PUFFER))) {
                                    istEingetragen = true;
                                    break;
                                }
                            }
                        }

                    }
                } else {
                    /* es wurde nur eine Schicht definiert*/
                    /* wenn die Stempelzeiten innerhalb dieser definierten Schicht (+/- puffer) liegen, wurde sie erkannt*/
                    defSchicht = mStatus.mJob.getDefaultSchichten().getAktive(0);
                    if (mVon.getAlsMinuten() >= (defSchicht.getVon() - PUFFER) && (mBis.getAlsMinuten() <= (defSchicht.getBis() + PUFFER))) {
                        istEingetragen = true;
                    }

                }

                /* wenn die Erkennung einen Treffer ergeben hat, die betreffende Schicht überschreiben*/
                if (istEingetragen) {
                    Arbeitsschicht aSchicht = null;
                    if (mStatus.getOption(StempeluhrStatus.OPT_EXTRASCHICHT)) {
                        mTag.addSchicht(-1, defSchicht);
                        aSchicht = mTag.getSchicht(mTag.getSchichtzahl() - 1);
                    } else {
                        if (mStatus.mJob.isTeilschicht()) {
                            for (int ss = 0; ss < mTag.getSchichtzahl(); ss++) {
                                if (mTag.getSchicht(ss).getDefaultSchichtId() == defSchicht.getID()) {
                                    aSchicht = mTag.getSchicht(ss);
                                    break;
                                }
                            }

                        } else {
                            aSchicht = mTag.getSchicht(0);
                            aSchicht.setDefaultSchicht(defSchicht);
                        }
                    }

                    if (aSchicht != null) {
                        aSchicht.setAbwesenheit(mStatus.mJob.getAbwesenheiten().getAktive(Abwesenheit.ARBEITSZEIT), 0);
                        if (mStatus.getOption(StempeluhrStatus.OPT_SET_VON_REAL))
                            aSchicht.setVon(mVon.getAlsMinuten());
                        if (mStatus.getOption(StempeluhrStatus.OPT_SET_BIS_REAL))
                            aSchicht.setBis(mBis.getAlsMinuten());
                        if (mStatus.getOption(StempeluhrStatus.OPT_SET_PAUSE_REAL))
                            aSchicht.setPause(mPause.getAlsMinuten());
                    }
                }
            }

            /* Schichterkennung ist abgewählt oder hat keinen Treffer ergeben */
            if (!istEingetragen) {
                if (!mStatus.getOption(StempeluhrStatus.OPT_EXTRASCHICHT)) {

                    for (i = 0; i < mTag.getSchichtzahl(); i++) {
                        mSchicht = mTag.getSchicht(i);
                        if (mSchicht.getAbwesenheit().getKategorie() == Abwesenheit.KAT_KEINESCHICHT) {
                            istEingetragen = true;
                            mSchicht.setAll(mStatus.mAbwesenheit,
                                    mVon,
                                    mBis,
                                    mPause,
                                    mStatus.mJob.isTeilschicht() ? mSchicht.getDefaultSchichtId() : 0);
                            break;
                        }
                    }
                } else {
                    i = mTag.getSchichtzahl();
                }

                if (!istEingetragen) {
                    mTag.addSchicht(-1, ASetup.res.getString(R.string.schicht_nr, (i + 1)));
                    mSchicht = mTag.getSchicht(i);
                    mSchicht.setAll(mStatus.mAbwesenheit,
                            mVon,
                            mBis,
                            mPause,
                            -1);
                }

            }

            // den betreffenden Monat aktuallisieren
            Arbeitsmonat mMonat = new Arbeitsmonat(
                    mStatus.mJob,
                    mTag.getKalender().get(Calendar.YEAR),
                    mTag.getKalender().get(Calendar.MONTH),
                    true,
                    true);
            mMonat.updateSaldo(true);

            String mMeldung = ASetup.res.getString(R.string.widget_add_schicht);
            Toast toast = Toast.makeText(context, mMeldung, Toast.LENGTH_LONG);
            toast.show();

            if (mStatus.getOption(StempeluhrStatus.OPT_TAG_OEFFEN)) {
                Intent mMainIntent = new Intent();
                mMainIntent.setClass(context, MainActivity.class);
                mMainIntent.putExtra(ISetup.KEY_ANZEIGE_VIEW, ISetup.VIEW_TAG);
                mMainIntent.putExtra(ISetup.KEY_ANZEIGE_DATUM, mTag.getKalender().getTimeInMillis());
                mMainIntent.putExtra(ISetup.KEY_JOBID, mStatus.mJob.getId());
                mMainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mMainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(mMainIntent);
            }

        } else {
            String mMeldung = ASetup.res.getString(R.string.widget_add_no_schicht);
            Toast toast = Toast.makeText(context, mMeldung, Toast.LENGTH_LONG);
            toast.show();
        }

        // Widget zurück setzen
        mStatus.resetWidget();
        mStatus.save(ASetup.mPreferenzen);
    }

    protected static PendingIntent getPendingSelfIntent(Context context, String action, int widgetID) {
        Intent intent = new Intent(context, Stempeluhr.class);
        intent.putExtra(ISetup.KEY_WIDGET_ID, widgetID);
        intent.setAction(action);
        return PendingIntent.getBroadcast(
                context,
                widgetID,
                intent,
                (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) ? PendingIntent.FLAG_IMMUTABLE : 0);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        // wenn noch kein Update Alarm gesetzt wurde,
        // bzw. dieser nach Reboot oder Appupdate gelöscht wurde,
        // einen neuen setzen
        if (mAlarmManager == null) {
            onEnabled(context);
        }

        // Alle Widgets neu zeichnen
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // Ein Widget wurde gelöscht,
        // dessen Einstellungen und Daten werden auch gelöscht
        for (int appWidgetId : appWidgetIds) {
            StempeluhrConfigureActivity.deletePref(context, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        //super.onEnabled(context);
        // das erste Widget wurde angelegt,
        // einen Alarm setzen, der das Update der Widgets triggert
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.SECOND, 0);
        //cal.add(Calendar.MINUTE, 1);
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mAlarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 60000, getPendingSelfIntent(context, UPDATE, 0));
    }

    @Override
    public void onDisabled(Context context) {
        // das letzte Widget wurde gelöscht,
        // also wird auch der Alarm gelöscht, der das Update der Widgets triggert
        if (mAlarmManager != null) {
            mAlarmManager.cancel(getPendingSelfIntent(context, UPDATE, 0));
            mAlarmManager = null;
        }
    }

    public static void updateAllWidgets(Context context) {
        // neu Zeichnen der Stempeluhren anstoßen
        AppWidgetManager wm = AppWidgetManager.getInstance(context);
        ComponentName cn = new ComponentName(context.getPackageName(), Stempeluhr.class.getName());
        int[] wi = wm.getAppWidgetIds(cn);
        for (int w : wi) {
            updateAppWidget(context, wm, w);
        }

        // Timer neu setzen
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.SECOND, 0);
        mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        mAlarmManager.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                cal.getTimeInMillis(),
                60000,
                getPendingSelfIntent(context, UPDATE, 0)
        );
    }
}

