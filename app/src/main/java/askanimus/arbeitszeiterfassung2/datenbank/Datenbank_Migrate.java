/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.datenbank;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Environment;

import androidx.documentfile.provider.DocumentFile;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilderFactory;

import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitstag.Arbeitstag;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.einsatzort.Einsatzort;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsschicht.SchichtDefault;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzfeldDefinition;

@SuppressLint("Range")
public class Datenbank_Migrate {

    //Sql Verbindung
    private final SQLiteDatabase connection;

    //Backup Pfad - entsprechend anpassen
    //private final String BACKUP_PATH;/*= Environment.getExternalStorageDirectory().toString() + "/" +Einstellungen.res.getString(R.string.app_verzeichnis_toMigrate) + Einstellungen.backupVerzeichnis;*/

    StorageHelper mStorageHelper;
    // Der Applikationscontext zum anzeigen von Meldungen und Dialogen
    private final Context mContext;

    private long[] mJobsAlt;
    private long[] mJobsNeu;

    private ArrayList<Long> mEortAlt;
    private ArrayList<Long> mEortNeu;

    private Boolean Anzeige_Dezimal;

    private long aktJob;

    /**
     * Konstruktor
     *
     * @param paramSQLiteDatabase Arbeitszeiterfassung Datenbank zum aufnehmen der Datensicherung
     * @param storageHelper Helperklasse, die das Verzeichnis der Wiederherstellungsdatei bereitstellt
     * @param context Programm Context
     */
    public Datenbank_Migrate(SQLiteDatabase paramSQLiteDatabase, StorageHelper storageHelper, Context context) {
        this.connection = paramSQLiteDatabase;
        //BACKUP_PATH = pfad;
        mStorageHelper = storageHelper;
        mContext = context;
    }

    /**
     * Nimmt die database.xml Datei und stellt die datenbank wieder her
     */
    public void einlesen(String restore_file) throws Exception {
        Document localDocument = null;
        DocumentFile migrateFile = mStorageHelper.getVerzeichnisFile();

        if (migrateFile != null && migrateFile.exists()) {
            migrateFile = migrateFile.findFile(restore_file);

            if (migrateFile != null && migrateFile.exists()) {
                InputStream inputStream = mContext
                        .getContentResolver()
                        .openInputStream(migrateFile.getUri());
                if (inputStream != null) {
                    localDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
                }
            }

            if (localDocument != null) {
                localDocument.getDocumentElement().normalize();
                NodeList localNodeList = localDocument.getElementsByTagName("table");

                boolean altDaten = false;
                boolean addUeber = false;
                Uhrzeit mZeit = new Uhrzeit(0);
                int dbVersion = 0;

                //for (int i = 0; i<localNodeList.getLength() ; i++)
                for (int i = localNodeList.getLength() - 1; i >= 0; i--) {

                    Node localNode = localNodeList.item(i);
                    if (localNode.getNodeType() == 1) {

                        Element localElement = (Element) localNode;
                        //Log.d("TABLE",  localElement.getAttribute("name"));
                        this.connection.execSQL("DELETE FROM " + localElement.getAttribute("name"));

                        NodeList nl = localElement.getElementsByTagName("item");

                        ///////////konvertieren alter Datenbankbackups < Vers. 5 ///////////////////////////
                        if (localElement.getAttribute("name").equals(Datenbank.DB_T_SETTINGS)) {
                            //wenn noch kein Versionseintrag bestehht sind es alte Daten
                            if (nl.item(0).getChildNodes().getLength() < 8)
                                altDaten = true;
                        }
                        ///////////////////////////////////////////////////////////////////////////////////

                        for (int a = 0; a < nl.getLength(); a++) {

                            ArrayList<String> aSpalten = new ArrayList<>();
                            ArrayList<String> aWerte = new ArrayList<>();

                            Node el = nl.item(a);

                            NodeList spalten = el.getChildNodes();

                            for (int s = 0; s < spalten.getLength(); s++) {
                                Node node = spalten.item(s);

                                aSpalten.add(node.getNodeName());

                                ///////////konvertieren alter Datenbankbackups < Vers. 5 ///////////////////
                                if (node.getNodeName().equals(Datenbank.DB_F_VERSION)) {
                                    dbVersion = Integer.parseInt(node.getTextContent());
                                    if (dbVersion < 1500)
                                        altDaten = true;
                                    if (dbVersion < 1723) {
                                        addUeber = true;
                                    }
                                }

                                if (altDaten)
                                    if (node.getNodeName().equals(Datenbank.DB_F_STARTSALDO) ||
                                            node.getNodeName().equals(Datenbank.DB_F_SOLL_H) ||
                                            node.getNodeName().equals(Datenbank.DB_F_IST_H) ||
                                            node.getNodeName().equals(Datenbank.DB_F_SALDO_H)) {
                                        mZeit.set(Float.parseFloat(node.getTextContent()));

                                        node.setTextContent(String.valueOf(mZeit.getAlsMinuten()));
                                    }
                                if (addUeber) {
                                    int mModule;
                                    if (node.getNodeName().equals(Datenbank.DB_F_MODULE)) {
                                        mModule = Integer.parseInt(node.getTextContent());
                                        mModule = mModule | Arbeitsplatz.OPT_BEZ_UEBERSTUNDEN;
                                        node.setTextContent(String.valueOf(mModule));
                                    }
                                }
                                ////////////////////////////////////////////////////////////////////
                                aWerte.add(node.getTextContent());
                            }


                            StringBuilder sql = new StringBuilder("INSERT INTO " + localElement.getAttribute("name") + "(");
                            StringBuilder dummy = new StringBuilder();

                            for (String s : aSpalten) {
                                sql.append("`").append(s).append("`");
                                dummy.append("?");
                                if (!aSpalten.get(aSpalten.size() - 1).equals(s)) {
                                    sql.append(",");
                                    dummy.append(",");
                                }
                            }

                            sql.append(") VALUES (").append(dummy).append(");");

                            SQLiteStatement stmt = connection.compileStatement(sql.toString());

                            int x = 1;
                            for (String s : aWerte) {
                                String ERSATZ_ZEILENUMBRUCH = "¬nl¬";
                                s = s.replace(ERSATZ_ZEILENUMBRUCH, "\n");
                                String ERSATZ_HOCHKOMMA = " &apos;";
                                s = s.replace(ERSATZ_HOCHKOMMA, "'");
                                String ERSATZ_ANFUERUNGSSTRICHE = " &quot;";
                                s = s.replace(ERSATZ_ANFUERUNGSSTRICHE, "\"");
                                stmt.bindString(x, s);
                                x++;

                                if (x <= aWerte.size()) {
                                    sql.append(",");
                                }
                            }

                            stmt.execute();

                            aSpalten.clear();
                            aWerte.clear();
                        }
                    }
                }

                /*
                 *
                 * Ältere Backups mit den Daten neuerer Versionen vervollständigen
                 *
                 */

                // ab Version 1800(Datenbankversion 8) werden Zeitzusätze in der datenbank gespeichert, alte Sicherungen werden ergänzt
                if (dbVersion < 1800) {
                    // Ab version 8 der datenbank (APP V 1800) sind der E-Mailtext und Währungseinstellungen dazu gekommnen
                    ContentValues werte = new ContentValues(2);

                    // Die Währungseinstellungen
                    werte.put(Datenbank.DB_F_W_KUERZEL, ASetup.mPreferenzen.getString(ISetup.KEY_ANZEIGE_W_KUERZEL, ""));
                    //werte.put(Datenbank.DB_F_W_TRENNER, ASetup.mPreferenzen.getString(ISetup.KEY_ANZEIGE_W_TRENNER, ""));
                    connection.update(Datenbank.DB_T_SETTINGS, werte, Datenbank.DB_F_ID + "=?", new String[]{"1"});

                    // Der E-Mailtext für alle Arbeitsplätze
                    StringBuilder sql = new StringBuilder(64);
                    sql.append("select ");
                    sql.append(Datenbank.DB_F_ID).append(" from ");
                    sql.append(Datenbank.DB_T_JOB);

                    Cursor result = connection.rawQuery(sql.toString(), null);

                    if (result.getCount() > 0) {
                        werte.clear();
                        werte.put(Datenbank.DB_F_EMAIL_TEXT, ASetup.res.getString(R.string.mailtext) + " " + ASetup.mPreferenzen.getString(ISetup.KEY_USERNAME, ""));
                        while (result.moveToNext()) {
                            connection.update(Datenbank.DB_T_JOB, werte, Datenbank.DB_F_ID + "=?", new String[]{Long.toString(result.getLong(result.getColumnIndex(Datenbank.DB_F_ID)))});
                        }
                    }
                    result.close();

                    //////////////Tabelle mit den Zeitzusätzen für jeden Arbeitsplatzaufbauen //////////////////
                    sql = new StringBuilder(64);//.delete(0, sql.length());

                    // Zeitzusatztabelle leeren
                    this.connection.execSQL("DELETE FROM " + "zeitzusatz");

                    // Arrays mit den Zeitzusätzen aufbauen
                    String[] Zusatz_Name = ASetup.res.getStringArray(R.array.zusatz_name);
                    String[] Zusatz_Kurz = ASetup.res.getStringArray(R.array.zusatz_kurz);
                    int[] Zusatz_Wirkung = ASetup.res.getIntArray(R.array.zusatz_wirkung);
                    String[] Zusatz_Icon = ASetup.res.getStringArray(R.array.zusatz_icon);

                    // alle Arbeitsplätze lesen
                    werte = new ContentValues();
                    sql.append("select id from ");
                    sql.append(Datenbank.DB_T_JOB);
                    result = connection.rawQuery(sql.toString(), null);

                    // Zeitzusatztabellen schreiben
                    while (result.moveToNext()) {
                        for (int i = 0; i < Zusatz_Wirkung.length; i++) {
                            werte.put(Datenbank.DB_F_JOB, result.getLong(result.getColumnIndex(Datenbank.DB_F_ID)));
                            werte.put("zusatz", i);
                            werte.put(Datenbank.DB_F_NAME, Zusatz_Name[i]);
                            werte.put(Datenbank.DB_F_KURZ, Zusatz_Kurz[i]);
                            werte.put(Datenbank.DB_F_WIRKUNG, Zusatz_Wirkung[i]);
                            werte.put(Datenbank.DB_F_ICON, Zusatz_Icon[i]);
                            connection.insert("zeitzusatz", null, werte);
                            werte.clear();
                        }
                    }
                    result.close();
                }

                // ab Versoin 1900 werden die Namen der Einträge für Spesen und Km in den Einstellungen
                // zum arbeitsplatz gespeichert
                if (dbVersion < 1900) {
                    ContentValues werte = new ContentValues(2);
                    Cursor result = connection.rawQuery("select " +
                            Datenbank.DB_F_ID + " from " +
                            Datenbank.DB_T_JOB, null);

                    if (result.getCount() > 0) {
                        werte.put("t_spesen", ASetup.res.getString(R.string.spesen));
                        werte.put("t_km", ASetup.res.getString(R.string.strecke));
                        while (result.moveToNext()) {
                            connection.update(Datenbank.DB_T_JOB, werte,
                                    Datenbank.DB_F_ID + "=?",
                                    new String[]{result.getString(result.getColumnIndex(Datenbank.DB_F_ID))});
                        }
                    }
                    result.close();
                }
            }
        }
    }

    /*
     * alte Daten löschen
     */
    public void resetDB() {
        ASetup.stundenDB.close();
        mContext.deleteDatabase(mContext.getString(R.string.dbName));
        ASetup.stundenDB = new Datenbank(mContext);
        ASetup.mDatenbank = ASetup.stundenDB.getWritableDatabase();
    }


    /*
     * *** Überträgt die allgemeinen Einstellungen
     */
    public void Migrate_Einstellungen() {
        //boolean mStatus = true;
        SQLiteDatabase mDatenbank = ASetup.mDatenbank;/*stundenDB.getWritableDatabase();*/

        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();

        String datPfad = Environment.getExternalStorageDirectory().toString();
        datPfad += "/" + ASetup.res.getString(R.string.app_verzeichnis);

        mEdit.putString(ISetup.KEY_DATEN_DIR, datPfad);

        // Einstellungen lesen
        String sql = ("select * from ") +
                Datenbank.DB_T_SETTINGS +
                " where " +
                Datenbank.DB_F_ID +
                "=1 LIMIT 1 ";
        // Anfrage an datenbank
        Cursor result = connection.rawQuery(sql, null);

        // Resultat der Anfrage auswerten
        if (result.getCount() > 0) {
            result.moveToFirst();
            //mEdit.putLong(ISetup.KEY_JOBID, result.getLong(result.getColumnIndex(Datenbank.DB_F_JOB)));
            aktJob = result.getLong(result.getColumnIndex(Datenbank.DB_F_JOB));
            mEdit.putString(ISetup.KEY_USERNAME, result.getString(result.getColumnIndex(Datenbank.DB_F_USER)));
            mEdit.putString(ISetup.KEY_ANZEIGE_W_KUERZEL, result.getString(result.getColumnIndex(Datenbank.DB_F_W_KUERZEL)));
            //mEdit.putString(ISetup.KEY_ANZEIGE_W_TRENNER, result.getString(result.getColumnIndex(Datenbank.DB_F_W_TRENNER)));
            //mEdit.putBoolean(Einstellungen.KEY_ANZEIGE_DEZIMAL, (result.getInt(result.getColumnIndex("anzeige_dezimal")) == 1));
            Anzeige_Dezimal = (result.getInt(result.getColumnIndex("anzeige_dezimal")) == 1);
            switch (result.getInt(result.getColumnIndex(Datenbank.DB_F_VIEW))) {
                case 1:
                    mEdit.putInt(ISetup.KEY_ANZEIGE_VIEW, ISetup.VIEW_MONAT);
                    break;
                case 3:
                    mEdit.putInt(ISetup.KEY_ANZEIGE_VIEW, ISetup.VIEW_LETZTER);
                    break;
                default:
                    mEdit.putInt(ISetup.KEY_ANZEIGE_VIEW, ISetup.VIEW_TAG);
            }
            mEdit.apply();

            // in der alten App nicht vorhanden Einstellungen abfragen
            // Benutzeranschrift, Entfernungseinheit, Sortierrichtung, erweiterter Saldo

            // Einstellungen in datenbank schreiben
            ContentValues mWerte = new ContentValues();

            mWerte.put(Datenbank.DB_F_JOB, aktJob);
            mWerte.put(Datenbank.DB_F_USER, result.getString(result.getColumnIndex(Datenbank.DB_F_USER)));
            mWerte.put(Datenbank.DB_F_ANSCHRIFT, "");
            mWerte.put(Datenbank.DB_F_W_KUERZEL, result.getString(result.getColumnIndex(Datenbank.DB_F_W_KUERZEL)));
            //mWerte.put(Datenbank.DB_F_W_TRENNER, result.getString(result.getColumnIndex(Datenbank.DB_F_W_TRENNER)));
            mWerte.put(Datenbank.DB_F_E_KUERZEL, "km");
            mWerte.put(Datenbank.DB_F_OPTIONEN, ISetup.OPT_ANZ_ERW_SALDO);
            mWerte.put(Datenbank.DB_F_VIEW, ASetup.mPreferenzen.getInt(ISetup.KEY_ANZEIGE_VIEW, ISetup.VIEW_TAG));
            mWerte.put(Datenbank.DB_F_DATEN_DIR, datPfad);
            mWerte.put(Datenbank.DB_F_BACKUP_DIR, "");


            mDatenbank.insert(Datenbank.DB_T_SETTINGS, null, mWerte);

        }
        result.close();
    }


    /*
     * *** Überträgt die Arbeitsplätze und deren Einstellungen
     */
    public void Migrate_Arbeitsplatz() {
        Arbeitsplatz mArbeitsplatz;


        // Arbeitsplätze lesen
        String sql = ("select * from ") +
                Datenbank.DB_T_JOB;

        // Anfrage an datenbank
        Cursor result = connection.rawQuery(sql, null);

        // Resultat der Anfrage auswerten
        if (result.getCount() > 0) {
            // den angelegten Defaultjob löschen
            ASetup.aktJob.delete();

            int mJob = 0;
            int mModule;
            mJobsAlt = new long[result.getCount()];
            mJobsNeu = new long[result.getCount()];
            /*result.moveToFirst();
            do {*/
            while (result.moveToNext()) {
                // id des alten Jobs ablegen
                mJobsAlt[mJob] = result.getLong(result.getColumnIndex(Datenbank.DB_F_ID));

                // neuen arbeitsplatz anlegen
                mArbeitsplatz = new Arbeitsplatz(-1);

                // id des neu angelegten Jobs ablegen
                mJobsNeu[mJob] = mArbeitsplatz.getId();

                // die ID des letzten offnen Arbeitsplatzes in der Datensicherung erkennen
                if (mJobsAlt[mJob] == aktJob) {
                    // den, als letzten offenen Job markierten, Arbeitsplatz wieder als aktuellen markieren
                    ASetup.mPreferenzen.edit().putLong(ISetup.KEY_JOBID, mJobsNeu[mJob]).apply();
                }

                // vorhandene Daten übertragen
                mArbeitsplatz.setName(result.getString(result.getColumnIndex(Datenbank.DB_F_NAME)));
                mArbeitsplatz.setAnschrift("");
                mArbeitsplatz.setEmail(result.getString(result.getColumnIndex(Datenbank.DB_F_EMAIL)));
                mArbeitsplatz.setMailText(result.getString(result.getColumnIndex(Datenbank.DB_F_EMAIL_TEXT)));
                mArbeitsplatz.setSartdatum(new Datum(result.getLong(result.getColumnIndex("start_datum")), Calendar.MONDAY).getTime(), false);
                mArbeitsplatz.setEnddatum(new Datum(result.getLong(result.getColumnIndex("end_datum")), Calendar.MONDAY).getTime());
                mArbeitsplatz.setAnzeige_Zukunft(result.getInt(result.getColumnIndex(Datenbank.DB_F_ANZEIGE_ZUKUNFT)));
                //mArbeitsplatz.setSollstunden(result.getInt(result.getColumnIndex(Einstellungen.DB_F_SOLL_H)));
                mArbeitsplatz.setStundenmodell(result.getInt(result.getColumnIndex("soll_woche")),
                        result.getInt(result.getColumnIndex(Datenbank.DB_F_SOLL_H)));
                mArbeitsplatz.setStartsaldo(result.getInt(result.getColumnIndex(Datenbank.DB_F_STARTSALDO)));
                mArbeitsplatz.setFarbe(result.getInt(result.getColumnIndexOrThrow(Datenbank.DB_F_FARBE)));
                //mArbeitsplatz.setTitel_Spesen(result.getString(result.getColumnIndex(Einstellungen.DB_F_TITEL_SPESEN)));
                //mArbeitsplatz.setTitel_Strecke(result.getString(result.getColumnIndex("t_km")));
                // die Optionen übertragen
                mModule = result.getInt(result.getColumnIndex(Datenbank.DB_F_MODULE));
                mArbeitsplatz.setOption(Arbeitsplatz.OPT_WERT_NOTIZ, ((mModule & Arbeitsplatz.OPT_WERT_NOTIZ) != 0));
                mArbeitsplatz.setOption(Arbeitsplatz.OPT_WERT_SPESEN, ((mModule & Arbeitsplatz.OPT_WERT_SPESEN) != 0));
                mArbeitsplatz.setOption(Arbeitsplatz.OPT_WERT_STRECKE, ((mModule & Arbeitsplatz.OPT_WERT_STRECKE) != 0));
                mArbeitsplatz.setOption(Arbeitsplatz.OPT_WERT_EORT, ((mModule & Arbeitsplatz.OPT_WERT_EORT) != 0));
                mArbeitsplatz.setOption(Arbeitsplatz.OPT_BEZ_UEBERSTUNDEN, ((mModule & Arbeitsplatz.OPT_BEZ_UEBERSTUNDEN) != 0));
                mArbeitsplatz.setOption(Arbeitsplatz.OPT_RESET_SALDO, ((mModule & 128) != 0));// hat sich verschoben
                mArbeitsplatz.setOption(Arbeitsplatz.OPT_PAUSE_BEZAHLT, ((mModule & Arbeitsplatz.OPT_PAUSE_BEZAHLT) != 0));
                mArbeitsplatz.setOption(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL, Anzeige_Dezimal);

                // die Zusatzeinträge erstellen
                if (mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_WERT_SPESEN)) {
                    ZusatzfeldDefinition zd = new ZusatzfeldDefinition(
                            mArbeitsplatz.getId(),
                            result.getString(result.getColumnIndex(Datenbank.DB_F_TITEL_SPESEN)),
                            IZusatzfeld.TYP_ZAHL,
                            ASetup.sWaehrung,
                            IZusatzfeld.NEUTRAL,
                            mArbeitsplatz.getZusatzfeldListe().size(),
                            1,
                            mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL));
                    zd.speichern();
                    mArbeitsplatz.getZusatzfeldListe().add(zd);
                }

                if (mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_WERT_STRECKE)) {
                    ZusatzfeldDefinition zd = new ZusatzfeldDefinition(
                            mArbeitsplatz.getId(),
                            result.getString(result.getColumnIndex("t_km")),
                            IZusatzfeld.TYP_ZAHL,
                            ASetup.sEntfernung,
                            IZusatzfeld.NEUTRAL,
                            mArbeitsplatz.getZusatzfeldListe().size(),
                            1,
                            mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL));
                    zd.speichern();
                    mArbeitsplatz.getZusatzfeldListe().add(zd);
                }

                // Das Notizfeld wird beim anlegen eines neuen Arbeitsplatzes immer mit angelegt
                // wird es im zu importierenden Arbeitsplatz nicht verwendet, muss es gelöscht werden
                if (!mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_WERT_NOTIZ)) {
                    mArbeitsplatz.getZusatzfeldListe().deleteFeld(mArbeitsplatz.getZusatzDefinition(0));
                } /*else {
                    ZusatzfeldDefinition zd = new ZusatzfeldDefinition(
                            mArbeitsplatz.getId(),
                            Einstellungen.res.getString(R.string.notiz),
                            IZusatzfeld.ZUS_TYP_TEXT,
                            "",
                            IZusatzfeld.ZUS_NEUTRAL,
                            mArbeitsplatz.getZusatzfeldListe().size(),
                            IZusatzfeld.MAX_COLUM,
                            mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL));
                    zd.speichern();
                    mArbeitsplatz.getZusatzfeldListe().add(zd);
                }*/


                // neue Einstellungen abfragen oder Hinweis ausgeben
                // Adresse, Monatsbeginn, Urlaub, Tagesstundensätze, Fahrzeittext, Fahrzeit, Teilschicht oder Vollschicht

                // Defaultschichten
                // 1. Schicht
                SchichtDefault mDefaultschicht = mArbeitsplatz.getDefaultSchichten().getAktive(0);
                mDefaultschicht.setVon(result.getInt(result.getColumnIndex("von_1")));
                mDefaultschicht.setBis(result.getInt(result.getColumnIndex("bis_1")));
                mDefaultschicht.setPause(result.getInt(result.getColumnIndex(Datenbank.DB_F_PAUSE)) / 2);
                if (!result.isNull(result.getColumnIndex("schicht_name_1")))
                    mDefaultschicht.setName(result.getString(result.getColumnIndex("schicht_name_1")));

                // 2. Schicht
                mDefaultschicht = mArbeitsplatz.getDefaultSchichten().getAktive(1);
                if (!result.isNull(result.getColumnIndex("schicht_name_2")))
                    mDefaultschicht.setName(result.getString(result.getColumnIndex("schicht_name_2")));
                if (mDefaultschicht.getName().equals("")) {
                    mArbeitsplatz.setIsTeilschicht(false);
                    mArbeitsplatz.getDefaultSchichten().delete(1);
                    mArbeitsplatz.setSchichtzahl(1);
                } else {
                    mDefaultschicht.setVon(result.getInt(result.getColumnIndex("von_2")));
                    mDefaultschicht.setBis(result.getInt(result.getColumnIndex("bis_2")));
                    mDefaultschicht.setPause(result.getInt(result.getColumnIndex(Datenbank.DB_F_PAUSE)) / 2);
                }

                // abwesenheiten(Zeitzusätze)
                String sqlAbwesenheit =
                        "select " + Datenbank.DB_F_WIRKUNG +
                                ", zusatz" +
                                " from zeitzusatz" +
                                " where " + Datenbank.DB_F_JOB +
                                " = " + mJobsAlt[mJob] +
                                " order by zusatz";

                Cursor resultAbwesenheit = connection.rawQuery(sqlAbwesenheit, null);
                int nAbwesenheit;
                while (resultAbwesenheit.moveToNext()) {
                    nAbwesenheit = resultAbwesenheit.getInt(resultAbwesenheit.getColumnIndex("zusatz"));
                    if (nAbwesenheit != 2 && nAbwesenheit != 3) {
                        mArbeitsplatz.getAbwesenheiten().getAktive(nAbwesenheit).setWirkung(
                                resultAbwesenheit.getInt(resultAbwesenheit.getColumnIndex(Datenbank.DB_F_WIRKUNG)));
                    } else if (nAbwesenheit == 3) {
                        mArbeitsplatz.getAbwesenheiten().getAktive(2).setWirkung(
                                resultAbwesenheit.getInt(resultAbwesenheit.getColumnIndex(Datenbank.DB_F_WIRKUNG)));
                    } else {
                        mArbeitsplatz.getAbwesenheiten().getAktive(3).setWirkung(
                                resultAbwesenheit.getInt(resultAbwesenheit.getColumnIndex(Datenbank.DB_F_WIRKUNG)));
                    }
                }
                resultAbwesenheit.close();

                mArbeitsplatz.schreibeJob();
                // der nächste arbeitsplatz
                mJob++;
            }
        }

        result.close();
    }

    /*
     * ***** Überträgt die Einsatzorttabelle und
     *      tauscht dabei die arbeitsplatz Ids aus
     */
    public void Migrate_Eorte() {
        //boolean mStatus = true;
        String sqlEort = "select * from einsatzort where arbeitsplatz = ";
        Einsatzort mEort;

        mEortAlt = new ArrayList<>();
        mEortNeu = new ArrayList<>();

        for (int i = 0; i < mJobsAlt.length; i++) {
            Cursor resultEorte = connection.rawQuery(sqlEort + mJobsAlt[i], null);
            while (resultEorte.moveToNext()) {
                mEortAlt.add(resultEorte.getLong(resultEorte.getColumnIndex(Datenbank.DB_F_ID)));
                // Daten übertragen
                mEort = new Einsatzort(
                        -1,
                        mJobsNeu[i],
                        resultEorte.getString(resultEorte.getColumnIndex(Datenbank.DB_F_NAME)),
                        resultEorte.getInt(resultEorte.getColumnIndex(Datenbank.DB_F_STATUS)), 0,
                        0);
                mEort.save();
                mEortNeu.add(mEort.getId());
            }
            resultEorte.close();
        }
    }

    /*
     * *** Überträgt die Stundendaten und erzeugt Jahre, Monate, Tage und Schichten
     */
    public void Migrate_Zeiten() {
        Arbeitsplatz mJob;
        //arbeitsmonat mMonat;
        Arbeitstag mTag;
        //String sqlMonat = "select id, jahr, monat, auszahlung_ue  from monat where arbeitsplatz = ";
        String sqlMonat = "select * from monat where arbeitsplatz = ";
        String sqlTag = "select * from tag where monat = ";
        Datum mDatum = new Datum(Calendar.MONDAY);
        Abwesenheit mAbwesenheit_1;
        Abwesenheit mAbwesenheit_2;
        int aSchichtzahl;
        int mProzent_1;
        int mProzent_2;
        int mNetto_1;
        int mNetto_2;
        int mIndexAbwesenheit_1;
        int mIndexAbwesenheit_2;
        int mPause = 0;
        String mNotiz;
        ArrayList<Integer> mMarker = new ArrayList<>();
        int mAnzahlTage;
        int mAnzahlTage2Arbeitsschicht;
        Einsatzort eort;
        int mPosNotiz = 0;

        //boolean mStatus = true;

        SQLiteDatabase mDatenbank = ASetup.stundenDB.getWritableDatabase();
        ContentValues werte = new ContentValues();

        for (int i = 0; i < mJobsAlt.length; i++) {
            int mPosSpesen = 0;
            int mPosStrecke = 0;
            mAnzahlTage = 0;
            mAnzahlTage2Arbeitsschicht = 0;

            // arbeitsplatz öffnen
            mJob = new Arbeitsplatz(mJobsNeu[i]);
            // den Aktuellen Job setzen weil viele Funktionen  nur diesen berücksichtigen
            ASetup.aktJob = mJob;
            // Positionen der drei Zusatzwerte in der Zusatzwertliste bestimmen
            if (mJob.isOptionSet(Arbeitsplatz.OPT_WERT_SPESEN)) {
                mPosSpesen = mJob.isOptionSet(Arbeitsplatz.OPT_WERT_NOTIZ) ? 1 : 0;
            }
            if (mJob.isOptionSet(Arbeitsplatz.OPT_WERT_STRECKE)) {
                mPosStrecke = mJob.isOptionSet(Arbeitsplatz.OPT_WERT_NOTIZ) ? 1 : 0;
                mPosStrecke += mPosSpesen;
            }
            // zugehörige Monate aus der alten datenbank lesen
            Cursor resultMonate = connection.rawQuery(sqlMonat + mJobsAlt[i], null);
             /*if (resultMonate.getCount() > 0) {
                 resultMonate.moveToFirst();

                 do {*/
            int auszahlung;
            while (resultMonate.moveToNext()) {
                werte.clear();
                //Monat in der datenbank speichern
                werte.put(Datenbank.DB_F_JOB, mJob.getId());
                werte.put(Datenbank.DB_F_JAHR, resultMonate.getInt(resultMonate.getColumnIndex(Datenbank.DB_F_JAHR)));
                werte.put(Datenbank.DB_F_MONAT, resultMonate.getInt(resultMonate.getColumnIndex(Datenbank.DB_F_MONAT)));
                werte.put(Datenbank.DB_F_IST_H, resultMonate.getInt(resultMonate.getColumnIndex(Datenbank.DB_F_IST_H)));
                werte.put(Datenbank.DB_F_SOLL_H, resultMonate.getInt(resultMonate.getColumnIndex(Datenbank.DB_F_SOLL_H)));
                werte.put(Datenbank.DB_F_SALDO_H, resultMonate.getInt(resultMonate.getColumnIndex(Datenbank.DB_F_SALDO_H)));
                /*
                 * Ab Version 2.06.000 ist die automatishce Auszahlung von Überstunden ab einem Schwellwert hinzu gekommen
                 * In allen Monaten mit keiner Überstundenauszahlung wird die Auszahlung auf -1 gesetzt
                 * Dadurch werden diese Monate beim einschalten der autom. Auszahlung neu berechnet
                 * ohne die schon eingetragenen Auszahlungen zu verändern
                 */
                auszahlung = resultMonate.getInt(resultMonate.getColumnIndex(Datenbank.DB_F_AUSZAHLUNG));
                werte.put(Datenbank.DB_F_AUSZAHLUNG, (auszahlung == 0)? -1 : auszahlung );

                werte.put(Datenbank.DB_F_SOLL_MANUELL, -1);

                if (!mDatenbank.isOpen())
                    mDatenbank = ASetup.stundenDB.getWritableDatabase();

                mDatenbank.insert(Datenbank.DB_T_MONAT, null, werte);

                // zugehörige Tage lesen
                Cursor resultTage = connection.rawQuery(sqlTag + resultMonate.getLong(resultMonate.getColumnIndex("id")), null);
                     /*if (resultTage.getCount() > 0) {
                         resultTage.moveToFirst();
                         do {*/
                while (resultTage.moveToNext()) {
                    mAnzahlTage++;

                    aSchichtzahl = 0;
                    mProzent_1 = 50;
                    mProzent_2 = 50;
                    mNetto_1 = 0;
                    mNetto_2 = 0;

                    // Datum für den Tag zusammen stellen
                    mDatum.set(
                            resultMonate.getInt(resultMonate.getColumnIndex("jahr")),
                            resultMonate.getInt(resultMonate.getColumnIndex("monat")),
                            resultTage.getInt(resultTage.getColumnIndex("tag")));
                    // Tag und Schichten anlegen
                    mTag = new Arbeitstag(
                            mDatum.getCalendar(),
                            mJob,
                            mJob.getSollstundenTag(mDatum),
                            mJob.getSollstundenTagPauschal(mDatum.get(Calendar.YEAR), mDatum.get(Calendar.MONTH)));
                    mIndexAbwesenheit_1 = resultTage.getInt(resultTage.getColumnIndex("zusatz_1"));
                    mIndexAbwesenheit_2 = resultTage.getInt(resultTage.getColumnIndex("zusatz_2"));
                    // in der neuen app sind Ruhetag(a:3, n:2) und Schule (a:2, n:3) vertauscht
                    mIndexAbwesenheit_1 = (mIndexAbwesenheit_1 == 2) ? 3 : (mIndexAbwesenheit_1 == 3) ? 2 : mIndexAbwesenheit_1;
                    mIndexAbwesenheit_2 = (mIndexAbwesenheit_2 == 2) ? 3 : (mIndexAbwesenheit_2 == 3) ? 2 : mIndexAbwesenheit_2;
                    mAbwesenheit_1 = mJob.getAbwesenheiten().getAktive(mIndexAbwesenheit_1);
                    mAbwesenheit_2 = mJob.getAbwesenheiten().getAktive(mIndexAbwesenheit_2);

                    // Anzahl der Arbeitsschichten ermitteln
                    if (mAbwesenheit_1.getWirkung() == Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV) {
                        aSchichtzahl = (mAbwesenheit_1.getKategorie() == Abwesenheit.KAT_ARBEITSZEIT) ? 1 : 0;
                        mNetto_1 = resultTage.getInt(resultTage.getColumnIndex("bis_1")) - resultTage.getInt(resultTage.getColumnIndex("von_1"));
                        if (mNetto_1 < 0)
                            mNetto_1 += 1440; // Arbeitszeit reicht in den neuen Tag
                    }

                    if (mAbwesenheit_2.getWirkung() == Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV) {
                        aSchichtzahl += (mAbwesenheit_2.getKategorie() == Abwesenheit.KAT_ARBEITSZEIT) ? 1 : 0;
                        mNetto_2 = resultTage.getInt(resultTage.getColumnIndex("bis_2")) - resultTage.getInt(resultTage.getColumnIndex("von_2"));
                        if (mNetto_2 < 0)
                            mNetto_2 += 1440; // Arbeitszeit reicht in den neuen Tag
                    }

                    if (aSchichtzahl > 1)
                        mAnzahlTage2Arbeitsschicht++;

                    // Pausen abziehen wenn sie nicht bezahlt werden
                    if (!mJob.isOptionSet(Arbeitsplatz.OPT_PAUSE_BEZAHLT)) {
                        mPause = resultTage.getInt(resultTage.getColumnIndex("pause"));
                        if (mNetto_1 > 0 && mAbwesenheit_1.getKategorie() == Abwesenheit.KAT_ARBEITSZEIT) {
                            mNetto_1 -= mPause / aSchichtzahl;
                            if (mNetto_1 < 0)
                                mNetto_1 = 0;
                        }
                        if (mNetto_2 > 0 && mAbwesenheit_1.getKategorie() == Abwesenheit.KAT_ARBEITSZEIT) {
                            mNetto_2 -= mPause / aSchichtzahl;
                            if (mPause % aSchichtzahl > 0)
                                mNetto_2 -= 1;
                            if (mNetto_2 < 0)
                                mNetto_2 = 0;
                        }
                    }

                    // den Anteil der Schichten in  Prozent ermitteln
                    if (mAbwesenheit_2.getKategorie() == Abwesenheit.KAT_KEINESCHICHT) {
                        mProzent_1 = 100;
                    } else if (mNetto_1 == 0 && mAbwesenheit_2.getWirkung() == Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV) {
                        mProzent_1 = 100 - (Math.round(100f / (((float) mJob.getSollstundenTagPauschal() / mNetto_2))));
                        if (mProzent_1 > 100)
                            mProzent_1 = 100;
                    }


                    if (mAbwesenheit_1.getKategorie() == Abwesenheit.KAT_KEINESCHICHT) {
                        mProzent_2 = 100;
                    } else if (mNetto_2 == 0 && mAbwesenheit_1.getWirkung() == Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV) {
                        mProzent_2 = 100 - (Math.round(100f / (((float) mJob.getSollstundenTagPauschal() / mNetto_1))));
                        if (mProzent_2 > 100)
                            mProzent_2 = 100;
                    }

                    // Anteil jeder Teilschicht in Prozent ermitteln
                    // Werte der Schichten übertragen
                    if (mAbwesenheit_1.getKategorie() != Abwesenheit.KAT_KEINESCHICHT) {
                        mTag.getSchicht(0).setAbwesenheit(mAbwesenheit_1, mProzent_1);
                        if (mAbwesenheit_1.getWirkung() == Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV) {
                            mTag.getSchicht(0).setVon(resultTage.getInt(resultTage.getColumnIndex("von_1")));
                            mTag.getSchicht(0).setBis(resultTage.getInt(resultTage.getColumnIndex("bis_1")));

                            if (mAbwesenheit_1.getKategorie() == Abwesenheit.KAT_ARBEITSZEIT) {
                                mTag.getSchicht(0).setPause(mPause / aSchichtzahl);
                                eort = mJob.getEinsatzortListe().getOrt(get_EortID(resultTage.getLong(resultTage.getColumnIndex("eort_1"))));
                                mTag.getSchicht(0).setEinsatzort(eort);
                                if (eort != null)
                                    eort.add_Verwendung(false);
                                if (mJob.isOptionSet(Arbeitsplatz.OPT_WERT_SPESEN)) {
                                    mTag.getSchicht(0)
                                            .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                            .get(mPosSpesen).setWert(resultTage
                                                    .getFloat(resultTage.getColumnIndex("spesen")) / aSchichtzahl);
                                    mTag.getSchicht(0)
                                            .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                            .get(mPosSpesen).save(false);
                                }
                                if (mJob.isOptionSet(Arbeitsplatz.OPT_WERT_STRECKE)) {
                                    mTag
                                            .getSchicht(0)
                                            .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                            .get(mPosStrecke).
                                            setWert(resultTage.getFloat(resultTage.getColumnIndex("km_privat")) / aSchichtzahl);
                                    mTag.getSchicht(0)
                                            .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                            .get(mPosStrecke).save(false);
                                }
                            }
                        }
                    }

                    if (mJob.getAnzahlSchichtenTag() > 1) {
                        if (mAbwesenheit_2.getKategorie() != Abwesenheit.KAT_KEINESCHICHT) {
                            mTag.getSchicht(1).setAbwesenheit(mAbwesenheit_2, mProzent_2);
                            if (mAbwesenheit_2.getWirkung() == Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV) {
                                mTag.getSchicht(1).setVon(resultTage.getInt(resultTage.getColumnIndex("von_2")));
                                mTag.getSchicht(1).setBis(resultTage.getInt(resultTage.getColumnIndex("bis_2")));

                                if (mAbwesenheit_2.getKategorie() == Abwesenheit.KAT_ARBEITSZEIT) {
                                    mTag.getSchicht(1).setPause(mPause / aSchichtzahl);
                                    eort = mJob.getEinsatzortListe().getOrt(get_EortID(resultTage.getLong(resultTage.getColumnIndex("eort_2"))));
                                    mTag.getSchicht(1).setEinsatzort(eort);
                                    if (eort != null)
                                        eort.add_Verwendung(false);
                                    if (mPause % aSchichtzahl > 0)
                                        mTag.getSchicht(1).setPause(mTag.getSchicht(1).getPause() + 1);
                                    if (mJob.isOptionSet(Arbeitsplatz.OPT_WERT_SPESEN)) {
                                        mTag
                                                .getSchicht(1)
                                                .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                                .get(mPosSpesen)
                                                .setWert(resultTage.getFloat(resultTage.getColumnIndex("spesen")) / aSchichtzahl);
                                        mTag.getSchicht(1)
                                                .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                                .get(mPosSpesen).save(false);
                                    }
                                    if (mJob.isOptionSet(Arbeitsplatz.OPT_WERT_STRECKE)) {
                                        mTag
                                                .getSchicht(1)
                                                .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                                .get(mPosStrecke)
                                                .setWert(resultTage.getFloat(resultTage.getColumnIndex("km_privat")) / aSchichtzahl);
                                        mTag.getSchicht(1)
                                                .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                                .get(mPosStrecke).save(false);
                                    }
                                }
                            }
                        }

                        // leere zweite Schicht löschen
                        if (mAbwesenheit_2.getKategorie() == Abwesenheit.KAT_KEINESCHICHT) {
                            mTag.loescheSchicht(1);
                        }
                    }

                    // leere erste Schicht löschen wenn es nicht die einzige ist
                    if (mTag.getSchichtzahl() > 1 &&
                            mAbwesenheit_1.getKategorie() == Abwesenheit.KAT_KEINESCHICHT) {
                        mTag.loescheSchicht(0);
                    }

                    // Die Tagesnotiz aufteilen und den beiden Schichten zuordnen
                    if (mJob.isOptionSet(Arbeitsplatz.OPT_WERT_NOTIZ)) {
                        //int position = mJob.getZusatzfeldListe().size()-1;
                        // der komplette Notiz String
                        mNotiz = resultTage.getString(resultTage.getColumnIndex(Datenbank.DB_F_BEMERKUNG));
                        // wenn es zwei Schichten gibt alle Zeilenumbrüche finden
                        // if(mJob.getAnzahlSchichtenTag() > 1)
                        if (mTag.getSchichtzahl() > 1) {
                            mMarker.clear();
                            mMarker.add(mNotiz.indexOf("\n"));
                            int mPos = mMarker.get(mMarker.size() - 1);
                            while (mPos >= 0) {
                                mPos++;
                                mPos = mNotiz.indexOf("\n", mPos);
                                mMarker.add(mPos);
                            }
                            // Wenn es mindestens einen Umbruch gibt,
                            // dann den Mittleren als Trenner für die beiden Schichten finden
                            // und in die Schichten eintragen
                            if (mMarker.size() >= 2) {
                                int mPosition = mMarker.get((mMarker.size() / 2) - 1) + 1;
                                mTag
                                        .getSchicht(0)
                                        .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                        .get(mPosNotiz)
                                        .setWert(mNotiz.substring(0, mPosition));
                                mTag
                                        .getSchicht(0)
                                        .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                        .get(mPosNotiz).save(false);
                                mTag
                                        .getSchicht(1)
                                        .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                        .get(mPosNotiz)
                                        .setWert(mNotiz.substring(mPosition));
                                mTag
                                        .getSchicht(1)
                                        .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                        .get(mPosNotiz).save(false);
                            } else {
                                mTag
                                        .getSchicht(0)
                                        .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                        .get(mPosNotiz)
                                        .setWert(mNotiz);
                                mTag
                                        .getSchicht(0)
                                        .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                        .get(mPosNotiz).save(false);
                            }
                        } else {
                            mTag
                                    .getSchicht(0)
                                    .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                    .get(mPosNotiz)
                                    .setWert(mNotiz);
                            mTag
                                    .getSchicht(0)
                                    .getZusatzfelder(IZusatzfeld.TEXT_VOLL)
                                    .get(mPosNotiz).save(false);
                        }
                    }

                }
                resultTage.close();
            }
            resultMonate.close();

            // Die Einsatzortliste speichern
            mJob.getEinsatzortListe().save();

            /*
             * entscheiden ob ein Teilschichtmodell
             * oder Wechselschichtmodell besser geeignet ist
             * wenn mehr als 85% der Tage nur eine arbeitsschicht enthält
             * dann Wechselschichten
             */
            if (mAnzahlTage2Arbeitsschicht > 0) {
                if (((float) mAnzahlTage / mAnzahlTage2Arbeitsschicht) <= (100f / 85f)) {
                    mJob.setIsTeilschicht(false);
                    mJob.schreibeJob();
                }
            } else {
                mJob.setIsTeilschicht(false);
                mJob.schreibeJob();
            }
        }
    }


    /*
     * ID des Einsatzortes in der neuen datenbank zurück geben
     */
    private long get_EortID(long alteID) {
        long mID = 0;

        if (alteID > 0) {
            for (int i = 0; i < mEortAlt.size(); i++) {
                if (alteID == mEortAlt.get(i)) {
                    mID = mEortNeu.get(i);
                    break;
                }
            }
        }

        return mID;
    }

    /*
     **** alte datenbank löschen
     */
    public void Delete_AlteDB() {
        mContext.deleteDatabase(mContext.getString(R.string.dbName_toMigrate));
    }
}