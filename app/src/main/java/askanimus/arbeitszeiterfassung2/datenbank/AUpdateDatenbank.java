/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.datenbank;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;

import androidx.core.content.res.ResourcesCompat;
import java.util.Calendar;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.MainActivity;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsmonat.Arbeitsmonat;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

public abstract class AUpdateDatenbank {

    public static void updateDatenbank(Activity context, Arbeitsplatz arbeitsplatz) {

        // Fortschritsdialog öffnen
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setIndeterminate(true);
        dialog.setIndeterminateDrawable(
                ResourcesCompat.getDrawable(
                        context.getResources(),
                        R.drawable.progress_dialog_anim,
                        context.getTheme()));
        dialog.setMessage(context.getString(R.string.progress_dbupdate));
        dialog.setCancelable(false);
        dialog.show();
        Handler mHandler = new Handler();
        new Thread(() -> {

            // Aufruf der Updateroutine im neuen Thread
            dbUpdate(arbeitsplatz);

            mHandler.post(() -> {
                // Progressdialog ausblenden
                if(dialog.isShowing()) {
                    dialog.dismiss();
                }

                // Haupt Activity starten
                Intent mMainIntent = new Intent();
                mMainIntent.setClass(context, MainActivity.class);
                //mMainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                mMainIntent.setAction(ISetup.APP_RESET);
                context.startActivity(mMainIntent);

                // aufgerufende Activity stoppen
                context.finish();
            });
        }).start();

    }

    private static void dbUpdate(Arbeitsplatz arbeitsplatz){
       if(arbeitsplatz != null) {
           arbeitsplatz.schreibeJob();
           int vSaldo = arbeitsplatz.getStartsaldo();

           // den ersten Monat der Aufzeichnungen ermitteln
           Datum mKalStart = arbeitsplatz.getAbrechnungsmonat(arbeitsplatz.getStartDatum());
           Datum mKal = new Datum(mKalStart);
           // den letzten Monat der Aufzeichnungen berechnen
           ASetup.letzterAnzeigeTag.set(ASetup.getLetzterAnzeigeTag(arbeitsplatz).getTime());
           // Monate aktuallisieren indem sie einmal aufgerufen werden
           // Während des öffnens des Monats wird der Saldo des Vormonats und der Saldo aktuallisiert
           while (!mKal.liegtNach(ASetup.letzterAnzeigeTag)) {
               Arbeitsmonat mMonat = new Arbeitsmonat(arbeitsplatz,
                       mKal.get(Calendar.YEAR),
                       mKal.get(Calendar.MONTH),
                       true,
                       false);
               if (mMonat.getId() > 0) {
                   mMonat.updateSollStunden();
                   mMonat.setSaldoVormonat(vSaldo);
                   mMonat.updateSaldo(true);
                   vSaldo = mMonat.getSaldo();
               }
               mKal.add(Calendar.MONTH, 1);
           }
       }
    }
}
