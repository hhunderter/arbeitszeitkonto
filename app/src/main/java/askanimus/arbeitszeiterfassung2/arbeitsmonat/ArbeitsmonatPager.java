/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsmonat;

import android.app.Activity;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.view.OneShotPreDrawListener;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;

import askanimus.arbeitszeiterfassung2.MainActivity;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.ArbeitsplatzListAdapter;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;


/**
 * @author askanimus@gmail.com on 26.08.15.
 */
public class ArbeitsmonatPager extends Fragment{
    private static final String ARG_DATUM = "datum";

    private ViewPager2 mViewPager;
    private TextView tMonat;

    private Context mContext;


    /*
     * Neue Instanz anlegen
     */
    public static ArbeitsmonatPager newInstance(long datum) {
        ArbeitsmonatPager fragment = new ArbeitsmonatPager();
        Bundle bundle = new Bundle();

        bundle.putLong(ARG_DATUM, datum);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mContext = getContext();

        return inflater.inflate(R.layout.fragment_pager, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ASetup.init(mContext, this::resume);
    }

    private void resume() {
        final Datum kDatum;

        //if (savedInstanceState == null) {
        Bundle mArgs = getArguments();

        if (mArgs != null) {
            kDatum = new Datum(mArgs.getLong(ARG_DATUM), ASetup.aktJob.getWochenbeginn());
        } else {
            kDatum = new Datum(ASetup.aktDatum.getTime(), ASetup.aktJob.getWochenbeginn());
        }
        if (kDatum.get(Calendar.DAY_OF_MONTH) < ASetup.aktJob.getMonatsbeginn())
            kDatum.add(Calendar.MONTH, -1);
        kDatum.setTag(ASetup.aktJob.getMonatsbeginn());

        // Werte der Kopfzeile eintragen
        View mInhalt = getView();

        if (mInhalt != null) {
            // Werte der Kopfzeile eintragen
            tMonat = mInhalt.findViewById(R.id.P_wert_monat);
            LinearLayout bKopf = mInhalt.findViewById(R.id.P_box_kopf);
            tMonat.setTextColor(ASetup.aktJob.getFarbe_Schrift_Titel());

            bKopf.setBackgroundColor(ASetup.aktJob.getFarbe());


            // Erzeugt den Adapter der für jede Seite entsprechnd der Seitennummer
            // den dazu gehörenden Tag als Fragment einbindet.
            ArbeitsmonatPagerAdapter mPagerAdapter = new ArbeitsmonatPagerAdapter(this);

            // Der View-Pager
            mViewPager = mInhalt.findViewById(R.id.pager);
            mViewPager.setAdapter(mPagerAdapter);
            mViewPager.setOffscreenPageLimit(5);

            mViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    Datum mKal = ASetup.aktJob.getAbrechnungsmonat(ASetup.aktJob.getStartDatum());
                    mKal.setTag(ASetup.aktJob.getMonatsbeginn());
                    mKal.add(Calendar.MONTH, position);
                    setMonat(mKal);
                    kDatum.set(mKal.getTime());
                }
            });

            // vom Beginn der Aufzeichnunge bis zum anzuzeigenden Datum scrollen
            Datum mKal = ASetup.aktJob.getAbrechnungsmonat(ASetup.aktJob.getStartDatum());
            mKal.setTag(ASetup.aktJob.getMonatsbeginn());
            int mDiff = mKal.monateBis(kDatum);
            OneShotPreDrawListener.add(mViewPager,
                    () -> mViewPager.setCurrentItem(mDiff,false));

            // Die Arbeitsplatzliste
            AppCompatSpinner sJobs = mInhalt.findViewById(R.id.P_wert_job);

            // der Adapter zum wechseln des Arbeitsplatzes
            final ArbeitsplatzListAdapter jobListeAdapter = new ArbeitsplatzListAdapter(mContext);
            sJobs.setAdapter(jobListeAdapter);
            sJobs.setSelection(0);
            sJobs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    long ArbeitsplatzID = jobListeAdapter.getItemId(i);
                    if (ArbeitsplatzID != ASetup.aktJob.getId()) {

                        Activity mActivity = getActivity();
                        if (mActivity != null) {
                            SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
                            mEdit.putLong(ISetup.KEY_JOBID, ArbeitsplatzID).apply();
                            Intent mainIntent = new Intent();
                            mainIntent.setClass(mActivity, MainActivity.class);
                            mainIntent.putExtra(ISetup.KEY_JOBID, ArbeitsplatzID);
                            mainIntent.putExtra(ISetup.KEY_ANZEIGE_VIEW, ISetup.VIEW_MONAT);
                            mainIntent.putExtra(ISetup.KEY_ANZEIGE_DATUM, kDatum.getTimeInMillis());
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(mainIntent);
                            mActivity.finish();
                        }

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            // Anzuzeigenden Monat errechnen
            setMonat(kDatum);
        }
    }
    
    private void setMonat(Datum datum){
        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
        // Datum der letzten Ansicht speichern
        mEdit.putLong(ISetup.KEY_ANZEIGE_DATUM, datum.getTimeInMillis()).apply();

        tMonat.setText(datum.getString_Monat_Jahr(ASetup.aktJob.getMonatsbeginn(), false));
    }


    /**
     * A {@link FragmentStateAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public static class ArbeitsmonatPagerAdapter
            extends FragmentStateAdapter
            /*implements ArbeitsmonatFragment.ArbeitsmonatFragmentCallbacks*/{
        int mSeiten;

        ArbeitsmonatPagerAdapter(Fragment f) {
            super(f);
            Datum mDatumStart = ASetup.aktJob.getAbrechnungsmonat(ASetup.aktJob.getStartDatum());
            Datum mDatumEnd = ASetup.aktJob.getAbrechnungsmonat(ASetup.letzterAnzeigeTag);

            mSeiten = mDatumStart.monateBis(mDatumEnd) +1;
            if (mSeiten < 1) mSeiten = 1;
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            // getItem is called to instantiate the fragment for the given page.
            Datum mKal = ASetup.aktJob.getAbrechnungsmonat(ASetup.aktJob.getStartDatum());
            mKal.add(Calendar.MONTH, position);

            return ArbeitsmonatFragment.newInstance(mKal/*, this*/);
        }

        @Override
        public int getItemCount() {
            return mSeiten;
        }

        /*@Override
        public void onChangeMonat() {
            requestBackup();
        }*/
    }


    // Backup im Google Konto anfordern
    /*private void requestBackup() {
        BackupManager bm = new BackupManager(mContext);
        try {
            bm.dataChanged();
        } catch (NullPointerException ne){
            ne.printStackTrace();
        }
    }*/
}
