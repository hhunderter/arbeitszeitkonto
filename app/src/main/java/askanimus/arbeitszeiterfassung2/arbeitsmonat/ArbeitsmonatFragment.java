/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsmonat;


import android.annotation.SuppressLint;
import android.app.backup.BackupManager;
import android.content.Context;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.numberpicker.NumberPickerBuilder;
import com.codetroopers.betterpickers.numberpicker.NumberPickerDialogFragment;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;

import askanimus.arbeitszeiterfassung2.MinutenInterpretationDialog;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitstag.ArbeitstagExpandListAdapter;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ISetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzWertListe;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzWertViewAdapter;


/**
 * @author askanimus@gmail.com on 19.08.15.
 */
public class ArbeitsmonatFragment extends Fragment
        implements View.OnClickListener,
        View.OnLongClickListener,
        NumberPickerDialogFragment.NumberPickerDialogHandlerV2,
        FragmentDialogSoll.EditSollDialogListener {

    private static final String ARG_JAHR = "jahr";
    private static final String ARG_MONAT = "monat";
    private static final String ARG_TAG = "tag";
    private static final int EDITSOLLDIALOG_TAG = 999;

    private TextView tSoll;
    private TextView titelIst;
    private TextView tIst;
    private TextView tDiff;
    private TextView tSaldoVormonat;
    private LinearLayout cSaldoHeute;
    private TextView tSaldoHeute;
    private TextView tSaldo;
    private TextView wUeber;
    private ImageView iSoll;
    private ExpandableListView lTage;
    private LinearLayout cStundenlohn;
    private TextView tStundenlohn;

    private LinearLayout cErgebnis;
    ZusatzWertViewAdapter mAdapter_kopf;
    private RecyclerView lZusatzwerte;

    private ImageView iCompact;
    private boolean isCompact;

    private Context mContext;

    // der arbeitsmonat
    private Arbeitsmonat mMonat;

    private boolean isAufzeichnungEnde;
    // Callback wenn sich etwas am Monat geändert hat
    //private static ArbeitsmonatFragmentCallbacks mCallback;


    /*
     * Neue Instanz anlegen
     */
    public static ArbeitsmonatFragment newInstance(Datum cal/*, ArbeitsmonatFragmentCallbacks cb*/) {
        //mCallback = cb;
        ArbeitsmonatFragment fragment = new ArbeitsmonatFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_JAHR, cal.get(Calendar.YEAR));
        args.putInt(ARG_MONAT, cal.get(Calendar.MONTH));
        args.putInt(ARG_TAG, cal.get(Calendar.DAY_OF_MONTH));
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mContext = getContext();
        View view = inflater.inflate(R.layout.fragment_arbeitsmonat, container, false);
        lZusatzwerte = view.findViewById(R.id.M_liste_zusatzwerte);

        mAdapter_kopf =
                new ZusatzWertViewAdapter(ZusatzWertViewAdapter.VIEW_KOPF);
        GridLayoutManager layoutManger =
        new GridLayoutManager(
                mContext,
                1 );
        lZusatzwerte.setLayoutManager(layoutManger);
        lZusatzwerte.setAdapter(mAdapter_kopf);
        return view;
    }


    //@SuppressLint("UseCompatLoadingForDrawables")
    @Override
    public void onResume() {
        super.onResume();

        ASetup.init(mContext, this::resume);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void resume() {
        Bundle args = getArguments();
        Datum mKalender;
        if (args != null) {
            mKalender = new Datum(
                    args.getInt(ARG_JAHR),
                    args.getInt(ARG_MONAT),
                    args.getInt(ARG_TAG),
                    ASetup.aktJob.getWochenbeginn());
        } else {
            mKalender = new Datum(ASetup.aktDatum.getCalendar(), ASetup.aktJob.getWochenbeginn());
        }

        // Aufzeichnungsende erreicht?
        isAufzeichnungEnde = ASetup.aktJob.isEndeAufzeichnung(ASetup.aktDatum);

        // Standartelemente finden
        View mInhalt = getView();

        if (mInhalt != null) {
            // Sonstige Werte
            RelativeLayout cDatum = mInhalt.findViewById(R.id.M_box_datum);
            cErgebnis = mInhalt.findViewById(R.id.M_box_ergebnis);
            TextView tDatum = mInhalt.findViewById(R.id.M_wert_datum);
            tSoll = mInhalt.findViewById(R.id.M_wert_soll);
            iSoll = mInhalt.findViewById(R.id.M_button_soll);
            titelIst = mInhalt.findViewById(R.id.M_titel_ist);
            tIst = mInhalt.findViewById(R.id.M_wert_ist);
            tDiff = mInhalt.findViewById(R.id.M_wert_diff);
            tSaldoVormonat = mInhalt.findViewById(R.id.M_wert_saldo_vm);
            cSaldoHeute = mInhalt.findViewById(R.id.M_box_saldo_akt);
            tSaldoHeute = mInhalt.findViewById(R.id.M_wert_saldo_akt);
            tSaldo = mInhalt.findViewById(R.id.M_wert_saldo);
            TextView tUeber = mInhalt.findViewById(R.id.M_titel_ausbezahlt);
            wUeber = mInhalt.findViewById(R.id.M_wert_ausbezahlt);

            lTage = mInhalt.findViewById(R.id.M_liste_tage);

            // Sonstige Werte
            cStundenlohn = mInhalt.findViewById(R.id.M_box_stundenlohn);
            tStundenlohn = mInhalt.findViewById(R.id.M_wert_stundenlohn);

            iCompact = mInhalt.findViewById(R.id.M_icon_fold);

            // Farben des Kopfes setzen
            cDatum.setBackgroundColor(ASetup.aktJob.getFarbe_Tag());

            // Icon zum ein- und ausklappen der Zusammenfassung anpassen
            if (ASetup.res.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                isCompact = ASetup.mPreferenzen.getBoolean(
                        ISetup.KEY_ANZEIGE_MONAT_COMPACT, false);
                iCompact.setImageDrawable(
                        VectorDrawableCompat.create(
                                ASetup.res,
                                isCompact ?
                                        R.drawable.arrow_down :
                                        R.drawable.arrow_up,
                                mContext.getTheme())
                );
                iCompact.setOnClickListener(this);
                cDatum.setOnClickListener(this);
                cErgebnis.setVisibility(isCompact ? View.GONE : View.VISIBLE);
            } else
                iCompact.setVisibility(View.GONE);

            // die Daten eines Monats
            mMonat = new Arbeitsmonat(
                    ASetup.aktJob,
                    mKalender.get(Calendar.YEAR), mKalender.get(Calendar.MONTH),
                    true,
                    true
            );

            if (mMonat.getTagZahl() > 0) {
                tDatum.setText(
                        mMonat.getTagInListe(0).getKalender().getString_Datum_Bereich(
                                mContext,
                                0,
                                mMonat.getTagZahl() - 1,
                                Calendar.DAY_OF_MONTH
                        )
                );

                // Saldo heute anzeigen oder ausblenden
                if(ASetup.aktJob.getAbrechnungsmonat(mMonat.getDatumErsterTag()).istGleich(
                        ASetup.aktJob.getAbrechnungsmonat(ASetup.aktDatum),
                        Calendar.MONTH)
                ){
                   cSaldoHeute.setVisibility(View.VISIBLE);
                } else {
                   cSaldoHeute.setVisibility(View.GONE);
                }

                // Tagesliste erzeugen
                // Der Adapter der Tagesliste
                ArbeitstagExpandListAdapter aTage = new ArbeitstagExpandListAdapter(
                        mContext,
                        mMonat.getTagListe(),
                        (ArbeitstagExpandListAdapter.ArbeitstagListeCallbacks) getActivity()
                );
                lTage.setAdapter(aTage);

                // handler für das öffnen der Kindelemente, nur eins soll zur gleichen Zeit offen sein
                lTage.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                    int previousGroup = -1;

                    @Override
                    public void onGroupExpand(int groupPosition) {
                        if (groupPosition != previousGroup)
                            lTage.collapseGroup(previousGroup);
                        previousGroup = groupPosition;
                    }
                });


                // an die richtige Stelle scrollen, damit der aktuelle Tag sichtbar wird
                int monatView = mKalender.get(Calendar.YEAR) * 12 + mKalender.get(Calendar.MONTH);
                int monatAkt;
                if (ASetup.aktDatum.get(Calendar.DAY_OF_MONTH) < ASetup.aktJob.getMonatsbeginn()) {
                    int aJahr;
                    int aMonat = ASetup.aktDatum.get(Calendar.MONTH) - 1;
                    if (aMonat == 0) {
                        aMonat = 12;
                        aJahr = ASetup.aktDatum.get(Calendar.YEAR) - 1;
                    } else
                        aJahr = ASetup.aktDatum.get(Calendar.YEAR);
                    monatAkt = aJahr * 12 + aMonat;

                } else
                    monatAkt = ASetup.aktDatum.get(Calendar.YEAR) * 12 + ASetup.aktDatum.get(Calendar.MONTH);

                boolean umgekehrt_sort = ASetup.mPreferenzen.getBoolean(ISetup.KEY_ANZEIGE_UMG_SORT, false);

                if (monatView < monatAkt || ASetup.aktJob.isEndeAufzeichnung(ASetup.aktDatum)) {
                    // in vergangenen Monaten ansd Monatsende scrollen
                    lTage.setSelection(umgekehrt_sort ? 0 : lTage.getCount() - 1);
                } else if (monatView == monatAkt) {
                    // im aktuellen Monat zu8m aktuellen Tag scrollen und dessen Informationen ausklappen
                    int pos;
                    int posMax = lTage.getCount() - 1;

                    if (ASetup.aktJob.isAnzeigeZukunft()) {
                        pos = mMonat.getTagPosition(ASetup.aktDatum.get(Calendar.DAY_OF_MONTH));
                        if (umgekehrt_sort) {
                            pos = posMax - pos;
                        }
                    } else {
                        pos = umgekehrt_sort ? 0 : posMax;
                    }

                    if (pos < 0) {
                        pos = 0;
                    } else if (pos > posMax) {
                        pos = posMax;
                    }


                    try {
                        // zum aktuellen Tag scrollen
                        lTage.setSelection(pos);
                        // den aktuellen Tag aufklappen
                        if (ASetup.mPreferenzen.getBoolean(ISetup.KEY_ANZEIGE_AKTTAG, true)) {
                            lTage.expandGroup(pos);
                        }
                    } catch (RuntimeException re) {
                        re.printStackTrace();
                    }
                } else {
                    // in zukünftigen Monaten zum Listenanfang scrollen
                    lTage.setSelection(umgekehrt_sort ? lTage.getCount() - 1 : 0);
                }


                // Klickhandler für manuelle Sollstunden und bezahlte Überstunden
                if(isAufzeichnungEnde){
                    iSoll.setVisibility(View.GONE);
                } else {
                    iSoll.setOnClickListener(this);
                }

                // die bezahlten Überstunden bzw. bezahlten Stunden
                tUeber.setText(
                        (mMonat.getSollBrutto() == 0) ?
                                R.string.stunden_ausbezahlt : R.string.ueberstunden_ausbezahlt
                );

                if (isAufzeichnungEnde) {
                    wUeber.setBackground(null);
                } else {
                    wUeber.setOnLongClickListener(this);
                    wUeber.setOnClickListener(this);
                }

                // die Zusatzwerte anzeigen
                ZusatzWertListe mZusatzwerteSumme = mMonat.getZusatzeintragSummenListe();
                if (mZusatzwerteSumme.size() > 0/*ASetup.isZusatzfelder*/) {
                    lZusatzwerte.setVisibility(View.VISIBLE);
                    mAdapter_kopf.setUp(
                            mZusatzwerteSumme.getListe(),
                            null
                    );
                    mAdapter_kopf.notifyDataSetChanged();
                } else {
                    lZusatzwerte.setVisibility(View.GONE);
                }

                updateKopf();
            }
        }
    }

    private void updateKopf() {
        Uhrzeit mZeit = new Uhrzeit(mMonat.getSollNetto());

        tSoll.setText(
                mZeit.getStundenString(
                        true,
                        ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
                )
        );

        if (mMonat.isSollManuell()) {
            iSoll.setImageResource(R.drawable.ic_action_undo);
            tSoll.setTextColor(ASetup.res.getColor(android.R.color.holo_purple));
        } else {
            tSoll.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
            iSoll.setImageResource(R.drawable.ic_action_edit);
        }

        mZeit.set(mMonat.getIstNettoMinusUeberstundenpauschale());
        tIst.setText(mZeit.getStundenString(true, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

        if(ASetup.isVerdienst){
            cStundenlohn.setVisibility(View.VISIBLE);
            Handler mHandler = new Handler();
            new Thread(()->{
                float verdienst = mMonat.getVerdienst();
                mHandler.post(() -> {
                    tStundenlohn.setText(ASetup.waehrungformat.format(verdienst));
                });
            }).start();
            //tStundenlohn.setText(ASetup.waehrungformat.format(mMonat.getVerdienst()));
        } else {
            cStundenlohn.setVisibility(View.GONE);
        }

        mZeit.set(mMonat.getDifferenz());
        tDiff.setText(mZeit.getStundenString(true, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        if (mZeit.getAlsMinuten() == 0)
            tDiff.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
        else if (mZeit.getAlsMinuten() < 0)
            tDiff.setTextColor(ASetup.cNegativText);
        else
            tDiff.setTextColor(ASetup.cPositivText);

        mZeit.set(mMonat.getSaldoVormonat());
        tSaldoVormonat.setText(mZeit.getStundenString(true, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        if (mZeit.getAlsMinuten() == 0)
            tSaldoVormonat.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
        else if (mZeit.getAlsMinuten() < 0)
            tSaldoVormonat.setTextColor(ASetup.cNegativText);
        else
            tSaldoVormonat.setTextColor(ASetup.cPositivText);

        mZeit.set(mMonat.getSaldo());
        tSaldo.setText(mZeit.getStundenString(true, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        if (mZeit.getAlsMinuten() == 0)
            tSaldo.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
        else if (mZeit.getAlsMinuten() < 0)
            tSaldo.setTextColor(ASetup.cNegativText);
        else
            tSaldo.setTextColor(ASetup.cPositivText);


        if(cSaldoHeute.getVisibility() == View.VISIBLE) {
            mZeit.set(mMonat.getSaldo_Aktuell(ASetup.aktDatum.get(Calendar.DAY_OF_MONTH) /*mTag*/));
            if(ASetup.aktJob.getModell() == Arbeitsplatz.Soll_Woche_rollend)
               tSaldoHeute.setText(
                       String.format(
                               "≈ %1s",
                               mZeit.getStundenString(
                                       true,
                                       ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
                               )
                       )
               );
            else
                tSaldoHeute.setText(mZeit.getStundenString(true, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

            if (mZeit.getAlsMinuten() == 0)
                tSaldoHeute.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
            else if (mZeit.getAlsMinuten() < 0)
                tSaldoHeute.setTextColor(ASetup.cNegativText);
            else
                tSaldoHeute.setTextColor(ASetup.cPositivText);
        }

        mZeit.set(mMonat.getAuszahlung());
        if (mZeit.getAlsMinuten() > 0)
            wUeber.setText(mZeit.getStundenString(true, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        else
            wUeber.setText("");

        // Wenn Überstunden anteilig pauschal mit Lohn abgegolten werden
        int uep = mMonat.getIstNetto() - mMonat.getIstNettoMinusUeberstundenpauschale();
        if(uep > 0){
            titelIst.setText(getString(
                    R.string.ist_ue_p,
                    new Uhrzeit(uep).getStundenString(
                            true,
                            ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
                    )
            ));
        }
    }

    @Override
    public void onDialogNumberSet(final int reference, final BigInteger number, final double decimal, boolean isNegative, BigDecimal fullNumber) {
        new MinutenInterpretationDialog(
                mContext,
                ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL),
                /*number,*/
                decimal,
                fullNumber,
                z -> {
                    if (reference == R.id.M_wert_ausbezahlt) {//Uhrzeit mAuszahlung = new Uhrzeit((float) fullNumber);
                        if (mMonat.setAuszahlung(z.getAlsMinuten())) {
                            updateKopf();
                            requestBackup();
                            //mCallback.onChangeMonat();
                        }
                    }
                }

        );
    }



    @Override
    public void onClick(View v) {
        FragmentManager fManager;

        try {
            fManager = getParentFragmentManager();
            int id = v.getId();
            if (id == R.id.M_button_soll) {
                if (mMonat.isSollManuell()) {
                    if (mMonat.setSollstundenManuell(-1)) {
                        updateKopf();
                        ((BaseAdapter)lTage.getAdapter()).notifyDataSetChanged();
                        requestBackup();
                        //mCallback.onChangeMonat();
                    }
                } else {
                    // erweiterter Eingabedialog
                    FragmentDialogSoll mDialog = new FragmentDialogSoll();
                    mDialog.setup(mMonat, ASetup.aktJob, this, EDITSOLLDIALOG_TAG);
                    mDialog.show(fManager, "EditSollDialog");
                }
            } else if (id == R.id.M_wert_ausbezahlt) {
                if (mMonat.getSaldo() > 0 || mMonat.getAuszahlung() > 0) {
                    Uhrzeit mMax;
                    if (mMonat.getJahr() == ASetup.aktDatum.get(Calendar.YEAR) &&
                            mMonat.getMonat() == ASetup.aktDatum.get(Calendar.MONTH)) {
                        mMax = new Uhrzeit(mMonat.getSaldo_Aktuell(ASetup.aktDatum.get(Calendar.DAY_OF_MONTH)));
                    } else {
                        mMax = new Uhrzeit(mMonat.getSaldo());
                    }
                    mMax.set(mMax.getAlsMinuten() + mMonat.getAuszahlung());
                    if (mMax.getAlsMinuten() < 0)
                        mMax.set(0);
                    NumberPickerBuilder AuszahlungPicker = new NumberPickerBuilder()
                            .setFragmentManager(fManager)
                            .setStyleResId(ASetup.themePicker)
                            .setLabelText(getString(R.string.k_stunde))
                            .setMinNumber(BigDecimal.valueOf(0))
                            .setMaxNumber(BigDecimal.valueOf(Math.ceil(mMax.getAlsDezimalZeit())))
                            .setPlusMinusVisibility(View.INVISIBLE)
                            .setDecimalVisibility(View.VISIBLE)
                            .setTargetFragment(this)
                            .setReference(R.id.M_wert_ausbezahlt);
                    AuszahlungPicker.show();
                } else {
                    Toast.makeText(this.getActivity(), getString(R.string.ueberstunden_keine), Toast.LENGTH_LONG).show();
                }
            } else if (id == R.id.M_box_datum || id == R.id.M_icon_fold) {
                SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
                isCompact = !isCompact;
                mEdit.putBoolean(ISetup.KEY_ANZEIGE_MONAT_COMPACT, isCompact);
                mEdit.apply();
                cErgebnis.setVisibility(isCompact ? View.GONE : View.VISIBLE);
                iCompact.setImageDrawable(
                        VectorDrawableCompat.create(
                                ASetup.res,
                                isCompact ?
                                        R.drawable.arrow_down :
                                        R.drawable.arrow_up,
                                mContext.getTheme())
                );
            }
        } catch (IllegalStateException ignored){
        }
    }

    @Override
    public boolean onLongClick(View v) {
        FragmentManager fManager;

        try {
            fManager = getParentFragmentManager();
            if (v.getId() == R.id.M_wert_ausbezahlt) {
                NumberPickerBuilder AuszahlungPicker = new NumberPickerBuilder()
                        .setFragmentManager(fManager)
                        .setStyleResId(ASetup.themePicker)
                        .setLabelText(getString(R.string.k_stunde))
                        .setMinNumber(BigDecimal.valueOf(0))
                        .setPlusMinusVisibility(View.INVISIBLE)
                        .setDecimalVisibility(View.VISIBLE)
                        .setTargetFragment(this)
                        .setReference(R.id.M_wert_ausbezahlt);
                AuszahlungPicker.show();
                return true;
            } else
                return false;
        } catch (IllegalStateException ignored){
            return false;
        }
    }

    @Override
    public void onEditSollPositiveClick(int dialog_tag, Uhrzeit wert) {
        if(mMonat.setSollstundenManuell(wert.getAlsMinuten())) {
            updateKopf();
            requestBackup();
            ((BaseAdapter)lTage.getAdapter()).notifyDataSetChanged();
            //mCallback.onChangeMonat();
        }
    }

    @Override
    public void onEditSollNegativeClick(int dialog_tag) {

    }

    // Backup im Google Konto anfordern
    private void requestBackup() {
        BackupManager bm = new BackupManager(mContext);
        try {
            bm.dataChanged();
        } catch (NullPointerException ne){
            ne.printStackTrace();
        }
    }

    /*
         * Callback Interfaces
         */
    //public interface ArbeitsmonatFragmentCallbacks {
        /*
         * Aufgerufen wenn sich Sollstunden oder
         * Ausbezahlte Überstunden geändert haben
         */
      //  void onChangeMonat();
    //}
}
