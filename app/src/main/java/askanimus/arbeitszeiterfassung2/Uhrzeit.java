/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import askanimus.arbeitszeiterfassung2.setup.ASetup;

/**
 * @author askanimus@gmail.com on 19.01.14.
 *
 * Eine einfache Uhrzeit-Klasse
 * Die Uhrzeit wird als Integer Wert gespeichert
 */

// Uhrzeit-Klasse
public class Uhrzeit {
    private int t;
    //DateFormat zeitFormat = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
    //DecimalFormat zahlenFormat = new DecimalFormat("###,##0.00");

    /*
     * Konstruktoren
     */

    /**
     * Mit aktueller Uhrzeit initialisieren
     */
    public Uhrzeit() {
        set();
    }

    /**
     * Mit Uhrzeit in Minuten initialisieren
     *
     * @param ti Uhrzeit in Minuten
     */
    public Uhrzeit(int ti){
        t = ti;
    }

    /**
     * Mit Uhrzeit initialisieren
     *
     * @param h Stundenteil der Uhrzeit
     * @param m Minutenteil der Uhrzeit
     */
    public Uhrzeit(int h, int m) {
        t = (h * 60) + m;
    }

    /**
     * Mit Uhrzeit in Dezimalform initialisieren
     *
     * @param td Uhrzeit als Industrie- oder Digitalzeit (4,5 entspricht 4:30)
     */
    public Uhrzeit(float td){
        t= Math.round(td * 60);
    }

    /*
     * Set-Methoden
     */


    /**
     * mit der aktuellen Zeit setzen
     */
    public void set() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        t = (cal.get(Calendar.HOUR_OF_DAY) *60) + cal.get(Calendar.MINUTE);
    }


    /**
     * Uhrzeit als Stunden und Minuten übergeben
     *
     * @param h Stundenteil der Uhrzeit
     * @param m Minutenteil der Uhrzeit
     */
    public void set(int h, int m) {
        t = (h *60)+m;
    }

    // Zeit in Minuten

    /**
     * Zeit in Minuten übergeben
     *
     * @param ti Zeit in Minuten
     */
    public void set(int ti){ t=ti;}

    /**
     * Zeit als Industrie- bzw. Detzimalzeit übergeben
     *
     * @param td Zeit als Dezimalwert (4,5 entspricht 4:30 Uhr)
     */
    public void set (float td){
        t= Math.round(td * 60);
    }

    /**
     * Anzahl Minuten addieren
     *
     * @param minuten zu addierende Minuten
     */
    public void add(int minuten){
        t += minuten;
    }


    /**
     * Industrie- bzw. Dezimalzeit addieren
     *
     * @param td zu addierende Zeit (4,5 entspricht 4:30 Uhr)
     */
    public void add(float td){
        t += Math.round(td * 60);
    }


    /**
     *  Anzahl Minuten subtrahieren
     *
     * @param minuten zu subtrahierende Minuten
     */
    public void sub(int minuten){
        t -= minuten;
    }

    /**
     * Industrie- bzw. Dezimalzeit subtrahieren
     *
     * @param td zu subtrahierende Zeit (4,5 entspricht 4:30 Uhr)
     */
    public void sub(float td){
        t -= Math.round(td * 60);
    }


    /*
     * get-Methoden
    */

    /**
     * Zeit in Minuten zurückgeben
     *
     * @return Zeit in Minuten
     */
    public int getAlsMinuten() {
        return (t);
    }

    /**
     * Zeit als Industrie- bzw. Dezimalzeit zurückgeben
     *
     * @return Zeit in Industrie- bzw. Dezimalzeit (4:30Uhr entspricht 4,5)
     */
    public float getAlsDezimalZeit() {
        return( (float)t/60 );
    }

    /**
     * Gibt den Minutenteil der gespeicherten Uhrzeit zurück
     *
     * @return Minutenanteil der Zeit - dieser Wert ist immer positiv
     */
    public int getMinuten() {
        int mMinuten = t%60;
        // negative Minuten in positive umwandeln
            if (mMinuten < 0)
                mMinuten = -mMinuten;
        return (mMinuten);
    }


    /**
     * Gibt den Stundenanteil der gespeicherten Uhrzeit zurück
     *
     * @return Stundenanteil der gespeicherten Zeit
     */
    public int getStunden() {
        return (t/60);
    }

    /**
     * Gibt die Uhrzeit als formatierte Zeichenkette aus
     * und berücksichtigt dabei lokale Besonderheiten
     *
     * @return formatierte Uhrzeit Zeichenkette
     */
    public String getUhrzeitString() {
        Calendar cal = Calendar.getInstance();
        cal.set(0, 0, 0, getStunden(), getMinuten(), 0);
        return ASetup.zeitformat.format(cal.getTime());
        //return DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault()).format(cal.getTime());
    }

    /**
     * formatierte Zeichenkette als
     * Industrie- bzw. Dezimalzeit - 4,5h
     * oder in analoger Schreibweise - 4:30h
     *
     * @param symbol soll das Stundensymbol mit angezeigt werden
     * @param dezimal true = Industrie- bzw. Dezimalzeit, false = analoge Schreibweise
     * @return
     */
    public String getStundenString(boolean symbol, boolean dezimal) {
        StringBuilder ts = new StringBuilder();

        if(dezimal){
            //ts.append(zahlenFormat.format(getAlsDezimalZeit()));
            ts.append(ASetup.zahlenformat.format(getAlsDezimalZeit()));
        } else {
            // den Zeitstring bauen
            // nur wenn es nur negative Minuten sind, dann ein Minus voranstellen
            // bei mehr als einer negativen Stunde würden sonst zwei Minuszeichen erscheinen
            if(t < 0 && t > -60)
                ts.append('-');
            ts.append(String.format(
                    Locale.getDefault(),
                    "%d:%02d",
                    getStunden(), getMinuten()
            ));
        }

        if(symbol) {
            ts.append(" ").append(ASetup.res.getString(R.string.k_stunde));
        }
        return(ts.toString());
    }

    /**
     * Wandelt Stunden und Minuten in Minuten um
     *
     * @param h Anzahl Stunden
     * @param minuten Anzahl Minuten
     * @return Anzahl der gesamten Minuten - (Stunden * 60) + Minuten
     */
    public static int makeMinuten(int h, int minuten){
        return ((h * 60) + minuten);
    }
}
