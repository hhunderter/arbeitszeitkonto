/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.gallerie;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

public class IconGalleryAdapter extends RecyclerView.Adapter<IconGalleryAdapter.ViewHolder> {
    private final int[] iconListe;
    private int positionSelect;
    private ObjectAnimator animationSelect;
    Context mContext;
    ImageView imageSelect = null;

    public IconGalleryAdapter(int[] icons, int selectedIconID) {
        iconListe = icons;

        // die Position des aktuell gewählten Icons ermitteln
        positionSelect = 0;
        for (int id : iconListe) {
            if (id == selectedIconID) {
                break;
            }
            positionSelect++;
        }
    }

    @NonNull
    @Override
    public IconGalleryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.item_gallery, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position > 0) {
            holder.imageView.setImageResource(iconListe[position]);
        } else {
            holder.imageView.setImageResource(R.drawable.noicon);
        }

        if (position == positionSelect) {
            imageSelect = holder.imageView;

            animationSelect = ObjectAnimator.ofFloat(imageSelect, "rotationY", 0.0f, 85f, 60f, -30f, -10.0f, 0.0f);
            animationSelect.setDuration(3600);
            animationSelect.setRepeatCount(ObjectAnimator.INFINITE);
            animationSelect.setInterpolator(new AccelerateDecelerateInterpolator());
            animationSelect.start();
        }
    }

    @Override
    public int getItemCount() {
        return iconListe.length;
    }

    public int getPositionSelect() {
        return positionSelect;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.displayImage);
            itemView.setBackgroundColor(ASetup.aktJob.getFarbe_Hintergrund());
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            imageSelect = imageView;
            positionSelect = getAdapterPosition();

            if(animationSelect != null){
                animationSelect.end();
            }
            animationSelect = ObjectAnimator.ofFloat(imageSelect, "rotationY", 0.0f, 85f, 60f, -30f, -10.0f, 0.0f);
            animationSelect.setDuration(3600);
            animationSelect.setRepeatCount(ObjectAnimator.INFINITE);
            animationSelect.setInterpolator(new AccelerateDecelerateInterpolator());
            animationSelect.start();

        }
    }
}
