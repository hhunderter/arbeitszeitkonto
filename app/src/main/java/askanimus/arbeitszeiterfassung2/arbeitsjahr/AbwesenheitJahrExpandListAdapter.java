/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsjahr;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.abwesenheiten.AbwesenheitListe;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

/**
 * @author askanimus@gmail.com on 06.12.15.
 */
class AbwesenheitJahrExpandListAdapter extends BaseExpandableListAdapter {
    private final Context mContext;

    private Arbeitsjahr_summe mJahr;
    private final AbwesenheitListe stammAbwesenheiten = ASetup.aktJob.getAbwesenheiten();

    private final ArrayList<Abwesenheit> mAbwesenheiten;
    private final ArrayList<ArrayList<Float>> mWerte;

    AbwesenheitJahrExpandListAdapter(Context context) {
        mContext = context;
        mAbwesenheiten = new ArrayList<>();
        mWerte = new ArrayList<>();
    }


    protected void setUp(Arbeitsjahr_summe jahr){
        mJahr = jahr;

        for (int i = 0; i < stammAbwesenheiten.size(); i++) {
            ArrayList<Float> mAbwesenheit = mJahr.getAbwesenheitMonate(i);
            if(mAbwesenheit.get(mJahr.INDEX_SUMME) > 0) {
                mAbwesenheiten.add(stammAbwesenheiten.get(i));
                mWerte.add(mAbwesenheit);
            }
        }
    }





    @Override
    public int getGroupCount() {
        return mWerte.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public ArrayList<Float> getGroup(int groupPosition) {
        return mWerte.get(groupPosition);
    }

    @Override
    public ArrayList<Float> getChild(int groupPosition, int childPosition) {
        return mWerte.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View contentView, ViewGroup parent) {
        View view = contentView;

        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (mInflater != null) {
            view = mInflater.inflate(R.layout.item_monat, parent, false);

            // die Anzeigeelemente
            TextView tAbwesenheit = view.findViewById(R.id.MI_monat);
            TextView tSumme = view.findViewById(R.id.MI_saldo);

            tAbwesenheit.setText(mAbwesenheiten.get(groupPosition).getName());

            if (mAbwesenheiten.get(groupPosition).getKategorie() == Abwesenheit.KAT_URLAUB &&
                    ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN)) {
                //Uhrzeit mStunden = new Uhrzeit();
                //mStunden.set(Math.round(getGroup(groupPosition).get(mJahr.INDEX_SUMME)));
                float t = (getGroup(groupPosition).get(mJahr.INDEX_SUMME) / ASetup.aktJob.getSollstundenTagPauschal());
                tSumme.setText((ASetup.zahlenformat.format(t)));
            } else {
                tSumme.setText(ASetup.zahlenformat.format(getGroup(groupPosition).get(mJahr.INDEX_SUMME)));
            }
            view.setBackgroundColor(ASetup.aktJob.getFarbe_Tag());
        }
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView, ViewGroup parent) {
        View view = contentView;

        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (mInflater != null) {
            view = mInflater.inflate(R.layout.item_abwesenheit_info, parent, false);

            TextView[] wMonate = {
                    null,
                    view.findViewById(R.id.AI_wert_jan),
                    view.findViewById(R.id.AI_wert_feb),
                    view.findViewById(R.id.AI_wert_mar),
                    view.findViewById(R.id.AI_wert_apr),
                    view.findViewById(R.id.AI_wert_mai),
                    view.findViewById(R.id.AI_wert_jun),
                    view.findViewById(R.id.AI_wert_jul),
                    view.findViewById(R.id.AI_wert_aug),
                    view.findViewById(R.id.AI_wert_sep),
                    view.findViewById(R.id.AI_wert_okt),
                    view.findViewById(R.id.AI_wert_nov),
                    view.findViewById(R.id.AI_wert_dez)
            };

            ArrayList<Float> mMonate = getGroup(groupPosition);
            Uhrzeit mStunden = new Uhrzeit(0);
            Abwesenheit mAbwesenheit = mAbwesenheiten.get(groupPosition);
            for (int a = 1; a < mMonate.size(); a++) {
                int aa = a + (mJahr.MonatBeginn - 1);
                switch (mAbwesenheit.getWirkung()) {
                    case Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV:
                        if (mAbwesenheit.getKategorie() == Abwesenheit.KAT_URLAUB &&
                                ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN)) {
                            mStunden.set(mJahr.bezogenUrlaub[aa]);
                            wMonate[aa]
                                    .setText(mStunden.getStundenString(
                                            true,
                                            mJahr.mArbeitsplatz.isOptionSet(
                                                    Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
                        } else {
                            mStunden.set((mJahr.listMonate.get(a - 1)).getSummeAlternativMinuten(mAbwesenheit.getID()));
                            wMonate[aa].setText((ASetup.tageformat.format(mMonate.get(a))
                                    + "("
                                    + mStunden.getStundenString(true, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL))
                                    + ")"));

                        }
                        break;
                    case Abwesenheit.WIRKUNG_IST_PLUS_PAUSCHAL:
                        if (mAbwesenheit.getKategorie() == Abwesenheit.KAT_URLAUB &&
                                ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN)) {
                            mStunden.set(mJahr.bezogenUrlaub[aa]);
                            wMonate[aa].setText(
                                            mStunden.getStundenString(
                                                    true,
                                                    mJahr.mArbeitsplatz.isOptionSet(
                                                            Arbeitsplatz.OPT_ANZEIGE_DEZIMAL
                                                    )));
                        } else {
                            mStunden.set(Math.round(mMonate.get(a) * mJahr.mArbeitsplatz.getSollstundenTagPauschal()));
                            wMonate[aa].setText(
                                    ASetup.res.getString(R.string.wert_klammer,
                                            ASetup.tageformat.format(mMonate.get(a)),
                                            mStunden.getStundenString(true,
                                                    mJahr.mArbeitsplatz.isOptionSet(
                                                            Arbeitsplatz.OPT_ANZEIGE_DEZIMAL))
                                    ));
                        }
                        break;
                    default:
                        if (mAbwesenheit.getKategorie() == Abwesenheit.KAT_URLAUB &&
                                ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN)) {
                            mStunden.set(mJahr.bezogenUrlaub[aa]);
                            wMonate[aa].setText(
                                    mStunden.getStundenString(
                                            true,
                                            mJahr.mArbeitsplatz.isOptionSet(
                                                    Arbeitsplatz.OPT_ANZEIGE_DEZIMAL))
                            );
                        } else
                            wMonate[aa].setText(ASetup.tageformat.format(mMonate.get(a)));
                }
            }


            /*if (mAbwesenheit.getWirkung() == Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV) {
                for (int a = 1; a < mMonate.size(); a++) {
                    mStunden.set((mJahr.listMonate.get(a - 1)).getSummeAlternativMinuten(mAbwesenheit.getID()));
                    wMonate[a + (mJahr.MonatBeginn - 1)].setText((ASetup.zahlenformat.format(mMonate.get(a))
                            + "("
                            + mStunden.getStundenString(true, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL))
                            + ")"));
                }

            } else if (mAbwesenheit.getWirkung() == Abwesenheit.WIRKUNG_IST_PLUS_PAUSCHAL) {
                for (int a = 1; a < mMonate.size(); a++) {
                    if (mAbwesenheit.getKategorie() == Abwesenheit.KAT_URLAUB &&
                            ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN)) {
                        mStunden.set(Math.round(mMonate.get(a)));
                        float t = (mMonate.get(a) / ASetup.aktJob.getTagSollPauschal());
                        wMonate[a + (mJahr.MonatBeginn - 1)].setText(
                                ASetup.res.getString(
                                        R.string.wert_klammer, ASetup.zahlenformat.format(t),
                                        mStunden.getStundenString(
                                                true,
                                                mJahr.mArbeitsplatz.isOptionSet(
                                                        Arbeitsplatz.OPT_ANZEIGE_DEZIMAL
                                                ))));
                    } else {
                        mStunden.set(Math.round(mMonate.get(a) * mJahr.mArbeitsplatz.getTagSollPauschal()));
                        wMonate[a + (mJahr.MonatBeginn - 1)].setText(
                                ASetup.res.getString(R.string.wert_klammer,
                                        ASetup.zahlenformat.format(mMonate.get(a)),
                                        mStunden.getStundenString(true,
                                                mJahr.mArbeitsplatz.isOptionSet(
                                                        Arbeitsplatz.OPT_ANZEIGE_DEZIMAL))
                                ));
                    }
                }

            } else {
                for (int a = 1; a < mMonate.size(); a++) {
                    if (mAbwesenheit.getKategorie() == Abwesenheit.KAT_URLAUB &&
                            ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN)) {
                        mStunden.set(Math.round(mMonate.get(a)));
                        float t = (mMonate.get(a) / ASetup.aktJob.getTagSollPauschal());
                        wMonate[a + (mJahr.MonatBeginn - 1)].setText(
                                ASetup.res.getString(R.string.wert_klammer,
                                        (ASetup.zahlenformat.format(t)),
                                        mStunden.getStundenString(true,
                                                mJahr.mArbeitsplatz.isOptionSet(
                                                        Arbeitsplatz.OPT_ANZEIGE_DEZIMAL))
                                ));
                    } else
                        wMonate[a + (mJahr.MonatBeginn - 1)].setText(ASetup.zahlenformat.format(mMonate.get(a)));
                }
            }*/
        }
        return view;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

}
