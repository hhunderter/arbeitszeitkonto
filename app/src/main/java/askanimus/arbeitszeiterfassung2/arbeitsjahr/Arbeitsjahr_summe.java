/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsjahr;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.arbeitsmonat.Arbeitsmonat;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;

/**
 * @author askanimus@gmail.com on 12.12.15.
 */
public class Arbeitsjahr_summe extends Arbeitsjahr {
    public final int INDEX_SUMME = 0;
    private final ArrayList<ArrayList<Float>> tabelleAbwesenheiten;

    public Arbeitsjahr_summe(int jahr, Arbeitsplatz arbeitsplatz){
        super(jahr, arbeitsplatz, true);
        tabelleAbwesenheiten = new ArrayList<>();
        setSummeWerte();
    }


    private void setSummeWerte() {
        tabelleAbwesenheiten.add(new ArrayList<>());
        for (int a = 0; a < mArbeitsplatz.getAbwesenheiten().size(); a++) {
            tabelleAbwesenheiten.get(INDEX_SUMME).add(0f);
        }
        for ( Arbeitsmonat monat : listMonate) {
            if (monat.getId() > 0) {
                // Jahresarbeitszeiten
                hSoll += monat.getSollNetto();
                hIst += monat.getIstNettoMinusUeberstundenpauschale();
                hAusbezahlt += monat.getAuszahlung();
                //Jahres sonstige Werte
                for (int i = 0; i< mZusatzwerteSumme.size(); i++) {
                    if (mZusatzwerteSumme.get(i).getDatenTyp() != IZusatzfeld.TYP_TEXT) {
                        mZusatzwerteSumme.get(i).add(monat.getSummeZusatzeintrag(i));
                    }
                }
                sVerdienst += monat.getVerdienst();

                ArrayList<Float> mAbwesenheitenliste = monat.getSummeAlternativTage();

                tabelleAbwesenheiten.add(mAbwesenheitenliste);
                for (int a = 0; a < mArbeitsplatz.getAbwesenheiten().size(); a++) {
                    tabelleAbwesenheiten.get(INDEX_SUMME).set(a, tabelleAbwesenheiten.get(INDEX_SUMME).get(a)+mAbwesenheitenliste.get(a));
                }
            }
        }

    }
    /*
     * gibt eine Liste mit aller Monate des Jahres mit Anzahl Tage in die die gegebenen Abwesenheit
     * eingetragen wurde
     */
    public ArrayList<Float> getAbwesenheitMonate(int abwesenheit) {
        ArrayList<Float> mAbwesenheit = null;

        if (abwesenheit < tabelleAbwesenheiten.get(INDEX_SUMME).size()) {
            mAbwesenheit = new ArrayList<>();
            for (int m = 0; m < tabelleAbwesenheiten.size(); m++) {
                if (abwesenheit < tabelleAbwesenheiten.get(m).size())
                    mAbwesenheit.add(tabelleAbwesenheiten.get(m).get(abwesenheit));
                else
                    mAbwesenheit.add(0f);
            }
        }

        return mAbwesenheit;
    }

    /*
     * gibt eine Liste aller Abwesemnheiten eines Monats und deren Anzahl von Einträgen
     */
    ArrayList<Float> getMonatAbwesenheiten(int monat) {
        if(monat < tabelleAbwesenheiten.size())
            return tabelleAbwesenheiten.get(monat);
        else
            return null;
    }

    /*
     * gibt eine Liste aller Abwesemnheiten und deren Anzahl von Einträgen
     */
    public ArrayList<ArrayList<Float>> getAbwesenheiten(){
        return tabelleAbwesenheiten;
    }

    protected float getUrlaubIstMonat(int monat){
        if(monat < listMonate.size())
            return (listMonate.get(monat)).getSummeKategorieTage(Abwesenheit.KAT_URLAUB);
        else
            return 0;
    }

    public int getArbeitszeitSollMonat(int monat){
        if(monat < listMonate.size())
            return (listMonate.get(monat)).getSollNetto();
        else
            return 0;
    }

    public int getArbeitszeitIstMonat(int monat){
        if(monat < listMonate.size())
            return (listMonate.get(monat)).getIstNettoMinusUeberstundenpauschale();
        else
            return 0;
    }

    public int getArbeitszeitDiffMonat(int monat){
        if(monat < listMonate.size())
            return (listMonate.get(monat)).getIstNettoMinusUeberstundenpauschale() -
                    (listMonate.get(monat)).getSollNetto();
        else
            return 0;
    }

    public int getArbeitszeitAusbezahltMonat(int monat){
        if(monat < listMonate.size())
            return (listMonate.get(monat)).getAuszahlung();
        else
            return 0;
    }

    public int getArbeitszeitSaldoVormonat(int monat){
        if(monat < listMonate.size())
            return (listMonate.get(monat)).getSaldoVormonat();
        else
            return 0;
    }

    public int getArbeitszeitSaldoMonat(int monat){
        if(monat < listMonate.size())
            return (listMonate.get(monat)).getSaldo();
        else
            return 0;
    }

    public IZusatzfeld getZusatzeintragMonat(int monat, int index){
        if(monat < listMonate.size())
            return (listMonate.get(monat)).getSummeZusatzeintrag(index);
        else
            return null;
    }

    public int getAnzahlMonate() {
        return listMonate.size();
    }



}

