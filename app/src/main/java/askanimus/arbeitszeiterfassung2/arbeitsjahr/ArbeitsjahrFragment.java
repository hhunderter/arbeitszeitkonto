/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsjahr;

import android.app.ProgressDialog;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.codetroopers.betterpickers.numberpicker.NumberPickerBuilder;
import com.codetroopers.betterpickers.numberpicker.NumberPickerDialogFragment;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Objects;

import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ISetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzWertViewAdapter;

/**
 * @author askanimus@gmail.com on 19.08.15.
 */
public class ArbeitsjahrFragment
        extends Fragment
        implements View.OnClickListener,
        CalendarDatePickerDialogFragment.OnDateSetListener,
        NumberPickerDialogFragment.NumberPickerDialogHandlerV2 {

    //private static ArbeitsjahrCallbacks mCallback;

    private static final String ARG_JAHR = "jahr";
    private Arbeitsjahr_summe mJahr;

    private LinearLayout cErgebnis;

    private TextView tDatum;
    private TextView tUrlaub_RestSaldo;
    private TextView tUrlaub_Soll;
    private TextView tUrlaub_Saldo;
    private TextView tUrlaub_Geplant;
    private TableRow rUrlaub_Geplant;
    private TextView tUrlaub_Ist;
    private TextView tUrlaub_Rest;
    private AppCompatImageView bEditSoll;
    private TextView tUrlaub_RestIst;
    private AppCompatImageView bRsetRest;

    private TextView tStunden_Soll;
    private TextView tStunden_Ist;
    private TextView tStunden_Diff;
    private TextView tStunden_Vor;
    private TextView tStunden_Saldo;
    private TextView tStunden_Saldo_akt;
    private TextView tBez_Ueber;

    private AppCompatButton bAbwesenheiten;
    private AppCompatButton bMonate;

    private LinearLayout cSonstige;
    private LinearLayout zVerdienst;
    private TextView wVerdienst;
    ZusatzWertViewAdapter mAdapter_kopf;
    private RecyclerView lZusatzwerte;


    private ExpandableListView lListe;
    private AbwesenheitJahrExpandListAdapter mAbwesenheitListAdapter;
    private MonateJahrExpandListAdapter mMonateListAdapter;

    private ImageView iCompact;
    private boolean   isCompact;

    private Context mContext;


    /*
     * Neue Instance anlegen
     */
    public static ArbeitsjahrFragment newInstance(int jahr/*, ArbeitsjahrCallbacks cb*/) {
        ArbeitsjahrFragment fragment = new ArbeitsjahrFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_JAHR, jahr);
        fragment.setArguments(args);

        /*mCallback = cb;*/

        return fragment;
    }


   /* public ArbeitsjahrFragment(){}*/

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mContext = getContext();
        View view = inflater.inflate(R.layout.fragment_arbeitsjahr, container, false);
        lZusatzwerte = view.findViewById(R.id.AJ_liste_zusatzwerte);

        mAdapter_kopf =
                new ZusatzWertViewAdapter(ZusatzWertViewAdapter.VIEW_KOPF);
        GridLayoutManager layoutManger =
        new GridLayoutManager(
                mContext,
                1 );
        lZusatzwerte.setLayoutManager(layoutManger);
        lZusatzwerte.setAdapter(mAdapter_kopf);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ASetup.init(mContext, this::resume);
    }

    private void resume() {
        View mInhalt = getView();
        if (mInhalt != null) {
            RelativeLayout cDatum = mInhalt.findViewById(R.id.AJ_box_datum);
            tDatum = mInhalt.findViewById(R.id.AJ_wert_datum);
            iCompact = mInhalt.findViewById(R.id.AJ_icon_fold);
            cErgebnis = mInhalt.findViewById(R.id.AJ_box_sollist);

            LinearLayout cArbeitszeit = mInhalt.findViewById(R.id.AJ_box_azeit);
            tStunden_Soll = mInhalt.findViewById(R.id.AJ_wert_azeit_soll);
            tStunden_Ist = mInhalt.findViewById(R.id.AJ_wert_azeit_ist);
            tStunden_Diff = mInhalt.findViewById(R.id.AJ_wert_azeit_diff);
            tStunden_Vor = mInhalt.findViewById(R.id.AJ_wert_azeit_svor);
            tStunden_Saldo = mInhalt.findViewById(R.id.AJ_wert_azeit_saldo);
            tStunden_Saldo_akt = mInhalt.findViewById(R.id.AJ_wert_azeit_saldo_akt);
            tBez_Ueber = mInhalt.findViewById(R.id.AJ_wert_bezueber);

            bAbwesenheiten = mInhalt.findViewById(R.id.AJ_button_abwesenheit);
            bMonate = mInhalt.findViewById(R.id.AJ_button_monat);
            bEditSoll = mInhalt.findViewById(R.id.AJ_button_urlaub_soll);

            LinearLayout cUrlaub = mInhalt.findViewById(R.id.AJ_box_urlaub);
            tUrlaub_Soll = mInhalt.findViewById(R.id.AJ_wert_urlaub_soll);
            tUrlaub_Ist = mInhalt.findViewById(R.id.AJ_wert_urlaub_ist);
            tUrlaub_Saldo = mInhalt.findViewById(R.id.AJ_wert_urlaub_saldo);
            tUrlaub_Geplant = mInhalt.findViewById(R.id.AJ_wert_urlaub_geplant);
            rUrlaub_Geplant = mInhalt.findViewById(R.id.AJ_row_urlaub_geplant);
            tUrlaub_Rest = mInhalt.findViewById(R.id.AJ_wert_urlaub_rest);
            tUrlaub_RestIst = mInhalt.findViewById(R.id.AJ_wert_urlaub_restist);
            tUrlaub_RestSaldo = mInhalt.findViewById(R.id.AJ_wert_urlaub_restsaldo);
            bRsetRest = mInhalt.findViewById(R.id.AJ_button_urlaub_rest);

            cSonstige = mInhalt.findViewById(R.id.AJ_box_sonstige);
            zVerdienst = mInhalt.findViewById(R.id.AJ_zeile_verdienst);
            wVerdienst = mInhalt.findViewById(R.id.AJ_wert_verdienst);
            lZusatzwerte = mInhalt.findViewById(R.id.AJ_liste_zusatzwerte);
            lListe = mInhalt.findViewById(R.id.AJ_liste_werte);

            // Icon zum ein- und ausklappen der Zusammenfassung anpassen
            if (ASetup.res.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                isCompact = ASetup.mPreferenzen.getBoolean(
                        ISetup.KEY_ANZEIGE_JAHR_COMPACT, false);
                iCompact.setImageDrawable(
                        VectorDrawableCompat.create(
                                ASetup.res,
                                isCompact ?
                                        R.drawable.arrow_down :
                                        R.drawable.arrow_up,
                                mContext.getTheme())
                );
                //iCompact.setColorFilter(Color.WHITE);
                iCompact.setOnClickListener(this);
                cDatum.setOnClickListener(this);
                cErgebnis.setVisibility(isCompact ? View.GONE : View.VISIBLE);
            } else
                iCompact.setVisibility(View.GONE);

            cDatum.setBackgroundColor(ASetup.aktJob.getFarbe_Tag());
            cArbeitszeit.setBackgroundColor(ASetup.cHintergrundArbeitszeit);

            if (ASetup.aktJob.getSoll_Urlaub() > 0) {
                cUrlaub.setBackgroundColor(ASetup.cHintergrundUrlaub);
            } else
                cUrlaub.setVisibility(View.GONE);

            // Die Listadapter registrieren
            mAbwesenheitListAdapter = new AbwesenheitJahrExpandListAdapter(mContext);
            mMonateListAdapter = new MonateJahrExpandListAdapter(mContext);

            ladeJahr();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void updateView() {
        int iSoll;
        int iIst;
        float fIst;
        float fSoll;
        Uhrzeit mZeit = new Uhrzeit(0);

        Datum mKal;
        mKal = new Datum(
                mJahr.getJahr(),
                mJahr.getMonatBeginn(),
                ASetup.aktJob.getMonatsbeginn(),
                ASetup.aktJob.getWochenbeginn());
        int tage =
                mKal.getAktuellMaximum((Calendar.DAY_OF_YEAR))
                - mKal.get(Calendar.DAY_OF_YEAR);
        tage += ASetup.aktJob.getMonatsbeginn() -1;
        tDatum.setText(
                mKal.getString_Datum_Bereich(
                        mContext,
                        0,
                        tage,
                        Calendar.DAY_OF_YEAR));

        //
        // Urlaub
        //
        // Urlaubsanspruch
        fSoll = mJahr.getUrlaubSoll();
        tUrlaub_Soll.setText(ASetup.zahlenformat.format(fSoll));
        if (mJahr.isUrlaubManuell())
            tUrlaub_Soll.setTextColor(ASetup.cManuellText);
        else
            tUrlaub_Soll.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());

        // Bezogene Urlaubstage
        fIst = mJahr.getIstUrlaub();
        tUrlaub_Ist.setText(ASetup.zahlenformat.format(fIst));

        // die noch nicht bezogenen Urlaubstage
        fIst = mJahr.getUrlaubSaldo();
        tUrlaub_Saldo.setText(ASetup.zahlenformat.format(fIst));
        if (fIst == 0)
            tUrlaub_Saldo.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
        else if (fIst < 0)
            tUrlaub_Saldo.setTextColor(ASetup.cNegativText);
        else
            tUrlaub_Saldo.setTextColor(ASetup.cPositivText);

        // Der Resturlaub aus dem Vorjahr
        fSoll = mJahr.getResturlaub();
        tUrlaub_Rest.setText(ASetup.zahlenformat.format(fSoll));

        // Die Tage, die vom Resturlaub bezogen wurden
        fIst = mJahr.getResturlaubIst();
        tUrlaub_RestIst.setText(ASetup.zahlenformat.format(fIst));

        // der Resturlaub des letzten Jahres
        fIst = fSoll - fIst;
        if (fIst > 0 && !ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_RESTURLAUB_NO_VERFALL)) {
            bRsetRest.setVisibility(View.VISIBLE);
            // Resturlaub verfallen lassen
            bRsetRest.setOnClickListener(this);
        } else
            bRsetRest.setVisibility(View.GONE);

        int m;
        if(mJahr.getJahr() < ASetup.aktDatum.get(Calendar.YEAR))
            m = 12;
        else if(mJahr.getJahr() > ASetup.aktDatum.get(Calendar.YEAR))
            m = 1;
        else
            m = ASetup.aktDatum.get(Calendar.MONTH);

        tUrlaub_RestSaldo.setText(ASetup.zahlenformat.format(fIst));
        if (mJahr.isUrlaubVerfallen(m) && fIst > 0)
            tUrlaub_RestSaldo.setPaintFlags(tUrlaub_RestSaldo.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        else {
            tUrlaub_RestSaldo.setPaintFlags(tUrlaub_RestSaldo.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
        }

        if (fIst == 0)
            tUrlaub_RestSaldo.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
        else
            tUrlaub_RestSaldo.setTextColor(ASetup.cPositivText);

        // der geplante Urlaub
        if(mJahr.getUrlaubGeplant() > 0){
            tUrlaub_Geplant.setText(ASetup.zahlenformat.format(mJahr.getUrlaubGeplant()));
        } else {
            rUrlaub_Geplant.setVisibility(View.GONE);
        }


        // Arbeitszeit
        iSoll = mJahr.getSoll();
        mZeit.set(iSoll);
        tStunden_Soll.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

        iIst = mJahr.getIst();
        mZeit.set(iIst);
        tStunden_Ist.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

        mZeit.set(iIst - iSoll);
        tStunden_Diff.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        if (mZeit.getAlsMinuten() == 0)
            tStunden_Diff.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
        else if (mZeit.getAlsMinuten() < 0)
            tStunden_Diff.setTextColor(ASetup.cNegativText);
        else
            tStunden_Diff.setTextColor(ASetup.cPositivText);


        mZeit.set(mJahr.getSaldoVorjahr());
        tStunden_Vor.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        if (mZeit.getAlsMinuten() == 0)
            tStunden_Vor.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
        else if (mZeit.getAlsMinuten() < 0)
            tStunden_Vor.setTextColor(ASetup.cNegativText);
        else
            tStunden_Vor.setTextColor(ASetup.cPositivText);

        // die ausbezahlten Überstunden
        mZeit.set(mJahr.getAusgezahlt());
        tBez_Ueber.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

        // der Saldo des ganzen Jahres
        mZeit.set(mJahr.getSaldo());
        tStunden_Saldo.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        if (mZeit.getAlsMinuten() == 0)
            tStunden_Saldo.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
        else if (mZeit.getAlsMinuten() < 0)
            tStunden_Saldo.setTextColor(ASetup.cNegativText);
        else
            tStunden_Saldo.setTextColor(ASetup.cPositivText);

        // der Saldo bis heute
        mZeit.set(mJahr.getSaldo_aktuell());
        tStunden_Saldo_akt.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        if (mZeit.getAlsMinuten() == 0)
            tStunden_Saldo_akt.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
        else if (mZeit.getAlsMinuten() < 0)
            tStunden_Saldo_akt.setTextColor(ASetup.cNegativText);
        else
            tStunden_Saldo_akt.setTextColor(ASetup.cPositivText);


       // Die optionalen Werte
        if (ASetup.isZusatzfelder || ASetup.isVerdienst) {
            cSonstige.setBackgroundColor(ASetup.cHintergrundSonstiges);

            if (ASetup.isVerdienst) {
                wVerdienst.setText(ASetup.waehrungformat.format(mJahr.getVerdienst()));
            } else {
                zVerdienst.setVisibility(View.GONE);
            }

            if (ASetup.isZusatzfelder) {
                mAdapter_kopf.setUp(
                        mJahr.getZusatzeintragSummenListe().getListe(),
                        null);
                //mAdapter_kopf.notifyDataSetChanged();

            } else {
                lZusatzwerte.setVisibility(View.GONE);
            }

        } else {
            cSonstige.setVisibility(View.GONE);
        }

        // Errechnete Urlaustage überschreiben
        if (mJahr.isUrlaubManuell())
            bEditSoll.setImageResource(R.drawable.ic_action_undo);
        else
            bEditSoll.setImageResource(R.drawable.ic_action_edit);
        bEditSoll.setOnClickListener(this);

        // handler für das öffnen der Kindelemente, nur eins soll zur gleichen Zeit offen sein
        lListe.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    lListe.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });

        // Der Schalter für die Ansicht der Abwesenheiten Zusammenfassung
        mAbwesenheitListAdapter.setUp(mJahr);
        ViewCompat.setBackgroundTintList(bAbwesenheiten, ASetup.aktJob.getFarbe_Button());
        bAbwesenheiten.setTextColor(ASetup.aktJob.getFarbe_Schrift_Button());
        bAbwesenheiten.setSelected(true);
        bAbwesenheiten.setOnClickListener(v -> {
            lListe.setAdapter(mAbwesenheitListAdapter);
            bAbwesenheiten.setSelected(false);
            bMonate.setSelected(true);
        });

        // Der Schalter für die Ansicht der Monate Zusammenfassung
        mMonateListAdapter.setUp(mJahr);
        lListe.setAdapter(mMonateListAdapter);
        ViewCompat.setBackgroundTintList(bMonate, ASetup.aktJob.getFarbe_Button());
        bMonate.setTextColor(ASetup.aktJob.getFarbe_Schrift_Button());
        bMonate.setSelected(false);
        bMonate.setOnClickListener(v -> {
            lListe.setAdapter(mMonateListAdapter);
            bMonate.setSelected(false);
            bAbwesenheiten.setSelected(true);
        });

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        FragmentManager fManager;

        try {
            fManager = getParentFragmentManager();

            if (id == R.id.AJ_button_urlaub_rest) {
                //if (fManger != null) {
                    CalendarDatePickerDialogFragment vonKalenderPicker =
                            new CalendarDatePickerDialogFragment()
                                    .setOnDateSetListener(this)
                                    .setPreselectedDate(mJahr.getJahr(), mJahr.verfallMonat - 1, mJahr.verfallTag)
                                    .setDateRange(
                                            new MonthAdapter.CalendarDay(mJahr.getJahr(), 0, 1),
                                            new MonthAdapter.CalendarDay(mJahr.getJahr(), 11, 31));
                    if (ASetup.isThemaDunkel)
                        vonKalenderPicker.setThemeDark();
                    else
                        vonKalenderPicker.setThemeLight();
                    vonKalenderPicker.show(fManager, getString(R.string.resturlaub));
                //}
            } else if (id == R.id.AJ_button_urlaub_soll) {
                if (mJahr.isUrlaubManuell()) {
                    mJahr.setSollUrlaub(-1);
                    tUrlaub_Soll.setText(ASetup.zahlenformat.format(mJahr.getUrlaubSoll()));
                    tUrlaub_Saldo.setText(ASetup.zahlenformat.format(mJahr.getUrlaubSaldo()));
                    tUrlaub_Soll.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
                    bEditSoll.setImageResource(android.R.drawable.ic_menu_edit);
                    updateView();
                    requestBackup();
                    //mCallback.onJahrChanged();
                } else {
                    NumberPickerBuilder mMonatsbeginnPicker = new NumberPickerBuilder()
                            .setFragmentManager(fManager)
                            .setStyleResId(ASetup.themePicker)
                            .setMinNumber(BigDecimal.valueOf(0))
                            .setMaxNumber(BigDecimal.valueOf(366))
                            .setPlusMinusVisibility(View.INVISIBLE)
                            .setDecimalVisibility(View.VISIBLE)
                            .setLabelText(getString(
                                    ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN)
                                            ? R.string.k_stunde : R.string.tage))
                            .setTargetFragment(this)
                            .setReference(R.id.AJ_button_urlaub_soll);
                    mMonatsbeginnPicker.show();
                }
            } else if (id == R.id.AJ_box_datum || id == R.id.AJ_icon_fold) {
                SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
                isCompact = !isCompact;
                mEdit.putBoolean(ISetup.KEY_ANZEIGE_JAHR_COMPACT, isCompact);
                mEdit.apply();
                cErgebnis.setVisibility(isCompact ? View.GONE : View.VISIBLE);
                iCompact.setImageDrawable(
                        VectorDrawableCompat.create(
                                ASetup.res,
                                isCompact ?
                                        R.drawable.arrow_down :
                                        R.drawable.arrow_up,
                                mContext.getTheme())
                );
            }
        } catch (IllegalStateException ise){
            ise.printStackTrace();
        }
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        if (Objects.equals(dialog.getTag(), getString(R.string.resturlaub))) {
            mJahr.setVerfall(monthOfYear + 1, dayOfMonth);
            int m;
            if (mJahr.getJahr() < ASetup.aktDatum.get(Calendar.YEAR))
                m = 12;
            else if (mJahr.getJahr() > ASetup.aktDatum.get(Calendar.YEAR))
                m = 1;
            else
                m = ASetup.aktDatum.get(Calendar.MONTH);
            if (mJahr.isUrlaubVerfallen(m))
                tUrlaub_RestSaldo.setPaintFlags(tUrlaub_RestSaldo.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            else
                tUrlaub_RestSaldo.setPaintFlags(tUrlaub_RestSaldo.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
            updateView();
            requestBackup();
            //mCallback.onJahrChanged();
        }
    }


    @Override
    public void onDialogNumberSet(int reference, BigInteger number, double decimal, boolean isNegative, BigDecimal fullNumber) {
        if (reference == R.id.AJ_button_urlaub_soll) {
            if(ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN)){
                Uhrzeit mZeit = new Uhrzeit(fullNumber.floatValue());
                mJahr.setSollUrlaub(mZeit.getAlsMinuten());
            } else {
                mJahr.setSollUrlaub(fullNumber.floatValue());
            }
            updateView();
            requestBackup();
            //mCallback.onJahrChanged();
        }
    }


    // Backup im Google Konto anfordern
    private void requestBackup() {
        BackupManager bm = new BackupManager(mContext);
        try {
            bm.dataChanged();
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }
    }


    // Laden der Jahresdaten im Hintergrund
    private void ladeJahr() {
        if(mJahr == null) {
            // Fortschritsdialog öffnen
            ProgressDialog dialog = new ProgressDialog(mContext);
            dialog.setIndeterminate(true);
            dialog.setIndeterminateDrawable(
                    ResourcesCompat.getDrawable(
                            getResources(),
                            R.drawable.progress_dialog_anim,
                            mContext.getTheme()));
            dialog.setMessage(getString(R.string.progress_laden));
            dialog.setCancelable(false);
            dialog.show();

            Handler mHandler = new Handler();
            new Thread(() -> {
                if (getArguments() != null) {
                    int jahr = getArguments().getInt(ARG_JAHR);
                    mJahr = new Arbeitsjahr_summe(jahr, ASetup.aktJob);
                    mHandler.post(() -> {
                        // Fortschritsdialog schliessen
                        if(dialog.isShowing()) {
                            try {
                                dialog.dismiss();
                            } catch (IllegalArgumentException ignored){
                            }
                        }
                        updateView();
                    });
                }
            }).start();
        } else {
            updateView();
        }
    }

    /*
     * Callback Interfaces
     */
    //public interface ArbeitsjahrCallbacks {
        /*
         * Aufgerufen wenn sich Werte des Jahres geändert haben
         */
        //void onJahrChanged();
    //}

}
