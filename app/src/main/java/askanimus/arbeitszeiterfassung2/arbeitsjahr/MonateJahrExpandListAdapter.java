/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsjahr;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;

import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.abwesenheiten.AbwesenheitListe;
import askanimus.arbeitszeiterfassung2.arbeitsmonat.Arbeitsmonat;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzWertListe;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzWertViewAdapter;

/**
 * @author askanimus@gmail.com on 06.12.15.
 */
public class MonateJahrExpandListAdapter extends BaseExpandableListAdapter {
    private final Context mContext;

    private Arbeitsjahr_summe mJahr;
    private final AbwesenheitListe stammAbwesenheiten = ASetup.aktJob.getAbwesenheiten();
    //private SimpleDateFormat fMonat = new SimpleDateFormat("MMMM");
    //private SimpleDateFormat fMonat_kurz = new SimpleDateFormat("MMM yy");

    // Einstellungen zur Anzeige und Eingabe
    //private final Boolean isSonstiges = Einstellungen.isSonstiges;


    MonateJahrExpandListAdapter(Context context) {
        mContext = context;
    }

    public void setUp(Arbeitsjahr_summe jahr) {
        mJahr = jahr;
    }


    @Override
    public int getGroupCount() {
        if(mJahr != null) {
            return mJahr.getAnzahlMonate();
        } else {
            return 0;
        }
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public ArrayList<Float> getGroup(int groupPosition) {
        return mJahr.getMonatAbwesenheiten(groupPosition + 1);
    }

    @Override
    public ArrayList<Float> getChild(int groupPosition, int childPosition) {
        return mJahr.getMonatAbwesenheiten(groupPosition + 1);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View contentView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (mInflater != null) {
            contentView = mInflater.inflate(R.layout.item_monat, parent, false);


            Arbeitsmonat mMonat = mJahr.listMonate.get(groupPosition);

            Datum mKal = new Datum(
                    mMonat.getJahr(),
                    mMonat.getMonat(),
                    ASetup.aktJob.getMonatsbeginn(),
                    ASetup.aktJob.getWochenbeginn()
            );

            Uhrzeit mZeit;

            // die Anzeigeelemente
            TextView tMonat = contentView.findViewById(R.id.MI_monat);
            TextView tSaldo = contentView.findViewById(R.id.MI_saldo);
            TextView tSaldo_akt = contentView.findViewById(R.id.MI_saldo_akt);

            contentView.setBackgroundColor(ASetup.aktJob.getFarbe_Tag());
            tMonat.setText(mKal.getString_Monat_Jahr(ASetup.aktJob.getMonatsbeginn(), false));

            // den Gesamtsaldo des Monats eintragen
            mZeit = new Uhrzeit(mMonat.getSaldo());
            tSaldo.setText(mZeit.getStundenString(true, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
            if (mZeit.getAlsMinuten() == 0)
                tSaldo.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
            else if (mZeit.getAlsMinuten() < 0)
                tSaldo.setTextColor(ASetup.cNegativText);
            else
                tSaldo.setTextColor(ASetup.cPositivText);

            // den Saldo bis zum aktuellen Tag zeigen
            if(mJahr.getJahr() == ASetup.aktDatum.get(Calendar.YEAR) &&
                    mMonat.getMonat() == ASetup.aktDatum.get(Calendar.MONTH)) {
                contentView.findViewById(R.id.MI_box_saldo_akt).setVisibility(View.VISIBLE);
                mZeit.set(mJahr.getSaldo_aktuell());
                tSaldo_akt.setText(mZeit.getStundenString(true,
                                mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
                if (mZeit.getAlsMinuten() == 0)
                    tSaldo_akt.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
                else if (mZeit.getAlsMinuten() < 0)
                    tSaldo_akt.setTextColor(ASetup.cNegativText);
                else
                    tSaldo_akt.setTextColor(ASetup.cPositivText);
            }
        }
        return contentView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View contentView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (mInflater != null) {
            contentView = mInflater.inflate(R.layout.item_monat_info_summe, parent, false);

            Arbeitsmonat mMonat = mJahr.listMonate.get(groupPosition);

            Uhrzeit mZeit = new Uhrzeit(mMonat.getSollNetto());

            TableLayout lAbwesenheiten = contentView.findViewById((R.id.MI_tabelle_Abwesenheiten));

            // Das Datum
            TextView wDatum = contentView.findViewById((R.id.MI_wert_datum));

            Datum mKal = new Datum(
                    mMonat.getJahr(),
                    mMonat.getMonat(),
                    ASetup.aktJob.getMonatsbeginn(),
                    ASetup.aktJob.getWochenbeginn());
            if (mKal.liegtVor(ASetup.aktJob.getStartDatum()))
                mKal.set(ASetup.aktJob.getStartDatum().getTime());

            int tage = mKal.getAktuellMaximum((Calendar.DAY_OF_MONTH)) - mKal.get(Calendar.DAY_OF_MONTH);
            tage += ASetup.aktJob.getMonatsbeginn() - 1;
            wDatum.setText(
                    mKal.getString_Datum_Bereich(
                            mContext,
                            0,
                            tage,
                            Calendar.DAY_OF_MONTH)
            );

            // Sollstunden
            TextView wSoll = contentView.findViewById((R.id.MI_wert_soll));
            wSoll.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

            // die gearbeiteten Stunden
            TextView wIst = contentView.findViewById((R.id.MI_wert_ist));
            mZeit.set(mMonat.getIstNettoMinusUeberstundenpauschale());
            wIst.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

            // die Differenz der beiden obigen Werte
            TextView wDiff = contentView.findViewById((R.id.MI_wert_diff));
            mZeit.set(mMonat.getDifferenz());
            wDiff.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
            if (mZeit.getAlsMinuten() == 0)
                wDiff.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
            else if (mZeit.getAlsMinuten() < 0)
                wDiff.setTextColor(ASetup.cNegativText);
            else
                wDiff.setTextColor(ASetup.cPositivText);

            // der Saldo des Vormonats
            TextView wSvor = contentView.findViewById((R.id.MI_wert_svor));
            mZeit.set(mMonat.getSaldoVormonat());
            wSvor.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
            if (mZeit.getAlsMinuten() == 0)
                wSvor.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
            else if (mZeit.getAlsMinuten() < 0)
                wSvor.setTextColor(ASetup.cNegativText);
            else
                wSvor.setTextColor(ASetup.cPositivText);

            // der Saldo des Monats
            TextView wSaldo = contentView.findViewById((R.id.MI_wert_saldo));
            mZeit.set(mMonat.getSaldo());
            wSaldo.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
            if (mZeit.getAlsMinuten() == 0)
                wSaldo.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
            else if (mZeit.getAlsMinuten() < 0)
                wSaldo.setTextColor(ASetup.cNegativText);
            else
                wSaldo.setTextColor(ASetup.cPositivText);

            // Ausbezahlte Überstunden
            TextView wBezUeber = contentView.findViewById((R.id.MI_wert_bzueber));
            mZeit.set(-mMonat.getAuszahlung());
            wBezUeber.setText(mZeit.getStundenString(false, mJahr.mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
            if (mZeit.getAlsMinuten() < 0)
                wBezUeber.setTextColor(ASetup.cNegativText);

            // die bezogenen Urlaubstage/-stunden
            if (ASetup.aktJob.getSoll_Urlaub() > 0) {
                TextView wUrlaub = contentView.findViewById((R.id.MI_wert_urlaub));
                wUrlaub.setText(ASetup.zahlenformat.format(
                        mJahr.bezogenRest[mMonat.getMonat()] +
                                mJahr.bezogenUrlaub[mMonat.getMonat()]));

                // den geplanten Urlaub des Monats, wenn es der aktuelle ist
                TableRow tr = contentView.findViewById((R.id.MI_zeile_urlaub_plan));
                if (mJahr.geplanterUrlaubAktMonat > 0 && mKal.istGleich(ASetup.aktJob.getAbrechnungsmonat(ASetup.aktDatum), Calendar.MONTH)) {
                   // if (mJahr.geplanterUrlaubAktMonat > 0) {
                        tr.setVisibility(View.VISIBLE);
                        TextView wUrlaubGeplant = contentView.findViewById((R.id.MI_wert_urlaub_plan));
                        wUrlaubGeplant.setText(ASetup.zahlenformat.format(
                                mJahr.geplanterUrlaubAktMonat
                        ));
                   /* } else {
                        tr.setVisibility(View.GONE);
                    }*/
                } else {
                    tr.setVisibility(View.GONE);
                }
            } else {
                TableRow rUrlaub = contentView.findViewById((R.id.MI_zeile_urlaub));
                rUrlaub.setVisibility(View.GONE);
            }


            // Zusätzliche Eingabewerte
            TableRow rZeile;
            TextView tWert;

            if (ASetup.isVerdienst) {
                tWert = contentView.findViewById((R.id.MI_wert_verdienst));
                tWert.setText(ASetup.waehrungformat.format(mMonat.getVerdienst()));
            } else {
                rZeile = contentView.findViewById((R.id.MI_zeile_verdienst));
                rZeile.setVisibility(View.GONE);
            }

            // die Zusatzwerte anzeigen
            RecyclerView lZusatzwerte = contentView.findViewById(R.id.MI_liste_zusatzwerte);
            if (ASetup.isZusatzfelder) {
                ZusatzWertListe mZusatzwerteSumme = mMonat.getZusatzeintragSummenListe();
                ZusatzWertViewAdapter mAdapter =
                        new ZusatzWertViewAdapter(
                                mZusatzwerteSumme.getListe(),
                                null,
                                ZusatzWertViewAdapter.VIEW_KOPF);
                GridLayoutManager layoutManger = new GridLayoutManager(mContext, 1);
                lZusatzwerte.setLayoutManager(layoutManger);
                lZusatzwerte.setAdapter(mAdapter);
            } else
                lZusatzwerte.setVisibility(View.GONE);

            // die Summen der einzelnen abwesenheiten für diesen Monat
            AbwesenheitInfoListadapter mAdapter = new AbwesenheitInfoListadapter();
            mAdapter.setUp(groupPosition);
            erzeugeTabelle(mAdapter, lAbwesenheiten);
        }
        // Die Anzeigeelemente
        return contentView;
    }

    public void erzeugeTabelle(AbwesenheitInfoListadapter adapter, TableLayout tabelle){
        for (int i = 0; i < adapter.getCount(); i++) {
            tabelle.addView(adapter.getView(i, new TableRow(tabelle.getContext()), tabelle));
        }
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


    private class AbwesenheitInfoListadapter extends BaseAdapter {

        private ArrayList<Float> mWerte;
        private ArrayList<Abwesenheit> mAbwesenheiten;
        protected void setUp(int monat){
            ArrayList<Float> mAbwesenheitenWert = mJahr.getMonatAbwesenheiten(monat +1);
            mWerte = new ArrayList<>();
            mAbwesenheiten = new ArrayList<>();
            if(mAbwesenheitenWert != null) {
                for (int i = 0; i < mAbwesenheitenWert.size(); i++) {
                    if (mAbwesenheitenWert.get(i) > 0) {
                        mAbwesenheiten.add(stammAbwesenheiten.get(i));
                        mWerte.add(mAbwesenheitenWert.get(i));
                    }
                }
            }
        }

        @Override
        public int getCount() {
            return mWerte.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (mInflater != null) {
                convertView = mInflater.inflate(R.layout.item_tablerow_2, parent, false);

                TextView tTitel = convertView.findViewById((R.id.TR_titel));
                TextView tWert = convertView.findViewById((R.id.TR_wert));

                tTitel.setText(mAbwesenheiten.get(position).getName());

                if(mAbwesenheiten.get(position).getKategorie() == Abwesenheit.KAT_URLAUB &&
                ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN)) {
                    float t = (mWerte.get(position) / ASetup.aktJob.getSollstundenTagPauschal());
                    tWert.setText((ASetup.zahlenformat.format(t)));
                } else {
                    tWert.setText(ASetup.zahlenformat.format(mWerte.get(position)));
                }
            }
            return convertView;
        }
    }
}
