/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

class Zelle_CSV {
    private final int WERT_TYP_INT = 1;
    private final int WERT_TYP_FLOAT = 2;
    private final int WERT_TYP_STRING = 3;
    private final int WERT_TYP_UHRZEIT = 4;
    private final int WERT_TYP_STUNDEN = 5;

    private int wTyp;
    private Object oWert;

    public Zelle_CSV(int wert){
        wTyp = WERT_TYP_INT;
        //iWert = wert;
        oWert = new Object();
        oWert = wert;
    }

    public Zelle_CSV(float wert){
        wTyp = WERT_TYP_FLOAT;
        //fWert = wert;
        oWert = new Object();
        oWert = wert;
    }

    public Zelle_CSV(String wert){
        wTyp = WERT_TYP_STRING;
        //sWert = wert;
        oWert = new Object();
        oWert = wert;
    }

    public Zelle_CSV(int wert, boolean isUhrzeit){
        oWert = new Uhrzeit(wert);
        //zWert = new Uhrzeit(wert);
        wTyp = isUhrzeit ? WERT_TYP_UHRZEIT : WERT_TYP_STUNDEN;
    }

    public void set(String wert){
        wTyp = WERT_TYP_STRING;
        //oWert = new Object();
        oWert = wert;
    }


    public String get(){
        switch (wTyp){
            case WERT_TYP_INT :
                return String.valueOf(oWert);
            case WERT_TYP_FLOAT :
                return ASetup.zahlenformat.format(oWert);
            case WERT_TYP_STUNDEN :
                return ((Uhrzeit) oWert).getStundenString(false, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL));
            case WERT_TYP_UHRZEIT :
                return ((Uhrzeit) oWert).getUhrzeitString();
            case WERT_TYP_STRING :
                return "\"" + oWert + "\"";
            default:
                return "";
        }
    }

}
