/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import android.content.Context;

import java.io.PrintWriter;
import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;

abstract class AExportBasis_CSV extends AExportBasis implements IExport_Basis_CSV {
    PrintWriter ausgabeStream;
    String mTrenner;

    AExportBasis_CSV(Context context) {
        super(context);
        mTrenner = ASetup.mPreferenzen.getString(ISetup.KEY_EXPORT_CSV_TRENNER, ";");
    }

    @Override
    public void oeffneAusgabe() {
        ausgabeStream = new PrintWriter(mDatei);
    }

    // eine Tabelle ins CSV Format übertragen
    @Override
    public void schreibeSeiten() {
        ArrayList<ArrayList<Zelle_CSV>> mTabelle;
        mTabelle = erzeugeTabelle();

        if (mTabelle != null) {
            ArrayList<Zelle_CSV> mZeile;
            StringBuilder sZeile;

            for (int i = 0; i < mTabelle.size(); i++) {
                mZeile = mTabelle.get(i);
                sZeile = new StringBuilder();
                for (int ii = 0; ii < mZeile.size(); ii++) {
                    if (ii > 0)
                        sZeile.append(mTrenner);
                    sZeile.append(mZeile.get(ii).get());
                }
                ausgabeStream.println(sZeile.toString());
            }
        }
    }

    @Override
    public void schliesseAusgabe() {
        ausgabeStream.flush();
        ausgabeStream.close();
    }


    // Leerzeile einfügen
    public void makeLeerzeile(ArrayList<ArrayList<Zelle_CSV>> tableData, int anzahl) {
        ArrayList<Zelle_CSV> row = new ArrayList<>();
        //row.add(new Zelle_CSV(""));
        for (int z = 0; z < anzahl; z++) {
            tableData.add(row);
        }
    }

    public void erzeugeSeitenTitel(ArrayList<ArrayList<Zelle_CSV>> tableData, Arbeitsplatz arbeitsplatz, String titel) {
        String[] links;
        String[] mitte;
        String[] rechts;
        int zeilen;
        ArrayList<Zelle_CSV> mZeile;
        Zelle_CSV mLeerzelle = new Zelle_CSV("");

        links = (arbeitsplatz.getName()+"\n"+ arbeitsplatz.getAnschrift()).split("[\\r\\n]+");
        zeilen = links.length;

        mitte = titel.split("[\\r\\n]+");
        zeilen = Math.max(zeilen, mitte.length);

        rechts = (
                ASetup.mPreferenzen.getString(ISetup.KEY_USERNAME, "")
                        + "\n"
                        + ASetup.mPreferenzen.getString(ISetup.KEY_USERANSCHRIFT, "")
        ).split("[\\r\\n]+");
        zeilen = Math.max(zeilen, rechts.length);



        // Adresszeilen schreiben
        for (int i = 0; i < zeilen; i++) {
            mZeile = new ArrayList<>();
            if (links.length > i) {
                mZeile.add(new Zelle_CSV(links[i]));
            } else {
                mZeile.add(mLeerzelle);
            }
            //Freiraum für Seitentitel einfügen
            mZeile.add(mLeerzelle);
            mZeile.add(mLeerzelle);
            if (rechts.length > i) {
                mZeile.add(mLeerzelle);
                mZeile.add(new Zelle_CSV(rechts[i]));
            }
            tableData.add(mZeile);
        }

        // Seitentitel schreiben
        for (String s : mitte) {
            mZeile = new ArrayList<>();
            mZeile.add(mLeerzelle);
            mZeile.add(mLeerzelle);
            mZeile.add(new Zelle_CSV(s));
            tableData.add(mZeile);
        }

        // zwei Zeilen Abstand zum Inhalt
        makeLeerzeile(tableData, 2);
    }
    @Override
    public Zelle_CSV makeZelleLeer() {
        return new Zelle_CSV("");
    }

    @Override
    public Zelle_CSV makeZelleString(String text) {
        return new Zelle_CSV(text);
    }
    @Override
    public Zelle_CSV makeZelleWert(float wert) {
        return new Zelle_CSV(wert);
    }
    @Override
    public Zelle_CSV makeZelleStunden(int wert) {
        return new Zelle_CSV(wert, false);
    }
    @Override
    public Zelle_CSV makeZelleUhrzeit(int wert) {
        return new Zelle_CSV(wert, true);
    }

    @Override
    public Zelle_CSV makeZelleZusatzwert(IZusatzfeld zfeld, boolean inklNotiz){
        Zelle_CSV zelle;
        switch (zfeld.getDatenTyp()) {
            case IZusatzfeld.TYP_BEREICH_ZAHL:
            case IZusatzfeld.TYP_ZAHL:
                zelle = makeZelleWert(
                        (float) zfeld.getWert());
                break;
            case IZusatzfeld.TYP_BEREICH_ZEIT:
            case IZusatzfeld.TYP_ZEIT:
                zelle = makeZelleStunden((int) zfeld.getWert());
                break;
            default:
                if(inklNotiz) {
                    zelle = makeZelleString(zfeld.getString(false));
                } else {
                    zelle = makeZelleLeer();
                }
        }
        return zelle;
    }
}
