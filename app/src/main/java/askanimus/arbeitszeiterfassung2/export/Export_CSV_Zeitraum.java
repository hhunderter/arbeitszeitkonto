/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import android.content.Context;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Locale;

import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.Zeitraum.IZeitraum;
import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.arbeitsjahr.Arbeitsjahr_summe;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitsschicht.Arbeitsschicht;
import askanimus.arbeitszeiterfassung2.arbeitstag.Arbeitstag;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

class Export_CSV_Zeitraum extends AExportBasis_CSV {
    private ArrayList<ArrayList<Zelle_CSV>>  mTabelle;
    private IZeitraum mZeitraum;
    private ArrayList<Arbeitsplatz> mArbeitsplatzListe;
    private Arbeitsplatz mArbeitsplatz;
    private ArrayList<Spalte> mSpaltenSatz; // Namen und Positionen der einzelnen Spalten
    private BitSet mOptionen;
    private BitSet mSpalten;
    private BitSet mZusatzwerte;
    private boolean isSpalteTagsoll = false;

    //Formater für Wochentag
    DateFormat fWochentag = new SimpleDateFormat("E", Locale.getDefault());

    // Formater für kurzes Datum
    DateFormat fDatum = new SimpleDateFormat("d.M.yyyy", Locale.getDefault());


    Export_CSV_Zeitraum(
            Context context,
            IZeitraum zeitraum,
            BitSet spalten,
            BitSet optionen,
            BitSet zusatzwerte,
            StorageHelper storageHelper
            //String pfad
    ) throws Exception {
        super(context);
        mZeitraum = zeitraum;

        mSpalten = spalten;
        mZusatzwerte = zusatzwerte;
        mOptionen = optionen;

        if(mOptionen.get(OPTION_ALL_JOBS)) {
           mArbeitsplatzListe = ASetup.jobListe.getListe();
        } else {
            mArbeitsplatzListe = new ArrayList<>();
            mArbeitsplatzListe.add(mZeitraum.getArbeitsplatz());
        }
        // den Dateiname zusammenstellen
        //String dateiname = mZeitraum.getDateiname(context, 0);
        //dateiname = ExportActivity.makeExportpfad(context, pfad, dateiname);
        schreibeTabelle(
                storageHelper,
                mZeitraum.getDateiname(context, 0),
                TYP_CSV
        );
    }

    @Override
    public ArrayList<ArrayList<Zelle_CSV>> erzeugeTabelle() {
        mTabelle = new ArrayList<>();
        for (int i = 0; i < mArbeitsplatzListe.size(); i++) {
            mArbeitsplatz = mArbeitsplatzListe.get(i);
            mSpaltenSatz = makeSpaltenSet(mSpalten, mZusatzwerte, mArbeitsplatzListe.get(0));
            if(i>0){
                // 4 Leehrzeilen einfügen
                makeLeerzeile(mTabelle,4);
                mZeitraum = mZeitraum.wechselArbeitsplatz(mArbeitsplatz);
            }
            // den Seitentitel schreiben
            erzeugeSeitenTitel(mTabelle, mArbeitsplatz, mZeitraum.getTitel(mContext));
            // die Kopfzeile der Tabelle
            makeTabellenKopf(mTabelle);
            // Datenzeilen Tage/Schichten
            for (Arbeitstag tag:mZeitraum.getTage()) {
                makeTageszeile(mTabelle, tag);
            }
            // die Summenzeile der Tabelle
            makeSummenzeile(mTabelle);
        }

        return mTabelle;
    }

    public void makeTabellenKopf(ArrayList<ArrayList<Zelle_CSV>> tabelle){
        ArrayList<Zelle_CSV> row = new ArrayList<>();
        for (AExportBasis.Spalte spalte: mSpaltenSatz) {
            if(spalte.mSpalte == SPALTE_DATUM){
                row.add(makeZelleString(ASetup.res.getString(R.string.wochentag)));
            }
            row.add(makeZelleString(spalte.mName));
            if(spalte.mSpalte == SPALTE_TAGSOLL){
                isSpalteTagsoll = true;
            }
        }
        tabelle.add(row);
    }



    public void makeTageszeile(ArrayList<ArrayList<Zelle_CSV>> tabelle, Arbeitstag tag) {
        //Arbeitsschicht aSchicht;
        //int mSchicht = 0;
        Uhrzeit mZeit = new Uhrzeit(0);
        ArrayList<Zelle_CSV> row = null;

        Zelle_CSV zelleSoll = null;
        Zelle_CSV zelleDifferenz = null;


        //aSchicht = tag.getSchicht(mSchicht);
        for (Arbeitsschicht aSchicht : tag.getSchichten()) {
            if (aSchicht.getAbwesenheit().getKategorie() != Abwesenheit.KAT_KEINESCHICHT) {
                row = new ArrayList<>();
                for (AExportBasis.Spalte spalte : mSpaltenSatz) {
                    switch (spalte.mSpalte) {
                        case SPALTE_DATUM:
                            //if(mSchicht == 0) {
                            row.add(makeZelleString(fWochentag.format(tag.getKalender().getTime())));
                            row.add(makeZelleString(fDatum.format(tag.getKalender().getTime())));
                        /*} else {
                            row.add(makeZelleLeer());
                            row.add(makeZelleLeer());
                        }*/
                            break;
                        case SPALTE_SCHICHTNAME:
                            if (aSchicht.getAbwesenheit().getKategorie() == Abwesenheit.KAT_ARBEITSZEIT) {
                                row.add(makeZelleString(aSchicht.getName()));
                            } else {
                                row.add(makeZelleString(aSchicht.getAbwesenheit().getName()));
                            }
                            break;
                        case SPALTE_EORT:
                            row.add(makeZelleString(aSchicht.getNameEinsatzort()));
                            break;
                        case SPALTE_VON:
                            if (aSchicht.getAbwesenheit().getWirkung() == Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV) {
                                row.add(makeZelleUhrzeit(aSchicht.getVon()));
                            } else {
                                row.add(makeZelleString(""));
                            }
                            break;
                        case SPALTE_BIS:
                            if (aSchicht.getAbwesenheit().getWirkung() == Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV) {
                                row.add(makeZelleUhrzeit(aSchicht.getBis()));
                            } else {
                                row.add(makeZelleString(""));
                            }
                            break;
                        case SPALTE_BRUTTO:
                            row.add(makeZelleStunden(aSchicht.getBrutto()));
                            break;
                        case SPALTE_PAUSE:
                            row.add(makeZelleStunden(aSchicht.getPause()));
                            break;
                        case SPALTE_NETTO:
                            row.add(makeZelleStunden(aSchicht.getNetto()));
                            break;
                        case SPALTE_TAGSOLL:
                            row.add(makeZelleString(""));
                            if (!mOptionen.get(OPTION_ZEILE_SUMMETAG)) {
                                zelleSoll = row.get(row.size() - 1);
                            }
                            /*if (mSchicht < tag.getSchichtzahl() - 1 || mOptionen.get(OPTION_ZEILE_SUMMETAG)) {
                                row.add(makeZelleString(""));
                            } else {
                                row.add(makeZelleStunden(tag.getSoll()));
                            }*/
                            break;
                        case SPALTE_TAGSALDO:
                            row.add(makeZelleString(""));
                            if (!mOptionen.get(OPTION_ZEILE_SUMMETAG)) {
                                zelleDifferenz = row.get(row.size() - 1);
                            }
                            /*if (mSchicht < tag.getSchichtzahl() - 1 || mOptionen.get(OPTION_ZEILE_SUMMETAG)) {
                                row.add(makeZelleString(""));
                            } else {
                                row.add(makeZelleStunden((tag.getTagNetto() - tag.getSoll())));
                            }*/
                            break;
                        case SPALTE_VERDIENST:
                            mZeit.set(aSchicht.getNetto());
                            row.add(makeZelleWert(
                                    mZeit.getAlsDezimalZeit() * mArbeitsplatz.getStundenlohn()));
                            break;
                        default:
                            if (spalte.mSpalte >= SPALTE_ZUSATZ) {
                                row.add(makeZelleZusatzwert(
                                        aSchicht.getZusatzwert(spalte.mSpalte - SPALTE_ZUSATZ),
                                        true
                                ));
                            } else {
                                row.add(makeZelleString(""));
                            }
                    }
                }
                tabelle.add(row);
            }
            //Schicht++;
            //aSchicht = tag.getSchicht(mSchicht);
        }
        // Zellenwerte für Tagsoll und Tagesdifferenz eintragen
        if (zelleSoll != null) {
            Uhrzeit uz = new Uhrzeit(tag.getTagSollNetto());
            zelleSoll.set(
                    uz.getStundenString(
                            false,
                            mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        }

        if (zelleDifferenz != null) {
            Uhrzeit uz = new Uhrzeit(tag.getTagNetto() - tag.getTagSollNetto());
            zelleDifferenz.set(
                    uz.getStundenString(
                            false,
                            mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        }


        // die Tageszusammenfassung
        if (mOptionen.get(OPTION_ZEILE_SUMMETAG) && row != null) {
            row = new ArrayList<>();
            row.add(makeZelleString(ASetup.res.getString(R.string.summe)));

            for (AExportBasis.Spalte spalte : mSpaltenSatz) {
                switch (spalte.mSpalte) {
                    case SPALTE_BRUTTO:
                        row.add(makeZelleStunden(tag.getTagBrutto()));
                        break;
                    case SPALTE_PAUSE:
                        row.add(makeZelleStunden(tag.getTagPause()));
                        break;
                    case SPALTE_NETTO:
                        row.add(makeZelleStunden(tag.getTagNetto()));
                        break;
                    case SPALTE_TAGSOLL:
                        row.add(makeZelleStunden(tag.getTagSollNetto()));
                        break;
                    case SPALTE_TAGSALDO:
                        row.add(makeZelleStunden((tag.getTagNetto() - tag.getTagSollNetto())));
                        break;
                    case SPALTE_VERDIENST:
                        mZeit.set(tag.getTagNetto());
                        row.add(makeZelleWert(
                                mZeit.getAlsDezimalZeit() * mArbeitsplatz.getStundenlohn()
                        ));
                        break;
                    default:
                        if (spalte.mSpalte >= SPALTE_ZUSATZ) {
                            row.add(makeZelleZusatzwert(
                                    tag.getTagZusatzwert(spalte.mSpalte - SPALTE_ZUSATZ),
                                    false
                            ));
                        } else {
                            row.add(makeZelleLeer());
                        }
                }
            }
            tabelle.add(row);
        }
    }

    public  void makeSummenzeile(ArrayList<ArrayList<Zelle_CSV>> tabelle){
        Uhrzeit mZeit = new Uhrzeit(0);
        ArrayList<Zelle_CSV> row;
        int posTitelSaldo = 0;
        // Gesamt der Woche
        if(mOptionen.get(OPTION_ZEILE_SUMMEZEITRAUM)) {
            row = new ArrayList<>();
            row.add(makeZelleString(""));

            for (AExportBasis.Spalte spalte : mSpaltenSatz) {
                switch (spalte.mSpalte) {
                    case SPALTE_BRUTTO:
                        posTitelSaldo = row.size() - 1;
                        row.add(makeZelleStunden(mZeitraum.getBrutto()));
                        break;
                    case SPALTE_PAUSE:
                        posTitelSaldo = row.size() - 1;
                        row.add(makeZelleStunden(mZeitraum.getPause()));
                        break;
                    case SPALTE_NETTO:
                        posTitelSaldo = row.size() - 1;
                        row.add(makeZelleStunden(mZeitraum.getIstNetto()));
                        break;
                    case SPALTE_TAGSOLL:
                        posTitelSaldo = row.size() - 1;
                        row.add(makeZelleStunden(mZeitraum.getSoll()));
                        break;
                    case SPALTE_TAGSALDO:
                        posTitelSaldo = row.size() - 1;
                        row.add(makeZelleStunden((mZeitraum.getDifferenz())));
                        break;
                    case SPALTE_VERDIENST:
                        mZeit.set(mZeitraum.getIstNetto());
                        row.add(makeZelleWert(
                                mZeit.getAlsDezimalZeit() * mArbeitsplatz.getStundenlohn()
                        ));
                        break;
                    default:
                        if (spalte.mSpalte >= SPALTE_ZUSATZ) {
                            row.add(makeZelleZusatzwert(
                                    mZeitraum.getZusatzeintragSummenListe().get(spalte.mSpalte - SPALTE_ZUSATZ),
                                    false
                            ));
                        } else {
                            row.add(makeZelleString(""));
                        }
                }
            }
            tabelle.add(row);
        }
        // Soll und Differenz ans untere Ende der tabelle
        if (mOptionen.get(OPTION_ZEILE_SALDOZEITRAUM)) {
            // wenn die Soll/Ist Differenz nicht angezeigt
            // Netto
            if (!mOptionen.get(OPTION_ZEILE_SUMMEZEITRAUM) || !mSpalten.get(SPALTE_NETTO)) {
                row = new ArrayList<>();
                for (int i = 0; i < posTitelSaldo; i++) {
                    row.add(makeZelleLeer());
                }
                row.add(makeZelleString(ASetup.res.getString(R.string.ist)));
                row.add(makeZelleStunden(mZeitraum.getIstNetto()));
                tabelle.add(row);
            }

            // Soll
            if (!mOptionen.get(OPTION_ZEILE_SUMMEZEITRAUM) || !mSpalten.get(SPALTE_TAGSOLL)) {
                row = new ArrayList<>();
                for (int i = 0; i < posTitelSaldo; i++) {
                    row.add(makeZelleLeer());
                }
                row.add(makeZelleString(ASetup.res.getString(R.string.soll)));
                row.add(makeZelleStunden(mZeitraum.getSoll()));
                tabelle.add(row);
            }

            // Differenz
            if (!mOptionen.get(OPTION_ZEILE_SUMMEZEITRAUM) || !mSpalten.get(SPALTE_TAGSALDO)) {
                row = new ArrayList<>();
                for (int i = 0; i < posTitelSaldo; i++) {
                    row.add(makeZelleLeer());
                }
                row.add(makeZelleString(ASetup.res.getString(R.string.diff)));
                row.add(makeZelleStunden(mZeitraum.getDifferenz()));
                tabelle.add(row);
            }

            if (mZeitraum.isMonat()) {
                // Saldo Vormonat
                row = new ArrayList<>();
                for (int i = 0; i < posTitelSaldo; i++) {
                    row.add(makeZelleLeer());
                }
                row.add(makeZelleString(ASetup.res.getString(R.string.saldo_vm)));
                row.add(makeZelleStunden(mZeitraum.getSaldoUebertrag()));
                tabelle.add(row);
                // ausgezahlte Überstunden
                row = new ArrayList<>();
                for (int i = 0; i < posTitelSaldo; i++) {
                    row.add(makeZelleLeer());
                }
                row.add(makeZelleString(ASetup.res.getString(R.string.ueberstunden_ausbezahlt)));
                row.add(makeZelleStunden(mZeitraum.getAuszahlung()));
                tabelle.add(row);
            }
                // Saldo
                row = new ArrayList<>();
                for (int i = 0; i < posTitelSaldo; i++) {
                    row.add(makeZelleLeer());
                }
                row.add(makeZelleString(ASetup.res.getString(R.string.saldo)));
                row.add(makeZelleStunden(mZeitraum.getSaldo()));
                tabelle.add(row);
        }

        // Urlaubszusammenfassung
        if (mZeitraum.isMonat()) {
            if (mArbeitsplatz.getSoll_Urlaub() > 0) {
                Arbeitsjahr_summe mJahr = new Arbeitsjahr_summe(
                        mZeitraum.getBeginn().get((Calendar.YEAR)),
                        mArbeitsplatz
                );
                int monat = mZeitraum.getBeginn().get(Calendar.MONTH);
                Datum mAktuell = mArbeitsplatz.getAbrechnungsmonat(ASetup.aktDatum);

                float mRestSoll = mJahr.getResturlaub();
                float mRestBezogen = mJahr.bezogenRest[monat];
                float mRestIst = mJahr.restUrlaubIstMonat[monat];
                float mRestSaldo = mRestSoll - mRestIst;

                float mBezogenMonat = mJahr.bezogenUrlaub[monat];
                float mBezogenGesamt = mJahr.istUrlaubMonat[monat];
                float mAnspruch = mJahr.getUrlaubSoll();
                float mGeplant = mJahr.getUrlaubGeplant();
                float mGeplantMonat;
                float mGeplantResturlaub;
                boolean isVerfallen = mJahr.isUrlaubVerfallen(monat);
                DecimalFormat anzeigeFormat =
                        mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN) ?
                                ASetup.stundenformat :
                                ASetup.tageformat;

                // aufteilen des geplanten Urlaubs für diesen Monat
                // in Anteil zum Abbau von Resturlaub und regulären Urlaubsanspruch
                if (mZeitraum.getBeginn().istGleich(mAktuell, Calendar.MONTH)) {
                    mGeplantMonat = mJahr.geplanterUrlaubAktMonat;

                    if (mRestSaldo > 0) {
                        mGeplantResturlaub = mGeplantMonat;
                        // den zuviel vom Resturlaub abgezogenen Urlaub auf den geplanten Urlaub übertragen
                        if (mRestSaldo - mGeplantResturlaub < 0) {
                            mGeplantMonat = 0 - (mRestSaldo - mGeplantResturlaub);
                            mGeplantResturlaub -= mGeplantMonat;
                        } else {
                            mGeplantMonat = 0;
                        }
                        // den bezogenen Resturlaub korrigieren
                        mRestBezogen += mGeplantResturlaub;

                        mRestIst += mGeplantResturlaub;
                        mRestSaldo -= mGeplantResturlaub;
                    }
                }

                // zwei Leerzeilen einfügen
                makeLeerzeile(tabelle, 2);
                row = new ArrayList<>();
                // Titelzeile
                //String t = ASetup.res.getString(R.string.urlaub);
                if (mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN)) {
                    row.add(makeZelleString(ASetup.res.getString(R.string.urlaub_stunden)));
                } else {
                    row.add(makeZelleString(ASetup.res.getString(R.string.urlaub_tage)));
                }

                tabelle.add(row);
                // Resturlaub
                row = new ArrayList<>();
                row.add(makeZelleString(
                        ASetup.res.getString(R.string.titel_resturlaub, mJahr.Jahr-1)));
                tabelle.add(row);

                row = new ArrayList<>();
                row.add(makeZelleString(ASetup.res.getString(R.string.resturlaub)));
                row.add(makeZelleWert(mRestSoll));
                tabelle.add(row);
                // Resturlaub bezogen in diesen Monat
                row = new ArrayList<>();
                row.add(makeZelleString(ASetup.res.getString(R.string.abgebaut_monat)));
                if (mRestSoll > 0)
                    row.add(makeZelleWert(mRestBezogen));
                tabelle.add(row);

                // Resturlaub bezogen
                row = new ArrayList<>();
                row.add(makeZelleString(ASetup.res.getString(R.string.abgebaut_gesamt)));
                row.add(makeZelleWert(mRestIst));
                tabelle.add(row);

                // Resturlaub Saldo
                if (mRestSaldo < 0)
                    mRestSaldo = 0;
                row = new ArrayList<>();
                row.add(makeZelleString(ASetup.res.getString(R.string.rest)));
                row.add(makeZelleWert(mRestSaldo));
                if (mJahr.isUrlaubVerfallen(monat))
                    row.add(makeZelleString("("+ASetup.res.getString(R.string.verfallen)+")"));
                tabelle.add(row);

                // Urlaubsanspruch
                makeLeerzeile(tabelle,1);
                row = new ArrayList<>();
                row.add(makeZelleString(
                        ASetup.res.getString(R.string.titel_anspruch, mJahr.Jahr)
                ));
                tabelle.add(row);

                row = new ArrayList<>();
                row.add(makeZelleString(ASetup.res.getString(R.string.anspruch)));
                row.add(makeZelleWert(mAnspruch));
                tabelle.add(row);
                // Urlaub bezogen diesen Monat
                row = new ArrayList<>();
                row.add(makeZelleString(ASetup.res.getString(R.string.bezogen_monat)));
                row.add(makeZelleWert(mBezogenMonat));
                tabelle.add(row);
                // Urlaub bezogen bis zu diesen Monat
                row = new ArrayList<>();
                row.add(makeZelleString(ASetup.res.getString(R.string.bezogen_gesamt)));
                row.add(makeZelleWert(mBezogenGesamt));
                tabelle.add(row);

                // Urlaub Saldo + Resturlaub Saldo
                if (mRestSaldo > 0 && !isVerfallen) {
                    // Saldo Urlaubsanspruch
                    mAnspruch -= mBezogenGesamt;
                    row = new ArrayList<>();
                    row.add(makeZelleString(ASetup.res.getString(R.string.rest)));
                    row.add(makeZelleWert(mAnspruch));
                    tabelle.add(row);

                    // Saldo Urlaubsanspruch + Resturlaub
                    makeLeerzeile(tabelle,1);
                    row = new ArrayList<>();
                    row.add(makeZelleString(ASetup.res.getString(R.string.restanspruch)));
                    row.add(makeZelleWert(mAnspruch + mRestSaldo));tabelle.add(row);
                } else {
                    // Saldo Urlaubsanspruch
                    makeLeerzeile(tabelle,1);
                    row = new ArrayList<>();
                    row.add(makeZelleString(ASetup.res.getString(R.string.restanspruch)));
                    row.add(makeZelleWert(mAnspruch - mBezogenGesamt));
                    tabelle.add(row);
                }


                // wenn noch geplanter Urlaub offen ist und es der aktuelle Monat ist
                if (mZeitraum.getBeginn().istGleich(mAktuell, Calendar.MONTH) && mGeplant > 0) {
                    makeLeerzeile(tabelle,1);
                    row = new ArrayList<>();
                    row.add(makeZelleString(ASetup.res.getString(R.string.geplant_urlaub)));
                    row.add(makeZelleWert(mGeplant));
                    tabelle.add(row);
                }
            }
        }
    }

}
