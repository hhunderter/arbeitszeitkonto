/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.widget.CompoundButtonCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.BitSet;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzfeldDefinition;

public class SpaltenExportViewAdapter
        extends RecyclerView.Adapter<SpaltenExportViewAdapter.ViewHolder>{
    private final ArrayList<AExportBasis.Spalte> mSpalten;
    private final LayoutInflater mInflater;
    private final ButtonClickListener mCallback;
    private final Arbeitsplatz mArbeitsplatz;
    private final BitSet bSpalten;
    private final BitSet bZusatzwerte;
    private final BitSet bSpaltenDeaktiviert;

    public SpaltenExportViewAdapter(
            Context context,
            Arbeitsplatz job,
            BitSet spalten,
            BitSet zusatzwerte,
            BitSet deaktiv,
            ButtonClickListener listener) {
        mArbeitsplatz = job;
        mInflater = LayoutInflater.from(context);
        mCallback = listener;
        bSpalten = spalten;
        bZusatzwerte = zusatzwerte;
        bSpaltenDeaktiviert = deaktiv;
        mSpalten = makeSpaltenSet();
    }


    // erzeugt eine Liste aller Spalten
    private ArrayList<AExportBasis.Spalte> makeSpaltenSet() {
        ArrayList<AExportBasis.Spalte> listSpalten = new ArrayList<>();
        String[] namen = ASetup.res.getStringArray(R.array.export_spalten_dialog);
        //int spalteNetto = 0;
        //int spalteVerdienst = 0;
        boolean isText;

        // die regulären Spalten
        for (int s = 0; s <= IExport_Basis.DEF_MAXBIT_SPALTE; s++) {
            if(!bSpaltenDeaktiviert.get(s)) {
                isText = s==IExport_Basis.SPALTE_DATUM ||
                        s==IExport_Basis.SPALTE_EORT ||
                        s==IExport_Basis.SPALTE_SCHICHTNAME ||
                        s==IExport_Basis.SPALTE_ANGELEGT ||
                        s==IExport_Basis.SPALTE_AENDERUNG;
                listSpalten.add(new AExportBasis.Spalte(
                        s,
                        namen[s],
                        isText));
            }
        }

        // die Zusatzwerte
        for (int z = 0; z < mArbeitsplatz.getZusatzfeldListe().size(); z++) {
            ZusatzfeldDefinition zDef = mArbeitsplatz.getZusatzDefinition(z);
            if (zDef != null) {
                int s = IExport_Basis.SPALTE_ZUSATZ + z;
                listSpalten.add(new AExportBasis.Spalte(
                        s,
                        zDef.getName(),
                        (zDef.getTyp() == IZusatzfeld.TYP_TEXT)));
            }
        }

        return listSpalten;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_zusatzfeld_export, parent, false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final int spalte = getItem(position).mSpalte;

        holder.mButton.setText(getItem(position).mName);
        holder.mButton.setChecked(
                (spalte < IExport_Basis.SPALTE_ZUSATZ) ?
                        bSpalten.get(spalte) :
                        bZusatzwerte.get(spalte - IExport_Basis.SPALTE_ZUSATZ)
        );

        if(spalte <= IExport_Basis.DEF_MAXBIT_SPALTE && bSpaltenDeaktiviert.get(spalte)){
          holder.mButton.setEnabled(false);
        } else {
            holder.mButton.setOnCheckedChangeListener((compoundButton, b) -> {
                if(mCallback != null)
                    mCallback.onButtonClick(spalte, b);
            });
        }
    }

    @Override
    public int getItemCount() {
        return mSpalten.size();
    }

    AExportBasis.Spalte getItem(int index) {
        return mSpalten.get(index);
    }


    // parent activity will implement this method to respond to click events
    public interface ButtonClickListener {
        void onButtonClick(int button, boolean eingeschaltet);
    }

    // stores and recycles views as they are scrolled off screen
    public static class ViewHolder extends RecyclerView.ViewHolder{
        AppCompatCheckBox mButton;

        @SuppressLint("RestrictedApi")
        ViewHolder(View itemView) {
            super(itemView);
            mButton = itemView.findViewById(R.id.ZE_option);
            CompoundButtonCompat.setButtonTintList(
                    mButton,
                    ASetup.aktJob.getFarbe_Radio());
        }
    }
}

