/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import android.content.Context;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Locale;

import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.abwesenheiten.AbwesenheitListe;
import askanimus.arbeitszeiterfassung2.arbeitsjahr.Arbeitsjahr_summe;
import askanimus.arbeitszeiterfassung2.arbeitsmonat.Arbeitsmonat;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.arbeitsschicht.SchichtListeJahr;
import askanimus.arbeitszeiterfassung2.einsatzort.Einsatzort;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASetup;


/**
 * @author askanimus@gmail.com on 10.01.16.
 */
public class Export_CSV_Jahr  extends AExportBasis_CSV {
    private Arbeitsjahr_summe mJahr;
    private Arbeitsplatz mArbeitsplatz;
    private ArrayList<ArrayList<Zelle_CSV>>  mTabelle = new ArrayList<>();
    private BitSet mTabellen;
    private BitSet mZusatzwerte;

    Export_CSV_Jahr(
            Context context,
            Arbeitsplatz job,
            long beginn,
            //String pfad,
            StorageHelper storageHelper,
            BitSet tabellen,
            BitSet zusatzwerte
    ) throws Exception {
        super(context);
        Calendar mBeginn = Calendar.getInstance();
        mBeginn.setTimeInMillis(beginn);
        mTabellen = tabellen;
        mZusatzwerte = zusatzwerte;
        mArbeitsplatz = job;

        mJahr = new Arbeitsjahr_summe(mBeginn.get(Calendar.YEAR), mArbeitsplatz);

        /*String mDateiname = context.getString(
                R.string.exp_dateiname_zeitraum,
                getBasisDateiname(),
                context.getString(R.string.jahr),
                String.valueOf(mBeginn.get(Calendar.YEAR)),
                ""
        );*/
        //mDateiname = ExportActivity.makeExportpfad(context, pfad, mDateiname);

        schreibeTabelle(
                storageHelper,
                context.getString(
                        R.string.exp_dateiname_zeitraum,
                        getBasisDateiname(),
                        context.getString(R.string.jahr),
                        String.valueOf(mBeginn.get(Calendar.YEAR)),
                        ""
                ),
                TYP_CSV);
    }
    
    // Tabelle zusammenstellen
    @Override
    public ArrayList<ArrayList<Zelle_CSV>> erzeugeTabelle(){
        // den Seitentitel schreiben
        erzeugeSeitenTitel(mTabelle, mArbeitsplatz, String.valueOf(mJahr.Jahr));

        // Arbeitsstunden
        if(mTabellen.get(TAB_ARBEITSZEIT))
            tabelleStunden();

        // Arbeitstage
        if(mTabellen.get(TAB_ARBEITSTAGE)) {
            makeLeerzeile(mTabelle,1);
            tabelleTage();
        }

        // abwesenheiten
        if(mTabellen.get(TAB_ABWESENHEITEN)) {
            makeLeerzeile(mTabelle,1);
            tabelleAbwesenheiten();
        }

        // Sonstige Werte
        if(mTabellen.get(TAB_ZUSATZ)) {
            makeLeerzeile(mTabelle,1);
            tabelleZusatzwerte();
        }

        // Urlaub
        if(mTabellen.get(TAB_URLAUB)) {
            makeLeerzeile(mTabelle,1);
            tabelleUrlaub();
        }

        // Krank
        if(mTabellen.get(TAB_KRANK)) {
            makeLeerzeile(mTabelle,1);
            tabelleKrank();
        }

        // Unfall
        if(mTabellen.get(TAB_UNFALL)) {
            makeLeerzeile(mTabelle,1);
            tabelleUnfall();
        }
        
        // Einsatzort
        if(mTabellen.get(TAB_EINSATZORTE)) {
            makeLeerzeile(mTabelle,1);
            tabelleEinsatzort();
        }

        // Schichten
        if(mTabellen.get(TAB_SCHICHTEN)) {
            makeLeerzeile(mTabelle,1);
            tabelleSchichten();
        }
        return mTabelle;
    }

    // Teiltabelle - Arbeitszeit
    private void tabelleStunden(){
        /*
         * Arbeitszeiten
         */

        // Überschrift
        ArrayList<Zelle_CSV> rowTitel = new ArrayList<>();
        rowTitel.add(new Zelle_CSV(ASetup.res.getString(R.string.arbeitsstunden)));
        mTabelle.add(rowTitel);

        // Kopfzeile
        erzeugeTabellenKopf();

        // Datenzeilen
        // Kopfspalte
        ArrayList<Zelle_CSV> rowSoll = new ArrayList<>();
        ArrayList<Zelle_CSV> rowIst = new ArrayList<>();
        ArrayList<Zelle_CSV> rowDiff = new ArrayList<>();
        ArrayList<Zelle_CSV> rowVormonat = new ArrayList<>();
        ArrayList<Zelle_CSV> rowAusbezahlt = new ArrayList<>();
        ArrayList<Zelle_CSV> rowSaldo = new ArrayList<>();

        rowSoll.add(new Zelle_CSV( ASetup.res.getString(R.string.soll)));
        rowIst.add(new Zelle_CSV( ASetup.res.getString(R.string.ist)));
        rowDiff.add(new Zelle_CSV( ASetup.res.getString(R.string.diff)));
        rowVormonat.add(new Zelle_CSV( ASetup.res.getString(R.string.saldo_vm)));
        rowAusbezahlt.add(new Zelle_CSV( ASetup.res.getString(R.string.ueberstunden_ausbezahlt)));
        rowSaldo.add(new Zelle_CSV( ASetup.res.getString(R.string.saldo)));

        //Monate
        for (int i = 0; i < mJahr.getAnzahlMonate(); i++) {
            rowSoll.add(new Zelle_CSV(mJahr.getArbeitszeitSollMonat(i), false));

            rowIst.add(new Zelle_CSV(mJahr.getArbeitszeitIstMonat(i), false));

            rowDiff.add(new Zelle_CSV(mJahr.getArbeitszeitDiffMonat(i), false));

            rowVormonat.add(new Zelle_CSV(mJahr.getArbeitszeitSaldoVormonat(i), false));

            rowAusbezahlt.add(new Zelle_CSV(mJahr.getArbeitszeitAusbezahltMonat(i), false ));

            rowSaldo.add(new Zelle_CSV(mJahr.getArbeitszeitSaldoMonat(i), false));
        }

        // Summenspalte
        rowSoll.add(new Zelle_CSV(mJahr.getSoll(), false));

        rowIst.add(new Zelle_CSV(mJahr.getIst(), false));

        rowDiff.add(new Zelle_CSV(mJahr.getDifferenz(), false));

        rowVormonat.add(new Zelle_CSV(mJahr.getSaldoVorjahr(), false));

        rowAusbezahlt.add(new Zelle_CSV(mJahr.getAusgezahlt(), false));

        rowSaldo.add(new Zelle_CSV(mJahr.getSaldo(), false));

        // Zeilen zur Tabelle hinzu fügen
        mTabelle.add(rowSoll);
        mTabelle.add(rowIst);
        mTabelle.add(rowDiff);
        mTabelle.add(rowVormonat);
        mTabelle.add(rowAusbezahlt);
        mTabelle.add(rowSaldo);
    }

    // Teiltabelle - Arbeitstage
    private void tabelleTage() {

        // Überschrift
        ArrayList<Zelle_CSV> rowTitel = new ArrayList<>();
        rowTitel.add(new Zelle_CSV(ASetup.res.getString(R.string.arbeitstage)));
        mTabelle.add(rowTitel);

        // Kopfzeile
        erzeugeTabellenKopf();

        // Datenzeilen

        // Kopfspalte
        ArrayList<Zelle_CSV> rowSoll = new ArrayList<>();
        ArrayList<Zelle_CSV> rowIst = new ArrayList<>();
        ArrayList<Zelle_CSV> rowDiff = new ArrayList<>();

        rowSoll.add(new Zelle_CSV( ASetup.res.getString(R.string.soll)));
        rowIst.add(new Zelle_CSV( ASetup.res.getString(R.string.ist)));
        rowDiff.add(new Zelle_CSV( ASetup.res.getString(R.string.diff)));

        //Monate
        float mSoll;
        float mIst;
        float mSummeSoll = 0;
        float mSummeIst = 0;
        for (int i = 0; i < mJahr.getAnzahlMonate(); i++) {
            mSoll = (mJahr.listMonate.get(i)).getSollArbeitsTage(false);
            mIst = (mJahr.listMonate.get(i)).getSummeKategorieTage(Abwesenheit.KAT_ARBEITSZEIT);
            mSummeSoll += mSoll;
            mSummeIst += mIst;
            rowSoll.add(new Zelle_CSV(mSoll));
            rowIst.add(new Zelle_CSV(mIst));
            mIst -= mSoll;
            rowDiff.add(new Zelle_CSV(mIst));
        }

        // Summenspalte
        rowSoll.add(new Zelle_CSV(mSummeSoll));

        rowIst.add(new Zelle_CSV(mSummeIst));

        mSummeIst -= mSummeSoll;
        rowDiff.add(new Zelle_CSV(mSummeIst));

        // Zeilen zur Tabelle hinzu fügen
        mTabelle.add(rowSoll);
        mTabelle.add(rowIst);
        mTabelle.add(rowDiff);


    }

    // Teiltabelle - abwesenheiten
    private void tabelleAbwesenheiten() {

        // Überschrift
        ArrayList<Zelle_CSV> rowTitel = new ArrayList<>();
        rowTitel.add(new Zelle_CSV(ASetup.res.getString(R.string.summe_tage)));
        mTabelle.add(rowTitel);

        // Kopfzeile
        erzeugeTabellenKopf();

        ArrayList<Zelle_CSV> mZeile;
        ArrayList<Float> mAbwesenheit;

        for (int a = 0; a < mJahr.getAbwesenheiten().get(mJahr.INDEX_SUMME).size(); a++) {
            mZeile = new ArrayList<>();
            mAbwesenheit = mJahr.getAbwesenheitMonate(a);

            if( mAbwesenheit.get(mJahr.INDEX_SUMME) > 0) {
                mZeile.add(new Zelle_CSV( ASetup.aktJob.getAbwesenheiten().get(a).getName()));
                for (int m = 1; m <= mJahr.getAnzahlMonate(); m++){
                   mZeile.add(new Zelle_CSV(mAbwesenheit.get(m)));
                }
                mZeile.add(new Zelle_CSV(mAbwesenheit.get(mJahr.INDEX_SUMME)));
                mTabelle.add(mZeile);
            }
        }
    }

    // Teiltabelle - Sonstige Werte
    private void tabelleZusatzwerte() {
        // Überschrift
        ArrayList<Zelle_CSV> rowTitel = new ArrayList<>();
        rowTitel.add(new Zelle_CSV(ASetup.res.getString(R.string.sonstige)));
        mTabelle.add(rowTitel);

        // Kopfzeile
        erzeugeTabellenKopf();

        // die Zeilen
        for (int zf = 0; zf< mZusatzwerte.size();zf++) {
            if (mZusatzwerte.get(zf)) {
                ArrayList<Zelle_CSV> zeile = new ArrayList<>();
                // Der Name des Zusatzeintrages
                zeile.add(new Zelle_CSV(
                        mArbeitsplatz.getZusatzDefinition(zf).getName()
                ));
                // Monate
                for (int i = 0; i < mJahr.getAnzahlMonate(); i++) {
                    zeile.add(new Zelle_CSV(
                            (mJahr.listMonate.get(i))
                                    .getSummeZusatzeintrag(zf)
                                    .getStringWert(false))
                    );
                }
                //Summe
                zeile.add(new Zelle_CSV(mJahr.mZusatzwerteSumme.get(zf).getStringWert(false)));
                mTabelle.add(zeile);
            }
        }
    }

    // Teiltabelle - Urlaub
    private void tabelleUrlaub() {
        float mUrlaub = mJahr.getUrlaubSoll();
        float mResturlaub = mJahr.getResturlaub();
        // Überschrift
        ArrayList<Zelle_CSV> rowTitel = new ArrayList<>();
        rowTitel.add(new Zelle_CSV(String.format(
                        ASetup.res.getString(R.string.exp_titel_urlaub),
                        mJahr.getJahr()-1,
                        ASetup.zahlenformat.format(mJahr.getResturlaub()),
                        mJahr.getJahr(),
                        ASetup.zahlenformat.format(mJahr.getUrlaubSoll())
                        )));
        mTabelle.add(rowTitel);

        // Kopfzeile
        erzeugeTabellenKopf();

        // die Zeilen
        // ArrayList<CSVzelle> zRestAnspruch = new ArrayList<CSVzelle>();
        ArrayList<Zelle_CSV> zRestBezogen = new ArrayList<>();
        ArrayList<Zelle_CSV> zRestSaldo = new ArrayList<>();
        //ArrayList<CSVzelle> zAnspruch = new ArrayList<>();
        ArrayList<Zelle_CSV> zBezogen = new ArrayList<>();
        ArrayList<Zelle_CSV> zSaldo = new ArrayList<>();

        // die Zeilenköpfe
        //zRestAnspruch.add(new CSVzelle( Einstellungen.res.getString(R.string.resturlaub)));
        zRestBezogen.add(new Zelle_CSV( ASetup.res.getString(R.string.abgebaut)));
        zRestSaldo.add(new Zelle_CSV( ASetup.res.getString(R.string.rest)));
        //zAnspruch.add(new CSVzelle( Einstellungen.res.getString(R.string.anspruch)));
        zBezogen.add(new Zelle_CSV( ASetup.res.getString(R.string.bezogen)));
        zSaldo.add(new Zelle_CSV( ASetup.res.getString(R.string.saldo)));

        // die Monate
        int i;
        for (Object m : mJahr.listMonate) {
            Arbeitsmonat mMonat = (Arbeitsmonat)m;
            i =mMonat.getMonat();
            //zRestAnspruch.add(new CSVzelle( Einstellungen.zahlenformat.format(mJahr))));
            zRestBezogen.add(new Zelle_CSV(mJahr.bezogenRest[i]));
            mResturlaub -= mJahr.bezogenRest[i];
            zRestSaldo.add(new Zelle_CSV(mResturlaub));
            //zAnspruch.add(new CSVzelle( Einstellungen.res.getString(R.string.anspruch)));
            zBezogen.add(new Zelle_CSV(mJahr.bezogenUrlaub[i]));
            mUrlaub -= mJahr.bezogenUrlaub[i];
            zSaldo.add(new Zelle_CSV(mUrlaub));
        }

        // die Summen Spalte
        zRestBezogen.add(new Zelle_CSV(mJahr.getResturlaubIst()));
        zRestSaldo.add(new Zelle_CSV(mResturlaub));

        zBezogen.add(new Zelle_CSV(mJahr.getIstUrlaub()));
        zSaldo.add(new Zelle_CSV(mJahr.getUrlaubSaldo()));

        // Zeilen in die Tabelle einfügen
        mTabelle.add(zRestBezogen);
        mTabelle.add(zRestSaldo);
        mTabelle.add(zBezogen);
        mTabelle.add(zSaldo);
    }

    // Teiltabelle - Krankentage
    private void tabelleKrank() {
        ArrayList<ArrayList<Float>> wAbwesenheiten = mJahr.getAbwesenheiten();
        AbwesenheitListe mAbwesenheiten = mArbeitsplatz.getAbwesenheiten();
        //float mGesamt[] = new float[12];
        float[] mGesamt = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        float jGesamt = 0;

        // Überschrift
        ArrayList<Zelle_CSV> rowTitel = new ArrayList<>();
        rowTitel.add(new Zelle_CSV(ASetup.res.getString(R.string.krank)));
        mTabelle.add(rowTitel);

        // Kopfzeile
        erzeugeTabellenKopf();

        // die Zeilen anlegen
        for (int i = 0; i < mAbwesenheiten.sizeAktive(); i++) {
            if (mAbwesenheiten.getAktive(i).getKategorie() == Abwesenheit.KAT_KRANK) {
                ArrayList<Zelle_CSV> kAbwesenheit = new ArrayList<>();
                kAbwesenheit.add(new Zelle_CSV( mAbwesenheiten.getAktive(i).getName()));

                for (int m = 1; m < wAbwesenheiten.size(); m++) {
                    kAbwesenheit.add(new Zelle_CSV(wAbwesenheiten.get(m).get(i)));
                    mGesamt[m-1] += wAbwesenheiten.get(m).get(i);
                }
                kAbwesenheit.add(new Zelle_CSV(wAbwesenheiten.get(mJahr.INDEX_SUMME).get(i)));
                mTabelle.add(kAbwesenheit);
                jGesamt += wAbwesenheiten.get(mJahr.INDEX_SUMME).get(i);
            }
        }
        ArrayList<Zelle_CSV> kGesamt = new ArrayList<>();
        kGesamt.add(new Zelle_CSV( ASetup.res.getString(R.string.gesamt)));
        for (int m = 1; m < wAbwesenheiten.size(); m++) {
            kGesamt.add(new Zelle_CSV(mGesamt[m-1]));
        }
        kGesamt.add(new Zelle_CSV(jGesamt));
        mTabelle.add(kGesamt);

    }

    // Teiltabelle - Unfalltage
    private void tabelleUnfall() {
        ArrayList<ArrayList<Float>> wAbwesenheiten = mJahr.getAbwesenheiten();
        AbwesenheitListe mAbwesenheiten = mArbeitsplatz.getAbwesenheiten();
        //float mGesamt[] = new float[12];
        float[] mGesamt = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        float jGesamt = 0;

        // Überschrift
        ArrayList<Zelle_CSV> rowTitel = new ArrayList<>();
        rowTitel.add(new Zelle_CSV(ASetup.res.getString(R.string.unfall)));
        mTabelle.add(rowTitel);

        // Kopfzeile
        erzeugeTabellenKopf();

        // die Zeilen anlegen
        for (int i = 0; i < mAbwesenheiten.sizeAktive(); i++) {
            if (mAbwesenheiten.getAktive(i).getKategorie() == Abwesenheit.KAT_UNFALL) {
                ArrayList<Zelle_CSV> kAbwesenheit = new ArrayList<>();
                kAbwesenheit.add(new Zelle_CSV( mAbwesenheiten.getAktive(i).getName()));
                for (int m = 1; m < wAbwesenheiten.size(); m++) {
                    kAbwesenheit.add(new Zelle_CSV(wAbwesenheiten.get(m).get(i)));
                    mGesamt[m-1] += wAbwesenheiten.get(m).get(i);
                }
                kAbwesenheit.add(new Zelle_CSV(wAbwesenheiten.get(mJahr.INDEX_SUMME).get(i)));
                mTabelle.add(kAbwesenheit);
                jGesamt += wAbwesenheiten.get(mJahr.INDEX_SUMME).get(i);
            }
        }
        ArrayList<Zelle_CSV> kGesamt = new ArrayList<>();
        kGesamt.add(new Zelle_CSV( ASetup.res.getString(R.string.gesamt)));
        for (int m = 1; m < wAbwesenheiten.size(); m++) {
            kGesamt.add(new Zelle_CSV(mGesamt[m-1]));
        }
        kGesamt.add(new Zelle_CSV(jGesamt));
        mTabelle.add(kGesamt);
    }
    
    // Teiltabelle - Einsatzorte
    private void tabelleEinsatzort() {
        // Überschrift
        ArrayList<Zelle_CSV> rowTitel = new ArrayList<>();
        rowTitel.add(new Zelle_CSV(ASetup.res.getString(R.string.einsatzort)));
        mTabelle.add(rowTitel);

        // Kopfzeile
        erzeugeTabellenKopf();

        // die Zeilen anlegen
        // ein Monat pro Spalte und ein einsatzort pro Zeile
        // eine Spalte Gesamt

        // die Liste der Einsatzorte
        ArrayList<long[]> tabEorte = new ArrayList<>();
        int aLang = mJahr.listMonate.size()+2;
        // die aktiven Orte
        for (Einsatzort o:mArbeitsplatz.getEinsatzortListe().getAlle()) {
            long[] eOrt_Monate = new long[aLang];
            int m = 1;
            for (Object mo:mJahr.listMonate) {
                Arbeitsmonat mMonat = (Arbeitsmonat)mo;
                int mMinuten = mMonat.getSummeEinsatzortMinuten(o.getId());
                if(mMinuten > 0){
                    eOrt_Monate[m] = mMinuten;
                    eOrt_Monate[aLang - 1] += mMinuten;
                }
                m ++;
            }
            if(eOrt_Monate[aLang-1] > 0) {
                eOrt_Monate[0] = o.getId();
                tabEorte.add(eOrt_Monate);
            }
        }
        // die einzelnen Zeilen anlegen
        Uhrzeit uz = new Uhrzeit();
        for (long[] eOrt_Monate:tabEorte) {
            ArrayList<Zelle_CSV> kEinsatzort = new ArrayList<>();
            kEinsatzort.add(new Zelle_CSV(
                    mArbeitsplatz.getEinsatzortListe().getOrt(eOrt_Monate[0]).getName()
            ));
            for (int m=1; m <= mJahr.listMonate.size(); m++){
                uz.set((int)eOrt_Monate[m]);
                kEinsatzort.add(new Zelle_CSV(uz.getStundenString(true, mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL))));
            }
            uz.set((int)eOrt_Monate[aLang-1]);
            kEinsatzort.add(new Zelle_CSV(uz.getStundenString(true, mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL))));
           mTabelle.add(kEinsatzort);
        }


    }

    // Teiltabelle Anzahl der Schichten in Tagen
    private void tabelleSchichten() {
        // die Liste der verwendeten Schichten anlegen inkl. der gesmtwerte des Jahres
        SchichtListeJahr benutztSchichten = new SchichtListeJahr(mJahr);

        if(benutztSchichten.size() > 0) {
            // Überschrift
            ArrayList<Zelle_CSV> rowTitel = new ArrayList<>();
            rowTitel.add(new Zelle_CSV(ASetup.res.getString(R.string.schichten)));
            mTabelle.add(rowTitel);

            // Kopfzeile
            erzeugeTabellenKopf();

            // die Tabelle einlesen
            // Die Schleife für die Zeilen (Schichten)
            for (SchichtListeJahr.Schicht mSchicht : benutztSchichten.getJahr()) {
                ArrayList<Zelle_CSV> mZeile = new ArrayList<>();
                // die Bezeichnung der Schicht in die erste Spalte
                mZeile.add(new Zelle_CSV(mSchicht.getName()));

                // Die Schleife für die Monate
                for (int i : mSchicht.getMonate()) {
                    mZeile.add(new Zelle_CSV(String.valueOf(i)));
                }
                // Summenspalte erzeugen
                mZeile.add(new Zelle_CSV(
                        ASetup.zahlenformat.format(mSchicht.getTageJahr()))
                );
                // die tabelle schreiben
                mTabelle.add(mZeile);
            }
        }
    }


    // den Tabellenkopf zusammensetzen
    private  void erzeugeTabellenKopf(){
        Datum mKal = new Datum(mJahr.Jahr, mJahr.getMonatBeginn(),1, ASetup.aktJob.getWochenbeginn());
        ArrayList<Zelle_CSV> row = new ArrayList<>();

        // Formater für Monatsname
        DateFormat fMonat = new SimpleDateFormat("MMMM", Locale.getDefault());

        row.add(new Zelle_CSV(""));
        for (int i = mJahr.getMonatBeginn(); i <= mJahr.getMonatEnde(); i++) {
            row.add(new Zelle_CSV( fMonat.format(mKal.getTime())));
            mKal.add(Calendar.MONTH, 1);
        }
        row.add(new Zelle_CSV( ASetup.res.getString(R.string.gesamt)));

        mTabelle.add(row);
    }
}
