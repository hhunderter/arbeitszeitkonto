/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import android.content.Context;

import com.pdfjet.Cell;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.Zeitraum.IZeitraum;
import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.arbeitsschicht.Arbeitsschicht;
import askanimus.arbeitszeiterfassung2.arbeitstag.Arbeitstag;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

class Export_PDF_Zeitraum_Kurz extends AExport_Zeitraum_PDF {
    //boolean farbeHintergrund;

    Export_PDF_Zeitraum_Kurz(
            Context context,
            IZeitraum zeitraum,
            BitSet spalten,
            BitSet optionen,
            BitSet zusatzwerte,
            String notiz,
            StorageHelper storageHelper
            //String pfad
    ) throws Exception {
        super(context, zeitraum, spalten, optionen, zusatzwerte, null);
        //farbeHintergrund = false;

        // den Dateiname zusammenstellen
        //String dateiname = mZeitraum.getDateiname(context, R.string.exp_mon_kurz);
        //dateiname = ExportActivity.makeExportpfad(context, pfad, dateiname);

        if (mOptionen.get(OPTION_ZUSATZTABELLE_EORT)) {
            setupSeite(
                    TABTYP_ZEITRAUM,
                    mOptionen,
                    false,
                    zeitraum.getTitel(context),
                    context.getString(R.string.sort_eort),
                    notiz,
                    zeitraum.getPDFFontSize()
            );
        } else {
            setupSeite(
                    TABTYP_ZEITRAUM,
                    mOptionen,
                    false,
                    zeitraum.getTitel(context),
                    null,
                    notiz,
                    zeitraum.getPDFFontSize()
            );
        }

        schreibeTabelle(
                storageHelper,
                mZeitraum.getDateiname(context, R.string.exp_mon_kurz),
                TYP_PDF
        );
    }


    @Override
    public void makeTageszeile(List<List<Cell>> tableData, Arbeitstag tag, boolean farbeHintergrund) {
        Uhrzeit mZeit = new Uhrzeit(0);
        ArrayList<Cell> row = new ArrayList<>();

        for (Spalte spalte : mSpaltenSatz) {
            switch (spalte.mSpalte) {
                case SPALTE_DATUM:
                    row.add(makeZelleKopf(fDatum.format(tag.getKalender().getTime())));
                    break;
                case SPALTE_SCHICHTNAME:
                    StringBuilder sSchicht = new StringBuilder();
                    for (Arbeitsschicht schicht : tag.getSchichten()) {
                        switch (schicht.getAbwesenheit().getKategorie()) {
                            case Abwesenheit.KAT_ARBEITSZEIT:
                                if (sSchicht.length() > 0) {
                                    sSchicht.append(", ");
                                }
                                sSchicht.append(schicht.getName());
                                break;
                            case Abwesenheit.KAT_KEINESCHICHT:
                                break;
                            default:
                                if (sSchicht.length() > 0) {
                                    sSchicht.append(", ");
                                }
                                sSchicht.append(schicht.getAbwesenheit().getName());
                        }
                    }
                    row.add(makeZelleString(sSchicht.toString(), farbeHintergrund));
                    break;
                case SPALTE_BRUTTO:
                    row.add(makeZelleStunden(tag.getTagBrutto(), isDezimal, false, farbeHintergrund));
                    break;
                case SPALTE_PAUSE:
                    row.add(makeZelleStunden(tag.getTagPause(), isDezimal, false, farbeHintergrund));
                    break;
                case SPALTE_NETTO:
                    row.add(makeZelleStunden(tag.getTagNetto(), isDezimal, false, farbeHintergrund));
                    break;
                case SPALTE_TAGSOLL:
                    row.add(makeZelleStunden(tag.getTagSollNetto(), isDezimal, false, farbeHintergrund));
                    break;
                case SPALTE_TAGSALDO:
                    row.add(makeZelleSummeStunden((tag.getTagNetto() - tag.getTagSollNetto()), isDezimal, false, false));
                    break;
                case SPALTE_VERDIENST:
                    mZeit.set(tag.getTagNetto());
                    row.add(makeZelleSummeWert(
                            mZeit.getAlsDezimalZeit() * mArbeitsplatz.getStundenlohn(),
                            ASetup.waehrungformat,
                            false));
                    break;
                default:
                    if (spalte.mSpalte >= SPALTE_ZUSATZ) {
                        row.add(makeZelleZusatzwert(
                                tag.getTagZusatzwert(spalte.mSpalte - SPALTE_ZUSATZ),
                                isDezimal,
                                farbeHintergrund,
                                true));
                    }
            }
        }
        tableData.add(row);

        farbeHintergrund = !farbeHintergrund;
    }
}
