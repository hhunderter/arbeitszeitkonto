/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import android.content.Context;

import java.util.BitSet;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.Zeitraum.IZeitraum;

/**
 * @author askanimus@gmail.com on 10.01.16.
 */
public class Export_PDF_Zeitraum extends AExport_Zeitraum_PDF{
    Export_PDF_Zeitraum(
            Context context,
            IZeitraum zeitraum,
            BitSet spalten,
            BitSet optionen,
            BitSet zusatzwerte,
            String notiz,
            StorageHelper storageHelper
            //String pfad
    ) throws Exception {
        super(context, zeitraum, spalten, optionen, zusatzwerte, null);

        // den Dateiname zusammenstellen
        //String dateiname = mZeitraum.getDateiname(context, R.string.exp_mon_ausf);
        //dateiname = ExportActivity.makeExportpfad(context, pfad, dateiname);

        if (mOptionen.get(OPTION_ZUSATZTABELLE_EORT)) {
            setupSeite(
                    TABTYP_ZEITRAUM,
                    mOptionen,
                    false,
                    zeitraum.getTitel(context),
                    context.getString(R.string.sort_eort),
                    notiz,
                    zeitraum.getPDFFontSize()
            );
        } else {
            setupSeite(
                    TABTYP_ZEITRAUM,
                    mOptionen,
                    false,
                    zeitraum.getTitel(context),
                    null,
                    notiz,
                    zeitraum.getPDFFontSize()
            );
        }

        schreibeTabelle(
                storageHelper,
                mZeitraum.getDateiname(context, 0),
                TYP_PDF
        );
    }
}
