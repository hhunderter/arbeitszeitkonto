/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import java.util.ArrayList;
import java.util.BitSet;

import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;


public interface IExport_Basis {

    // Defaultwerte
    int DEF_SPALTEN = 0b11111111111;
    int DEF_OPTIONEN = 0b11111;
    int DEF_TABELLEN = 0b1111111111;

    // Bitsetgrössen
    int DEF_MAXBIT_SPALTE = 12;
    int DEF_MAXBIT_OPTION = 12;
    int DEF_MAXBIT_TABELLEN = 9;

    // Spaltennummern
    int SPALTE_DATUM = 0;
    int SPALTE_SCHICHTNAME = 1;
    int SPALTE_EORT = 2;
    int SPALTE_VON = 3;
    int SPALTE_BIS = 4;
    int SPALTE_BRUTTO = 5;
    int SPALTE_PAUSE = 6;
    int SPALTE_NETTO = 7;
    int SPALTE_TAGSOLL = 8;
    int SPALTE_TAGSALDO = 9;
    int SPALTE_VERDIENST = 10;
    int SPALTE_ANGELEGT = 11;
    int SPALTE_AENDERUNG = 12;
    int SPALTE_ZUSATZ = 20; // + Zusatzfeld Index

    // Zeilen und Optionen
    int OPTION_ZEILE_SUMMETAG = 0;
    int OPTION_ZEILE_SUMMEZEITRAUM = 1;
    int OPTION_ZEILE_SOLLZEITRAUM = 2;
    int OPTION_ZEILE_SALDOZEITRAUM = 3;
    int OPTION_ZEILE_UNTERSCHRIFT = 4;
    int OPTION_LAYOUT_QUEER = 5;
    //int OPTION_ZEILE_NOTIZ = 6;
    int OPTION_ZUSATZTABELLE_EORT = 6;
    int OPTION_ALL_JOBS = 7;
    int OPTION_NUR_ORTE = 8;
    int OPTION_TABELLE_JE_ORT = 9;
    int OPTION_ZUSAMMENFASSUNG = 10;
    int OPTION_LAYOUT_A3 = 11;
    int OPTION_WOCHENNUMMER = 12;

    // Tabellen
    int TAB_ARBEITSZEIT = 0;
    int TAB_ARBEITSTAGE = 1;
    int TAB_ABWESENHEITEN = 2;
    int TAB_URLAUB = 3;
    int TAB_KRANK = 4;
    int TAB_UNFALL = 5;
    int TAB_VERDIENST = 6;
    int TAB_SCHWELLEN = 7;
    int TAB_EINSATZORTE = 8;
    int TAB_SCHICHTEN = 9;
    int TAB_ZUSATZ = 20;  // + Zusatzfeld Index

    //
    // Exportperioden
    //
    int PERIODE_WOCHE = 0;
    int PERIODE_MONAT = 1;
    int PERIODE_JAHR = 2;
    int PERIODE_EORT = 3;
    int PERIODE_ZEITRAUM = 4;

    //
    // Aktionen mit exportierten Daten
    //
    int ACTION_SAVE = 0;
    int ACTION_SHARE = 1;
    int ACTION_SEND = 2;


    //
    // Exporttypen
    //
    int TYP_PDF = 0;
    int TYP_CSV = 1;
    int TYP_CAL_CSV = 2;
    int TYP_CAL_ICS = 3;

    //
    // Dateitypen
    //
    String DATEI_TYP_PDF = "application/pdf";
    String DATEI_TYP_TEXT = "text/plain";
    String DATEI_TYP_CSV = "text/csv";
    String DATEI_TYP_ICS = "text/calendar";
    String DATEI_TYP_XML = "text/xml";

    //
    // export Varianten für Monatsreport
    //
    int AUSFUERLICH = 1;
    int VERKUERZT = 2;
    int LGAV = 3;

    //
    // kleinste Schriftgrösse in PDF Berichten
    //
    int MIN_FONTSIZE = 4;

    //
    // Größe für das Unterschriftenfeld
    //
    float UNTERSCHRIFT_W = 175.00f;
    float UNTERSCHRIFT_H = 45.00f;

    void oeffneAusgabe() throws Exception;

    void schreibeSeiten() throws Exception;

    void schliesseAusgabe() throws Exception;

    ArrayList<AExportBasis.Spalte> makeSpaltenSet(
            BitSet spalten,
            BitSet zusatzwerte,
            Arbeitsplatz arbeitsplatz);
}