/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.BitSet;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

/**
 * @author askanimus@gmail.com on 17.05.16.
 */
public class Fragment_Dialog_Spalten extends DialogFragment
        implements SpaltenExportViewAdapter.ButtonClickListener{
    private EditSpaltenDialogListener mListener;

    private BitSet bsSpalten;
    private BitSet bsSpaltenDeaktiv;
    private BitSet bsZusatzfelder;


    public void setup(BitSet spalten, BitSet deaktiv, BitSet zusatzwerte, EditSpaltenDialogListener listener){
        bsSpalten = (BitSet)spalten.clone();
        bsSpaltenDeaktiv = deaktiv;
        bsZusatzfelder = (BitSet)zusatzwerte.clone();
        mListener = listener;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(bsSpalten != null) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            View mInhalt = inflater.inflate(R.layout.fragment_dialog_spalten_export, null);

            RecyclerView gSpalten = mInhalt.findViewById(R.id.DS_liste);
            SpaltenExportViewAdapter spaltenViewAdapter =
                    new SpaltenExportViewAdapter(
                            getContext(),
                            ASetup.aktJob,
                            bsSpalten,
                            bsZusatzfelder,
                            bsSpaltenDeaktiv,
                            this);
            GridLayoutManager layoutManger = new GridLayoutManager(getContext(), 2);
            gSpalten.setAdapter(spaltenViewAdapter);
            gSpalten.setLayoutManager(layoutManger);


            return new AlertDialog.Builder(getActivity())
                    .setView(mInhalt)
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> mListener.onEditSpaltenPositiveClick(bsSpalten, bsZusatzfelder))
                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                    })
                    .create();
        } else {
            return new AlertDialog.Builder(getActivity()).create();
        }
    }


    /*
     * eine Checkbox für optionale Zusatzeinträge wurde ein- oder ausgeschaltet
     */
    @Override
    public void onButtonClick(int spalte, boolean eingeschaltet) {
        if (spalte < IExport_Basis.SPALTE_ZUSATZ) {
            bsSpalten.set(spalte, eingeschaltet);
        } else {
            spalte -= IExport_Basis.SPALTE_ZUSATZ;
            bsZusatzfelder.set(spalte, eingeschaltet);
        }
    }


    public interface EditSpaltenDialogListener{
		void onEditSpaltenPositiveClick(BitSet spalten, BitSet zusatzwerte);
    }
}
