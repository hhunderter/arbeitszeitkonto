/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.widget.CompoundButtonCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.BitSet;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzfeldDefinition;

public class TabellenExportViewAdapter
        extends RecyclerView.Adapter<TabellenExportViewAdapter.ViewHolder>{
    private final Arbeitsplatz mArbeitsplatz;
    private final ArrayList<AExportBasis.Spalte> mTabellen;
    private final LayoutInflater mInflater;
    private final ButtonClickListener mCallback;
    private final BitSet bTabellen;
    private final BitSet bTabellenDeaktiviert;
    private final BitSet bZusatzwerte;

    public TabellenExportViewAdapter(
            Context context,
            Arbeitsplatz job,
            BitSet tabellen,
            BitSet deaktiv,
            BitSet zusatzwerte,
            ButtonClickListener listener) {
        mArbeitsplatz = job;
        mInflater = LayoutInflater.from(context);
        mTabellen = makeTabellenSet();
        mCallback = listener;
        bTabellen = tabellen;
        bZusatzwerte = zusatzwerte;
        bTabellenDeaktiviert = deaktiv;
    }


    // erzeugt eine Liste aller Tabellen
    private ArrayList<AExportBasis.Spalte> makeTabellenSet() {
        ArrayList<AExportBasis.Spalte> listTabellen = new ArrayList<>();
        String[] namen = ASetup.res.getStringArray(R.array.export_tabellen);
        boolean isText;

        // die regulären Spalten
        for (int s = 0; s <= IExport_Basis.DEF_MAXBIT_TABELLEN; s++) {
            if(s != IExport_Basis.TAB_SCHWELLEN) {
                isText = s==IExport_Basis.SPALTE_DATUM || s==IExport_Basis.SPALTE_EORT || s==IExport_Basis.SPALTE_SCHICHTNAME;
                listTabellen.add(new AExportBasis.Spalte(
                        s,
                        namen[s],
                        isText));
            }
        }

        // die Zusatzwerte
        for (int z = 0; z < mArbeitsplatz.getZusatzfeldListe().size(); z++) {
            ZusatzfeldDefinition zDef = mArbeitsplatz.getZusatzDefinition(z);
            if (zDef != null) {
                int s = IExport_Basis.SPALTE_ZUSATZ + z;
                if(zDef.getTyp() != IZusatzfeld.TYP_TEXT) {
                    listTabellen.add(new AExportBasis.Spalte(
                            s,
                            zDef.getName(),
                            false));
                }
            }
        }

        return listTabellen;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_zusatzfeld_export, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final int spalte = getItem(position).mSpalte;

        holder.mButton.setText(getItem(position).mName);
        holder.mButton.setChecked(
                (spalte < IExport_Basis.TAB_ZUSATZ) ?
                        bTabellen.get(spalte) :
                        bZusatzwerte.get(spalte - IExport_Basis.TAB_ZUSATZ)
        );

        if(spalte <= IExport_Basis.DEF_MAXBIT_TABELLEN && bTabellenDeaktiviert.get(spalte)){
          holder.mButton.setEnabled(false);
        } else {
            holder.mButton.setOnCheckedChangeListener((compoundButton, b) -> {
                if(mCallback != null)
                    mCallback.onButtonClick(spalte, b);
            });
        }
    }

    @Override
    public int getItemCount() {
        return mTabellen.size();
    }

    AExportBasis.Spalte getItem(int index) {
        return mTabellen.get(index);
    }


    // parent activity will implement this method to respond to click events
    public interface ButtonClickListener {
        void onButtonClick(int button, boolean eingeschaltet);
    }

    // stores and recycles views as they are scrolled off screen
    public static class ViewHolder extends RecyclerView.ViewHolder{
        AppCompatCheckBox mButton;

        ViewHolder(View itemView) {
            super(itemView);
            mButton = itemView.findViewById(R.id.ZE_option);
            CompoundButtonCompat.setButtonTintList(
                    mButton,
                    ASetup.aktJob.getFarbe_Radio());
        }
    }
}

