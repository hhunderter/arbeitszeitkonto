/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.core.widget.CompoundButtonCompat;
import androidx.fragment.app.DialogFragment;

import java.util.BitSet;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

/**
 * @author askanimus@gmail.com on 17.05.16.
 */
public class Fragment_Dialog_Zeilen
        extends
        DialogFragment
        implements
        AppCompatCheckBox.OnCheckedChangeListener{
    private EditZeilenDialogListener mListener;

    private BitSet bsZeilen;
    private BitSet bsZeilenDeaktiv;

    private LinearLayout bUnterschriften;
    private TextView wUnterschrift_AG;
    private TextView wUnterschrift_AN;

    private int mExportPeriode = 0;
    private Arbeitsplatz mJob;


    public void setup(
            Arbeitsplatz job,
            BitSet zeilen,
            BitSet deaktiv,
            EditZeilenDialogListener listener,
            int periode){
        mJob = job;
        bsZeilen = (BitSet)zeilen.clone();
        bsZeilenDeaktiv = deaktiv;
        mListener = listener;
        mExportPeriode = periode;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(bsZeilen != null) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());

            @SuppressLint("InflateParams")
            View mInhalt = inflater.inflate(R.layout.fragment_dialog_zeilen_export, null);

            AppCompatCheckBox cbWochenNummer;
            AppCompatCheckBox cbSummeTag;
            AppCompatCheckBox cbSummeZeitraum;
            AppCompatCheckBox cbSollZeitraum;
            AppCompatCheckBox cbSaldoZeitraum;
            AppCompatCheckBox cbUnterschrift;

            cbWochenNummer = mInhalt.findViewById(R.id.DZ_button_wochennummer);
            cbSummeTag = mInhalt.findViewById(R.id.DZ_button_summetag);
            cbSummeZeitraum = mInhalt.findViewById(R.id.DZ_button_summemonat);
            cbSollZeitraum = mInhalt.findViewById(R.id.DZ_button_sollmonat);
            cbSaldoZeitraum = mInhalt.findViewById(R.id.DZ_button_saldomonat);
            cbUnterschrift = mInhalt.findViewById(R.id.DZ_button_unterschrift);
            bUnterschriften = mInhalt.findViewById(R.id.DZ_box_unterschriften);
            wUnterschrift_AG = mInhalt.findViewById(R.id.DZ_wert_unterschrift_ag);
            wUnterschrift_AN = mInhalt.findViewById(R.id.DZ_wert_unterschrift_an);

            ((TextView) mInhalt.findViewById(R.id.DZ_titel_unterschrift))
                    .setTextColor(mJob.getFarbe_Schrift_default());

            // die Farbe der Checkboxen anpassen
            CompoundButtonCompat.setButtonTintList(
                    cbWochenNummer,
                    ASetup.aktJob.getFarbe_Radio()
            );
            CompoundButtonCompat.setButtonTintList(
                    cbSummeTag,
                    ASetup.aktJob.getFarbe_Radio()
            );
            CompoundButtonCompat.setButtonTintList(
                    cbSummeZeitraum,
                    ASetup.aktJob.getFarbe_Radio()
            );
            CompoundButtonCompat.setButtonTintList(
                    cbSaldoZeitraum,
                    ASetup.aktJob.getFarbe_Radio()
            );
            CompoundButtonCompat.setButtonTintList(
                    cbSollZeitraum,
                    ASetup.aktJob.getFarbe_Radio()
            );
            CompoundButtonCompat.setButtonTintList(
                    cbUnterschrift,
                    ASetup.aktJob.getFarbe_Radio()
            );

            cbWochenNummer.setOnCheckedChangeListener(this);
            cbSollZeitraum.setOnCheckedChangeListener(this);
            cbSaldoZeitraum.setOnCheckedChangeListener(this);
            cbSummeTag.setOnCheckedChangeListener(this);
            cbSummeZeitraum.setOnCheckedChangeListener(this);
            cbUnterschrift.setOnCheckedChangeListener(this);

            cbWochenNummer.setChecked(bsZeilen.get(IExport_Basis.OPTION_WOCHENNUMMER));
            cbWochenNummer.setVisibility(bsZeilenDeaktiv.get(IExport_Basis.OPTION_WOCHENNUMMER) ? View.GONE : View.VISIBLE);

            cbSollZeitraum.setChecked(bsZeilen.get(IExport_Basis.OPTION_ZEILE_SOLLZEITRAUM));
            cbSollZeitraum.setVisibility(bsZeilenDeaktiv.get(IExport_Basis.OPTION_ZEILE_SOLLZEITRAUM) ? View.GONE : View.VISIBLE);

            cbSaldoZeitraum.setChecked(bsZeilen.get(IExport_Basis.OPTION_ZEILE_SALDOZEITRAUM));
            cbSaldoZeitraum.setVisibility(bsZeilenDeaktiv.get(IExport_Basis.OPTION_ZEILE_SALDOZEITRAUM) ? View.GONE : View.VISIBLE);

            cbSummeTag.setChecked(bsZeilen.get(IExport_Basis.OPTION_ZEILE_SUMMETAG));
            cbSummeTag.setVisibility(bsZeilenDeaktiv.get(IExport_Basis.OPTION_ZEILE_SUMMETAG) ? View.GONE : View.VISIBLE);

            cbSummeZeitraum.setChecked(bsZeilen.get(IExport_Basis.OPTION_ZEILE_SUMMEZEITRAUM));
            cbSummeZeitraum.setVisibility(bsZeilenDeaktiv.get(IExport_Basis.OPTION_ZEILE_SUMMEZEITRAUM) ? View.GONE : View.VISIBLE);

            if (!bsZeilenDeaktiv.get(IExport_Basis.OPTION_ZEILE_UNTERSCHRIFT)) {
                cbUnterschrift.setChecked(bsZeilen.get(IExport_Basis.OPTION_ZEILE_UNTERSCHRIFT));
                cbUnterschrift.setVisibility(View.VISIBLE);
                if (cbUnterschrift.isChecked()) {
                    wUnterschrift_AG.setText(mJob.getUnterschrift_AG());
                    wUnterschrift_AN.setText(mJob.getUnterschrift_AN());
                } else {
                    bUnterschriften.setVisibility(View.GONE);
                }
            } else {
                cbUnterschrift.setVisibility(View.GONE);
                bUnterschriften.setVisibility(View.GONE);
            }

            switch (mExportPeriode) {
                case IExport_Basis.PERIODE_WOCHE:
                    ((TextView) mInhalt.findViewById(R.id.DZ_button_summemonat)).setText(getString(R.string.summe_woche));
                    ((TextView) mInhalt.findViewById(R.id.DZ_button_sollmonat)).setText(getString(R.string.soll_woche));
                    break;
                case IExport_Basis.PERIODE_EORT:
                case IExport_Basis.PERIODE_ZEITRAUM:
                    ((TextView) mInhalt.findViewById(R.id.DZ_button_summemonat)).setText(getString(R.string.summe_zeitraum));
                    ((TextView) mInhalt.findViewById(R.id.DZ_button_sollmonat)).setText(getString(R.string.soll_zeitraum));
                    break;
            }


            return new AlertDialog.Builder(getActivity())
                    .setView(mInhalt)
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> mListener.onEditZeilenPositiveClick(
                            bsZeilen,
                            wUnterschrift_AG.getText().toString(),
                            wUnterschrift_AN.getText().toString()))
                    .setNegativeButton(android.R.string.cancel, (dialog, which) -> {
                    })
                    .create();
        } else {
            return new AlertDialog.Builder(getActivity()).create();
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();// Zeilen

        if (id == R.id.DZ_button_wochennummer) {
            bsZeilen.set(IExport_Basis.OPTION_WOCHENNUMMER, isChecked);
        } else if (id == R.id.DZ_button_summetag) {
            bsZeilen.set(IExport_Basis.OPTION_ZEILE_SUMMETAG, isChecked);
        } else if (id == R.id.DZ_button_summemonat) {
            bsZeilen.set(IExport_Basis.OPTION_ZEILE_SUMMEZEITRAUM, isChecked);
        } else if (id == R.id.DZ_button_saldomonat) {
            bsZeilen.set(IExport_Basis.OPTION_ZEILE_SALDOZEITRAUM, isChecked);
        } else if (id == R.id.DZ_button_sollmonat) {
            bsZeilen.set(IExport_Basis.OPTION_ZEILE_SOLLZEITRAUM, isChecked);
        } else if (id == R.id.DZ_button_unterschrift) {
            bsZeilen.set(IExport_Basis.OPTION_ZEILE_UNTERSCHRIFT, isChecked);
            if (isChecked) {
                bUnterschriften.setVisibility(View.VISIBLE);
            } else {
                bUnterschriften.setVisibility(View.INVISIBLE);
            }
        }
    }

    public interface EditZeilenDialogListener{
		void onEditZeilenPositiveClick(BitSet zeilen, String unetrschriftAG, String unterschriftAN);
    }
}
