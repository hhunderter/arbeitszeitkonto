/* Copyright 2014-2022 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import java.util.ArrayList;
import java.util.Arrays;

import askanimus.arbeitszeiterfassung2.R;

public class MultiSelectSpinner extends androidx.appcompat.widget.AppCompatTextView implements
          DialogInterface.OnMultiChoiceClickListener {

    ArrayList<MultiSelectItem> items = null;
    boolean[] selection = null;
    TextView selectedItems;
    OnMultiSelectedListener callBack;

    public MultiSelectSpinner(@NonNull Context context) {
        super(context);

        selectedItems = findViewById(R.id.EO_spinner_orte);
    }

    public MultiSelectSpinner(@NonNull Context context, AttributeSet attrs) {
        super(context, attrs);

        selectedItems = findViewById(R.id.EO_spinner_orte);
    }

    @Override
    public void onClick(DialogInterface dialog, int idx, boolean isChecked) {
        if (selection != null && idx < selection.length) {
            selection[idx] = isChecked;

            selectedItems.setText(buildSelectedItemString());
        } else {
            throw new IllegalArgumentException(
                    "'idx' is out of bounds.");
        }
    }

    @Override
    public boolean performClick() {
        super.performClick();
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        String[] itemNames = new String[items.size()];

        for (int i = 0; i < items.size(); i++) {
            itemNames[i] = items.get(i).getName();
        }

        builder.setMultiChoiceItems(itemNames, selection, this);


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                if (callBack != null) {
                    callBack.onMultiSelection(getSelectedItems());
                }
            }
        });

        builder.show();

        return true;
    }

    public void setOnMultiSelected(OnMultiSelectedListener cb) {
        this.callBack = cb;
    }

    public void setItems(ArrayList<MultiSelectItem> items) {
        this.items = items;
        selection = new boolean[this.items.size()];

        selectedItems.setText(String.format("<%s>", getResources().getString(R.string.alle)));
        Arrays.fill(selection, false);
    }

    public void setSelection(ArrayList<MultiSelectItem> selection) {
        Arrays.fill(this.selection, false);

        for (MultiSelectItem sel : selection) {
            for (int j = 0; j < items.size(); ++j) {
                if (items.get(j).getSelect().equals(sel.getSelect())) {
                    this.selection[j] = true;
                }
            }
        }

        selectedItems.setText(buildSelectedItemString());
    }


    public ArrayList<MultiSelectItem> getSelectedItems() {
        ArrayList<MultiSelectItem> selectedItems = new ArrayList<>();

        for (int i = 0; i < items.size(); ++i) {
            if (selection[i]) {
                selectedItems.add(items.get(i));
            }
        }

        return selectedItems;
    }


    private String buildSelectedItemString() {
        StringBuilder sb = new StringBuilder();
        boolean foundOne = false;

        for (int i = 0; i < items.size(); ++i) {
            if (selection[i]) {
                if (foundOne) {
                    sb.append(", ");
                }

                foundOne = true;

                sb.append(items.get(i).getName());
            }
        }

        if (sb.length() <= 0) {
            sb.append("<")
                    .append(getResources().getString(R.string.alle))
                    .append(">"
                    );
        }
        return sb.toString();
    }

    public interface OnMultiSelectedListener {
        /**
         * Aufgerufen wenn sich die Ordnung in der Liste geändert hat
         */
        void onMultiSelection(ArrayList<MultiSelectItem> selectedItems);
    }
}
