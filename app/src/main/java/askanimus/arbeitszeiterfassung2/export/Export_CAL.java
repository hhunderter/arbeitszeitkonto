/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import android.content.Context;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;

import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.Zeitraum.IZeitraum;
import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitsschicht.Arbeitsschicht;
import askanimus.arbeitszeiterfassung2.arbeitstag.Arbeitstag;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;

class Export_CAL extends AExportBasis_CSV {
    IZeitraum mZeitraum;
    private BitSet mOptionen;
    private ArrayList<Arbeitsplatz> mArbeitsplatzListe;


    Export_CAL(
            Context context,
            IZeitraum zeitraum,
            BitSet optionen,
            StorageHelper storageHelper
            //String pfad
    ) throws Exception {
        super(context);
        mZeitraum = zeitraum;
        mOptionen = optionen;
        mTrenner = ",";

        if(mOptionen.get(OPTION_ALL_JOBS)) {
           mArbeitsplatzListe = ASetup.jobListe.getListe();
        } else {
            mArbeitsplatzListe = new ArrayList<>();
            mArbeitsplatzListe.add(mZeitraum.getArbeitsplatz());
        }

        // den Dateiname zusammenstellen
        //String dateiname = mZeitraum.getDateiname(context, R.string.exp_google_cal);

        // die Tabelle ausgeben
        schreibeTabelle(
                storageHelper,
                mZeitraum.getDateiname(context, R.string.exp_google_cal),
                TYP_CSV
        );
    }

    // Tabelle zusammenstellen
    @Override
    public ArrayList<ArrayList<Zelle_CSV>>  erzeugeTabelle() {
        ArrayList<ArrayList<Zelle_CSV>>  mTabelle = new ArrayList<>();
        ArrayList<Zelle_CSV> row = new ArrayList<>();
        Datum mDatum;

        // der Tabellenkopf
        // Kopfzeile
        row.add(new Zelle_CSV("Subject"));           // Name des Termins (Abwesenheit)
        row.add(new Zelle_CSV("Start Date"));        // Starttag
        row.add(new Zelle_CSV("Start Time"));        // Startzeit
        row.add(new Zelle_CSV("End Date"));          // Endtag
        row.add(new Zelle_CSV("End Time"));          // Endzeit
        row.add(new Zelle_CSV("All Day Event"));     // Ganzer Tag (true | false)
        row.add(new Zelle_CSV("Description"));       // Notiz
        row.add(new Zelle_CSV("Location"));          // Ort
        row.add(new Zelle_CSV("Private"));           // ist privat? (true | false)
        mTabelle.add(row);

        for (Arbeitsplatz a:mArbeitsplatzListe) {
            if (mZeitraum.getArbeitsplatz().getId() != a.getId()) {
                mZeitraum = mZeitraum.wechselArbeitsplatz(a);
            }
            // die einzelnen Termine (Schichten)
            for (Arbeitstag tag : mZeitraum.getTage()) {
                mDatum = tag.getKalender();

                for (Arbeitsschicht schicht : tag.getSchichten()) {
                    if (schicht.getAbwesenheit().getKategorie() != Abwesenheit.KAT_KEINESCHICHT) {
                        row = new ArrayList<>();
                        // Name des Termins (Abwesenheit)
                        row.add(new Zelle_CSV(schicht.getAbwesenheit().getName()));
                        // Starttag, von, Endtag, bis
                        row.add(new Zelle_CSV(mDatum.getString_Datum(mContext)));
                        if (schicht.getAbwesenheit().getWirkung() == Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV ||
                                schicht.getAbwesenheit().getWirkung() == Abwesenheit.WIRKUNG_SOLL_MINUS_EFFEKTIV) {
                            row.add(new Zelle_CSV(schicht.getVon(), true));

                            if (schicht.getBis() <= schicht.getVon())
                                mDatum.add(Calendar.DAY_OF_MONTH, 1);

                            row.add(new Zelle_CSV(mDatum.getString_Datum(mContext)));
                            row.add(new Zelle_CSV(schicht.getBis(), true));
                            // Ganzer Tag?
                            row.add(new Zelle_CSV("false"));
                        } else {
                            row.add(new Zelle_CSV(""));
                            row.add(new Zelle_CSV(mDatum.getString_Datum(mContext)));
                            row.add(new Zelle_CSV(""));
                            // Ganzer Tag?
                            row.add(new Zelle_CSV("true"));
                        }

                        // Notizen
                        StringBuilder mNotiz = new StringBuilder();
                        for (IZusatzfeld zf: schicht.getZusatzfelder(IZusatzfeld.TEXT_VOLL).getListe()) {
                            if(zf.getDatenTyp() == IZusatzfeld.TYP_TEXT){
                                mNotiz.append(zf.getString(false)).append(" ");
                            }
                        }
                        row.add(new Zelle_CSV(mNotiz.toString()));

                        //einsatzort
                        if (mZeitraum.getArbeitsplatz().isOptionSet(Arbeitsplatz.OPT_WERT_EORT))
                            row.add(new Zelle_CSV(
                                    schicht.getNameEinsatzort()
                            + " ("
                            + schicht.getArbeitsplatz().getName()
                            + ")"));
                        else
                            row.add(new Zelle_CSV(schicht.getArbeitsplatz().getName()));

                        // ist es Arbeitszeit oder private Zeit
                        if (schicht.getAbwesenheit().getKategorie() == Abwesenheit.KAT_ARBEITSZEIT ||
                                schicht.getAbwesenheit().getKategorie() == Abwesenheit.KAT_SCHULE)
                            row.add(new Zelle_CSV("false"));
                        else
                            row.add(new Zelle_CSV("true"));


                        // Zeile in Tabelle eintragen
                        mTabelle.add(row);
                    }
                }
            }
        }
    return mTabelle;
    }
}
