/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.export;

import com.pdfjet.Cell;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;

interface IExport_Basis_PDF extends IExport_Basis {
    int TABTYP_ZEITRAUM = 0;
    int TABTYP_JAHR = 1;
    int TABTYP_EORT = 2;

    ArrayList<TabellenArbeitsplatz> erzeugeTabellen() throws Exception;
    Cell makeZelleLeer(boolean noborder);

    Cell makeZelleKopf(String text);

    Cell makeZelleBezeichnung(String text);

    Cell makeZelleString(String text, boolean farbeHintergrund);

    Cell makeZelleWert(float wert, DecimalFormat formater, boolean farbeHintergrund);

    Cell makeZelleStunden(int wert, boolean isDezimal, boolean einheit, boolean farbeHintergrund);

    Cell makeZelleUhrzeit(int wert, boolean farbeHintergrund);

    Cell makeZelleZusatzwert(IZusatzfeld zfeld, boolean isDezimal, boolean farbeHintergrund, boolean inklNotiz);

    Cell makeZelleSummeWert(float wert, DecimalFormat formater, boolean isSaldo);
    Cell makeZelleSummeStunden(int wert, boolean isDezimal, boolean einheit, boolean isSaldo);
    Cell makeZelleSummeZusatzwert(IZusatzfeld zfeld, boolean isDezimal);

    ArrayList<Cell> makeZusammenfassungTitel(String text, Boolean isFirst);

    void makeLeerzeile(List<List<Cell>> tabeldata, int spalten, boolean noborder);

    void makeTitel(List<List<Cell>> tabeldata, String titel, int spalten);

}
