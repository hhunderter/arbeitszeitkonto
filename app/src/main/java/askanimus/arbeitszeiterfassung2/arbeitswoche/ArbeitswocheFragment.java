/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitswoche;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.Calendar;

import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitstag.Arbeitstag;
import askanimus.arbeitszeiterfassung2.arbeitstag.ArbeitstagExpandListAdapter;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ISetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzWertListe;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzWertViewAdapter;

/**
 * @author askanimus@gmail.com on 19.08.15.
 */
public class ArbeitswocheFragment extends Fragment
        implements View.OnClickListener {

    private static final String ARG_JAHR = "jahr";
    private static final String ARG_MONAT = "monat";
    private static final String ARG_TAG = "tag";

    // statische Anzeigeelemente
    private RelativeLayout      cErgebnis;
    private TextView            tWoche;
    private TextView            tDatum;
    private ExpandableListView  lTage;

    // Aktuell zu haltende Elemente der Anzeige
    private TextView        tSoll;
    private TextView        tIst;
    private TextView        tDifferenz;
    private TextView        tStundenlohn;

    // Sonstige Werte
    private LinearLayout    cSonstige;
    ZusatzWertViewAdapter mAdapter_kopf;
    private RecyclerView    lZusatzwerte;

    private ImageView       iCompact;
    private boolean         isCompact;

    // die Daten der Woche
    private Arbeitswoche mWoche;

    private Context mContext;


    /*
      * Neue Instanz anlegen
      */
    public static ArbeitswocheFragment newInstance(Datum cal) {
        ArbeitswocheFragment fragment = new ArbeitswocheFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_JAHR, cal.get(Calendar.YEAR));
        args.putInt(ARG_MONAT, cal.get(Calendar.MONTH));
        args.putInt(ARG_TAG, cal.get(Calendar.DAY_OF_MONTH));
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mContext = getContext();

        View view = inflater.inflate(R.layout.fragment_arbeitswoche, container, false);
        lZusatzwerte = view.findViewById(R.id.W_list_zusatzwerte);
        mAdapter_kopf =
                new ZusatzWertViewAdapter(ZusatzWertViewAdapter.VIEW_KOPF);
        GridLayoutManager layoutManger =
        new GridLayoutManager(
                mContext,
                1 );
        lZusatzwerte.setLayoutManager(layoutManger);
        lZusatzwerte.setAdapter(mAdapter_kopf);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // Einstellungen initialisieren
        ASetup.init(mContext, this::resume);
    }

    private void resume() {
        Bundle args = getArguments();
        Datum mKalender;
        if (args != null) {
            mKalender = new Datum(
                    args.getInt(ARG_JAHR),
                    args.getInt(ARG_MONAT),
                    args.getInt(ARG_TAG),
                    ASetup.aktJob.getWochenbeginn()
            );
        } else {
            mKalender = new Datum(ASetup.aktDatum.getTime(),
                    ASetup.aktJob.getWochenbeginn());
        }

        /*
         * das Aussehen der Oberfläche anpassen
         */
        View mInhalt = getView();
        if(mInhalt != null ) {
            RelativeLayout cDatum = mInhalt.findViewById(R.id.W_box_datum);
            cErgebnis = mInhalt.findViewById(R.id.W_box_ergebnis);
            tWoche = mInhalt.findViewById(R.id.W_wert_woche);
            tDatum = mInhalt.findViewById(R.id.W_wert_datum);
            tSoll = mInhalt.findViewById(R.id.W_wert_soll);
            tIst = mInhalt.findViewById(R.id.W_wert_ist);
            tDifferenz = mInhalt.findViewById(R.id.W_wert_diff);
            lTage = mInhalt.findViewById(R.id.W_liste_tage);

            // Sonstige Werte
            cSonstige = mInhalt.findViewById(R.id.W_box_sonstige);
            //lZusatzwerte = mInhalt.findViewById(R.id.W_list_zusatzwerte);
            LinearLayout cStundenlohn = mInhalt.findViewById(R.id.W_box_stundenlohn);
            tStundenlohn = mInhalt.findViewById(R.id.W_wert_Stundenlohn);

            iCompact = mInhalt.findViewById(R.id.W_icon_fold);

            // Farben des Kopfes setzen
            cDatum.setBackgroundColor(ASetup.aktJob.getFarbe_Tag());


            // Icon zum ein- und ausklappen der Zusammenfassung anpassen
            if (ASetup.res.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                isCompact = ASetup.mPreferenzen.getBoolean(
                        ISetup.KEY_ANZEIGE_WOCHE_COMPACT, false);
                iCompact.setImageDrawable(
                        VectorDrawableCompat.create(
                                ASetup.res,
                                isCompact ?
                                        R.drawable.arrow_down :
                                        R.drawable.arrow_up,
                                mContext.getTheme())
                );
                //iCompact.setColorFilter(Color.WHITE);
                iCompact.setOnClickListener(this);
                cDatum.setOnClickListener(this);
                cErgebnis.setVisibility(isCompact ? View.GONE : View.VISIBLE);
            } else
                iCompact.setVisibility(View.GONE);

            if (!ASetup.isVerdienst)
                cStundenlohn.setVisibility(View.GONE);

            mWoche = new Arbeitswoche(mKalender.getTimeInMillis(), ASetup.aktJob);

            int anzahlTage =  mWoche.getTagzahl();
            if (anzahlTage > 0) {
                // Tagesliste erzeugen
                ArbeitstagExpandListAdapter aTage =
                        new ArbeitstagExpandListAdapter(
                                mContext,
                                mWoche.getTagListe(),
                                (ArbeitstagExpandListAdapter.ArbeitstagListeCallbacks) getActivity());
                lTage.setAdapter(aTage);
                // handler für das öffnen der Kindelemente, nur eins soll zur gleichen Zeit offen sein
                lTage.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                    int previousGroup = -1;

                    @Override
                    public void onGroupExpand(int groupPosition) {
                        if (groupPosition != previousGroup)
                            lTage.collapseGroup(previousGroup);
                        previousGroup = groupPosition;
                    }
                });

                if (mKalender.get(Calendar.YEAR) == ASetup.aktDatum.get(Calendar.YEAR) &&
                        mKalender.get(Calendar.WEEK_OF_YEAR) == ASetup.aktDatum.get(Calendar.WEEK_OF_YEAR)) {
                    int pos;
                    anzahlTage -= 1;

                    if (ASetup.aktJob.isAnzeigeZukunft()) {
                        pos = ASetup.aktDatum.get(Calendar.DAY_OF_WEEK) - mKalender.getWochenbeginn();
                        if (pos < 0)
                            pos += 7;
                        if (ASetup.mPreferenzen.getBoolean(ISetup.KEY_ANZEIGE_UMG_SORT, false)) {
                            pos = anzahlTage - pos;
                        }
                    } else {
                        pos = ASetup.mPreferenzen.getBoolean(ISetup.KEY_ANZEIGE_UMG_SORT, false) ?
                                0 :
                                anzahlTage;
                    }
                    try {
                        lTage.setSelection(pos);

                        if (ASetup.mPreferenzen.getBoolean(ISetup.KEY_ANZEIGE_AKTTAG, true))
                            lTage.expandGroup(pos);
                    } catch (RuntimeException re) {
                        re.printStackTrace();
                    }
                }
            }
            //Statische Werte eintragen
            setDatum();
            //Dynamische Werte eintragen
            setBerechnung();
        }
    }

    // das Datum anzeigen
    private void setDatum(){
        tWoche.setText(getString(R.string.woche_nummer, mWoche.getNummer()));

        if(mWoche.getTagzahl() > 0) {
            tDatum.setText(
                    mWoche.getTag(0).getKalender().getString_Datum_Bereich(
                            mContext,
                            0,
                            mWoche.getTagzahl()-1,
                            Calendar.DAY_OF_WEEK
                    )
            );
        } else {
           tDatum.setText(
                    mWoche.getDatumErsterTag().getString_Datum_Bereich(
                            mContext,
                            0,
                            6,
                            Calendar.DAY_OF_WEEK
                    )
            );
        }
    }

    // die Berechnung der Stunden
    @SuppressLint("NotifyDataSetChanged")
    private void setBerechnung() {
        Uhrzeit mZeit = new Uhrzeit(mWoche.getSoll());

        tSoll.setText(mZeit.getStundenString(true, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

        mZeit.set(mWoche.getIst());
        tIst.setText(mZeit.getStundenString(true, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

        if (ASetup.isVerdienst)
            tStundenlohn.setText(ASetup.waehrungformat.format(mWoche.getVerdienst()));

        mZeit.set(mZeit.getAlsMinuten() - mWoche.getSoll());
        tDifferenz.setText(mZeit.getStundenString(false, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        if (mZeit.getAlsMinuten() == 0)
            tDifferenz.setTextColor(ASetup.aktJob.getFarbe_Schrift_default());
        else if (mZeit.getAlsMinuten() < 0)
            tDifferenz.setTextColor(ASetup.cNegativText);
        else
            tDifferenz.setTextColor(ASetup.cPositivText);

        // die Zusatzwerte anzeigen
        ZusatzWertListe mZusatzwerteSumme = new ZusatzWertListe(ASetup.aktJob.getZusatzfeldListe(), false);
        if (mZusatzwerteSumme.size() > 0) {
            for (Arbeitstag tag : mWoche.getTagListe()) {
                mZusatzwerteSumme.addListenWerte(tag.getTagZusatzwerte(IZusatzfeld.TEXT_NO));
            }
            mAdapter_kopf.setUp(
                    mZusatzwerteSumme.getListe(),
                    null);
            mAdapter_kopf.notifyDataSetChanged();
        } else
            cSonstige.setVisibility(View.GONE);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.W_box_datum || view.getId() == R.id.W_icon_fold) {
            SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
            isCompact = !isCompact;
            mEdit.putBoolean(ISetup.KEY_ANZEIGE_WOCHE_COMPACT, isCompact).apply();
            cErgebnis.setVisibility(isCompact ? View.GONE : View.VISIBLE);
            iCompact.setImageDrawable(
                    VectorDrawableCompat.create(
                            ASetup.res,
                            isCompact ?
                                    R.drawable.arrow_down :
                                    R.drawable.arrow_up,
                            mContext.getTheme())
            );
        }
    }

}
