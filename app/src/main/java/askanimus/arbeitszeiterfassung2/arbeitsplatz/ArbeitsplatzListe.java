/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsplatz;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.setup.ASetup;


/**
 * @author askanimus@gmail.com on 23.12.15.
 */
public class ArbeitsplatzListe {
    private final static String SQL_READ_ALL_JOB_IDS =
            "select " +
            Datenbank.DB_F_ID +
            " from " +
            Datenbank.DB_T_JOB;
    ArrayList<Arbeitsplatz> mArbeitsplaetze;

    @SuppressLint("Range")
    public ArbeitsplatzListe(Arbeitsplatz aktiv) {
        SQLiteDatabase mDatenbank = ASetup.mDatenbank;/*stundenDB.getWritableDatabase();*/

        mArbeitsplaetze = new ArrayList<>();

        // der aktuelle Job ist immer der erste in der Liste
        if (aktiv != null) {
            mArbeitsplaetze.add(aktiv);
        }

        Cursor result = mDatenbank.rawQuery(SQL_READ_ALL_JOB_IDS, null);

        while (result.moveToNext()){
            long id = result.getLong(result.getColumnIndex(Datenbank.DB_F_ID));
                if (aktiv == null || id != aktiv.getId())
                    mArbeitsplaetze.add(new Arbeitsplatz(id));
            }

        result.close();
    }

    public void add(Arbeitsplatz job){
        mArbeitsplaetze.add(job);
    }

    public Arbeitsplatz add(){
        Arbeitsplatz mJob = new Arbeitsplatz(-1);
        mArbeitsplaetze.add(mJob);
        return mJob; //mArbeitsplaetze.get(mArbeitsplaetze.size() -1);
    }

    public void remove(int index){
        getVonIndex(index).delete();
        mArbeitsplaetze.remove(index);
    }

    public void remove(long ID){
        getVonID(ID).delete();
        mArbeitsplaetze.remove(getIndex(ID));
    }

    public ArrayList<Arbeitsplatz> getListe() {
        return mArbeitsplaetze;
    }

    public Arbeitsplatz getVonID(long id) {
        for (Arbeitsplatz a : mArbeitsplaetze) {
            if (a.getId() == id)
                return a;
        }
        return null;
    }

    public Arbeitsplatz getVonIndex(int index) {
        if (index < mArbeitsplaetze.size())
            return mArbeitsplaetze.get(index);
        return null;
    }

    public ArrayList<String> getNamen(){
        ArrayList<String> al = new ArrayList<>();
        for (Arbeitsplatz j:mArbeitsplaetze) {
            al.add(j.getName());
        }
        return al;
    }

    public int getIndex(long id){
        int i = 0;
        for (Arbeitsplatz j : mArbeitsplaetze) {
            if (j.getId() == id){
                return i;
            }
            i++;
        }
        return 0;
    }

    public Arbeitsplatz newAktivJob(long id) {
        int i = 0;
        if (mArbeitsplaetze.get(i).getId() != id)
            for (Arbeitsplatz a : mArbeitsplaetze) {
                if (a.getId() == id) {
                    // gewünschten Job an den Listenanfang kopieren
                    mArbeitsplaetze.add(0, mArbeitsplaetze.get(i));
                    // Original löschen
                    mArbeitsplaetze.remove(i+1);
                    break;
                }
                i++;
            }
        return mArbeitsplaetze.get(0);
    }
}
