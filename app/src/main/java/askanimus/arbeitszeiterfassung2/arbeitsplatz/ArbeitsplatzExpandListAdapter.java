/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsplatz;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

/**
 * @author askanimus@gmail.com on 23.12.15.
 */
public class ArbeitsplatzExpandListAdapter extends BaseExpandableListAdapter {
    private final Context mContext;

    private ArrayList<Arbeitsplatz> mArbeitsplaetze;

    private final ArbeitsplatzExpandListeCallbacks mCallbacks;

    ArbeitsplatzExpandListAdapter(Context context, ArbeitsplatzExpandListeCallbacks callbacks){
        mContext = context;
        mCallbacks = callbacks;

        reloadListe();
    }

    void reloadListe(){
        ArbeitsplatzListe mArbeitsplatzliste = ASetup.jobListe;
        mArbeitsplaetze = mArbeitsplatzliste.mArbeitsplaetze;
    }

    @Override
    public int getGroupCount() {
        return mArbeitsplaetze.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mArbeitsplaetze.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return mArbeitsplaetze.get(groupPosition).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (mInflater != null) {
            convertView = mInflater.inflate(R.layout.item_arbeitsplatz, parent, false);

            // Anzeigen finden
            TextView tName = convertView.findViewById(R.id.A_wert_name);
            TextView tAdresse = convertView.findViewById(R.id.A_wert_adresse);
            ImageView iStatus = convertView.findViewById(R.id.A_wert_status);
            RelativeLayout cAll = convertView.findViewById(R.id.A_box_basis);

            // Werte eintragen
            tName.setText(mArbeitsplaetze.get(groupPosition).getName());
            tAdresse.setText(mArbeitsplaetze.get(groupPosition).getAnschrift());

            tName.setTextColor(mArbeitsplaetze.get(groupPosition).getFarbe_Schrift_Titel());
            tAdresse.setTextColor(mArbeitsplaetze.get(groupPosition).getFarbe_Schrift_Titel());
            cAll.setBackgroundColor(mArbeitsplaetze.get(groupPosition).getFarbe());

            if (mArbeitsplaetze.get(groupPosition).isEndeAufzeichnung(ASetup.aktDatum))
                iStatus.setImageDrawable(
                        ResourcesCompat.getDrawable(
                                ASetup.res,
                                android.R.drawable.presence_busy,
                                mContext.getTheme())
                );
            else if (mArbeitsplaetze.get(groupPosition).getId() != ASetup.aktJob.getId())
                iStatus.setImageDrawable(
                        ResourcesCompat.getDrawable(
                                ASetup.res,
                                android.R.drawable.presence_invisible,
                                mContext.getTheme())
                );

            if (mArbeitsplaetze.get(groupPosition).getId() != ASetup.aktJob.getId())
                iStatus.setOnClickListener(v -> {
                    if (mCallbacks != null)
                        mCallbacks.onArbeitsplatzSelect(mArbeitsplaetze.get(groupPosition).getId());
                });

        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final Arbeitsplatz mArbeitsplatz = mArbeitsplaetze.get(groupPosition);
        
        LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (mInflater != null) {
            convertView = mInflater.inflate(R.layout.item_arbeitsplatz_info, parent, false);

            //LinearLayout bInhalt = convertView.findViewById(R.id.A_box_info);

            TextView tDatum = convertView.findViewById(R.id.A_wert_datum);
            TextView tMonatsbeginn = convertView.findViewById(R.id.A_wert_monatsbeginn);
            TextView tModell = convertView.findViewById(R.id.A_wert_modell);
            TextView tAzeitname = convertView.findViewById(R.id.A_name_stunden);
            TextView tAzeit = convertView.findViewById(R.id.A_wert_stunden);
            TextView tUrlaub = convertView.findViewById(R.id.A_wert_urlaub);

            ImageView iLoeschen = convertView.findViewById(R.id.A_button_loeschen);
            ImageView iAktiv = convertView.findViewById(R.id.A_button_aktiv);
            ImageView iEinstellungen = convertView.findViewById(R.id.A_button_einstellungen);
            ImageView iKlone = convertView.findViewById(R.id.A_button_klone);

            //bInhalt.setBackgroundColor(mArbeitsplatz.getFarbe());

            convertView.setBackgroundColor(mArbeitsplatz.getFarbe_Tag());

            String sDatum = mArbeitsplatz.getStartDatum().getString_Datum(mContext) + " - ";
            if (mArbeitsplatz.isSetEnde())
                sDatum += mArbeitsplatz.getEndDatum().getString_Datum(mContext);
            else
                sDatum += ASetup.res.getString(R.string.ende_offen);
            tDatum.setText(sDatum);

            tMonatsbeginn.setText(String.format("%s.", mArbeitsplatz.getMonatsbeginn()));

            String[] mModelle = ASetup.res.getStringArray(R.array.pref_soll_modell_texte);
            tModell.setText(mModelle[mArbeitsplatz.getModell()]);

            if (mArbeitsplatz.getModell() == Arbeitsplatz.Soll_Monat_pauschal)
                tAzeitname.setText(R.string.soll_monat);
            else
                tAzeitname.setText(R.string.soll_woche);
            tAzeit.setText(new Uhrzeit(
                    mArbeitsplatz.getSollstundenWoche())
                    .getStundenString(
                            true,
                            mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL))
            );

            tUrlaub.setText(ASetup.zahlenformat.format(mArbeitsplatz.getSoll_Urlaub()));

            if (mArbeitsplaetze.size() > 1) {
                iLoeschen.setOnClickListener(v -> {
                    if (mCallbacks != null)
                        mCallbacks.onArbeitsplatzDelete(groupPosition);
                });
            } else {
                iLoeschen.setVisibility(View.INVISIBLE);
            }

            if (mArbeitsplatz.getId() == ASetup.aktJob.getId())
                iAktiv.setVisibility(View.INVISIBLE);
            else
                iAktiv.setOnClickListener(v -> {
                    if (mCallbacks != null)
                        mCallbacks.onArbeitsplatzSelect(mArbeitsplatz.getId());

                });

            iEinstellungen.setOnClickListener(v -> {
                if (mCallbacks != null)
                    mCallbacks.onArbeitsplatzEdit(mArbeitsplatz.getId());
            });

            iKlone.setOnClickListener(v -> {
                if (mCallbacks != null)
                    mCallbacks.onArbeitsplatzKlone(mArbeitsplatz.getId());
            });
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
    
    
    /*
     * Callback Interfaces
     */
    public interface ArbeitsplatzExpandListeCallbacks {
        /**
         * Aufgerufen wenn arbeitsplatz Aktionen ausgeführt werden sollen
         */
        void onArbeitsplatzDelete(int index);
        void onArbeitsplatzSelect(long ArbeitsplatzID);
        void onArbeitsplatzEdit(long ArbeitsplatzID);
        void onArbeitsplatzKlone(long ArbeitsplatzID);
    }
}
