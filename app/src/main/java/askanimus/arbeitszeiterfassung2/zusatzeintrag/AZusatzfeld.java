/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import android.content.ContentValues;
import java.text.DecimalFormat;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

abstract class AZusatzfeld implements IZusatzfeld {
    ZusatzfeldDefinition mDefinition;
    long mID;
    boolean isNotSave;
    long mSchichtID;

    AZusatzfeld(ZusatzfeldDefinition definition, long id, long schichID){
        mID = id;
        mDefinition = definition;
        mSchichtID = schichID;
        isNotSave = true;
    }

    @Override
    public long save(boolean isDefaultSchicht) {
        if(isNotSave && mSchichtID > 0) {
            ContentValues werte = new ContentValues();

            if(!getStringforDatenbank().isEmpty()) {
                werte.put(Datenbank.DB_F_ZUSATZFELD, mDefinition.getID());
                werte.put(
                        (isDefaultSchicht ? Datenbank.DB_F_SCHICHT_DEFAULT : Datenbank.DB_F_SCHICHT)
                        , mSchichtID);
                werte.put(Datenbank.DB_F_WERT, getStringforDatenbank());

                if (mID < 0) {
                    mID = ASetup.mDatenbank.insert(
                            (isDefaultSchicht ? Datenbank.DB_T_ZUSATZWERT_DEFAULT : Datenbank.DB_T_ZUSATZWERT),
                            null,
                            werte);
                } else {
                    ASetup.mDatenbank.update(
                            (isDefaultSchicht ? Datenbank.DB_T_ZUSATZWERT_DEFAULT : Datenbank.DB_T_ZUSATZWERT),
                            werte,
                            Datenbank.DB_F_ID + "=?", new String[]{Long.toString(mID)});
                }
            } else {
                if (mID > 0){
                    ASetup.mDatenbank.delete(
                            (isDefaultSchicht ? Datenbank.DB_T_ZUSATZWERT_DEFAULT : Datenbank.DB_T_ZUSATZWERT),
                            Datenbank.DB_F_ID + "=?",
                            new String[]{Long.toString(mID)}
                    );
                }
            }
            isNotSave = false;
        }
        return mID;
    }

    @Override
    public boolean isNotSave(){
        return isNotSave;
    }

    @Override
    public void setSchichtId(long schichtId){
        if(mSchichtID != schichtId) {
            mSchichtID = schichtId;
            isNotSave = true;
        }
    }

    /*@Override
    public void setName(String name){
        if(!name.equals(mDefinition.mName)){
            mDefinition.mName = name;
            isNotSave = true;
        }
    }

    @Override
    public void setWirkung(int wirkung){
        if(wirkung != mDefinition.mWirkung){
            mDefinition.mWirkung = wirkung;
            isNotSave = true;
        }
    }*/

    @Override
    public long getId(){
        return mID;
    }

    @Override
    public long getSchichtId() {
        return mSchichtID;
    }

    @Override
    public String getName(){
        return mDefinition.getName();
    }

    @Override
    public String getString(boolean einheit){
        return getStringWert(einheit);
    }

    @Override
    public String getEinheit(){
        return mDefinition.getEinheit();
    }

    @Override
    public DecimalFormat getFormater(){
        return mDefinition.getFormater();
    }

    @Override
    public int getWirkung() {
        return mDefinition.getWirkung();
    }

    @Override
    public int getDatenTyp(){
        return mDefinition.getTyp();
    }

    @Override
    public int getColums(){
        return mDefinition.getColums();
    }

    @Override
    public long getDefinitionID(){
        return mDefinition.getID();
    }

    @Override
    public int getPosition(){
        return mDefinition.getPosition();
    }

    @Override
    public ZusatzfeldDefinition getDefinition(){
        return mDefinition;
    }
}
