/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import android.database.Cursor;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

public class ZusatzfeldDefinitionenListe {
    private final int STARTPOSITION = 0;
    ArrayList<ZusatzfeldDefinition> mDefinitionen;

    public ZusatzfeldDefinitionenListe(long jobID, boolean isdezimal) {
        final String SQL_READ_DEFINITIONEN =
                "select * from "
                        + Datenbank.DB_T_ZUSATZFELD
                        + " where "
                        + Datenbank.DB_F_JOB
                        + " = ?" +
                        " order by " + Datenbank.DB_F_POSITION + " ASC";

        mDefinitionen = new ArrayList<>();

        Cursor result = ASetup.mDatenbank.rawQuery(
                SQL_READ_DEFINITIONEN,
                new String[]{Long.toString(jobID)}
        );

        int i = STARTPOSITION;
        while (result.moveToNext()) {
            ZusatzfeldDefinition newDef = new ZusatzfeldDefinition(result, isdezimal);
            newDef.setPosition(i);
            mDefinitionen.add(newDef);
            i++;
        }

        result.close();
    }

    public void add(ZusatzfeldDefinition zusatzfeldDefinition){
        mDefinitionen.add(zusatzfeldDefinition.getPosition(), zusatzfeldDefinition);
    }

    public ZusatzfeldDefinition get(int index){
        if(index < mDefinitionen.size())
            return mDefinitionen.get(index);
        else
            return null;
    }

    public ZusatzfeldDefinition get(long id){
        for (ZusatzfeldDefinition d : mDefinitionen) {
            if(d.getID() == id)
                return d;
        }
        return null;
    }



    public ArrayList<ZusatzfeldDefinition> getListe(){
        return mDefinitionen;
    }

    public int size(){
        return mDefinitionen.size();
    }

    public void ListeKlone(long jobID_neu){
        for (ZusatzfeldDefinition def:mDefinitionen) {
            def.Klone(jobID_neu);
        }
        /*int i = 0;
        while (i<mDefinitionen.size()){
            mDefinitionen.get(i).Klone(jobID_neu);
            i++;
        }*/
    }

    public void deleteFeld(ZusatzfeldDefinition feld){
        mDefinitionen.remove(feld);
        feld.delete();
        // die Feldpositionen anpassen
        int i = STARTPOSITION;
        for (ZusatzfeldDefinition zd : mDefinitionen) {
            zd.setPosition(i);
            zd.speichern();
            i++;
        }
    }
}
