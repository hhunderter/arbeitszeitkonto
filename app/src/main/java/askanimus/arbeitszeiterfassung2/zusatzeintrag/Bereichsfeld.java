/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;
import androidx.annotation.NonNull;

import java.util.Locale;

import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

public class Bereichsfeld extends AZusatzfeld {
    static final String BEREICH_TRENNER = "bis";
    IZusatzfeld wBeginn;
    IZusatzfeld wEnde;

    Bereichsfeld(long id, long schichtID, @NonNull ZusatzfeldDefinition definition, float beginn, float ende) {
        super(definition, id, schichtID);
        wBeginn = new Zahlenfeld(id, schichtID, definition, beginn);
        wEnde = new Zahlenfeld(id, schichtID, definition, ende);
    }

    Bereichsfeld(long id, long schichtID, @NonNull ZusatzfeldDefinition definition, int beginn, int ende) {
        super(definition, id, schichtID);
        wBeginn = new Zeitfeld(id, schichtID, definition, beginn, true);
        wEnde = new Zeitfeld(id, schichtID, definition, ende, true);
    }

    @Override
    public void set(IZusatzfeld wert) {
        if (wert != null) {
            if (wert.getDatenTyp() == IZusatzfeld.TYP_BEREICH_ZEIT ||
                    wert.getDatenTyp() == IZusatzfeld.TYP_BEREICH_ZAHL) {
                if (((Bereichsfeld) wert).wBeginn.getWert() != wBeginn.getWert()) {
                    wBeginn.setWert(((Bereichsfeld) wert).wBeginn.getWert());
                    setNotSave();
                }
                if (((Bereichsfeld) wert).wEnde.getWert() != wEnde.getWert()) {
                    wEnde.setWert(((Bereichsfeld) wert).wEnde.getWert());
                    setNotSave();
                }
            }
        } else {
            wBeginn.setWert(null);
            wEnde.setWert(null);
        }
    }

    @Override
    public void setWert(Object wert) {
        if (wert != null) {
            if (mDefinition.getTyp() == IZusatzfeld.TYP_BEREICH_ZEIT) {
                int[] w = (int[]) wert;
                wBeginn.setWert(w[0]);
                wEnde.setWert(w[1]);
            } else {
                float[] w = (float[]) wert;
                wBeginn.setWert(w[0]);
                wEnde.setWert(w[1]);
            }
            setNotSave();
        } else {
            wBeginn.setWert(null);
            wEnde.setWert(null);
        }
    }

    public void setNotSave(){
        isNotSave = wBeginn.isNotSave() || wEnde.isNotSave();
    }

    /*@Override
    public boolean isNotSave(){
        return isNotSave;
    }*/

    @Override
    public void add(IZusatzfeld bereichsfeld) {
        if (bereichsfeld != null) {
            if (bereichsfeld.getDatenTyp() == IZusatzfeld.TYP_BEREICH_ZAHL
                    || bereichsfeld.getDatenTyp() == IZusatzfeld.TYP_BEREICH_ZEIT) {
                wBeginn.add(((Bereichsfeld) bereichsfeld).wBeginn);
                wEnde.add(((Bereichsfeld) bereichsfeld).wEnde);
            }
        }
    }

    @Override
    public IZusatzfeld getKopie() {
        Bereichsfeld z;
        if(mDefinition.getTyp()==IZusatzfeld.TYP_BEREICH_ZAHL) {
            z = new Bereichsfeld(
                    -1,
                    mSchichtID,
                    mDefinition,
                    (float)wBeginn.getWert(),
                    (float)wEnde.getWert());
        } else {
            z = new Bereichsfeld(
                    -1,
                    mSchichtID,
                    mDefinition,
                    (int)wBeginn.getWert(),
                    (int)wEnde.getWert());
        }
        return z;
    }

    @Override
    public IZusatzfeld[] get() {
        return new IZusatzfeld[]{wBeginn, wEnde};
    }

    @Override
    public Object getWert() {
        if (mDefinition.getTyp() == IZusatzfeld.TYP_BEREICH_ZEIT) {
            int wert = (int)wEnde.getWert() - (int)wBeginn.getWert();
            if(wert < 0){
                wert += ISetup.Minuten_TAG;
            }
            return wert;
        } else {
            return (float)wEnde.getWert() - (float)wBeginn.getWert();
        }
    }

    @Override
    public String getString(boolean einheit) {
       return String.format("%s ‒ %s",
                wBeginn.getString(false),
                wEnde.getString(einheit));
    }

    @Override
    public String getStringWert(boolean einheit) {
        String anzeige;
        if (mDefinition.getTyp() == IZusatzfeld.TYP_BEREICH_ZEIT) {
            Uhrzeit mStunden = new Uhrzeit(
                    ((int)wEnde.getWert() - (int)wBeginn.getWert()));
            if (mStunden.getAlsMinuten() < 0){
                mStunden.add(ISetup.Minuten_TAG);
            }
            anzeige = mStunden.getStundenString(einheit, ASetup.mPreferenzen.getBoolean(ISetup.KEY_ANZEIGE_DEZIMAL, true));
        } else {
            float w = (float)getWert();
            if (einheit)
                anzeige = String.format(Locale.getDefault(), "%.2f %s", w, getEinheit());
            else
                anzeige = String.format(Locale.getDefault(), "%.2f", w);
        }
        return anzeige;
    }

    @Override
    public String getStringforDatenbank(){
        return String.format("%s"+BEREICH_TRENNER+"%s",
                wBeginn.getStringforDatenbank(),
                wEnde.getStringforDatenbank());
    }

}
