/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;
public class ZusatzWerteAuswahlListadapter extends BaseAdapter {
    private final Context context;
    ZusatzWertAuswahlListe auswahlListe;
    ZusatzwertAuswahlListeCallbacks callback;

    List<Integer> mFilterData;
    private ValueFilter valueFilter;
    String eingabeFilter;

    ZusatzWerteAuswahlListadapter(ZusatzWertAuswahlListe liste, Context ctx, ZusatzwertAuswahlListeCallbacks cb) {
        context = ctx;
        auswahlListe = liste;
        callback = cb;
    }

    public void update(){
        auswahlListe.sort(
                ASetup.mPreferenzen.getInt(
                    ISetup.KEY_SORT_AUSWAHLLISTE,
                    ISetup.SORT_NO
                )
            );
        valueFilter = null;
        mFilterData = null;
        callback.onZusatzwerteChange();
    }

    @Override
    public int getCount() {
        if(mFilterData != null){
            return mFilterData.size();
        }
        return auswahlListe.size();
    }

    @Override
    public IZusatzfeld getItem(int position) {
        if(mFilterData != null){
            return auswahlListe.getEintragWert(mFilterData.get(position));
        }
        return auswahlListe.getEintragWert(position);
    }

    @Override
    public long getItemId(int position) {
        if(mFilterData != null){
            return auswahlListe.getEintragWert(mFilterData.get(position)).getId();
        }
        return auswahlListe.getEintragWert(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null && context != null) {
            LayoutInflater layInflator =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (layInflator != null) {
                convertView = layInflator.inflate(R.layout.item_auswahl_zeile, parent, false);
            }
        }

        if (convertView != null) {
            convertView.setBackgroundColor(((position % 2 == 0) ?
                    ASetup.aktJob.getFarbe_Zeile_gerade() :
                    ASetup.aktJob.getFarbe_Zeile_ungerade()));
            convertView.setOnClickListener(v -> {
                auswahlListe.zusatzWertAddVerwendung(position);
                callback.onSelectItem(getItem(position));
            });

            // dert Wert
            final TextView vText = convertView.findViewById(R.id.auswahl_Text);
            vText.setText(getItem(position).getString(true));

            // das Icon zum bearbeiten des Wertes
            /*ImageView iEdit = convertView.findViewById(R.id.auswahl_buttonL);
            iEdit.setOnClickListener(v -> callback.onEditItem(position));*/

            //das Icon zum löschen des Wertes
            ImageView iDelete = convertView.findViewById(R.id.auswahl_buttonR);
            iDelete.setOnClickListener(v -> callback.onDeleteItem(position));

        }
        return convertView;
    }

    /*
     * Listenfilter
     */
    Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            eingabeFilter = constraint.toString();
            List<Integer> filterList = new ArrayList<>();
            FilterResults results = new FilterResults();

            if (constraint.length() > 0) {
                int i = 0;
                for (ZusatzWertAuswahlListe.zusatzWertAuswahlItem item : auswahlListe.getListe()) {
                    if (item.getWert().getString(false).toLowerCase()
                            .contains(constraint.toString().toLowerCase())
                    ) {
                        filterList.add(i);
                    }
                    i++;
                }
                results.count = filterList.size();
                results.values = filterList;
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mFilterData = (List<Integer>) results.values;
            notifyDataSetChanged();
        }
    }

    /*
     * Callback Interfaces
     */
    public interface ZusatzwertAuswahlListeCallbacks {
        /**
         * Aufgerufen wenn sich die Ordnung in der Liste geändert hat
         */
        void onZusatzwerteChange();

        /**
         * wird aufgerufen wenn ein Wert ausgewählt wurde
         * @param item der ausgewählte Wert
         */
        void onSelectItem(IZusatzfeld item);

        void onDeleteItem(int position);
        //void onEditItem(int position);
    }
}
