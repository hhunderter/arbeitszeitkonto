/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;
import androidx.annotation.NonNull;
import java.util.Locale;

public class Zahlenfeld extends AZusatzfeld {
    float mWert;

    Zahlenfeld(long id, long schichtID, @NonNull ZusatzfeldDefinition definition, float wert) {
        super(definition, id, schichtID);
        mWert = wert;
    }

    @Override
    public void set(IZusatzfeld wert) {
        if (wert != null) {
            if (wert.getDatenTyp() == IZusatzfeld.TYP_ZAHL || wert.getDatenTyp() == IZusatzfeld.TYP_AUSWAHL_ZAHL) {
                float f = (float) wert.getWert();
                if (f != mWert) {
                    mWert = f;
                    isNotSave = true;
                }
            }
        } else {
            mWert = 0f;
            isNotSave = true;
        }
    }

    @Override
    public void setWert(Object wert) {
        if(wert != null) {
            float f = (float) wert;
            if (f != mWert) {
                mWert = f;
                isNotSave = true;
            }
        } else {
            mWert = 0f;
            isNotSave = true;
        }
    }

    @Override
    public void add(IZusatzfeld zahlenfeld) {
        if (zahlenfeld != null)
            if (zahlenfeld.getDatenTyp() == IZusatzfeld.TYP_ZAHL ||
                   zahlenfeld.getDatenTyp() == IZusatzfeld.TYP_BEREICH_ZAHL) {
                mWert += (float) zahlenfeld.getWert();
            }
    }

    @Override
    public IZusatzfeld getKopie(){
        return new Zahlenfeld(-1, mSchichtID, mDefinition, mWert);
    }

    @Override
    public IZusatzfeld[] get() {
        return new IZusatzfeld[]{this};
    }

    @Override
    public Object getWert() {
        return mWert;
    }

    @Override
    public String getStringWert(boolean symbol) {
        if(symbol)
            return String.format(Locale.getDefault(),"%.2f %s", mWert, getEinheit());
        else
            return String.format(Locale.getDefault(),"%.2f", mWert);
    }

    @Override
    public String getStringforDatenbank(){
        if(mWert != 0) {
            return String.valueOf(mWert);
        } else {
            return "";
        }
    }

    /*@Override
    public int getColums() {
        return 1;
    }*/
}
