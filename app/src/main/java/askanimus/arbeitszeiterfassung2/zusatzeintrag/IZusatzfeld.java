/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import android.view.View;

import java.text.DecimalFormat;

public interface IZusatzfeld {
    //
    // Anzahl der Spalten im Anzeigeraster
    //
    int MAX_COLUM = 4;

    //
    // Typen für Zusatzeinträge
    //
    int TYP_TEXT = 0;
    int TYP_ZAHL = 1;
    int TYP_ZEIT = 2;
    int TYP_BEREICH_ZAHL = 3;
    int TYP_BEREICH_ZEIT = 4;
    int TYP_AUSWAHL_TEXT = 10;
    int TYP_AUSWAHL_ZAHL = 11;
    int TYP_AUSWAHL_ZEIT = 12;
    int TYP_ONOFF = 20;

    //
    // Wirkungen der Zusatzwerte auf Stunden und Verdienst
    //
    int NEUTRAL = 0;
    int ADD_ISTSTUNDEN = 1;
    int ADD_VERDIENST = 2;
    int SUB_ISTSTUNDEN = 3;
    int SUB_SOLLSTUNDEN = 4;
    int SUB_VERDIENST = 5;

    //
    // Rückgabeform von Textfeldern
    //
    int TEXT_NO = 0;
    int TEXT_LEER = 1;
    int TEXT_VOLL = 2;

    /*
     * Eingaben

    void setName(String name);

    void setWirkung(int wirkung);*/

    void set(IZusatzfeld wert);

    void setWert(Object wert);

    /*
     * Manipulationen
     */
    void add(IZusatzfeld feld); // den Wert eines Feldes zu diesen addieren

    IZusatzfeld getKopie(); // liefert die kopie des Zusatzfeldes zurück, mit id -1;

    void setSchichtId(long schichtId);

    /*
     * Ausgaben
     */
    long getId();
    long getSchichtId();
    String getName();
    String getEinheit();
    DecimalFormat getFormater();

    IZusatzfeld[] get();

    Object getWert(); // gibt den Wert für Berechnungen aus Zeit=hh.min, Text=0.0, Zahl=x.xx)
                    // Bereich = wert_bis minus wert_von
    int getWirkung();

    int getDatenTyp();

    String getString(boolean einheit);
    String getStringWert(boolean einheit);//gibt den Anzeigestring mit oder ohne angehängte Einheit aus
    String getStringforDatenbank();  //gibt den String, der in der Datenbank gespeichert wird aus
                        // Zeit="minuten", Text=String, Zahl="xx.xx" Bereich= "xxxbisxxx"
    int getColums();    // gibt die Anzahl der Spalten zurück 1-3

    int getPosition(); // gibt die Position des Wertes in der Liste zurück, die in der Definition festgelegt wurde

    long getDefinitionID();// gibt die ID der Zusatzfeld Definition zurück

    ZusatzfeldDefinition getDefinition();// gibt die Definition des Feldes aus

    long save(boolean isDefaultSchicht);
    boolean isNotSave();
}
