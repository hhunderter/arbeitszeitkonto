/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.codetroopers.betterpickers.numberpicker.NumberPickerBuilder;
import com.codetroopers.betterpickers.numberpicker.NumberPickerDialogFragment;

import java.math.BigDecimal;
import java.math.BigInteger;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

public class Fragment_Dialog_Zusatzfeld_Edit
        extends
            DialogFragment
        implements
        NumberPickerDialogFragment.NumberPickerDialogHandlerV2,
        View.OnClickListener {

    private ZusatzfeldDefinition mZusatzfeld;
    //private int mEditZusatzfeldDialog_tag;
    private EditZusatzfeldDialogListener mListener;
    private boolean mIstNeu;

    private ZusatzfeldWirkungListAdapter mWirkungListAdapter;

    TextView tName;
    Spinner sTyp;
    TextView tEinheit;
    TextView tWirkung;
    Spinner sWirkung;
    TextView tSpalten;
    RelativeLayout bOptional;

    public void setup(ZusatzfeldDefinition zusatzfeld, EditZusatzfeldDialogListener listener, boolean isNeu){
       mZusatzfeld = zusatzfeld;
       //mEditZusatzfeldDialog_tag = tag;
       mListener = listener;
       mIstNeu = isNeu;
   }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setRetainInstance(true);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());

        View mInhalt = inflater.inflate(R.layout.fragment_edit_zusatzfeld, null);
        //mInhalt.setBackgroundColor(mJob.getFarbe_Hintergrund());

        // die Felder suchen
        tName = mInhalt.findViewById(R.id.ZE_wert_name);
        sTyp = mInhalt.findViewById(R.id.ZE_spinner_typ);
        bOptional = mInhalt.findViewById(R.id.ZE_box_optional);
        tEinheit = mInhalt.findViewById(R.id.ZE_wert_einheit);
        tWirkung = mInhalt.findViewById(R.id.ZE_titel_wirkung);
        sWirkung = mInhalt.findViewById(R.id.ZE_spinner_wirkung);
        tSpalten = mInhalt.findViewById(R.id.ZE_wert_spalten);

        // Der Name des Zusatzfeldes
        tName.setText(mZusatzfeld.getName());

        //Der Typ des Zusatzfeldes
        // die Typenliste enthält die Tpnummern in der Reihenfolge des angezeigten Typenmenüs
        int[] typenListe = ASetup.res.getIntArray(R.array.zusatzfeld_typ_nummer);
        int mTyp = mZusatzfeld.getTyp();
        for (int i = 0; i < typenListe.length; i++) {
            if(mTyp == typenListe[i]){
              sTyp.setSelection(i);
              break;
            }
        }
        sTyp.setEnabled(mIstNeu);
        sTyp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int item, long l) {
                int mTyp = typenListe[item];
                if(mTyp != mZusatzfeld.getTyp()) {
                    mZusatzfeld.setWirkung(IZusatzfeld.NEUTRAL);
                    sWirkung.setSelection(IZusatzfeld.NEUTRAL);
                }
                switch (mTyp) {
                    case IZusatzfeld.TYP_BEREICH_ZAHL:
                    case IZusatzfeld.TYP_ZAHL:
                    case IZusatzfeld.TYP_AUSWAHL_ZAHL:
                        tWirkung.setText(getString(R.string.feld_wirkung_zahl));
                        bOptional.setVisibility(View.VISIBLE);
                        tEinheit.setText(mZusatzfeld.getEinheit());
                        tEinheit.setEnabled(true);
                        break;
                    case IZusatzfeld.TYP_BEREICH_ZEIT:
                    case IZusatzfeld.TYP_ZEIT:
                    case IZusatzfeld.TYP_AUSWAHL_ZEIT:
                        tWirkung.setText(getString(R.string.wirkung));
                        bOptional.setVisibility(View.VISIBLE);
                        tEinheit.setText(ASetup.res.getString(R.string.k_stunde));
                        tEinheit.setEnabled(false);
                        break;
                    default:
                        bOptional.setVisibility(View.GONE);
                        tEinheit.setText("");
                }
                mWirkungListAdapter.setTyp(mTyp);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // Die Einheit des Zusatzfeldes
        tEinheit.setText(mZusatzfeld.getEinheit());

        // Auswahl für die Wirkung des Zusatzfeldes
        mWirkungListAdapter = new ZusatzfeldWirkungListAdapter(getContext(), mTyp);
        sWirkung.setAdapter(mWirkungListAdapter);
        sWirkung.setSelection(mZusatzfeld.getWirkung());

        // Die Anzahl der Spalten
        tSpalten.setText(String.valueOf(mZusatzfeld.getColums()));
        tSpalten.setOnClickListener(this);


        // Den Dialog erstellen und zurück geben
        return new AlertDialog.Builder(requireContext())
                /*.setTitle(R.string.dialog_soll_titel)*/
                .setView(mInhalt)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mZusatzfeld.setName(tName.getText().toString());
                        mZusatzfeld.setTyp(typenListe[sTyp.getSelectedItemPosition()]);
                        mZusatzfeld.setEinheit(tEinheit.getText().toString());
                        mZusatzfeld.setWirkung(sWirkung.getSelectedItemPosition());
                        mZusatzfeld.setColums(Integer.parseInt(tSpalten.getText().toString()));
                        mZusatzfeld.speichern();
                        mListener.onEditZusatzwertPositiveClick(mZusatzfeld, mIstNeu);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onEditZusatzwertNegativeClick();
                    }
                })
                .create();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == tSpalten.getId()) {
            NumberPickerBuilder mPicker = new NumberPickerBuilder()
                    .setFragmentManager(getParentFragmentManager())
                    .setStyleResId(ASetup.themePicker)
                    .setMinNumber(BigDecimal.valueOf(1))
                    .setMaxNumber(BigDecimal.valueOf(IZusatzfeld.MAX_COLUM))
                    .setLabelText(getString(R.string.spalten))
                    .setPlusMinusVisibility(View.INVISIBLE)
                    .setDecimalVisibility(View.INVISIBLE)
                    .setReference(666)
                    .setTargetFragment(this);
            mPicker.show();
        }
    }

    @Override
    public void onDialogNumberSet(int reference, BigInteger number, double decimal, boolean isNegative, BigDecimal fullNumber) {
        if(reference == 666){
           tSpalten.setText(String.valueOf(number));
        }
    }

    public interface EditZusatzfeldDialogListener{
		void onEditZusatzwertPositiveClick(ZusatzfeldDefinition zusatzFeld, boolean isNeu);

		void onEditZusatzwertNegativeClick();
    }

}
