/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;

import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

public class ZusatzWertAuswahlListe {
    private final ArrayList<zusatzWertAuswahlItem> mAuswahlListe;
    private final ZusatzfeldDefinition zusatzfeldDefinition;

    // erstellt die Auswahlliste eines Zusatzwertes
    @SuppressLint("Range")
    public ZusatzWertAuswahlListe(IZusatzfeld zusatzfeld) {
        zusatzfeldDefinition = ASetup.aktJob.getZusatzDefinition(zusatzfeld.getDefinitionID());
        mAuswahlListe = new ArrayList<>();

        final String SQL_READ_ZUSAETZE =
                "select * from "
                        + Datenbank.DB_T_ZUSATZWERT_AUSWAHL
                        + " where "
                        + Datenbank.DB_F_ZUSATZFELD
                        + " = ?";

        Cursor result = ASetup.mDatenbank.rawQuery(
                SQL_READ_ZUSAETZE,
                new String[]{Long.toString(zusatzfeldDefinition.getID())}
        );

        while (result.moveToNext()) {
                mAuswahlListe.add(new zusatzWertAuswahlItem(result));
            }
        result.close();
        sort(ASetup.mPreferenzen.getInt(
                    ISetup.KEY_SORT_AUSWAHLLISTE,
                    ISetup.SORT_NO
                )
        );
    }

    // kopiert die ZusatzwertAuswahlListe
    public ZusatzWertAuswahlListe copy(IZusatzfeld zusatzfeld){
        ZusatzWertAuswahlListe ziel = new ZusatzWertAuswahlListe(zusatzfeld);
        zusatzWertAuswahlItem neu;
        for (zusatzWertAuswahlItem auswahlItem : mAuswahlListe) {
             neu = new zusatzWertAuswahlItem();
             neu.wert.set(auswahlItem.getWert());
            ziel.mAuswahlListe.add(neu);

        }
        return ziel;
    }

    public void sort(int sortBy){
        switch(sortBy){
                case ISetup.SORT_AZ:
                    sortNachName(false);
                    break;
                case ISetup.SORT_ZA:
                    sortNachName(true);
                    break;
                default:
                    sortNachBenutzung();
        }
    }
    private void sortNachName(boolean rev) {
        Comparator<zusatzWertAuswahlItem> comp = rev ? new NameComparator_ZA() : new NameComparator_AZ();
        try {
            Collections.sort(mAuswahlListe, comp);
        } catch (ConcurrentModificationException ce) {
            ce.printStackTrace();
        }
    }

    private void sortNachBenutzung() {
        Comparator<zusatzWertAuswahlItem> comp = new BenutzungComparator();
        try {
            Collections.sort(mAuswahlListe, comp);
        } catch (ConcurrentModificationException ce){
            ce.printStackTrace();
        }
    }

    public void speichern() {
        for (zusatzWertAuswahlItem auswahlItem : mAuswahlListe) {
            auswahlItem.save();
        }
    }

    public void zusatzWertAddVerwendung(int wert){
        mAuswahlListe.get(wert).addVerwendet();
    }

    public void zusatzWertSubVerwendung(IZusatzfeld wert) {
        for (zusatzWertAuswahlItem item : mAuswahlListe) {
            if (wert.getStringWert(false).equals(item.getWert().getStringWert(false))) {
                item.subVerwendet();
                break;
            }
        }
    }


    public zusatzWertAuswahlItem addEintrag(@NonNull IZusatzfeld feld){
        zusatzWertAuswahlItem neu = new zusatzWertAuswahlItem();
        neu.wert.set(feld);
        neu.addVerwendet();
        mAuswahlListe.add(neu);
        return neu;
    }

    public void setEintragWert(int position, @NonNull IZusatzfeld wert){
        mAuswahlListe.get(position).setWert(wert);
    }

    public int size(){
       return mAuswahlListe.size();
    }

    public IZusatzfeld getEintragWert(int index){
        if(index < mAuswahlListe.size())
            return mAuswahlListe.get(index).wert;
        else
            return null;
    }

    public IZusatzfeld getEintragWert(long id){
        for (zusatzWertAuswahlItem auswahlItem: mAuswahlListe) {
            if(auswahlItem.id == id)
                return auswahlItem.wert;
        }
        return null;
    }

    public ArrayList<zusatzWertAuswahlItem> getListe(){
        return mAuswahlListe;
    }

    public void loescheEintrag(int position){
        mAuswahlListe.get(position).loeschen();
        mAuswahlListe.remove(position);
    }

    /*
     * Sortierer für die Liste
     */
    private static class NameComparator_AZ implements Comparator<zusatzWertAuswahlItem> {

        @Override
        public int compare(zusatzWertAuswahlItem wert1, zusatzWertAuswahlItem wert2) {
            return wert1.wert.getString(false).toLowerCase()
                    .compareTo(wert2.wert.getString(false).toLowerCase());
        }
    }
    private static class NameComparator_ZA implements Comparator<zusatzWertAuswahlItem> {
        @Override
        public int compare(zusatzWertAuswahlItem wert1, zusatzWertAuswahlItem wert2) {
            return wert2.wert.getString(false).toLowerCase()
                    .compareTo(wert1.wert.getString(false).toLowerCase());
        }
    }

    private static class BenutzungComparator implements Comparator<zusatzWertAuswahlItem> {
        @Override
        public int compare(zusatzWertAuswahlItem wert1, zusatzWertAuswahlItem wert2) {
            return wert2.verwendetMenge - wert1.verwendetMenge;
        }
    }

    /*
     + Innere Klasse eines Listenitems
     */
    public class zusatzWertAuswahlItem {
        private int verwendetMenge = 0;
        private long id = -1;
        private final IZusatzfeld wert;
        private boolean isNotSave;
        zusatzWertAuswahlItem(){
            wert = zusatzfeldDefinition.makeNewZusatzfeld();
            isNotSave = true;
        }
        @SuppressLint("Range")
        zusatzWertAuswahlItem(Cursor result) {
            verwendetMenge = result.getInt(result.getColumnIndex(Datenbank.DB_F_ANZAHL_VERWENDET));
            id = result.getLong(result.getColumnIndex(Datenbank.DB_F_ID));
            wert = zusatzfeldDefinition.makeNewZusatzfeld(result);
            isNotSave = false;
        }

        public void setWert(@NonNull IZusatzfeld wert){
            if(!this.wert.getStringWert(false).equals(wert.getStringWert(false))) {
                this.wert.set(wert);
                isNotSave = true;
                save();
            }
        }

       IZusatzfeld getWert(){
            return wert;
        }

        public int getVerwendetMenge(){
            return verwendetMenge;
        }

        private void addVerwendet(){
            verwendetMenge++;
            isNotSave = true;
            save();
            //return verwendetMenge;
        }

        private void subVerwendet(){
            if(verwendetMenge > 0) {
                verwendetMenge--;
                isNotSave = true;
                save();
            }
            //return verwendetMenge;
        }

        private void loeschen(){
            ASetup.mDatenbank.delete(
                    Datenbank.DB_T_ZUSATZWERT_AUSWAHL,
                    Datenbank.DB_F_ID + "=?",
                    new String[]{Long.toString(id)});
        }

        private void save(){
          if(isNotSave){
              ContentValues werteSave = new ContentValues();
              String stringWert = wert.getStringforDatenbank();

              if(!stringWert.isEmpty()) {
                  werteSave.put(Datenbank.DB_F_ZUSATZFELD, wert.getDefinitionID());
                  werteSave.put(Datenbank.DB_F_ANZAHL_VERWENDET, verwendetMenge);
                  werteSave.put(Datenbank.DB_F_WERT, stringWert);

                  if(id < 0){
                      id = ASetup.mDatenbank.insert(
                              Datenbank.DB_T_ZUSATZWERT_AUSWAHL,
                              null,
                              werteSave
                      );
                  } else {
                      ASetup.mDatenbank.update(
                           Datenbank.DB_T_ZUSATZWERT_AUSWAHL ,
                            werteSave,
                            Datenbank.DB_F_ID + "=?", new String[]{Long.toString(id)});
                  }
              }
              isNotSave = false;
          }
        }

    }
}
