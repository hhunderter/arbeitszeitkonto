/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

public class ZusatzfeldDefinitionViewAdapter extends RecyclerView.Adapter<ZusatzfeldDefinitionViewAdapter.ViewHolder>  {
    private ZusatzfeldDefinitionenListe mWerte;
    private ItemClickListener mClickListener;
    private Arbeitsplatz mArbeitsplatz;

    public void setUp(Arbeitsplatz arbeitsplatz, ItemClickListener clickListener){
        mArbeitsplatz = arbeitsplatz;
        mWerte = mArbeitsplatz.getZusatzfeldListe();
        mClickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(mInflater.inflate(R.layout.item_zusatzfeld_definition, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(position < mWerte.size()) {
            ZusatzfeldDefinition zf = getItem(position);
            holder.mName.setText(zf.getName());
            switch (zf.getTyp()) {
                case IZusatzfeld.TYP_TEXT:
                    holder.mBeschreibung.setText(ASetup.res.getString(R.string.feld_freetext));
                    break;
                case IZusatzfeld.TYP_AUSWAHL_TEXT:
                    holder.mBeschreibung.setText(ASetup.res.getString(R.string.feld_auswahltext));
                    break;
                case IZusatzfeld.TYP_AUSWAHL_ZAHL:
                case IZusatzfeld.TYP_AUSWAHL_ZEIT:
                    holder.mBeschreibung.setText(ASetup.res.getString(R.string.feld_auswahlwert,
                                    zf.getEinheit()));
                    break;
                case IZusatzfeld.TYP_BEREICH_ZAHL:
                case IZusatzfeld.TYP_BEREICH_ZEIT:
                    holder.mBeschreibung.setText(ASetup.res.getString(R.string.feld_bereich));
                    break;
                default:
                    holder.mBeschreibung.setText(
                            ASetup.res.getString(R.string.feld_wert,
                                    zf.getEinheit())
                    );
            }
        }
    }

    @Override
    public int getItemCount() {
        if(mWerte != null) {
            return mWerte.size();
        }
        return 0;
    }

    ZusatzfeldDefinition getItem(int index) {
        if(index < mWerte.size()) {
            return mWerte.get(index);
        }
        return null;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onZusatzWertDefinitionClick(ZusatzfeldDefinition feld, boolean delete);
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mName;
        TextView mBeschreibung;
        ImageView mDelete;

        //@SuppressLint("UseCompatLoadingForDrawables")
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            mName = itemView.findViewById(R.id.IZ_name);
            mBeschreibung = itemView.findViewById(R.id.IZ_wert);
            mDelete = itemView.findViewById(R.id.IZ_button_delete);
            /*itemView.setBackground(
                    ResourcesCompat.getDrawable(
                            ASetup.res,
                            ASetup.isThemaDunkel ?
                                    android.R.drawable.editbox_dropdown_dark_frame :
                                    android.R.drawable.editbox_dropdown_light_frame,
                            itemView.getContext().getTheme()
                    ));*/
            itemView.setBackgroundColor(mArbeitsplatz.getFarbe_Tag());
            itemView.setOnClickListener(this);
            mDelete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null )
                mClickListener.onZusatzWertDefinitionClick(getItem(getAdapterPosition()), view == mDelete);
        }
    }
}

