/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import androidx.annotation.NonNull;
import askanimus.arbeitszeiterfassung2.Uhrzeit;

public class Zeitfeld extends AZusatzfeld {
    protected Uhrzeit mZeit;
    protected boolean istUhrzeit;

    Zeitfeld(
            long id,
            long schichtID,
            @NonNull ZusatzfeldDefinition definition,
            int minuten,
            boolean iszeit) {
        super(definition, id, schichtID);
        mZeit = new Uhrzeit(minuten);
        istUhrzeit = iszeit;
    }

    @Override
    public void set(IZusatzfeld wert) {
        if (wert != null) {
            if (wert.getDatenTyp() == IZusatzfeld.TYP_ZEIT || wert.getDatenTyp() == IZusatzfeld.TYP_AUSWAHL_ZEIT) {
                int w = (int) wert.getWert();
                if (w != mZeit.getAlsMinuten()) {
                    mZeit.set(w);
                    isNotSave = true;
                }
            }
        } else {
            mZeit.set(0);
            isNotSave = true;
        }
    }

    @Override
    public void setWert(Object wert) {
        if (wert != null) {
            int w = (int) wert;
            if (w != mZeit.getAlsMinuten()) {
                mZeit.set(w);
                isNotSave = true;
            }
        } else {
            mZeit.set(0);
            isNotSave = true;
        }
    }

    @Override
    public void add(IZusatzfeld zeitfeld) {
        if (zeitfeld != null)
            if (zeitfeld.getDatenTyp() == IZusatzfeld.TYP_ZEIT ||
                zeitfeld.getDatenTyp() == IZusatzfeld.TYP_BEREICH_ZEIT) {
                mZeit.add((int) zeitfeld.getWert());
            }
    }

    @Override
    public IZusatzfeld getKopie(){
        return new Zeitfeld(-1, mSchichtID, mDefinition, mZeit.getAlsMinuten(), istUhrzeit);
    }

    @Override
    public IZusatzfeld[] get() {
        return new IZusatzfeld[]{this};
    }

    @Override
    public Object getWert() {
        return mZeit.getAlsMinuten();
    }

    /*@Override
    public String getString(boolean symbol){
        if(istUhrzeit) {
            return mZeit.getUhrzeitString(symbol);
        } else {
           return mZeit.getStundenString(symbol, isDezimal);
        }
    }*/

   @Override
    public String getStringWert(boolean symbol) {
       if(istUhrzeit) {
           return mZeit.getUhrzeitString();
       } else {
           return mZeit.getStundenString(symbol, mDefinition.isDezimal());
       }
    }

    @Override
    public String getStringforDatenbank(){
       if(mZeit.getAlsMinuten() != 0) {
           return String.valueOf(mZeit.getAlsMinuten());
       } else {
           return "";
       }
    }

    /*@Override
    public int getColums() {
        return 1;
    }*/
}
