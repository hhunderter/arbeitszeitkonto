/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.R;

public class ZusatzWertViewAdapter extends RecyclerView.Adapter<ZusatzWertViewAdapter.ViewHolder> {
    public final static int VIEW_EDIT = 0;
    public final static int VIEW_INFO = 1;
    public final static int VIEW_KOPF = 2;

    private ArrayList<IZusatzfeld> mWerte;
    private ItemClickListener mClickListener = null;
    private final int mViewTyp;

    public ZusatzWertViewAdapter(int viewTyp){
        mViewTyp = viewTyp;
    }

    public ZusatzWertViewAdapter(ArrayList<IZusatzfeld> werte, ItemClickListener cl, int viewTyp) {
        mWerte = werte;
        mClickListener = cl;
        mViewTyp = viewTyp;
    }

    public void setUp(ArrayList<IZusatzfeld> werte, ItemClickListener cl){
        mWerte = werte;
        mClickListener = cl;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        switch (mViewTyp) {
            case VIEW_EDIT:
                view = mInflater.inflate(R.layout.item_zusatzwert_edit, parent, false);
                break;
            case VIEW_INFO:
                view = mInflater.inflate(R.layout.item_zusatzwert_info, parent, false);
                break;
            default:
                view = mInflater.inflate(R.layout.item_zusatzwert_kopf, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        IZusatzfeld mFeld = getItem(position);
        TextView tView;
        switch (mViewTyp) {
            case VIEW_EDIT:
                IZusatzfeld[] mWert = mFeld.get();
                ((TextView) holder.mWerteView.findViewById(R.id.ZW_name)).setText(mFeld.getName());
                tView = holder.mWerteView.findViewById(R.id.ZW_wert_1);
                tView.setText(mWert[0].getString(true));
                tView.setOnClickListener(holder);
                if (mWert.length > 1) {
                    tView = holder.mWerteView.findViewById(R.id.ZW_wert_2);
                    tView.setText(mWert[1].getString(true));
                    tView.setOnClickListener(holder);
                } else {
                    holder.mWerteView.findViewById(R.id.ZW_box_rechts).setVisibility(View.GONE);
                    holder.mWerteView.findViewById(R.id.ZW_trenner).setVisibility(View.GONE);
                }
                break;
            case VIEW_INFO:
                ((TextView) holder.mWerteView.findViewById(R.id.ZW_name)).setText(mFeld.getName());
                tView = holder.mWerteView.findViewById(R.id.ZW_wert);
                tView.setText(mFeld.getStringWert(true));
                break;
            default:
                // es ist die Anzeige der Zusammenfassung im Kopfbereich des Views (Jahr, Monat, Woche)
                // hier werden keine Notizen angezeigt
                if (mFeld.getDatenTyp() != IZusatzfeld.TYP_TEXT) {
                    ((TextView) holder.mWerteView.findViewById(R.id.ZW_name)).setText(mFeld.getName());
                    ((TextView) holder.mWerteView.findViewById(R.id.ZW_wert)).setText(mFeld.getStringWert(true));
                } else {
                    holder.mWerteView.setVisibility(View.GONE);
                }
        }
    }

    @Override
    public int getItemCount() {
        if(mWerte != null)
            return mWerte.size();
        return 0;
    }

    IZusatzfeld getItem(int index) {
        return mWerte.get(index);
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, IZusatzfeld feld);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        View mWerteView;

        ViewHolder(View itemView) {
            super(itemView);
            mWerteView = itemView;
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null )
                mClickListener.onItemClick(view, getItem(getAdapterPosition()));
        }
    }
}

