/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASetup;


/**
 * @author askanimus@gmail.com on 23.12.15.
 */
public class ZusatzfeldWirkungListAdapter extends BaseAdapter implements View.OnClickListener {
    private final Context mContext;

    private ArrayList<String> mWirkungen;
    private int mTyp;

    public ZusatzfeldWirkungListAdapter(Context ctx, int typ) {
        mContext = ctx;
        mWirkungen = new ArrayList<>(Arrays.asList(ASetup.res.getStringArray(R.array.zusatzfeld_wirkung)));
        mTyp = typ;
    }

    public void setTyp(int typ){
        mTyp = typ;
    }

    @Override
    public int getCount() {
        return mWirkungen.size();
    }

    @Override
    public Object getItem(int position) {
        return mWirkungen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            TextView row = (TextView) inflater.inflate(android.R.layout.simple_spinner_dropdown_item, viewGroup, false);
            row.setText(mWirkungen.get(position));

            switch (mTyp){
                case IZusatzfeld.TYP_TEXT:
                case IZusatzfeld.TYP_AUSWAHL_TEXT:
                    if(position != IZusatzfeld.NEUTRAL) {
                        row.setEnabled(false);
                        row.setOnClickListener(this);
                    }
                    break;
                case IZusatzfeld.TYP_BEREICH_ZAHL:
                case IZusatzfeld.TYP_ZAHL:
                case IZusatzfeld.TYP_AUSWAHL_ZAHL:
                    switch (position){
                        case IZusatzfeld.ADD_ISTSTUNDEN:
                        case IZusatzfeld.SUB_ISTSTUNDEN:
                        case IZusatzfeld.SUB_SOLLSTUNDEN:
                            row.setEnabled(false);
                            row.setOnClickListener(this);
                            break;
                    }
                    break;
                case IZusatzfeld.TYP_BEREICH_ZEIT:
                case IZusatzfeld.TYP_ZEIT:
                case IZusatzfeld.TYP_AUSWAHL_ZEIT:
                    switch (position){
                        case IZusatzfeld.ADD_VERDIENST:
                        case IZusatzfeld.SUB_VERDIENST:
                            row.setEnabled(false);
                            row.setOnClickListener(this);
                            break;
                    }
                    break;
            }

            return row;
        } else
            return null;
    }

    @Override
    public void onClick(View view) {
        // tut nichts, weil ein deaktiver Eintrag angetippt wurde
    }
}
