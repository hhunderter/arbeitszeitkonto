/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import android.annotation.SuppressLint;
import android.database.Cursor;
import java.util.ArrayList;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

public class TextFeldTextListe {
    ArrayList<String> listeTexte;

    @SuppressLint("Range")
    public TextFeldTextListe(long zusatzFeldID){
        listeTexte = new ArrayList<>();
        final String SQL_READ_ZUSAETZE =
                "select " +
                        Datenbank.DB_F_WERT +
                        " from "
                        + Datenbank.DB_T_ZUSATZWERT
                        + " where "
                        + Datenbank.DB_F_ZUSATZFELD
                        + " = ?";
        Cursor result = ASetup.mDatenbank.rawQuery(
                SQL_READ_ZUSAETZE,
                new String[]{Long.toString(zusatzFeldID)}
        );

        while (result.moveToNext()){
            String s = result.getString(result.getColumnIndex(Datenbank.DB_F_WERT));
            if(!listeTexte.contains(s)) {
                listeTexte.add(s);
            }
        }

        result.close();
    }

    public ArrayList<String> findText(String begintMit){
        ArrayList<String> fundListe = new ArrayList<>();
        for (String s : listeTexte) {
            if(s.toLowerCase().startsWith(begintMit.toLowerCase())){
                //if(!fundListe.contains(s)) {
                    fundListe.add(s);
               //}
            }
        }
        return fundListe;
    }

    public String[] getArray(){
        return listeTexte.toArray(new String[0]);
    }
}
