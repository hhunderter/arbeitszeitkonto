/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.zusatzeintrag;

import androidx.annotation.NonNull;

public class Textfeld extends AZusatzfeld {
    String mText;

    Textfeld(long id, long schichtID, @NonNull ZusatzfeldDefinition definition, @NonNull String text) {
        super(definition, id, schichtID);
        mText = text;
    }

    /*
     * Eingaben
     */

    @Override
    public void set(IZusatzfeld wert){
        if(wert != null){
            if (!mText.equals(wert.getString(false))) {
                mText = wert.getString(false);
                isNotSave = true;
            }
        } else {
            mText = "";
            isNotSave = true;
        }
    }

    @Override
    public void setWert(Object wert){
        if(wert != null) {
            if (!mText.equals(wert)) {
                mText = (String)wert;
                isNotSave = true;
            }
        } else {
            mText = "";
            isNotSave = true;
        }
    }

    /*
     * Manipulationen
     */
    @Override
    public void add(IZusatzfeld textfeld) {
        if (textfeld != null)
            if (textfeld.getDatenTyp() == IZusatzfeld.TYP_TEXT) {
                if (mText.isEmpty()) {
                    mText = textfeld.getString(false);
                } else {
                    mText = mText
                            + "\n"
                            + textfeld.getString(false);
                }
            }
    }

    @Override
    public IZusatzfeld getKopie(){
        return new Textfeld(-1, mSchichtID, mDefinition, mText);
    }


    /*
     * Ausgaben
     */
    @Override
    public IZusatzfeld[] get() {
        return new IZusatzfeld[]{this};
    }

    @Override
    public Object getWert() {
        return 0;
    }

    @Override
    public String getStringWert(boolean symbol) {
        return mText;
    }

    @Override
    public String getStringforDatenbank(){
        return mText;
    }
}
