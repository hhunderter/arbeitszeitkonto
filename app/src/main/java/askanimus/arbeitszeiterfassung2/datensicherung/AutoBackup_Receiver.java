/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.datensicherung;

import static androidx.core.app.NotificationCompat.EXTRA_NOTIFICATION_ID;
import android.app.NotificationChannel;
import android.app.NotificationManager;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Calendar;

import android.os.Build;
import android.os.Environment;

import androidx.documentfile.provider.DocumentFile;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import askanimus.arbeitszeiterfassung2.BuildConfig;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

public class AutoBackup_Receiver extends BroadcastReceiver implements ISetup {
    private StorageHelper mStorageHelper;
    private Context mContext;
    private int backupIntertvall;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        ASetup.init(context, this::receive);
    }

    private void receive(){
        backupIntertvall = ASetup.mPreferenzen.getInt(KEY_AUTOBACKUP_INTERVAL, AUTOBACKUP_NO);
        if(backupIntertvall > AUTOBACKUP_NO) {
            backupIntertvall = AUTOBACKUP_INTERVAL[backupIntertvall];
            String mExportPfad = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + File.separator
                    + ASetup.res.getString(R.string.app_verzeichnis);
            mExportPfad = ASetup.mPreferenzen.getString(ISetup.KEY_BACKUP_DIR, mExportPfad);

            if (mExportPfad != null) {
                mStorageHelper = new StorageHelper(
                        mContext,
                        mExportPfad,
                        Datenbank.DB_F_BACKUP_DIR,
                        ISetup.KEY_BACKUP_DIR,
                        true,
                        ISetup.REQ_FOLDER_PICKER_WRITE_BACKUP/*,
                        this::sicherungDB*/
                );
                sicherungDB();
            }
        }
    }

    /*
     * Sicherung anlegen, Erfolgsmeldung als Notification ausgeben
     */
    private void sicherungDB(){
        new Thread(() -> {
            boolean mStatus = false;
            // Datensicherung anlegen
            if (mStorageHelper.isWritheable()) {
                SQLiteDatabase mDatenbank = ASetup.mDatenbank;

                // nächste Sicherung planen
                SharedPreferences.Editor mEditor = ASetup.mPreferenzen.edit();
                Calendar next = Calendar.getInstance();
                next.setTimeInMillis(new Date().getTime());//ASetup.mPreferenzen.getLong(KEY_AUTOBACKUP_NAECHSTES, new Date().getTime()));
                next.set(Calendar.SECOND, 0);
                mEditor.putLong(KEY_AUTOBACKUP_LETZTES, next.getTimeInMillis());
                next.add(
                        backupIntertvall,
                        ASetup.mPreferenzen.getInt(KEY_AUTOBACKUP_SCHRITTE, 1)
                );
                mEditor.putLong(KEY_AUTOBACKUP_NAECHSTES, next.getTimeInMillis());
                mEditor.apply();

                // die allgem. Einstellungen sichern
                ContentValues mWerte = new ContentValues();
                mWerte.put(Datenbank.DB_F_VERSION, BuildConfig.VERSION_CODE);
                mWerte.put(Datenbank.DB_F_JOB, ASetup.aktJob.getId());
                mWerte.put(Datenbank.DB_F_VIEW, ASetup.mPreferenzen.getInt(ISetup.KEY_ANZEIGE_VIEW, ISetup.VIEW_TAG));
                if (!mDatenbank.isOpen()) {
                    mDatenbank = ASetup.stundenDB.getWritableDatabase();
                }
                mDatenbank.update(Datenbank.DB_T_SETTINGS, mWerte,
                        Datenbank.DB_F_ID + "=?", new String[]{String.valueOf(1)});

                // die Datenbank in XML File exportieren
                Datenbank_Backup mBackup = new Datenbank_Backup(mContext, mDatenbank, mStorageHelper);
                try {
                    mBackup.backup();
                    mStatus = true;
                } catch (NullPointerException | ParserConfigurationException | IOException | TransformerException e) {
                    e.printStackTrace();
                }

                // älteste Sicherung löschen
                loescheAlteSicherungen();
            }

            // Erfolgsmeldung
            showNotification(mContext, mStatus);
        }).start();
    }

    /*
     * älteste Sicherungen laut Einstellungen löschen
     */
    private void loescheAlteSicherungen() {
        ArrayList<DocumentFile> fSicherungen = new ArrayList<>();

        // Dateien einlesen
        int anzahl = ASetup.mPreferenzen.getInt(KEY_AUTOBACKUP_ANZAHL, 0);
        DocumentFile dir = mStorageHelper.getVerzeichnisFile();
        if (anzahl > 0 && dir != null && dir.exists()) {
            //Calendar cal = Calendar.getInstance();
            //cal.setTimeInMillis(ASetup.mPreferenzen.getLong(KEY_AUTOBACKUP_LETZTES, new Date().getTime()));
            //cal.add(backupIntertvall, -(anzahl * ASetup.mPreferenzen.getInt(KEY_AUTOBACKUP_SCHRITTE, 1)));
            //long letzte = cal.getTimeInMillis();

            // Liste einlesen
            // doppelte Sicherungen (enthalten im Namen '(x)') löschen
            for (DocumentFile docFile : dir.listFiles()) {
                if (docFile.getName() != null)
                    if (docFile.isFile() && docFile.getName().endsWith(".xml")) {
                        if(docFile.getName().contains("(")){
                            docFile.delete();
                        } else {
                            fSicherungen.add(docFile);
                        }
                    }
            }

            // Die Liste aufsteigend nach Dateiname (Datum und Uhrzeit) sortieren
            Collections.sort(fSicherungen, new Comparator<DocumentFile>() {
                final DateFormat f = new SimpleDateFormat(
                        "dd-MM-yyyy_hh.mm.ss",
                        Locale.getDefault());

                @Override
                public int compare(DocumentFile f1, DocumentFile f2) {
                    String o1 = f1.getName();
                    String o2 = f2.getName();
                    if(o1 != null & o2 != null) {
                        o1 = o1.replace(".xml", "");
                        o1 = o1.replace(ASetup.res.getString(R.string.sich_vom_datei), "");
                        o2 = o2.replace(".xml", "");
                        o2 = o2.replace(ASetup.res.getString(R.string.sich_vom_datei), "");

                        try {
                            return Objects.requireNonNull(
                                    f.parse(o2)).compareTo(f.parse(o1)
                            );
                        } catch (ParseException e) {
                            return 0;
                        }
                    }
                    return 0;
                }
            });

            // alle bis auf die letzten x Einträge ( die ersten in der Liste löschen
            for(DocumentFile file : fSicherungen){
                if (anzahl <= 0){
                    file.delete();
                } else {
                    anzahl --;
                }
            }
        }
    }

    private void showNotification(Context context, boolean status){
        String CHANNEL_ID = "999";

        // Notification Chanel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getResources().getString(R.string.autobackup);
            String description = context.getResources().getString(R.string.descript_autobackup);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }

        String meldung;
        String titel;

        if(status){
            titel = context.getResources().getString(R.string.not_titel_autobackup);
            meldung = context.getResources().getString(R.string.not_text_autobackup);
        } else {
            titel = context.getResources().getString(R.string.not_titel_autobackup_fail);

            if(!mStorageHelper.isExists()){
                meldung = context.getResources().getString(
                        R.string.not_text_autobackup_fail,
                        mStorageHelper.getPfadSubtree(),
                        context.getResources().getString(R.string.not_no_exists)
                );
            } else /*if(!mStorageHelper.isWritheable())*/{
                meldung = context.getResources().getString(
                        R.string.not_text_autobackup_fail,
                        mStorageHelper.getPfadSubtree(),
                        context.getResources().getString(R.string.not_no_writeable)
                );
            }
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_backup)
                .setContentTitle(titel)
                .setContentText(meldung)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(meldung))
                .setPriority(NotificationCompat.PRIORITY_LOW);

        if(!status) {
            Intent intent = new Intent(mContext, Datensicherung_Activity.class);
            intent.setAction(Datensicherung_Activity.ACTIVITY_START);

            PendingIntent pendingIntent = PendingIntent.getActivity(
                    mContext,
                    0,
                    intent,
                    (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)?PendingIntent.FLAG_MUTABLE:0);
            intent.putExtra(EXTRA_NOTIFICATION_ID, 0);
            builder.setContentIntent(pendingIntent);
            builder.addAction(
                    R.drawable.ic_stat_backup,
                    context.getResources().getString(R.string.not_button),
                    pendingIntent
            ).setAutoCancel(true);
        } else {
            Intent intent = new Intent(mContext, Datensicherung_Activity.class);
            intent.setAction(Datensicherung_Activity.ACTIVITY_START);

            PendingIntent pendingIntent = PendingIntent.getActivity(
                    mContext,
                    0,
                    intent,
                    (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)?PendingIntent.FLAG_MUTABLE:0
            );
            intent.putExtra(EXTRA_NOTIFICATION_ID, 0);
            builder.setContentIntent(pendingIntent)
                    .setAutoCancel(true);
        }


        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.notify(666, builder.build());
    }


}
