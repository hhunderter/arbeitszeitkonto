/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.datensicherung;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.backup.BackupManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import androidx.documentfile.provider.DocumentFile;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import askanimus.arbeitszeiterfassung2.BuildConfig;
import askanimus.arbeitszeiterfassung2.MainActivity;
import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.ArbeitsplatzListe;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank_Migrate;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank_toMigrate;
import askanimus.arbeitszeiterfassung2.export.IExport_Basis;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;
import askanimus.arbeitszeiterfassung2.setup.LocaleHelper;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.SettingsActivity;

public class Datensicherung_Activity
        extends
        AppCompatActivity
        implements
        View.OnClickListener,
        PopupMenu.OnMenuItemClickListener,
        ISetup,
        Datensicherung_ViewAdapter.ItemClickListener {

    static public final String ACTIVITY_START = "start_datensicherung";
    public static final int INPUTSTREAM_LAENGE = 46;
    public static final int UPDATE_INTEVAL =5;

    RelativeLayout mRootView;
    private Context mContext;

    private ArrayList<String> listeSicherungen;
    private Datensicherung_ViewAdapter myAdapter;
    private RecyclerView listView;

    private Toolbar mToolbar;

    private int positionSicherung;

    private AppCompatEditText tPfad;

    private StorageHelper mStorageHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setTheme(
                PreferenceManager
                        .getDefaultSharedPreferences(mContext)
                        .getBoolean(ASetup.KEY_THEMA_DUNKEL, false) ?
                        R.style.MyFullscreenTheme :
                        R.style.MyFullscreenTheme_Light
        );
        setContentView(R.layout.activity_datensicherung);
        mRootView = findViewById(R.id.D_root);
        mToolbar = findViewById(R.id.D_toolbar);
        if(getSupportActionBar() == null)
            setSupportActionBar(mToolbar);
    }

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(LocaleHelper.onAttach(base));
	}


    @Override
    protected void onResume() {
        super.onResume();

        ASetup.init(mContext, this::resume);
    }

    private void resume() {
        mToolbar.setTitle(ASetup.res.getString(R.string.title_activity_datensicherung));
        listView = findViewById(R.id.D_liste_sicherungen);
        tPfad = findViewById(R.id.D_wert_verzeichnis);
        tPfad.setOnClickListener(this);

        // das Backup Verzeichnis abgleichen
        if (mStorageHelper == null) {
            String mExportPfad;
            mExportPfad = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + File.separator
                    + getString(R.string.app_verzeichnis)
                    + File.separator
                    + getString(R.string.app_verzeichnis_backup
            );
            mExportPfad = ASetup.mPreferenzen.getString(
                        ISetup.KEY_BACKUP_DIR,
                        mExportPfad
                );

            if (mExportPfad != null) {
                mStorageHelper = new StorageHelper(
                        this,
                        mExportPfad,
                        Datenbank.DB_F_BACKUP_DIR,
                        ISetup.KEY_BACKUP_DIR,
                        true,
                        REQ_FOLDER_PICKER_WRITE_BACKUP
                );
            }
        }

        if (mStorageHelper.isWritheable()) {
            updateUI();
        }
    }

    void updateUI(){
        AppCompatButton bAktSicherung = findViewById(R.id.D_button_sicherung);
        ViewCompat.setBackgroundTintList(bAktSicherung, ASetup.aktJob.getFarbe_Button());
        bAktSicherung.setTextColor(ASetup.aktJob.getFarbe_Schrift_Button());

        if (mStorageHelper.isWritheable()) {
            tPfad.setText(mStorageHelper.getPfadSubtree());
            // Die Liste der Datensicherungen erzeugen, wenn sie nicht schon angelegt wurde
            if(listeSicherungen == null){
                setUpListe();
            }
            // der Button zum Backup anlegen
            bAktSicherung.setOnClickListener(this);
        } else {
            bAktSicherung.setEnabled(false);
        }
    }

    /*
     * Rückmeldung vom Rechtemanagent nach Rechteeinforderung
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
            requestCode = requestCode & 0x0000ffff;

            if (requestCode == REQ_DEMAND_WRITE) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mStorageHelper.setPfad(mStorageHelper.getPfad());
                    updateUI();
                } else {
                    // Recht verweigert die Exportaktivity wird geschlossen
                    Toast.makeText(
                            Datensicherung_Activity.this,
                            getString(R.string.err_keine_berechtigung),
                            Toast.LENGTH_LONG
                    ).show();
                    finish();
                }
            }
        }
    }


    /*
     * Liste der Sicherungen neu anlegen
     * wird beim Start der Activity und nach jeder Änderung( Eintrag löschen) ausgeführt
     */
    private void setUpListe() {
        myAdapter = new Datensicherung_ViewAdapter();
        listeSicherungen = myAdapter.setUp(mContext, mStorageHelper, this, true);
        GridLayoutManager gLayoutManager = new GridLayoutManager(
                mContext,
                1);
        listView.setLayoutManager(gLayoutManager);
        listView.setAdapter(myAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Ein Einstellungen Icon in der Toolbar anzeigen
        getMenuInflater().inflate(R.menu.menu_sicherungen_einstellungen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.sicherung_action_setting){
            Intent iSettings = new Intent();
            iSettings.setClass(this, SettingsActivity.class);
            iSettings.putExtra(KEY_EDIT_JOB, ASetup.aktJob.getId());
            iSettings.putExtra(KEY_INIT_SEITE, 1);
            finish();
            startActivity(iSettings);
        }
        return super.onOptionsItemSelected(item);
    }

    // Popup Menu mit Optionen zur Wiederherstellung
    public void showMenu(View view) {
        PopupMenu popup;
        popup = new PopupMenu(this, view);
        popup.setOnMenuItemClickListener(this);
        if (view.getId() == R.id.ID_button_send) {
            popup.inflate(R.menu.menu_sicherung_send);
        } else {
            popup.inflate(R.menu.menu_sicherung);
        }
        popup.show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        int id = menuItem.getItemId();
        final String mDateiname = listeSicherungen.get(positionSicherung);
            PackageManager pm = mContext.getPackageManager();

            if (id == R.id.sich_action_restore) {
                restoreSicherung(mDateiname);
            } else if (id == R.id.sich_action_kopie) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                intent.putExtra("android.content.extra.SHOW_ADVANCED", true);
                intent.putExtra("android.content.extra.FANCY", true);
                intent.putExtra("android.content.extra.SHOW_FILESIZE", true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    DocumentFile f = mStorageHelper.getVerzeichnisFile();
                    if (f != null) {
                        intent.putExtra(
                                DocumentsContract.EXTRA_INITIAL_URI,
                                f.getUri()
                        );
                    }
                }

                intent.putExtra(Datenbank.DB_F_NAME, mDateiname);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                        | Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                startActivityForResult(intent, REQ_MAKE_COPY);
            } else if (id == R.id.sich_action_send) {
                String s = getString(R.string.sich_von) + " " +
                        ASetup.mPreferenzen.getString(ISetup.KEY_USERNAME, "");

                //Datei als ausgewählter Typ mailen
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, s);
                emailIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.sich_mail) + " "
                        + ASetup.mPreferenzen.getString(ISetup.KEY_USERNAME, ""));
                emailIntent.setType("message/rfc822");
                Uri mURI = mStorageHelper.getDateiUri(mDateiname);
                if(mURI != null) {
                    emailIntent.putExtra(Intent.EXTRA_STREAM, mURI);

                    if (emailIntent.resolveActivity(pm) != null) {
                        startActivity(
                                Intent.createChooser(emailIntent, getString(R.string.mailwahl))
                        );
                    } else {
                        Toast.makeText(mContext, R.string.no_app, Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (id == R.id.sich_action_share) {
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                shareIntent.setType(IExport_Basis.DATEI_TYP_XML);

                Uri mUri = mStorageHelper.getDateiUri(mDateiname);
                if(mUri != null) {
                    shareIntent.putExtra(Intent.EXTRA_STREAM, mUri);

                    if (shareIntent.resolveActivity(pm) != null) {
                        startActivity(
                                Intent.createChooser(shareIntent, getString(R.string.zielwahl))
                        );
                    } else {
                        Toast.makeText(mContext, R.string.no_app, Toast.LENGTH_SHORT).show();
                    }
                }

            } else if (id == R.id.sich_action_delete) {
                deleteSicherung(mDateiname);
            }
      //  }
        return false;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.D_button_sicherung) {
            backupTask();
            requestBackup();
        } else if (id == R.id.D_wert_verzeichnis) {
            // einen neuen Speicherpfad wählen
            mStorageHelper.waehlePfad();
        }
    }

    @Override
    public void onSicherungClick(int position, int action, View view) {
        positionSicherung = position;
        switch (action){
            case Datensicherung_ViewAdapter.ACTION_DELETE:
                deleteSicherung(listeSicherungen.get(position));
                break;
            case Datensicherung_ViewAdapter.ACTION_RESTORE:
                restoreSicherung(listeSicherungen.get(position));
                break;
            default:
                showMenu(view);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        requestCode = requestCode & 0x0000ffff;

        if (resultCode == RESULT_OK) {
            Uri treeUri = data.getData();
            if (treeUri != null) {
                getContentResolver()
                        .takePersistableUriPermission(
                                treeUri,
                                Intent.FLAG_GRANT_READ_URI_PERMISSION
                                        | Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                        );

                switch (requestCode) {
                    case REQ_FOLDER_PICKER_WRITE_BACKUP:
                        if (mStorageHelper != null) {
                            mStorageHelper.setPfad(data.getDataString());
                            if (mStorageHelper.isExists()) {
                                tPfad.setText(mStorageHelper.getPfadSubtree());
                                setUpListe();
                            }
                        }
                        break;
                    case REQ_MAKE_COPY:
                        DocumentFile mQuelle = mStorageHelper
                                .getVerzeichnisFile()
                                .findFile(listeSicherungen.get(positionSicherung));
                        DocumentFile pickedDir = DocumentFile.fromTreeUri(mContext, treeUri);
                        kopieTask(mQuelle, pickedDir);
                        break;
                }
            }
        }
    }

    // Backup im Google Konto anfordern
    public void requestBackup() {
        BackupManager bm = new BackupManager(this);
        try {
            bm.dataChanged();
        } catch (NullPointerException ne){
            ne.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        // alten Zustand der App wieder anzeigen
        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
        mEdit.putBoolean(ISetup.KEY_RESUME_VIEW, true).apply();

        Intent mMainIntent = new Intent();
        mMainIntent.setClass(this, MainActivity.class);
        mMainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(mMainIntent);
    }

    @Override
    protected void onDestroy() {
        if(isChangingConfigurations()) {
            Intent mSicherungIntent = new Intent();
            mSicherungIntent.setClass(this, Datensicherung_Activity.class);
            mSicherungIntent.putExtra(ISetup.KEY_EDIT_JOB, ASetup.aktJob.getId());
            startActivity(mSicherungIntent);
        }
        super.onDestroy();
    }

    // stellt die Daten aus der ausgewählten Datei wieder her
    void restoreSicherung(final String name) {
        AlertDialog.Builder restorDialog = new AlertDialog.Builder(this);
        restorDialog.setMessage(getString(R.string.sich_frage_restore, name))
                .setIcon(R.mipmap.ic_launcher_foreground)
                .setTitle(R.string.sich_restore)
                .setPositiveButton(R.string.ja, (dialog, id) -> {
                    // Daten wiederherstellen
                    if(name.endsWith(".xml")){
                        restoreTask(name);
                    } else {
                        importTask(name);
                    }

                })
                .setNegativeButton(R.string.nein, (dialog, id) -> {
                    // nichts tun
                });
        // den Dialog erzeugen und anzeigen
        restorDialog.create().show();
    }

    // Löscht die ausgewählte Sicherungsdatei
    void deleteSicherung(final String name) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.sich_frage_delete, name))
                .setIcon(R.mipmap.ic_launcher_foreground)
                .setTitle(R.string.sich_delete)
                .setPositiveButton(R.string.ja, (dialog, id) -> {
                    if(mStorageHelper.isExists()) {
                        DocumentFile deleteFile = mStorageHelper.getVerzeichnisFile().findFile(name);// makeDocumentFile(mStorageHelper.getPfad(), name);
                        ////////////////////////////////////////////////////////////////////////////////////////////
                        // Löschen
                        ////////////////////////////////////////////////////////////////////////////////////////////
                        if (deleteFile != null && deleteFile.exists()) {
                            deleteTask(deleteFile);
                        }
                    }
                })
                .setNegativeButton(R.string.nein, (dialog, id) -> {
                    // nichts tun
                });
        // den Dialog erzeugen und anzeigen
        builder.create().show();
    }

    /*
    * Innere Klasse zum löschen einer Sicherung ausgelagert als Arbeitstask
     */
    private void deleteTask(DocumentFile datei) {
        Handler mHandler = new Handler();

        new Thread(() -> {
            boolean mStatus = false;
            if (datei != null && datei.exists()) {
                if (datei.delete())
                    mStatus = true;
            }
            final boolean fStatus = mStatus;
            mHandler.post(() -> {
                listeSicherungen.remove(positionSicherung);
                // Toast ausgeben
                Toast toast = Toast.makeText(
                        mContext,
                        fStatus ?
                                getString(R.string.delete_toast_erfolg) :
                                getString(R.string.delete_toast_miserfolg),
                        Toast.LENGTH_LONG);
                toast.show();

                // Liste der Sicherungen neu einlesen
                myAdapter.notifyItemRemoved(positionSicherung);
            });
        }).start();
    }

    /*
     * Innere Klasse zum kopieren einer Sicherung ausgelagert als Arbeitstask
     */
    private void kopieTask(DocumentFile quelle, DocumentFile zielpfad) {
        Handler mHandler = new Handler();

        new Thread(() -> {
            boolean mStatus = false;
            InputStream sourceStream = null;
            OutputStream destStream = null;
            try {
                DocumentFile mZiel = zielpfad.createFile("text/xml", quelle.getName());
                sourceStream = getContentResolver().openInputStream(quelle.getUri());
                destStream = getContentResolver().openOutputStream(mZiel.getUri());
                byte[] buffer = new byte[1024];
                int read;
                while ((read = sourceStream.read(buffer)) != -1) {
                    destStream.write(buffer, 0, read);
                }
                sourceStream.close();
                // die kopierten Daten schreiben
                assert destStream != null;
                destStream.flush();
                destStream.close();
                mStatus = true;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (sourceStream != null)
                        sourceStream.close();
                    if (destStream != null)
                        destStream.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }


            // Erfolgmeldung
            final boolean fStatus = mStatus;
            mHandler.post(() -> {
                // Toast ausgeben
                Toast toast = Toast.makeText(
                        mContext,
                        fStatus ?
                                getString(R.string.copy_ok) :
                                getString(R.string.copy_fail),
                        Toast.LENGTH_LONG);
                toast.show();
            });
        }).start();
    }


    /*
     * Sicherung, ausgelagert als Arbeits Task
     */
    protected void backupTask() {
        AtomicReference<String> filename = new AtomicReference<>("");
        final ProgressDialog mDialog = new ProgressDialog(mContext);
        Context mContext = this;

        // Fortschritsdialog öffnen
        mDialog.setIndeterminate(true);
        mDialog.setIndeterminateDrawable(
                ResourcesCompat.getDrawable(
                        getResources(),
                        R.drawable.progress_dialog_anim,
                        getTheme()));
        mDialog.setMessage(getString(R.string.progress_sicherung));
        mDialog.setCancelable(false);
        mDialog.show();

        Handler mHandler = new Handler();
        new Thread(() -> {
            boolean mStatus = false;
            SQLiteDatabase mDatenbank = ASetup.mDatenbank;

            // zuerst die allgem. Einstellungen sichern
            ContentValues mWerte = new ContentValues();
            mWerte.put(Datenbank.DB_F_VERSION, BuildConfig.VERSION_CODE);
            mWerte.put(Datenbank.DB_F_JOB, ASetup.aktJob.getId());
            mWerte.put(Datenbank.DB_F_VIEW, ASetup.mPreferenzen.getInt(ISetup.KEY_ANZEIGE_VIEW, ISetup.VIEW_TAG));
            if (!mDatenbank.isOpen()) {
                mDatenbank = ASetup.stundenDB.getWritableDatabase();
            }
            mDatenbank.update(Datenbank.DB_T_SETTINGS, mWerte,
                    Datenbank.DB_F_ID + "=?", new String[]{String.valueOf(1)});


            Datenbank_Backup mBackup = new Datenbank_Backup(mContext, mDatenbank, mStorageHelper);
            try {
                filename.set(mBackup.backup());
                mStatus = true;
            } catch (NullPointerException | ParserConfigurationException | IOException | TransformerException e) {
                e.printStackTrace();
            }

            // Fortschrittsdialog schliessen
            //mDialog.dismiss();

            // Erfolgsmeldung ausgeben
            final boolean fStatus = mStatus;
            mHandler.post(() -> {
                // Fortschrittsdialog schliessen
                if(mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                // Toast ausgeben
                Toast toast = Toast.makeText(
                        mContext,
                        fStatus ?
                                getString(R.string.backup_toast_erfolg) :
                                getString(R.string.backup_toast_misserfolg),
                        Toast.LENGTH_LONG);
                toast.show();

                // Liste der Sicherungen aktuallisieren
                if(!filename.toString().isEmpty()) {
                    listeSicherungen.add(0, filename.toString());
                    myAdapter.notifyItemInserted(0);
                }
            });
        }).start();
    }

    /*
     * Wiederherstellung der Sicherungen im Hintergrund
     */
    private void restoreTask(String pfad) {
        final ProgressDialog mDialog = new ProgressDialog(mContext);

        // Fortschritsdialog öffnen
        mDialog.setIndeterminate(true);
        mDialog.setIndeterminateDrawable(
                ResourcesCompat.getDrawable(
                        getResources(),
                        R.drawable.progress_dialog_anim,
                        getTheme()));
        mDialog.setMessage(getString(R.string.progress_restore));
        mDialog.setCancelable(false);
        mDialog.show();

        Handler mHandler = new Handler();
        new Thread(() -> {
            boolean mStatus = false;
            SQLiteDatabase mDatenbank = ASetup.mDatenbank;
            Datenbank_Backup mBackup = new Datenbank_Backup(mContext, mDatenbank, mStorageHelper);
            try {
                mBackup.restore(pfad);
                mStatus = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Erfolgsmeldung ausgeben
            final boolean fStatus = mStatus;
            mHandler.post(() -> {
                // Fortschrittsdialog schliessen
                if(mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                reset(fStatus);
            });
        }).start();
    }

    /*
     * Import Task - importiert Sicherungen aus "Arbeitszeiterfassung"
     */
    private void importTask(String filename) {
        final ProgressDialog mDialog = new ProgressDialog(mContext);

        // Fortschritsdialog öffnen
        mDialog.setIndeterminate(true);
        mDialog.setIndeterminateDrawable(
                ResourcesCompat.getDrawable(
                        getResources(),
                        R.drawable.progress_dialog_anim,
                        Objects.requireNonNull(mContext).getTheme()));
        mDialog.setMessage(getString(R.string.mig_prog_import));
        mDialog.setCancelable(false);
        mDialog.show();

        Handler mHandler = new Handler();
        new Thread(() -> {
            //String p = mStorageHelper.getPfad();
            boolean mStatus = false;
            // if (p != null) {
            SQLiteDatabase importDB = new Datenbank_toMigrate(mContext).getWritableDatabase();
            Datenbank_Migrate mMigradeDB = new Datenbank_Migrate(importDB, mStorageHelper, mContext);
            try {
                // Datenbank neu anlegen
                mMigradeDB.resetDB();

                // Einstellungen einlesen und Migration beginnen
                mMigradeDB.einlesen(filename);
                mHandler.post(() -> mDialog.setMessage(ASetup.res.getString(R.string.mig_prog_einstellungen)));
                mMigradeDB.Migrate_Einstellungen();
                mHandler.post(() -> mDialog.setMessage(ASetup.res.getString(R.string.mig_prog_arbeitsplatz)));
                mMigradeDB.Migrate_Arbeitsplatz();
                mHandler.post(() -> mDialog.setMessage(ASetup.res.getString(R.string.mig_prog_eort)));
                mMigradeDB.Migrate_Eorte();
                mHandler.post(() -> mDialog.setMessage(ASetup.res.getString(R.string.mig_prog_zeit)));
                mMigradeDB.Migrate_Zeiten();
                mMigradeDB.Delete_AlteDB();

                mStatus = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            // }


            final boolean fStatus = mStatus;
            mHandler.post(() -> {
                // Fortschrittsdialog schliessen
                if(mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                reset(fStatus);
            });
        }).start();
    }

    @SuppressLint("Range")
    private void reset(boolean status){

        // Allgemeine Einstellungen wiederherstellen
        String mSQL;
        Cursor mResult;

        mSQL = "SELECT * FROM " + Datenbank.DB_T_SETTINGS +
                " WHERE " + Datenbank.DB_F_ID + "=1 LIMIT 1 ";

        mResult = ASetup.mDatenbank.rawQuery(mSQL, null);

        if (mResult.getCount() > 0) {
            SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
            int mOptionen;
            mResult.moveToFirst();

            mEdit.putLong(ISetup.KEY_JOBID, mResult.getLong(mResult.getColumnIndex(Datenbank.DB_F_JOB)));
            mEdit.putString(ISetup.KEY_USERNAME, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_USER)));
            mEdit.putString(ISetup.KEY_USERANSCHRIFT, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_ANSCHRIFT)));
            mEdit.putString(ISetup.KEY_ANZEIGE_W_KUERZEL, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_W_KUERZEL)));
            //mEdit.putString(ISetup.KEY_ANZEIGE_W_TRENNER, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_W_TRENNER)));
            mEdit.putString(ISetup.KEY_ANZEIGE_E_KUERZEL, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_E_KUERZEL)));
            mEdit.putInt(ISetup.KEY_ANZEIGE_VIEW, mResult.getInt(mResult.getColumnIndex(Datenbank.DB_F_VIEW)));
            mEdit.putString(ISetup.KEY_DATEN_DIR, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_DATEN_DIR)));
            if (mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_BACKUP_DIR)) == null)
                mEdit.putString(ISetup.KEY_BACKUP_DIR, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_BACKUP_DIR)));

            mOptionen = mResult.getInt(mResult.getColumnIndex(Datenbank.DB_F_OPTIONEN));

            // die Option "dezimale Minutenanzeige" ist seit V 1.02.94 in die Arbeitsplatzeinstellungen gewandert
            if (mResult.getInt(mResult.getColumnIndex(Datenbank.DB_F_VERSION)) < 10294) {
                boolean dezimal = ((mOptionen & ISetup.OPT_ANZ_DEZIMAL) != 0);
                //ArbeitsplatzListe aListe = new ArbeitsplatzListe(null);
                ArbeitsplatzListe aListe = ASetup.jobListe;
                for (Arbeitsplatz a : aListe.getListe()) {
                    a.setOption(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL, dezimal);
                    a.schreibeJob();
                }
            }
            // mEdit.putBoolean(Einstellungen.KEY_ANZEIGE_DEZIMAL, ((mOptionen & Einstellungen.OPT_ANZ_DEZIMAL) != 0));
            mEdit.putBoolean(ISetup.KEY_ANZEIGE_ERW_SALDO, ((mOptionen & ISetup.OPT_ANZ_ERW_SALDO) != 0));
            mEdit.putBoolean(ISetup.KEY_ANZEIGE_UMG_SORT, ((mOptionen & ISetup.OPT_ANZ_UMG_SORT) != 0));
            mEdit.putBoolean(ISetup.KEY_THEMA_DUNKEL, ((mOptionen & ISetup.OPT_ANZ_THEMA_DUNKEL) != 0));

            mEdit.apply();

            if (!mResult.isNull(mResult.getColumnIndex(Datenbank.DB_F_SPRACHE))) {
                String s = LocaleHelper.getLanguage(mContext);

                switch (s) {
                    case "it":
                        LocaleHelper.setLocale(mContext, "it", Locale.getDefault().getCountry());
                        break;
                    case "de":
                        LocaleHelper.setLocale(mContext, "de", Locale.getDefault().getCountry());
                        break;
                    default:
                        LocaleHelper.setLocale(mContext, "en", Locale.getDefault().getCountry());
                }
            }

        }
        mResult.close();

        // den zuletzt aktiven arbeitsplatz aus der datenbank lesen
        //ASetup.aktJob = new Arbeitsplatz(ASetup.mPreferenzen.getLong(ISetup.KEY_JOBID, 0));

        // Toast ausgeben
        Toast toast = Toast.makeText(
                mContext,
                status ?
                        getString(R.string.restore_toast_erfolg) :
                        getString(R.string.restore_toast_misserfolg),
                Toast.LENGTH_LONG);
        toast.show();
        ASetup.zustand = ISetup.INIT_ZUSTAND_UNGELADEN;
        // alten Zustand der App wieder anzeigen
        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
        mEdit.putBoolean(ISetup.KEY_RESUME_VIEW, true).apply();

        Intent mMainIntent = new Intent();
        mMainIntent.setClass(this, MainActivity.class);
        mMainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //finish();
        startActivity(mMainIntent);
    }
}
