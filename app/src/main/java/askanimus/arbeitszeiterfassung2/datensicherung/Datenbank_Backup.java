/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.datensicherung;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.provider.DocumentsContract;
import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.export.IExport_Basis;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

public class Datenbank_Backup
{

    //Sql Verbindung
    private final SQLiteDatabase connection;

    StorageHelper mStorageHelper;

    private final String ERSATZ_HOCHKOMMA = " &apos;";
    private final String ERSATZ_ANFUERUNGSSTRICHE = " &quot;";

    private final Context mContext;

    /**
     * Konstruktor
     * @param paramSQLiteDatabase
     */
    public Datenbank_Backup(@NonNull Context context, @NonNull SQLiteDatabase paramSQLiteDatabase, @NonNull StorageHelper storageHelper/*String pfad*/)
    {
        this.connection = paramSQLiteDatabase;

        //BACKUP_PATH = pfad;
        mStorageHelper = storageHelper;
        mContext = context;
    }

    /**
     * Sichert die datenbank.
     * Im Script werden als String[] die Tabellen angebenen.
     * Spalten holt sich die Funktion selber.
     *
     * @return
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws TransformerFactoryConfigurationError
     */
    public String backup() throws ParserConfigurationException, IOException, TransformerConfigurationException, TransformerException, TransformerFactoryConfigurationError {
        // Filnamen zusammensetzen
        SimpleDateFormat datumsstring = new SimpleDateFormat("dd-MM-yyyy_HH.mm.ss", Locale.getDefault());

        String filename = ASetup.res.getString(
                R.string.sich_vom_datei) +
                datumsstring.format(new Date());

        //Hier alle Tabellen auflisten die gesichert werden sollen
        String[] tables = {
                Datenbank.DB_T_TAG,
                Datenbank.DB_T_MONAT,
                Datenbank.DB_T_JOB,
                Datenbank.DB_T_SETTINGS,
                Datenbank.DB_T_EORT,
                Datenbank.DB_T_ABWESENHEIT,
                Datenbank.DB_T_SCHICHT_DEFAULT,
                Datenbank.DB_T_SCHICHT,
                Datenbank.DB_T_AUTOREPORT,
                Datenbank.DB_T_JAHR,
                Datenbank.DB_T_ZUSATZFELD,
                Datenbank.DB_T_ZUSATZWERT_DEFAULT,
                Datenbank.DB_T_ZUSATZWERT,
                Datenbank.DB_T_ZUSATZWERT_AUSWAHL
        };

        Document localDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Element localElement1 = localDocument.createElement("backup");
        localDocument.appendChild(localElement1);

        for (String table : tables) {

            Cursor localCursor = this.connection.rawQuery("select * from " + table, null);
            Element localElement55 = localDocument.createElement("table");
            localElement55.setAttribute("name", table);
            localElement1.appendChild(localElement55);
            while (localCursor.moveToNext()) {
                String[] arrayOfString = localCursor.getColumnNames();
                Element localElement2 = localDocument.createElement("item");
                localElement55.appendChild(localElement2);
                for (int i = 0; i < arrayOfString.length; i++) {
                    Element localElement3 = localDocument.createElement(arrayOfString[i]);

                    String content;
                    /*if (localCursor.getString(i) == null) {
                        content = "";
                    } else */
                    if (localCursor.getString(i) != null) {
                        content = localCursor.getString(i).replace("'", ERSATZ_HOCHKOMMA);
                        content = content.replace("\"", ERSATZ_ANFUERUNGSSTRICHE);
                        localElement3.appendChild(localDocument.createTextNode(content));
                        localElement2.appendChild(localElement3);
                    }

                }
            } // ende while
            localCursor.close();
        } //ende for tables

        // Backupdatei schreiben
        DocumentFile backupFile = mStorageHelper.getVerzeichnisFile();
        if (backupFile != null && backupFile.exists()) {
            Uri docUri;
            try {
                docUri = DocumentsContract.createDocument(
                        mContext.getContentResolver(),
                        backupFile.getUri(),
                        IExport_Basis.DATEI_TYP_XML,
                        filename
                );
            } catch (Exception e) {
                //filename = null;
                throw new IOException();
            }

            if (docUri != null) {
                StreamResult result = new StreamResult();
                result.setOutputStream(
                        mContext
                                .getContentResolver()
                                .openOutputStream(docUri));
                TransformerFactory.newInstance().newTransformer().transform(new DOMSource(localDocument), result);
            } else {
                //filename = null;
                throw new IOException();
            }
        } else {
            //filename = null;
            throw new IOException();

        }
        return filename;
    }

    /**
     * Nimmt die database.xml Datei und stellt die datenbank wieder her
     */
    public void restore(String restore_file) throws Exception {
        Document localDocument = null;
        long appVersion = -1;

        DocumentFile restoreFile = mStorageHelper.getVerzeichnisFile();
        if (restoreFile != null && restoreFile.exists()) {
            restoreFile = restoreFile.findFile(restore_file);

            if (restoreFile != null && restoreFile.exists()) {
                InputStream inputStream = mContext.getContentResolver()
                        .openInputStream(restoreFile.getUri());
                localDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
            }

            if (localDocument != null) {
                localDocument.getDocumentElement().normalize();
                NodeList localNodeList = localDocument.getElementsByTagName("table");


                for (int i = localNodeList.getLength() - 1; i >= 0; i--) {

                    Node localNode = localNodeList.item(i);
                    if (localNode.getNodeType() == 1) {

                        Element localElement = (Element) localNode;
                        this.connection.execSQL("DELETE FROM " + localElement.getAttribute("name"));

                        NodeList nl = localElement.getElementsByTagName("item");

                        for (int a = 0; a < nl.getLength(); a++) {
                            ArrayList<String> aSpalten = new ArrayList<>();
                            ArrayList<String> aWerte = new ArrayList<>();

                            Node el = nl.item(a);

                            NodeList spalten = el.getChildNodes();

                            for (int s = 0; s < spalten.getLength(); s++) {
                                Node node = spalten.item(s);

                                aSpalten.add(node.getNodeName());
                                ///////////konvertieren alter Datenbankbackups < Vers. 9 ///////////////////
                                /*
                                 * Abfrage nach appVersion < 0 stellt sicher, dass nur die erste gefundene Einstellung
                                 * benutzt wird.
                                 * in frühen Versionen der App wurden mehrere Einstellungsversionen in die Datenbank geschrieben
                                 */
                                if (node.getNodeName().equals(Datenbank.DB_F_VERSION) && appVersion < 0) {
                                    String version = node.getTextContent();
                                    if (version != null) {
                                        try {
                                            appVersion = Long.parseLong(version);
                                        } catch (NumberFormatException ne) {
                                            appVersion = 0;
                                        }
                                    } else {
                                        appVersion = 0;
                                    }
                                }
                                aWerte.add(node.getTextContent());
                            }


                            StringBuilder sql = new StringBuilder("INSERT INTO " + localElement.getAttribute("name") + "(");
                            StringBuilder dummy = new StringBuilder();

                            for (String s : aSpalten) {
                                sql.append("`").append(s).append("`");
                                dummy.append("?");
                                if (!aSpalten.get(aSpalten.size() - 1).equals(s)) {
                                    sql.append(",");
                                    dummy.append(",");
                                }
                            }

                            sql.append(") VALUES (").append(dummy).append(");");

                            SQLiteStatement stmt = connection.compileStatement(sql.toString());

                            int x = 1;
                            for (String s : aWerte) {
                                //s = s.replace(ERSATZ_ZEILENUMBRUCH, "\n");
                                s = s.replace(ERSATZ_HOCHKOMMA, "'");
                                s = s.replace(ERSATZ_ANFUERUNGSSTRICHE, "\"");
                                stmt.bindString(x, s);
                                x++;

                                if (x <= aWerte.size()) {
                                    sql.append(",");
                                }
                            }

                            stmt.execute();

                            aSpalten.clear();
                            aWerte.clear();
                        }
                    }
                }
            }

            // wenn eine Sicherung einer Version vor 2.00.00 eingelesen wurde,
            // muss die Datenbank umgebaut werden
            if (appVersion < 200000) {
                ASetup.mDatenbank.execSQL("DROP TABLE IF EXISTS zusatzfeld;");
                ASetup.mDatenbank.execSQL("DROP TABLE IF EXISTS zusatzwert_default;");
                ASetup.mDatenbank.execSQL("DROP TABLE IF EXISTS zusatzwert;");
                Datenbank.zuNeun(ASetup.mDatenbank);
            }

            /*
             * Ab Version 2.01.00 wird, bei Wahl der Urlaubsabrechnung in Stunden,
             * der Urlaubsanspruch und der Resturlaub in Minuten gespeichert
             * beim ersten Start der App nach dem Update bzw. nach dem Wiederherstellen
             * einer alten Datensicherung werden die Werte einmalig umgerechnet
             */
            if (appVersion < 201000) {
                ASetup.mPreferenzen
                        .edit()
                        .putBoolean(ISetup.KEY_URLAUB_ALS_H_UMGERECHENET, false)
                        .apply();
            }

            /*
             * Überzählige Einträge in Einstellungen Tabelle löschen
             * bis Version 2.05.000 wurden mehrere Einstellungen Version gesichert
             */
            if(appVersion < 205001) {
                String count = "SELECT "
                        + Datenbank.DB_F_ID
                        + " FROM "
                        + Datenbank.DB_T_SETTINGS;
                Cursor result = ASetup.mDatenbank.rawQuery(count, null);

                while (result.moveToNext()){
                    @SuppressLint("Range") long id = result.getLong(result.getColumnIndex(Datenbank.DB_F_ID));
                    if( id != 1 ){
                        ASetup.mDatenbank.delete(
                                Datenbank.DB_T_SETTINGS,
                                Datenbank.DB_F_ID + "=?",
                                new String[]{Long.toString(id)}
                        );
                    }
                }

                result.close();
            }
            /*
             * Version 2.06.000
             *  - autom. Auszahlung von Überstunden hinzu gekommen
             *      - alle Monate mit 0 ausbezahlten Überstunden auf -1 ausbezahlten Überstunden setzen
             *      - -1 heisst, autom. berechnet, wenn die autom. Auszahlung eingestellt wurde
             *  - Auswahlmenüs für Zusatzwerte hinzu gekommen
             */
            if(appVersion < 206000) {
               Datenbank.zuVierzehn(ASetup.mDatenbank);
            }

        }
    }


    /*
     * löscht eine Sicherung aus Verzeichnis
     */
    public boolean delete(String delete_file) {
        DocumentFile deleteFile = mStorageHelper.getVerzeichnisFile().findFile(delete_file);
        if(deleteFile != null && deleteFile.exists()){
            return deleteFile.delete();
        }
        return false;
    }
}