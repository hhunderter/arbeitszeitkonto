/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.datensicherung;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import java.util.Calendar;
import java.util.Date;

import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

public abstract class AAutoBackup implements ISetup {
    private static final int REQESTCODE = 777;
    private static final String AUTOBACKUP = "make_backup";

    public static void init(Context context, int backupIntervall) {
        SharedPreferences.Editor mEditor = ASetup.mPreferenzen.edit();
        /*mEditor.putInt(KEY_AUTOBACKUP_INTERVALL, backupIntervall);
        mEditor.putInt(KEY_AUTOBACKUP_SCHRITTE, backupSchritte);
        mEditor.putInt(KEY_AUTOBACKUP_ANZAHL, anzahl);
        mEditor.putLong(KEY_AUTOBACKUP_LETZTES, 0);
        mEditor.apply();*/

        // alten Timer löschen
        deleteTimer(context);

        if(backupIntervall > AUTOBACKUP_NO) {
            // den Zeitpunkt für das erste Backup auf x + 2 Minuten setzen
            mEditor.putLong(KEY_AUTOBACKUP_NAECHSTES, new Date().getTime() + 120000).apply();

            // neuen Timer setzen
            setTimer(context);
        } else {
            mEditor.putLong(KEY_AUTOBACKUP_NAECHSTES, 0).apply();
        }
    }

    public static void updateTimer(Context context){
        ASetup.init(context, ()->setTimer(context));
    }

    private static void setTimer(Context context) {
        int backupIntervall = ASetup.mPreferenzen.getInt(KEY_AUTOBACKUP_INTERVAL, AUTOBACKUP_NO);

        if (backupIntervall > AUTOBACKUP_NO) {
            // In den Einstellungen wird das Intervall als Index auf ein Array mit Werten gespeichert,
            // die Konstanten in der Calendar Klasse entsprechen (z.B.: Calendar.MONTH)
            backupIntervall = AUTOBACKUP_INTERVAL[backupIntervall];

            int backupSchritte = ASetup.mPreferenzen.getInt(KEY_AUTOBACKUP_SCHRITTE, 0);
            long millisAktBackup = ASetup.mPreferenzen.getLong(KEY_AUTOBACKUP_NAECHSTES, 0);

            Calendar calAkt = Calendar.getInstance();
            calAkt.setTime(new Date());
            calAkt.set(Calendar.SECOND, 0);
            calAkt.add(Calendar.MINUTE, 1);
            long millisAktTime = calAkt.getTimeInMillis();

            if (millisAktBackup < millisAktTime)  {
                // Backup wurde verpasst und muss nachgeholt werden (in einer Minute)
                millisAktBackup = millisAktTime;
                millisAktBackup += 60000;
            }

            // den Zeitpunkt des darauf folgenden Backups berechnen
            Calendar calNaechstes = Calendar.getInstance();
            long millisNaechstes;

            calNaechstes.setTimeInMillis(millisAktBackup);
            calNaechstes.add(backupIntervall, backupSchritte);
            millisNaechstes = calNaechstes.getTimeInMillis();

            // den Alarmmanger als wiederholten Alarm setzen
            // der Empfänger der Alarmierung
            Intent intent = new Intent(context, AutoBackup_Receiver.class);
            intent.setAction(AUTOBACKUP);

            AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            mAlarmManager.setInexactRepeating(
                    AlarmManager.RTC_WAKEUP, // wird auch ausgeführt wenn das Handy im Ruhezustand
                    millisAktBackup, // der Zeitpunkt des nächsten Aufrufs
                    millisNaechstes - millisAktBackup, // danach alle x Millisekunden
                    PendingIntent.getBroadcast(
                            context,
                            REQESTCODE,
                            intent,
                            (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) ?
                                    PendingIntent.FLAG_MUTABLE :
                                    0
                    )
            );

            // den Zeitpunkt des nächsten Backups speichern
            // nach einem App Update oder Neustart des Gerätes sind die Timer gelöscht
            // und müssen neu gesetzt werden
            ASetup.mPreferenzen.edit().putLong(KEY_AUTOBACKUP_NAECHSTES, millisNaechstes).apply();
        }
    }

    static private void deleteTimer(Context context) {
        Intent intent = new Intent(context, AutoBackup_Receiver.class);
        intent.setAction(AUTOBACKUP);

        AlarmManager mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        mAlarmManager.cancel(PendingIntent.getBroadcast(
                context,
                REQESTCODE,
                intent,
                (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)?PendingIntent.FLAG_MUTABLE:0
        ));
    }
}
