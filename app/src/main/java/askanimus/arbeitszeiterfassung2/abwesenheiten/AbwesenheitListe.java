/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.abwesenheiten;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

/**
 * @author askanimus@gmail.com on 02.12.15.
 */
public class AbwesenheitListe {
    private static final String SQL_READ_ABWESENHEIT =
            "select * from " + Datenbank.DB_T_ABWESENHEIT +
                " where " + Datenbank.DB_F_JOB +
                " = ?" +
                " order by " + Datenbank.DB_F_POSITION + " ASC";

    private final long jobID;
    private final ArrayList<Abwesenheit> abwesenheitenAktiv;
    private final ArrayList<Abwesenheit> abwesenheitenPasiv;

    //private ArrayList<Integer> Aktive = new ArrayList<>();


    public AbwesenheitListe(long job) {
        abwesenheitenAktiv = new ArrayList<>();
        abwesenheitenPasiv = new ArrayList<>();
        jobID = job;
        Abwesenheit mAbwesenheit;
        Cursor result = ASetup.mDatenbank.rawQuery(
                SQL_READ_ABWESENHEIT,
                new String[]{Long.toString(jobID)}
                );

        // Abwesnheiten Liste aufbauen und dabei die Positionen beachten
        if (result.getCount() > 0) {
            result.moveToFirst();
            do {
                mAbwesenheit = new Abwesenheit(jobID, result);

                if (mAbwesenheit.getStatus()) {
                    /*
                     * Dies ist beim Übergang von V 2.01.05 notwendig,
                     * weil die Position von "Keine Schgicht" nicht mitgespeichert wurde und so diese
                     * ans Ende der Liste gelangte
                     */
                    if(mAbwesenheit.getKategorie() == Abwesenheit.KAT_KEINESCHICHT
                            && mAbwesenheit.getPosition() > 0)
                    {
                        mAbwesenheit.setPosition(-1);
                        abwesenheitenAktiv.add(0, mAbwesenheit);
                    } else {
                        mAbwesenheit.setPosition(abwesenheitenAktiv.size());
                        abwesenheitenAktiv.add(mAbwesenheit);
                    }
                } else {
                    abwesenheitenPasiv.add(mAbwesenheit);
                }
            } while (result.moveToNext());

            /*
             * Dies ist beim Übergang von V 2.01.05 notwendig,
             * weil die Position von "Keine Schgicht" nicht mitgespeichert wurde und so diese
             * ans Ende der Liste gelangte
             */
            if(abwesenheitenAktiv.get(0).getPosition() < 0) {
                // die Feldpositionen anpassen
                int i = 0;
                for (Abwesenheit abw : abwesenheitenAktiv) {
                    abw.setPosition(i);
                    i++;
                }
            }
            // Während des Updatevorganges von Version 10235 zu 10238
            // Wurden bei Einigen Usern alle Kategorien auf 0 gesetzt
            // Daher hier die Korrektur des Eintrages für Arbeitszeit, weil den der User nicht selbst ändern kann
            //
            /*try {
                if (abwesenheitenAktiv.size() > Abwesenheit.RUHETAG) {
                    abwesenheitenAktiv.get(Abwesenheit.ARBEITSZEIT).setKategorie(Abwesenheit.KAT_ARBEITSZEIT);
                }
            } catch (IndexOutOfBoundsException e){
                if (e.getMessage() != null)
                    Log.e(ASetup.res.getString(R.string.app_name), e.getMessage());
            }*/

        } else {
            // Voreingestellte abwesenheiten - übernehmmen von der ersten App-Version
            String[] Zusatz_Name = ASetup.res.getStringArray(R.array.zusatz_name);
            String[] Zusatz_Kurz = ASetup.res.getStringArray(R.array.zusatz_kurz);
            int[] Zusatz_Wirkung = ASetup.res.getIntArray(R.array.zusatz_wirkung);
            String[] Zusatz_Icon = ASetup.res.getStringArray(R.array.zusatz_icon);
            int[] Zusatz_Kategorie = ASetup.res.getIntArray(R.array.zusatz_kategorie);

            for (int i = 0; i < Zusatz_Wirkung.length; i++) {
                ContentValues daten = new ContentValues();
                daten.put(Datenbank.DB_F_ID, -1);
                daten.put(Datenbank.DB_F_KATEGORIE, Zusatz_Kategorie[i]);
                daten.put(Datenbank.DB_F_NAME, Zusatz_Name[i]);
                daten.put(Datenbank.DB_F_KURZ, Zusatz_Kurz[i]);
                daten.put(Datenbank.DB_F_WIRKUNG, Zusatz_Wirkung[i]);
                daten.put(Datenbank.DB_F_ICON, ASetup.res.getIdentifier(Zusatz_Icon[i], "drawable", "askanimus.arbeitszeiterfassung2"));
                daten.put(Datenbank.DB_F_INTERVAL, (i < 1 || i > 2) ? 1 : 0);
                daten.put(Datenbank.DB_F_STATUS, 1);
                daten.put(Datenbank.DB_F_POSITION, i);

                abwesenheitenAktiv.add(new Abwesenheit(jobID, daten));
            }
        }
        result.close();
    }

    public void add() {
        ContentValues daten = new ContentValues();
        daten.put(Datenbank.DB_F_ID, -1);
        daten.put(Datenbank.DB_F_KATEGORIE, Abwesenheit.KAT_RUHETAG);
        daten.put(Datenbank.DB_F_NAME, ASetup.res.getString(R.string.abwesenheit));
        daten.put(Datenbank.DB_F_KURZ, "");
        daten.put(Datenbank.DB_F_WIRKUNG, 0);
        daten.put(Datenbank.DB_F_ICON, ASetup.res.getIdentifier("freistellung", "drawable", "askanimus.arbeitszeiterfassung2"));
        daten.put(Datenbank.DB_F_INTERVAL, 0);
        daten.put(Datenbank.DB_F_STATUS, 1);
        daten.put(Datenbank.DB_F_POSITION, abwesenheitenAktiv.size());

        abwesenheitenAktiv.add(new Abwesenheit(jobID, daten));
    }

    public void delete(int index) {
        SQLiteDatabase mDatenbank = ASetup.mDatenbank;
        if (index > Abwesenheit.RUHETAG && index < abwesenheitenAktiv.size()) {
            Abwesenheit abwesenheit = abwesenheitenAktiv.get(index);
            String mSQL = "SELECT " + Datenbank.DB_F_ID +
                    " FROM " + Datenbank.DB_T_SCHICHT +
                    " WHERE " + Datenbank.DB_F_ABWESENHEIT +
                    " = " + abwesenheit.getID() +
                    " LIMIT 1";
            Cursor mResutlt = mDatenbank.rawQuery(mSQL, null);

            if (mResutlt.getCount() > 0) {
                abwesenheit.setStatus(false);
                abwesenheit.speichern();
                abwesenheitenPasiv.add(abwesenheit);
                abwesenheitenAktiv.remove(index);
            } else {
                mDatenbank.delete(
                        Datenbank.DB_T_ABWESENHEIT,
                        Datenbank.DB_F_ID + "=?",
                        new String[]{Long.toString(abwesenheit.getID())});
                abwesenheitenAktiv.remove(abwesenheit);
            }
            
            // die Feldpositionen anpassen
            int i = 0;
            for (Abwesenheit abw : abwesenheitenAktiv) {
                abw.setPosition(i);
                i++;
            }

            mResutlt.close();
        }
    }

     public int sizeAktive(){
        return abwesenheitenAktiv.size();
    }

    public int size(){
        return abwesenheitenAktiv.size() + abwesenheitenPasiv.size();
    }

    public Abwesenheit getVonId(long id){
        // in der Liste der aktiven Abwesenheiten suchen
        for (Abwesenheit abw : abwesenheitenAktiv) {
            if(abw.getID() == id){
                return abw;
            }
        }

        // in der Liste der passiven Abwesenheiten suchen
        for (Abwesenheit abw : abwesenheitenPasiv) {
            if(abw.getID() == id){
                return abw;
            }
        }
        return abwesenheitenAktiv.get(Abwesenheit.KEINESCHICHT);
    }

    public Abwesenheit getAktive(int index){
        try {
            return abwesenheitenAktiv.get(index);
        }catch (IndexOutOfBoundsException e){
            return abwesenheitenAktiv.get(Abwesenheit.KEINESCHICHT);
        }
    }

    public ArrayList<Abwesenheit> getListeAktive(){
        return abwesenheitenAktiv;
    }


    public Abwesenheit get(int index){
        int a = abwesenheitenAktiv.size();
        if(index < a){
           return abwesenheitenAktiv.get(index);
        } else {
            index -= a;
            if(index < abwesenheitenPasiv.size()) {
                return abwesenheitenPasiv.get(index);
            }
        }
        return abwesenheitenAktiv.get(0);
    }

    public boolean isKategorie(int kategorie){
        // in aktiven suchen
        for(Abwesenheit abw: abwesenheitenAktiv){
            if(abw.getKategorie() == kategorie){
                return true;
            }
        }
        // in pasiven suchen
        for(Abwesenheit abw: abwesenheitenPasiv){
            if(abw.getKategorie() == kategorie){
                return true;
            }
        }
        return false;
    }

    public void ListeKlone(long jobID_neu){
        //Aktive klonen
        for (Abwesenheit abw : abwesenheitenAktiv) {
            abw.klone(jobID_neu);
        }

        //Passive klonen
        for (Abwesenheit abw : abwesenheitenPasiv) {
            abw.klone(jobID_neu);
        }
    }
}
