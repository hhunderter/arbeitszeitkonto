/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.abwesenheiten;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

/**
 * @author askanimus@gmail.com on 08.08.15.
 */
public class Abwesenheit {
    // Konstanten

    //
    // Fixe Abwesenheiten, andere können selbst bestimmt qerden
    //
    public static final int KEINESCHICHT = 0;
    public static final int ARBEITSZEIT = 1;
    public static final int RUHETAG = 2;

    //
    // die Wirkungen auf die Berechnungen
    //
    public static final int WIRKUNG_KEINE = 0;          // Freizeit, leere Schicht, Überstundenkompensation
    public static final int WIRKUNG_IST_PLUS_EFFEKTIV = 1;      // Effektive Stunden nur die Stunden ohne Pause etc.
    public static final int WIRKUNG_IST_PLUS_PAUSCHAL = 2;      // Pauschale Regelarbeitszeit zum IST dazu gerechnet
    public static final int WIRKUNG_SOLL_MINUS_TAGE = -1;      // Abwesenheitstage(Urlaub, Krank usw.) werden von den Arbeitstagen abgezogen
    public static final int WIRKUNG_SOLL_MINUS_STUNDEN = -2;   // bezahlte Abwesenheit(Feiertag, bezahlte Tage werden als Regelarbeitszeit von den Sollstunden abgezogen
    public static final int WIRKUNG_SOLL_MINUS_EFFEKTIV = -3;  // Abzug von effektieven Stunden von den Sollstunden
    //
    // Abwesenheitskategorieen
    //
    public static final int KAT_KEINESCHICHT = -1;
    public static final int KAT_RUHETAG = 0;
    public static final int KAT_ARBEITSZEIT = 1;
    public static final int KAT_FEIERTAG = 2;
    public static final int KAT_URLAUB = 3;
    public static final int KAT_KRANK = 4;
    public static final int KAT_UNFALL = 5;
    public static final int KAT_SCHULE = 6;
    public static final int KAT_UNBEZAHLT = 7;
    public static final int KAT_BEZAHLT = 8;
    public static final int KAT_SONSTIGES = 9;
    public static final int KAT_ZUSCHLAG = 10;

    // Variable
    private long Id = -1;
    private long ArbeitsplatzID;
    private int Kategorie = KAT_KEINESCHICHT;
    private String Name = "";
    private String Kuerzel = "";
    private int Wirkung = WIRKUNG_KEINE;
    private int Icon_Id = 0;
    private boolean Interval = true;
    private boolean Status = true;
    private int Position = 0;

    @SuppressLint("Range")
    protected Abwesenheit(long arbeitsplatzID, Cursor daten) {
        ArbeitsplatzID = arbeitsplatzID;

        if (daten != null) {
            Id = daten.getLong(daten.getColumnIndex(Datenbank.DB_F_ID));
            Kategorie = daten.getInt(daten.getColumnIndex(Datenbank.DB_F_KATEGORIE));
            Name = daten.getString(daten.getColumnIndex(Datenbank.DB_F_NAME));
            Kuerzel = daten.getString(daten.getColumnIndex(Datenbank.DB_F_KURZ));
            Wirkung = daten.getInt(daten.getColumnIndex(Datenbank.DB_F_WIRKUNG));
            Icon_Id = ASetup.res.getIdentifier(
                    daten.getString(daten.getColumnIndex(Datenbank.DB_F_ICON)),
                    "drawable", "askanimus.arbeitszeiterfassung2"
            );
            Interval = (daten.getInt(daten.getColumnIndex(Datenbank.DB_F_INTERVAL)) == 1);
            Status = (daten.getInt(daten.getColumnIndex(Datenbank.DB_F_STATUS)) == 1);
            Position = daten.getInt(daten.getColumnIndex(Datenbank.DB_F_POSITION));
        }
    }

    protected Abwesenheit(long arbeitsplatzID, ContentValues daten) {
        ArbeitsplatzID = arbeitsplatzID;

        if (daten != null) {
            Id = daten.getAsLong(Datenbank.DB_F_ID);
            Kategorie = daten.getAsInteger(Datenbank.DB_F_KATEGORIE);
            Name = daten.getAsString(Datenbank.DB_F_NAME);
            Kuerzel = daten.getAsString(Datenbank.DB_F_KURZ);
            Wirkung = daten.getAsInteger(Datenbank.DB_F_WIRKUNG);
            Icon_Id = daten.getAsInteger(Datenbank.DB_F_ICON);
            Interval = (daten.getAsInteger(Datenbank.DB_F_INTERVAL) == 1);
            Status = (daten.getAsInteger(Datenbank.DB_F_STATUS) == 1);
            Position = daten.getAsInteger(Datenbank.DB_F_POSITION);
            speichern();
        }
    }

    void klone(long ArbeitsplatzID) {
        this.ArbeitsplatzID = ArbeitsplatzID;
        Id = -1;
        speichern();
    }

    protected void speichern() {
        SQLiteDatabase mDatenbank = ASetup.mDatenbank;/*stundenDB.getWritableDatabase();*/
        ContentValues werte = new ContentValues();
        werte.put(Datenbank.DB_F_JOB, ArbeitsplatzID);
        werte.put(Datenbank.DB_F_KATEGORIE, Kategorie);
        werte.put(Datenbank.DB_F_NAME, Name);
        werte.put(Datenbank.DB_F_KURZ, Kuerzel);
        werte.put(Datenbank.DB_F_WIRKUNG, Wirkung);
        werte.put(Datenbank.DB_F_ICON, ASetup.res.getResourceName(Icon_Id));
        werte.put(Datenbank.DB_F_INTERVAL, Interval ? 1 : 0);
        werte.put(Datenbank.DB_F_STATUS, Status ? 1 : 0);
        werte.put(Datenbank.DB_F_POSITION, Position);

        if (Id <= 0)
            Id = mDatenbank.insert(Datenbank.DB_T_ABWESENHEIT, null, werte);
        else
            mDatenbank.update(
                    Datenbank.DB_T_ABWESENHEIT,
                    werte,
                    Datenbank.DB_F_ID + "=?",
                    new String[]{Long.toString(Id)}
            );
    }

    // Eingaben
    public void setName(String name) {
        if (!Name.equals(name)) {
            Name = name;
            speichern();
        }
    }

    public void setKategorie(int kategorie) {
        if (Kategorie != kategorie) {
            Kategorie = kategorie;
            speichern();
        }
    }

    public void setKuerzel(String kurz) {
        if (!Kuerzel.equals(kurz)) {
            Kuerzel = kurz;
            speichern();
        }
    }

    public void setWirkung(int wirkung) {
        if (Wirkung != wirkung) {
            Wirkung = wirkung;
            speichern();
        }
    }

    public void setIcon_Id(int iconID) {
        if (Icon_Id != iconID) {
            Icon_Id = iconID;
            speichern();
        }
    }

    protected void setInterval(boolean interval) {
        if (Interval != interval) {
            Interval = interval;
            speichern();
        }
    }

    void setStatus(boolean status) {
        if (Status != status) {
            Status = status;
            speichern();
        }
    }

    public void setPosition(int position) {
        if (Position != position) {
            Position = position;
            speichern();
        }
    }

    // Ausgaben
    public String getName() {
        return Name;
    }

    public String getKuerzel() {
        return Kuerzel;
    }

    public int getKategorie() {
        return Kategorie;
    }

    public int getWirkung() {
        return Wirkung;
    }

    public int getIcon_Id() {
        return Icon_Id;
    }

    protected String getIconPfad() {
        if (Icon_Id > 0)
            return ASetup.res.getResourceName(Icon_Id);
        else
            return "null";
    }

    public long getID() {
        return Id;
    }

    protected boolean getInterval() {
        return Interval;
    }

    boolean getStatus() {
        return Status;
    }

    int getPosition() {
        return Position;
    }
}
