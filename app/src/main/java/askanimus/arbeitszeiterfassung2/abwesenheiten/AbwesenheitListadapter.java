/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.abwesenheiten;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import askanimus.arbeitszeiterfassung2.setup.ASetup;

/**
 * Created by ascanimus@gmail.com on 08.12.17.
 */

public class AbwesenheitListadapter extends BaseAdapter {
    private final Context mContext;
    private final Abwesenheit mAktuell;

    public AbwesenheitListadapter(Context context, Abwesenheit abwesenheit) {
        mContext = context;
        mAktuell = abwesenheit;
    }

    @Override
    public int getCount() {
        return ASetup.aktJob.getAbwesenheiten().sizeAktive() + 1;
    }

    @Override
    public Object getItem(int position) {
        if (position <= 0) {
            return mAktuell;
        } else {
            return ASetup.aktJob.getAbwesenheiten().getAktive(position - 1);
        }
    }

    @Override
    public long getItemId(int position) {
        if (position <= 0) {
            return mAktuell.getID();
        } else {
            return ASetup.aktJob.getAbwesenheiten().getAktive(position - 1).getID();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            TextView row = (TextView) inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
            if (position <= 0) {
                row.setText(mAktuell.getName());
                row.setEnabled(false);
            } else {
                row.setText(ASetup.aktJob.getAbwesenheiten().getAktive(position - 1).getName());
            }
            return (row);
        } else
            return null;
    }
}
