/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.dienstplan;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

/**
 * @author askanimus@gmail.com on 26.08.15.
 */
public class DienstplanPager extends Fragment {
    private TextView tMonat;

    /*private static final String ARG_JAHR = "jahr";
     private static final String ARG_MONAT = "monat";
     private static final String ARG_TAG = "tag";*/
    private static final String ARG_DATUM = "datum";

    /*
     * Neue Instanz anlegen
     */
    public static DienstplanPager newInstance(long datum) {
        DienstplanPager fragment = new DienstplanPager();
        Bundle bundle = new Bundle();

       /* bundle.putInt(ARG_JAHR, cal.get(Calendar.YEAR));
        bundle.putInt(ARG_MONAT, cal.get(Calendar.MONTH));
        bundle.putInt(ARG_TAG, cal.get(Calendar.DAY_OF_MONTH));*/

        bundle.putLong(ARG_DATUM, datum);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        ViewPager mViewPager;

        //Calendar mCal= Calendar.getInstance();
        Datum kDatum;

        View mInhalt = inflater.inflate(R.layout.fragment_pager, container, false);

        //if (savedInstanceState == null) {
        Bundle mArgs = getArguments();
        if (mArgs != null)
            kDatum = new Datum(mArgs.getLong(ARG_DATUM), ASetup.aktJob.getWochenbeginn());
        else
            kDatum = new Datum(ASetup.aktDatum.getTime(), ASetup.aktJob.getWochenbeginn());

        // auf den Montag dieser Woche stellen
        kDatum.setWocheBeginn(ASetup.aktJob.getWochenbeginn());
        // Werte der Kopfzeile eintragen
        TextView tJob = mInhalt.findViewById(R.id.P_wert_job);
        tMonat = mInhalt.findViewById(R.id.P_wert_monat);
        LinearLayout bKopf = mInhalt.findViewById(R.id.P_box_kopf);

        tJob.setText(ASetup.aktJob.getName());
        tJob.setTextColor(ASetup.aktJob.getFarbe_Schrift_Titel());
        tMonat.setTextColor(ASetup.aktJob.getFarbe_Schrift_Titel());
        bKopf.setBackgroundColor(ASetup.aktJob.getFarbe());

        setMonat(kDatum);

        // Erzeugt den Adapter der für jede Seite entsprechnd der Seitennummer
        // den dazu gehörenden Tag als Fragment einbindet.
        PagerAdapter mPagerAdapter = new PlanPagerAdapter(getChildFragmentManager());

        // Der View-Pager
        mViewPager = mInhalt.findViewById(R.id.pager);
        mViewPager.setAdapter(mPagerAdapter);

        // Anzuzeigende Woche errechnen
        // vom Beginn der Aufzeichnunge bis zum anzuzeigenden Datum
        Datum mKal = new Datum(ASetup.aktJob.getStartDatum().getTimeInMillis(),
                ASetup.aktJob.getWochenbeginn());
        mKal.setWocheBeginn(ASetup.aktJob.getWochenbeginn());

        int mDiff = mKal.wochenDiff(kDatum);

        /*if (mKal.liegtVor(Einstellungen.aktJob.getStartDatum().getCalendar())){
            if(mKal.get(Calendar.DAY_OF_WEEK) <= Calendar.MONDAY)
                mDiff--;
        }*/

        mViewPager.setCurrentItem(mDiff);

        mViewPager.setPageMargin(4);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Datum mKal = new Datum(ASetup.aktJob.getStartDatum().getTimeInMillis(),
                        ASetup.aktJob.getWochenbeginn());
                mKal.setWocheBeginn(ASetup.aktJob.getWochenbeginn());
                mKal.add(Calendar.WEEK_OF_YEAR, position);
                setMonat(mKal);
                // Datum der letzten Ansicht speichern
                SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
                mEdit.putLong(ISetup.KEY_ANZEIGE_DATUM, mKal.getTimeInMillis());
                mEdit.apply();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //}

        return mInhalt;
    }
    
    private void setMonat(Datum datum){
        String sMonat;
        Datum kBeginn = new Datum(datum.getTimeInMillis(), ASetup.aktJob.getWochenbeginn());
        Datum kEnde = new Datum(datum.getTimeInMillis(), ASetup.aktJob.getWochenbeginn());
        kEnde.add(Calendar.DAY_OF_MONTH, 6);

        if(kBeginn.liegtVor(ASetup.aktJob.getStartDatum()))
            kBeginn.set(ASetup.aktJob.getStartDatum().getCalendar());

        if (kBeginn.get(Calendar.MONTH) != kEnde.get(Calendar.MONTH)) {
            SimpleDateFormat dFormat = new SimpleDateFormat("MMM yy", Locale.getDefault());
            sMonat = dFormat.format(kBeginn.getTime());
            sMonat += "/" + dFormat.format(kEnde.getTime());
        } else {
            SimpleDateFormat dFormat = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
            sMonat = dFormat.format(kBeginn.getTime());
        }
        tMonat.setText(sMonat);        
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class PlanPagerAdapter extends FragmentPagerAdapter{
        int mSeiten;

        PlanPagerAdapter(FragmentManager fm) {
            super(fm);

            Datum mWoche = new Datum(ASetup.letzterAnzeigeTag.getTimeInMillis(), ASetup.aktJob.getWochenbeginn());
            mWoche.setWocheBeginn(ASetup.aktJob.getWochenbeginn());
            if (!mWoche.liegtNach(ASetup.letzterAnzeigeTag))
                mWoche.add(Calendar.WEEK_OF_YEAR, 1);
            mSeiten = mWoche.wochenAb(ASetup.aktJob.getStartDatum());
            if(mSeiten < 1) mSeiten = 1;

        }


        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            Datum mKal = new Datum(ASetup.aktJob.getStartDatum().getTimeInMillis(), ASetup.aktJob.getWochenbeginn());
            mKal.setWocheBeginn(ASetup.aktJob.getWochenbeginn());
            /*if (mKal.liegtVor(Einstellungen.aktJob.getStartDatum().getCalendar())){
                if(mKal.wochentagDiff(Einstellungen.aktJob.getStartDatum().get(Calendar.DAY_OF_WEEK)) < 0)
                    mKal.add(Calendar.WEEK_OF_YEAR, 1);
            }*/

            mKal.add(Calendar.WEEK_OF_YEAR, position);
            return DienstplanFragment.newInstance(mKal);
        }

        @Override
        public int getCount() {
           return mSeiten;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }
    }

}
