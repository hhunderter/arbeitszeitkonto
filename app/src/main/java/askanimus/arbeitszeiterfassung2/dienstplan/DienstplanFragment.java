/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.dienstplan;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import askanimus.arbeitszeiterfassung2.arbeitswoche.Arbeitswoche;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ASetup;

/**
 * @author askanimus@gmail.com on 19.08.15.
 */
public class DienstplanFragment extends Fragment {

    private static final String ARG_JAHR = "jahr";
    private static final String ARG_MONAT = "monat";
    private static final String ARG_TAG = "tag";

    // statische Anzeigeelemente
    //private LinearLayout    cKopf;
    private LinearLayout    cDatum;
    private TextView        tWoche;
    private TextView        tDatum;

    // Aktuell zu haltende Elemente der Anzeige
    private TextView        tSoll;
    private TextView        tIst;
    private TextView        tDifferenz;

    // Der Plan
    private ListView gPlan;
    ArrayList<DienstplanListItem> listData;

    // die Daten der Woche
    private Arbeitswoche mWoche;


    /*
      * Neue Instanz anlegen
      */
    public static DienstplanFragment newInstance(Datum cal) {
        DienstplanFragment fragment = new DienstplanFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_JAHR, cal.get(Calendar.YEAR));
        args.putInt(ARG_MONAT, cal.get(Calendar.MONTH));
        args.putInt(ARG_TAG, cal.get(Calendar.DAY_OF_MONTH));
        fragment.setArguments(args);

        return fragment;
    }


    /*public ArbeitswocheFragment() {
    }*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ASetup.init(getContext(), this::resume);
    }

    private void resume(){
        Bundle args = getArguments();
        Datum mKalender;


        if (args != null) {
            mKalender = new Datum(
                    args.getInt(ARG_JAHR),
                    args.getInt(ARG_MONAT),
                    args.getInt(ARG_TAG),
                    ASetup.aktJob.getWochenbeginn()
            );

        } else {
            mKalender = new Datum(ASetup.aktDatum.getTime(),
                    ASetup.aktJob.getWochenbeginn());

        }

        mWoche = new Arbeitswoche(mKalender.getTimeInMillis(), ASetup.aktJob);

        // Liste für Grid erzeugen
        /*listData = new ArrayList<DienstplanListItem>();

        for(int i = 0;i < 24;i++){
            listData.add(new DienstplanListItem(String.format("%02d", i), Einstellungen.aktJob.getFarbe(), Einstellungen.aktJob.getFarbe_Schrift_Titel()));
            for(int t = 0; t < mWoche.getTagzahl(); t++){
                arbeitstag mTag = mWoche.getTag(t);
                Boolean isDienst = false;
                for(int s = 0;s<mTag.getSchichtzahl();s++){
                    arbeitsschicht mSchicht = mTag.getSchicht(s);
                    if(mSchicht.getAbwesenheit().getKategorie() == Einstellungen.KAT_ARBEITSZEIT && i >= mSchicht.getVon()/60 && i <= mSchicht.getBis()/60)
                        isDienst = true;
                }
                if(isDienst)
                    listData.add(new DienstplanListItem("Arb.", Einstellungen.aktJob.getFarbe_Widget_Titel_Background(), Einstellungen.aktJob.getFarbe_Schrift_Titel()));
                else
                    listData.add(new DienstplanListItem("Frei", Einstellungen.aktJob.getFarbe_Hintergrund(), Einstellungen.aktJob.getFarbe_Schrift_Titel()));

            }
        }*/
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View mInhalt = inflater.inflate(R.layout.fragment_plan, container, false);
        // Anzeigeelemente finden
        //cKopf = (LinearLayout) mInhalt.findViewById(R.id.W_box_kopf);
        cDatum = mInhalt.findViewById(R.id.P_box_datum);
        tWoche = mInhalt.findViewById(R.id.P_wert_woche);
        tDatum = mInhalt.findViewById(R.id.P_wert_datum);
        tSoll = mInhalt.findViewById(R.id.P_wert_soll);
        tIst = mInhalt.findViewById(R.id.P_wert_ist);
        tDifferenz = mInhalt.findViewById(R.id.P_wert_diff);
        //gPlan = mInhalt.findViewById(R.id.P_liste_plan);
        return mInhalt;
    }

    @Override
    public void onResume() {
        super.onResume();

        //if (mWoche.getTagzahl() > 0) {
            // Itemliste erzeugen
            //gPlan.setAdapter(new DienstplanListAdapter(getContext(), mWoche.getTagListe()));
        

        //}
        //Statische Werte eintragen
        setDatum();

        //Dynamische Werte eintragen
        setBerechnung();

        // Farben des Kopfes setzen
        cDatum.setBackgroundColor(ASetup.aktJob.getFarbe_Tag());

    }

    // das Datum anzeigen
    private void setDatum(){
        /*StringBuilder sDatum;
        Datum mDatum = new Datum(mWoche.getDatumErsterTag());
        mDatum.setWocheBeginn();*/

        tWoche.setText(getString(R.string.woche_nummer, mWoche.getNummer()));

        if(mWoche.getTagzahl() > 0) {
           /* sDatum = new StringBuilder(Einstellungen.datumsformat.format(mWoche.getTag(0).getKalender().getTime()));
            sDatum.append(" - ");
            sDatum.append(Einstellungen.datumsformat.format(mWoche.getTag(mWoche.getTagzahl()-1).getKalender().getTime()));*/
            tDatum.setText(
                    mWoche.getTag(0).getKalender().getString_Datum_Bereich(
                            Objects.requireNonNull(getContext()),
                            0,
                            mWoche.getTagzahl()-1,
                            Calendar.DAY_OF_WEEK
                    )
            );
        } else {
            /*sDatum = new StringBuilder(Einstellungen.datumsformat.format(mDatum.getTime()));
            sDatum.append(" - ");
            mDatum.add(Calendar.DAY_OF_MONTH, 6);
            sDatum.append(Einstellungen.datumsformat.format(mDatum.getTime()));*/
           tDatum.setText(
                    mWoche.getDatumErsterTag().getString_Datum_Bereich(
                            Objects.requireNonNull(getContext()),
                            0,
                            6,
                            Calendar.DAY_OF_WEEK
                    )
            );
        }
        /*String mDatum = Einstellungen.datumsformat.format(mWoche.getTag(0).getKalender().getTime());
        mDatum += " - ";
        mDatum += Einstellungen.datumsformat.format(mWoche.getTag(mWoche.getTagzahl()-1).getKalender().getTime());*/

        //tDatum.setText(sDatum);
    }

    // die Berechnung der Stunden
    private void setBerechnung(){
        Uhrzeit mZeit = new Uhrzeit(mWoche.getSoll());

        tSoll.setText(mZeit.getStundenString(false, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

        mZeit.set(mWoche.getIst());
        tIst.setText(mZeit.getStundenString(false, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));

        mZeit.set(mZeit.getAlsMinuten() - mWoche.getSoll());
        tDifferenz.setText(mZeit.getStundenString(false, ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)));
        if (mZeit.getAlsMinuten() == 0)
            tDifferenz.setTextColor(ASetup.res.getColor(android.R.color.primary_text_light_nodisable));
        else if (mZeit.getAlsMinuten() < 0)
            tDifferenz.setTextColor(ASetup.res.getColor(android.R.color.holo_red_dark));
        else
            tDifferenz.setTextColor(ASetup.res.getColor(android.R.color.holo_green_dark));

        // Sonstige Werte
        /*if(isSonstiges){
            if(isSpesen)
                wSpesen.setText(Einstellungen.waehrungformat.format(mWoche.getSpesen()));
            if(isStrecke)
                wStrecke.setText(Einstellungen.streckeformat.format(mWoche.getStrecke()));
            if(isFahrzeit){
                mZeit.set(mWoche.getFahrzeit());
                wZeit.setText(mZeit.getStundenString(true));
            }
        }*/

    }

   /* @Override
    public void onArbeitstagChanged(int position) {
        mWoche = new ArbeitsWoche(mWoche.getTag(0).getKalender().getTimeInMillis());
        setBerechnung();
    }

    @Override
    public void onArbeitagListChanged() {
        aTage.notifyDataSetChanged();
    }*/

}
