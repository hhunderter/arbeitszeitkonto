/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.einsatzort;

import android.app.Dialog;
import android.content.Context;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.ViewCompat;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

public class EinsatzortAuswahlDialog implements
        EinsatzortListAdapter.EinsatzortListeCallbacks,
        ExpandableListView.OnGroupExpandListener,
        ExpandableListView.OnChildClickListener,
        View.OnClickListener,
        SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {
    private final Context mContext;
    private final EinsatzortAuswahlDialogCallbacks mCallBack;

    private final EinsatzortListAdapter mOrteListeAdapter;
    private final ExpandableListView mListView;

    private final ImageView mSort_AZ;
    private final ImageView mSort_Benutzt;
    private final SearchView mSuche;

    private final Dialog mDialog;

    private int mGruppeOffen;

    public EinsatzortAuswahlDialog(Context ctx, EinsatzortAuswahlDialogCallbacks cb) {
        mContext = ctx;
        mCallBack = cb;

        // die Einsatzorteliste einrichten
        mOrteListeAdapter = new EinsatzortListAdapter(ctx, ASetup.aktJob, this);

        // den Auswahldialog anlegen
        mDialog = new Dialog(ctx);
        mDialog.setTitle(R.string.eort_auswahl_titel);
        mDialog.setContentView(R.layout.fragment_eort_auswahl);

        // die grupierte (aktive, passive) Einstzortliste
        mListView = mDialog.findViewById(R.id.eort_Liste);
        mListView.setAdapter(mOrteListeAdapter);
        mListView.setOnGroupExpandListener(this);
        mListView.setOnChildClickListener(this);
        mListView.expandGroup(0);

        // einen neuen Eintrag anlegen
        AppCompatButton mListAdd = mDialog.findViewById(R.id.eort_Plusbutton);
        ViewCompat.setBackgroundTintList(mListAdd, ASetup.aktJob.getFarbe_Button());
        mListAdd.setOnClickListener(this);

        // keinen Einsatzort auswählen
        AppCompatButton mListNo = mDialog.findViewById(R.id.eort_Nobutton);
        mListNo.setOnClickListener(this);

        // Einsatzortsuche
        mSuche = mDialog.findViewById(R.id.eort_filter);
        mSuche.setQueryHint(mContext.getString(R.string.suche_eort_hint));
        mSuche.setIconified(false);
        mSuche.clearFocus();
        mSuche.setOnQueryTextListener(this);
        mSuche.setOnCloseListener(this);
        mOrteListeAdapter.mFilterData = null;

        // Sortierfunktionen
        mSort_AZ = mDialog.findViewById(R.id.eort_sort_az);
        mSort_Benutzt = mDialog.findViewById(R.id.eort_sort_benutzt);
        mSort_AZ.setOnClickListener(this);
        mSort_Benutzt.setOnClickListener(this);
    }

    public void open(){
        if(mDialog != null)
            mDialog.show();
    }



    @Override
    public void onEinsatzorteChange() {
        String q = mSuche.getQuery().toString();
        mSuche.setQuery("",false);
        mSuche.setQuery(q,false);

        mOrteListeAdapter.notifyDataSetChanged();
    }

    @Override
    public void onGroupExpand(int gruppenPosition) {
                if (gruppenPosition != mGruppeOffen) {
                    mListView.collapseGroup(mGruppeOffen);
                }
                mGruppeOffen = gruppenPosition;
    }

    @Override
    public boolean onChildClick(
            ExpandableListView expandableListView, View view,
            int gruppe, int ort, long id) {
        Einsatzort e_ort = mOrteListeAdapter.getChild(gruppe, ort);
        mOrteListeAdapter.update();
        mOrteListeAdapter.notifyDataSetChanged();
        mCallBack.onEinsatzortSet(e_ort);
        mDialog.dismiss();
        return false;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.eort_Nobutton) {
            mCallBack.onEinsatzortSet(null);
            mOrteListeAdapter.notifyDataSetChanged();
            mDialog.dismiss();
        } else if (id == R.id.eort_sort_az) {
            ASetup.mPreferenzen.edit()
                    .putInt(
                            ISetup.KEY_SORT_EORTLISTE,
                            (
                                    ASetup.mPreferenzen.getInt(
                                            ISetup.KEY_SORT_EORTLISTE,
                                            ISetup.SORT_NO) == ISetup.SORT_AZ
                            ) ?
                                    ISetup.SORT_ZA :
                                    ISetup.SORT_AZ)
                    .apply();
            mOrteListeAdapter.update();
            mOrteListeAdapter.notifyDataSetChanged();
        } else if (id == R.id.eort_sort_benutzt) {
            ASetup.mPreferenzen.edit()
                    .putInt(ISetup.KEY_SORT_EORTLISTE, ISetup.SORT_BENUTZT)
                    .apply();
            mOrteListeAdapter.update();
            mOrteListeAdapter.notifyDataSetChanged();
        } else if (id == R.id.eort_Plusbutton) {
            final InputMethodManager imm = (InputMethodManager)
                    mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            final EditText input = new EditText(mContext);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            input.setText(mOrteListeAdapter.eingabeFilter);
            input.setSelection(input.getText().length());
            input.setFocusableInTouchMode(true);
            input.requestFocus();
            input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            //Längenbegrenzung des Inputstrings
            InputFilter[] fa = new InputFilter[1];
            fa[0] = new InputFilter.LengthFilter(ISetup.LAENGE_EORT);
            input.setFilters(fa);

            new AlertDialog.Builder(mContext)
                    .setTitle(mContext.getString(R.string.einsatzort))
                    .setMessage(mContext.getString(R.string.neuer_einsatzort))
                    .setView(input)
                    .setPositiveButton(
                            mContext.getString(android.R.string.ok),
                            (dialog, whichButton) -> {
                                mCallBack.onEinsatzortSet(
                                        mOrteListeAdapter.add(input.getText().toString()));
                                mOrteListeAdapter.notifyDataSetChanged();
                                mDialog.dismiss();
                                if (imm != null) {
                                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                                }
                                mOrteListeAdapter.mFilterData = null;
                            }).setNegativeButton(
                    mContext.getString(android.R.string.cancel),
                            (dialog, whichButton) -> {
                                // Abbruchknopf gedrückt
                                if (imm != null) {
                                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                                }
                                mOrteListeAdapter.mFilterData = null;

                            }).show();
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        }

    }

    // die Suchhandler
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mOrteListeAdapter.getFilter().filter(newText);

        if (newText.isEmpty()) {
            mSort_AZ.setVisibility(View.VISIBLE);
            mSort_Benutzt.setVisibility(View.VISIBLE);
        } else {
            mSort_AZ.setVisibility(View.INVISIBLE);
            mSort_Benutzt.setVisibility(View.INVISIBLE);
        }
        return false;
    }

    @Override
    public boolean onClose() {
        mSort_AZ.setVisibility(View.VISIBLE);
        mSort_Benutzt.setVisibility(View.VISIBLE);
        return false;
    }

    public interface EinsatzortAuswahlDialogCallbacks {
        /**
         * Aufgerufen wenn ein Einsatzort gewählt wurde
         */
        void onEinsatzortSet(Einsatzort eort);
    }
}
