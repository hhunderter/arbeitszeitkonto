/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.einsatzort;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import java.util.Date;

import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

/**
 * @author askanimus@gmail.com on 13.12.14.
 */
public class Einsatzort /*implements Comparable<einsatzort>*/ {
    private long Id = -1;
    private long JobId;
    private String Name;
    private int Status = ISetup.STATUS_AKTIV;
    private int anzahl_verwendet;
    private long zueletzt_verwendet;

    public Einsatzort(long id, long arbeitsplatzID, String name, int status, int verwendet, long zuletzt) {
        Id = id;
        JobId = arbeitsplatzID;
        Name = name;
        Status = status;
        anzahl_verwendet = verwendet;
        zueletzt_verwendet = zuletzt;
        if(id < 0)
            save();
    }

    public long getId(){
        return Id;
    }

    public String getName(){

        return Name!= null? Name : "";
    }

    int getStatus(){
        return Status;
    }

    int getVerwendung() {
        return anzahl_verwendet;
    }

    long getzuletzt_Verwendet(){
        return zueletzt_verwendet;
    }

    protected String setName(String name){
        Name = name;
        save();
        return Name;
    }

    private void setStatus(int s){
        if(Status != s) {
            Status = s;
            save();
        }
    }

    void Klone(long ArbeitsplatzID){
        JobId = ArbeitsplatzID;
        Id = -1;
        save();
    }

    public void add_Verwendung(boolean speichern){
        anzahl_verwendet++;
        Datum d = new Datum(new Date().getTime(), ASetup.aktJob.getWochenbeginn());
        d.setTag(1);
        zueletzt_verwendet = d.getTimeInMillis();
        if(speichern)
            save();
    }

    public void sub_Verwendung(){
        if(anzahl_verwendet > 0) {
            anzahl_verwendet--;
            save();
        }
    }

    public void save(){
        SQLiteDatabase mDatenbank = ASetup.mDatenbank;/*stundenDB.getWritableDatabase();*/
        ContentValues werte = new ContentValues();

        werte.put(Datenbank.DB_F_NAME, Name);
        werte.put(Datenbank.DB_F_STATUS, Status);
        werte.put(Datenbank.DB_F_JOB, JobId);
        werte.put(Datenbank.DB_F_ANZAHL_VERWENDET, anzahl_verwendet);
        werte.put(Datenbank.DB_F_ZULETZT_VERWENDET, zueletzt_verwendet);

        if(!mDatenbank.isOpen())
            mDatenbank = ASetup.stundenDB.getWritableDatabase();

        if(Id <= -1)
            Id = mDatenbank.insert(Datenbank.DB_T_EORT, null, werte);
        else
            mDatenbank.update(Datenbank.DB_T_EORT,werte, Datenbank.DB_F_ID+"=?", new String[] {Long.toString(Id)});

        //mDatenbank.close();
    }

    /* Zweistufiges Löschen von Einsatzorten */
    void delete(){
        SQLiteDatabase mDatenbank = ASetup.mDatenbank;
        if(anzahl_verwendet <= 0){
                mDatenbank.delete(
                        Datenbank.DB_T_EORT,
                        Datenbank.DB_F_ID + "=?",
                        new String[]{Long.toString(Id)});
            setStatus(ISetup.STATUS_GELOESCHT);
        } else {
            if(Status == ISetup.STATUS_INAKTIV)
                setStatus(ISetup.STATUS_GELOESCHT);
            else
                setStatus(ISetup.STATUS_INAKTIV);
        }
    }

    void undelete(){
        setStatus(ISetup.STATUS_AKTIV);
    }
   /* @Override
    public int compareTo(@NonNull einsatzort o) {
        if (zueletzt_verwendet < o.zueletzt_verwendet) {
            return 1;
        } else if(zueletzt_verwendet > o.zueletzt_verwendet) {
            return -1;
        } else  if(anzahl_verwendet < o.anzahl_verwendet) {
            return 1;
        } else if (anzahl_verwendet > o.anzahl_verwendet) {
            return -1;
        } else{
            return Name.compareTo(o.Name);
        }
    }*/
}
