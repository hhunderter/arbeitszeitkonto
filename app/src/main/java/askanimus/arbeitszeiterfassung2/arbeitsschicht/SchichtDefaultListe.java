/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsschicht;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzfeldDefinitionenListe;

/**
 * @author askanimus@gmail.com on 02.12.15.
 */
public class SchichtDefaultListe {
    private final long mJobID;
    private final ZusatzfeldDefinitionenListe mDefZusatzfelder;
    private final ArrayList<SchichtDefault> schichtenPassiv = new ArrayList<>();
    private final ArrayList<SchichtDefault> schichtenAktiv = new ArrayList<>();


    public SchichtDefaultListe(long job, int menge, boolean teilschichten, ZusatzfeldDefinitionenListe defZusatzfelder) {
        SQLiteDatabase mDatenbank = ASetup.mDatenbank;/*stundenDB.getWritableDatabase();*/
        mJobID = job;
        mDefZusatzfelder = defZusatzfelder;
        String sql;

        sql = "select * from " + Datenbank.DB_T_SCHICHT_DEFAULT +
                " where " + Datenbank.DB_F_JOB +
                " = " + mJobID +
        " order by " + Datenbank.DB_F_POSITION;

        Cursor result = mDatenbank.rawQuery(sql, null);

        // alle vorhandenen Zusätze übertragen
        if (result.getCount() > 0) {
            while (result.moveToNext()){
                SchichtDefault schicht = new SchichtDefault(mJobID, result, defZusatzfelder);
                if(schicht.getStatus() == ISetup.STATUS_AKTIV){
                    schicht.setPosition(schichtenAktiv.size());
                    schichtenAktiv.add(schicht);
                } else {
                    //schicht.setPosition(schichtenPassiv.size());
                    schichtenPassiv.add(schicht);
                }
            }
        } else {
            // Default Schichten anlegen an Hand der voreinbgestellten Schichtmenge
            for (int i = 0; i < menge; i++) {
                add(teilschichten, i);
            }
        }
        result.close();
    }


    public void add(boolean teilschicht, int position) {
        ContentValues mWerte = new ContentValues(7);
        int mVon;
        int mBis;
        int mPause;

        // halbwegs vernünftige Werte berechnen
        if(schichtenAktiv.size() >= 1) {
            SchichtDefault mSchichtVor = schichtenAktiv.get(schichtenAktiv.size()-1);

            if(teilschicht){
                mVon = mSchichtVor.getBis() + (3*60);
                mBis = mVon + (5*60);
            } else {
                mVon = mSchichtVor.getVon() + (8*60);
                mBis = mVon + (8*60);
            }
            mPause = mSchichtVor.getPause();
        } else {
            if(teilschicht){
                mVon = Math.round(9.5f * 60);
                mBis = (14*60);
                mPause = 30;
            } else {
                mVon = (8*60);
                mBis = (17*60);
                mPause = 60;
            }
        }

        if(mVon >= (24 * 60))
            mVon -= (24 * 60);
        if(mBis >= (24 * 60))
            mBis -= (24 * 60);

        // Werte in Container eintragen
        mWerte.put(Datenbank.DB_F_ID, -1);
        mWerte.put(Datenbank.DB_F_NAME, ASetup.res.getString(R.string.schicht_nr, schichtenAktiv.size()+1));
        mWerte.put(Datenbank.DB_F_VON, mVon);
        mWerte.put(Datenbank.DB_F_BIS, mBis);
        mWerte.put(Datenbank.DB_F_PAUSE, mPause);
        mWerte.put(Datenbank.DB_F_EORT, 0);
        mWerte.put(Datenbank.DB_F_STATUS, 1);
        mWerte.put(Datenbank.DB_F_POSITION, position);

        // Schicht anlegen
        schichtenAktiv.add(new SchichtDefault(mJobID, mWerte, mDefZusatzfelder));
    }


    public void delete(int index) {
        if (index >= 0 && index < schichtenAktiv.size()) {
            SchichtDefault schicht = getAktive(index);

            SQLiteDatabase mDatenbank = ASetup.mDatenbank;/*stundenDB.getWritableDatabase();*/
            String mSQL = "SELECT " + Datenbank.DB_F_ID +
                    " FROM " + Datenbank.DB_T_SCHICHT +
                    " WHERE " + Datenbank.DB_F_SCHICHT_DEFAULT +
                    " = " + schicht.getID() +
                    " LIMIT 1";
            Cursor mResutlt = mDatenbank.rawQuery(mSQL, null);

            if (mResutlt.getCount() > 0) {
                schicht.setStatus(ISetup.STATUS_INAKTIV);
                schicht.speichern();
                schichtenPassiv.add(schicht);

            } else {
                mDatenbank.delete(
                        Datenbank.DB_T_SCHICHT_DEFAULT,
                        Datenbank.DB_F_ID + "=?",
                        new String[]{Long.toString(schicht.getID())});
            }
            schichtenAktiv.remove(index);

            // die Feldpositionen anpassen
            int i = 0;
            for (SchichtDefault sd : schichtenAktiv) {
                sd.setPosition(i);
                i++;
            }

            mResutlt.close();
        }
    }

     public int getSizeAktive(){
        return schichtenAktiv.size();
    }

    public SchichtDefault getVonId(long id){
        if (id != -1) {
            // in der Liste der aktiven Schichten suchen
            for (SchichtDefault s : schichtenAktiv) {
                if (s.getID() == id) {
                    return s;
                }
            }

            // in der Liste der passiven Schichten suchen
            for (SchichtDefault s : schichtenPassiv) {
                if (s.getID() == id) {
                    return s;
                }
            }
        }

        return null;
    }

    public SchichtDefault getAktive(int index){
        if(index < schichtenAktiv.size()){
            return schichtenAktiv.get(index);
        }

        return null;
    }

    String[] getAktiveNamen(){
        String[] sa = new String[schichtenAktiv.size()];
        int i= 0;
        for (SchichtDefault sd : schichtenAktiv) {
            sa[i] = sd.getName();
            i++;
        }
        return sa;
    }

    protected long getAktiveID(int index){
        SchichtDefault sd = getAktive(index);
        if(sd != null){
            return sd.getID();
        }
        return -1;
    }

    int getAktiveIndex(long id){
        int mIndex = 0;

        for (SchichtDefault sd : schichtenAktiv) {
            if(sd.getID() == id){
                return mIndex;
            }
            mIndex ++;
        }
        return -1;

    }

    public ArrayList<SchichtDefault>getAktive(){
        return schichtenAktiv;
    }


    public void ListeKlone(long jobID_neu){
        //Aktive klonen
        for (SchichtDefault sd : schichtenAktiv) {
            sd.klone(jobID_neu);
        }

        //Passive klonen
        for (SchichtDefault sd : schichtenPassiv) {
            sd.klone(jobID_neu);
        }
    }
}
