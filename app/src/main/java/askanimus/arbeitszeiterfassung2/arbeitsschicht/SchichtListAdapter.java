/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsschicht;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.abwesenheiten.AbwesenheitListadapter;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitstag.Arbeitstag;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.TextFeldTextListe;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.Textfeld;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzWertViewAdapter;

/**
 * @author askanimus@gmail.com on 27.11.15.
 */
public class SchichtListAdapter
        extends BaseAdapter
        implements ZusatzWertViewAdapter.ItemClickListener {
    private final Context mContext;
    private final SchichtListeCallbacks mCallbacks;

    private final Arbeitstag mTag;

    private RelativeLayout cWerte;
    private LinearLayout cZeiten;
    private LinearLayout cPause;
    private RelativeLayout cProzent;
    private LinearLayout cStunden;
    private LinearLayout cSonstiges;
    private LinearLayout cEinsatzort;

    private TextView tSchichtname;
    private Spinner sSchichtauswahl;
    private TextView tEinsatzort;

    private TextView tVon;
    private TextView tBis;
    private TextView tPause;
    private TextView tFolgetag;
    private TextView tProzent;
    private TextView tStunden;

    private final Boolean isTeilschicht;


    public SchichtListAdapter(Context context, Arbeitstag tag, SchichtListeCallbacks callback) {
        mContext = context;
        mCallbacks = callback;
        mTag = tag;
        isTeilschicht = ASetup.aktJob.isTeilschicht();
    }

    public int getCount() {
        int mZeilen;

        if(ASetup.aktJob.isStartAufzeichnung(mTag.getKalender()) )
            mZeilen = mTag.getSchichtzahl();
        else
            mZeilen = 0;

        return mZeilen;
    }

    @Override
    public Arbeitsschicht getItem(int position) {
        return mTag.getSchicht(position);
    }

    @Override
    public long getItemId(int position) {
        return mTag.getSchicht(position).getID();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Arbeitsschicht mSchicht = mTag.getSchicht(position);
        final boolean isNoSchichtselect;

        SchichtDefault defSchicht = ASetup.aktJob.getDefaultSchichten().getVonId(mSchicht.getDefaultSchichtId());

        if (defSchicht != null) {
            isNoSchichtselect = (isTeilschicht ||
                    ASetup.aktJob.getAnzahlSchichten() == 1 ||
                    position > 0 ||
                    mSchicht.getDefaultSchichtId() == 0 ||
                    defSchicht.getStatus() == ISetup.STATUS_INAKTIV);
        } else {
            isNoSchichtselect = (isTeilschicht ||
                    ASetup.aktJob.getAnzahlSchichten() == 1 ||
                    position > 0 ||
                    mSchicht.getDefaultSchichtId() == 0);
        }

        final LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater != null ? mInflater.inflate(R.layout.item_arbeitsschicht, parent, false) : null;


        // Elemente finden
        if (convertView != null) {
            // die Anzeigeelemente der Schicht
            LinearLayout bAll = convertView.findViewById(R.id.AS_box_all);
        cWerte = convertView.findViewById(R.id.AS_box_werte);
        cZeiten = convertView.findViewById(R.id.AS_box_zeiten);
        cPause = convertView.findViewById(R.id.AS_box_pause);
        cProzent = convertView.findViewById(R.id.AS_box_prozent);
        cStunden = convertView.findViewById(R.id.AS_box_stunden);
        cSonstiges = convertView.findViewById(R.id.AS_box_sonstiges);
        cEinsatzort = convertView.findViewById(R.id.AS_box_eort);

        bAll.setBackgroundColor(((position % 2 == 0) ? ASetup.aktJob.getFarbe_Schicht_gerade() : ASetup.aktJob.getFarbe_Schicht_ungerade()));

        tSchichtname = convertView.findViewById(R.id.AS_wert_name);
        sSchichtauswahl = convertView.findViewById(R.id.AS_spinner_name);

        if (isNoSchichtselect) {
            sSchichtauswahl.setVisibility(View.GONE);
            tSchichtname.setVisibility(View.VISIBLE);
        } else {
            sSchichtauswahl.setVisibility(View.INVISIBLE);
            tSchichtname.setVisibility(View.GONE);
        }

            Spinner sAbwesenheit = convertView.findViewById(R.id.AS_spinner_abwesenheit);
        tEinsatzort = convertView.findViewById(R.id.AS_wert_eort);

        tVon = convertView.findViewById(R.id.AS_wert_von);
        tBis = convertView.findViewById(R.id.AS_wert_bis);
        tPause = convertView.findViewById(R.id.AS_wert_pause);
        tProzent = convertView.findViewById(R.id.AS_wert_prozent);
        tStunden = convertView.findViewById(R.id.AS_wert_stunden);
        tFolgetag = convertView.findViewById(R.id.AS_wert_folgetag);

        if(mSchicht.getZusatzfelder(IZusatzfeld.TEXT_VOLL).size() > 0) {
            RecyclerView gZusatzwerte = convertView.findViewById(R.id.AS_grid_werte);
            ZusatzWertViewAdapter viewAdapter = new ZusatzWertViewAdapter(
                    mSchicht.getZusatzfelder(IZusatzfeld.TEXT_VOLL).getListe(),
                    this, ZusatzWertViewAdapter.VIEW_EDIT);
            GridLayoutManager layoutManger = new GridLayoutManager(mContext, IZusatzfeld.MAX_COLUM);
            layoutManger.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    return mSchicht.getZusatzfelder(IZusatzfeld.TEXT_VOLL).get(position).getColums();
                }
            });
            gZusatzwerte.setLayoutManager(layoutManger);

            gZusatzwerte.setAdapter(viewAdapter);
        } else {
            cSonstiges.setVisibility(View.GONE);
        }

            ImageView bDelete = convertView.findViewById(R.id.AS_button_delete);
            ImageView bHelpProzente = convertView.findViewById(R.id.AS_help_prozent);

        ////////////////////////// Werte setzen ////////////////////////////////////////////////

        //////////////////////// Handler definieren ///////////////////////////////////////////

        bHelpProzente.setOnClickListener(v -> new AlertDialog.Builder(mContext)
                .setTitle(mContext.getString(R.string.anteil))
                .setMessage(R.string.help_prozent)
                .setNeutralButton(
                        mContext.getString(android.R.string.ok),
                        (dialog, whichButton) -> {
                }).show());

        /*
         *   Definieren der einzelnen Clickhandler
         */

        if (position < ASetup.aktJob.getAnzahlSchichtenTag()) {
            bDelete.setVisibility(View.GONE);
        } else {
            bDelete.setOnClickListener(view -> {
                String sName;
                if (mSchicht.getName().length() > 0)
                    sName = mSchicht.getName();
                else
                    sName = mContext.getString(R.string.leere_schicht);
                new AlertDialog.Builder(mContext)
                        .setTitle(mContext.getString(R.string.dialog_delete, sName))
                        .setMessage(mContext.getString(R.string.dialog_delete_frage, sName))
                        .setPositiveButton(
                                mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                            mTag.loescheSchicht(position);
                            mCallbacks.onSchichtChanged(-1);
                            updateView(mSchicht);
                        })
                        .setNegativeButton(mContext.getString(android.R.string.cancel), (dialog, whichButton) -> {
                            // Do nothing.
                        }).show();
            });
        }

        // Schichtauswahl wenn es Vollschichten sind, mehrere zur Auswahl stehen und es nicht die zweite Schgicht ist
        final AdapterView.OnItemSelectedListener SchichtListSelect;
        final View.OnLongClickListener NameLongclickListener;
        if (isNoSchichtselect) {
            SchichtListSelect = null;

            // Für den Schichtname
            NameLongclickListener = view -> {
                final InputMethodManager imm =
                        (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                final EditText mInput = new EditText(mContext);
                mInput.setText(mSchicht.getName());
                mInput.setInputType(InputType.TYPE_CLASS_TEXT);
                mInput.setSelection(0,mInput.getText().length());
                mInput.setFocusableInTouchMode(true);
                mInput.requestFocus();
                //Längenbegrenzung des Inputstrings
                InputFilter[] fa = new InputFilter[1];
                fa[0] = new InputFilter.LengthFilter(ISetup.LAENGE_NAME);
                mInput.setFilters(fa);
                new AlertDialog.Builder(mContext)
                        .setTitle(mContext.getString(R.string.schichtname))
                        .setView(mInput)
                        .setPositiveButton(
                                mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                            if (whichButton == Dialog.BUTTON_POSITIVE) {
                                mSchicht.setName(mInput.getText().toString());
                                tSchichtname.setText(mInput.getText());
                                //myAdapter.notifyDataSetChanged();
                                mCallbacks.onSchichtChanged(mSchicht.getID());
                                if (imm != null) {
                                    imm.hideSoftInputFromWindow(mInput.getWindowToken(), 0);
                                }
                            }
                        }).setNegativeButton(mContext.getString(
                                android.R.string.cancel), (dialog, whichButton) -> {
                            // Abbruchknopf gedrückt
                            if (imm != null) {
                                imm.hideSoftInputFromWindow(mInput.getWindowToken(), 0);
                            }

                        }).show();
                if (imm != null) {
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }

                return true;
            };

        } else {
            NameLongclickListener = null;
            SchichtListSelect = new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (mSchicht.getAbwesenheit().getKategorie() == Abwesenheit.KAT_ARBEITSZEIT) {
                        SchichtDefault sSelect = ASetup.aktJob.getDefaultSchichten().getAktive(position);
                        if (mSchicht.getDefaultSchichtId() != sSelect.getID()) {
                            if (mSchicht.setDefaultSchicht(sSelect)) {
                                mCallbacks.onSchichtChanged(mSchicht.getID());
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            };
        }

        // Für Abwesenheiten
        AdapterView.OnItemSelectedListener AbwesenheitListSelect = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    if (mSchicht.getAbwesenheit().getID() != id) {
                        final int index = position - 1;
                        final Abwesenheit neueAbwesenheit = ASetup.aktJob.getAbwesenheiten().getAktive(index);
                        if (mSchicht.getAbwesenheit().getKategorie() != Abwesenheit.KAT_KEINESCHICHT) {
                            new AlertDialog.Builder(mContext)
                                    .setTitle(mContext.getString(R.string.dialog_aenderung))
                                    .setMessage(mContext.getString(R.string.dialog_aenderung_frage))
                                    .setPositiveButton(mContext.getString(
                                            android.R.string.ok), (dialog, whichButton) -> {
                                        mSchicht.setAbwesenheit(neueAbwesenheit,
                                                neueAbwesenheit.getKategorie()
                                                        == Abwesenheit.KAT_FEIERTAG ?
                                                        100 :
                                                        100 / mTag.getSchichtzahl());
                                        mCallbacks.onSchichtChanged(mSchicht.getID());
                                    })
                                    .setNegativeButton(mContext.getString(
                                            android.R.string.cancel), (dialog, whichButton) -> {
                                        // Abwesenheit beibehalten
                                        notifyDataSetChanged();
                                    }).show();
                        } else {
                            mSchicht.setAbwesenheit(
                                    neueAbwesenheit,
                                    neueAbwesenheit.getKategorie()
                                            == Abwesenheit.KAT_FEIERTAG
                                            ? 100
                                            : 100 / mTag.getSchichtzahl());

                            mCallbacks.onSchichtChanged(mSchicht.getID());
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

        // für Einsatzorte
        View.OnClickListener EortClicklistener =
                v -> mCallbacks.onSchichtOpenPicker(mSchicht, Arbeitsschicht.WERT_EORT);

        // Von
        View.OnClickListener VonClickListener =
                view -> mCallbacks.onSchichtOpenPicker(mSchicht, Arbeitsschicht.WERT_VON);

        // Bis
        View.OnClickListener BisClickListener =
                view -> mCallbacks.onSchichtOpenPicker(mSchicht, Arbeitsschicht.WERT_BIS);

        // Pause
        View.OnClickListener PauseClickListener =
                view -> mCallbacks.onSchichtOpenPicker(mSchicht, Arbeitsschicht.WERT_PAUSE);


        // Prozentsatz
        View.OnClickListener ProzentClickListener =
                view -> mCallbacks.onSchichtOpenPicker(mSchicht, Arbeitsschicht.WERT_PROZENT);


        // Stundensatz
        View.OnClickListener StundenClickListener =
                view -> mCallbacks.onSchichtOpenPicker(mSchicht, Arbeitsschicht.WERT_STUNDEN);

        /*
         *  Ende der Handlerdefinitionen
         */


        // der Schichtname bzw. die Schichtauswahl
        if (isNoSchichtselect) {
            tSchichtname.setText(mSchicht.getName());
            tSchichtname.setOnLongClickListener(NameLongclickListener);
        } else {
            if (ASetup.aktJob.getAnzahlSchichten() > 1) {
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(mContext,
                        android.R.layout.simple_spinner_dropdown_item,
                        ASetup.aktJob.getDefaultSchichten().getAktiveNamen());
                sSchichtauswahl.setAdapter(spinnerArrayAdapter);
                sSchichtauswahl.setSelection(
                        ASetup.aktJob.getDefaultSchichten()
                                .getAktiveIndex(mSchicht.getDefaultSchichtId())
                );
                sSchichtauswahl.setOnItemSelectedListener(SchichtListSelect);
            }
        }

        // die Abwesenheit
        AbwesenheitListadapter spinnerArrayAdapter =
                new AbwesenheitListadapter(mContext, mSchicht.getAbwesenheit());
        sAbwesenheit.setAdapter(spinnerArrayAdapter);
        sAbwesenheit.setSelection(0);
        sAbwesenheit.setOnItemSelectedListener(AbwesenheitListSelect);

        // der einsatzort
        if (ASetup.isEinsatzort) {
            cEinsatzort.setVisibility(View.VISIBLE);
            tEinsatzort.setOnClickListener(EortClicklistener);
        } else {
            cEinsatzort.setVisibility(View.GONE);
        }


        // Von
        tVon.setOnClickListener(VonClickListener);

        // bis
        tBis.setOnClickListener(BisClickListener);

        // Pause
        tPause.setOnClickListener(PauseClickListener);

        // Prozentsatz
        tProzent.setOnClickListener(ProzentClickListener);

        // Stundensatz
        tStunden.setOnClickListener(StundenClickListener);

        updateView(mSchicht);
        }
        return convertView;
    }

    private void updateView(Arbeitsschicht schicht) {
        int mWirkung = schicht.getWirkung();
        int mKategorie = schicht.getAbwesenheit().getKategorie();

        switch (mWirkung) {
            case Abwesenheit.WIRKUNG_SOLL_MINUS_EFFEKTIV:
            case Abwesenheit.WIRKUNG_IST_PLUS_EFFEKTIV:
                cWerte.setVisibility(View.VISIBLE);
                if (mKategorie == Abwesenheit.KAT_ZUSCHLAG) {
                    cProzent.setVisibility(View.GONE);
                    cStunden.setVisibility(View.VISIBLE);
                    cZeiten.setVisibility(View.GONE);
                    tStunden.setText(new Uhrzeit(schicht
                            .getBis())
                            .getStundenString(
                                    true,
                                    ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
                            )
                    );
                } else {
                    cProzent.setVisibility(View.GONE);
                    cStunden.setVisibility(View.GONE);
                    tVon.setText(new Uhrzeit(schicht.getVon()).getUhrzeitString());
                    tBis.setText(new Uhrzeit(schicht.getBis()).getUhrzeitString());

                    if (schicht.getBis() <= schicht.getVon() && (schicht.getVon() + schicht.getBis()) > 0) {
                        tFolgetag.setVisibility(View.VISIBLE);
                    } else {
                        tFolgetag.setVisibility(View.GONE);
                    }

                    if (mKategorie == Abwesenheit.KAT_ARBEITSZEIT) {
                        cPause.setVisibility(View.VISIBLE);
                        tPause.setText(new Uhrzeit(schicht
                                .getPause())
                                .getStundenString(
                                        true,
                                        ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
                                )
                        );
                        if (sSchichtauswahl.getVisibility() == View.INVISIBLE)
                            sSchichtauswahl.setVisibility(View.VISIBLE);
                    } else {
                        cPause.setVisibility(View.GONE);
                        if (sSchichtauswahl.getVisibility() == View.VISIBLE)
                            sSchichtauswahl.setVisibility(View.INVISIBLE);
                    }
                }
                break;
            case Abwesenheit.WIRKUNG_SOLL_MINUS_TAGE:
            case Abwesenheit.WIRKUNG_SOLL_MINUS_STUNDEN:
            case Abwesenheit.WIRKUNG_IST_PLUS_PAUSCHAL:
                cWerte.setVisibility(View.VISIBLE);
                cZeiten.setVisibility(View.GONE);
                cStunden.setVisibility(View.GONE);
                if (schicht.getBis() < 100 || mTag.getSchichtzahl() > 1) {
                    cProzent.setVisibility(View.VISIBLE);
                    tProzent.setText(ASetup.zahlenformat.format(schicht.getBis()) + " %");
                } else
                    cProzent.setVisibility(View.GONE);
                break;
            case Abwesenheit.WIRKUNG_KEINE:
                if (mKategorie != Abwesenheit.KAT_KEINESCHICHT) {
                    cWerte.setVisibility(View.VISIBLE);
                    cZeiten.setVisibility(View.GONE);
                    cStunden.setVisibility(View.GONE);
                    if (schicht.getBis() < 100 || mTag.getSchichtzahl() > 1) {
                        cProzent.setVisibility(View.VISIBLE);
                        tProzent.setText(ASetup.zahlenformat.format(schicht.getBis()) + " %");
                    } else
                        cProzent.setVisibility(View.GONE);
                } else {
                    cWerte.setVisibility(View.GONE);
                }

                break;
        }
        // einsatzort und Sonstige Werte anzeigen wenn die Schicht nicht leer ist
        if (mKategorie != Abwesenheit.KAT_KEINESCHICHT && mKategorie != Abwesenheit.KAT_ZUSCHLAG) {
            if (ASetup.isEinsatzort) {
                cEinsatzort.setVisibility(View.VISIBLE);
                tEinsatzort.setText(schicht.getNameEinsatzort());
            } else
                cEinsatzort.setVisibility(View.GONE);
        } else {
            cSonstiges.setVisibility(View.GONE);
            cEinsatzort.setVisibility(View.GONE);
        }
    }

    // auf einen Zusatzwert wurde geklickt, soll verändert werden
    @Override
    public void onItemClick(View view, IZusatzfeld feld) {
        switch (feld.getDatenTyp()) {
            case IZusatzfeld.TYP_TEXT:
                onTextClick((TextView) view, (Textfeld) feld);
                break;
            case IZusatzfeld.TYP_ZAHL:
                mCallbacks.onZusatzfeldOpenPicker(feld, Arbeitsschicht.WERT_ZUSATZ_ZAHL);
                break;
            case IZusatzfeld.TYP_ZEIT:
                mCallbacks.onZusatzfeldOpenPicker(feld, Arbeitsschicht.WERT_ZUSATZ_ZEIT);
                break;
            case IZusatzfeld.TYP_BEREICH_ZAHL:
                if(view.getId() == R.id.ZW_wert_1)
                    mCallbacks.onZusatzfeldOpenPicker(feld, Arbeitsschicht.WERT_ZUSATZ_ZAHL_VON);
                else  if(view.getId() == R.id.ZW_wert_2)
                    mCallbacks.onZusatzfeldOpenPicker(feld, Arbeitsschicht.WERT_ZUSATZ_ZAHL_BIS);
                break;
            case IZusatzfeld.TYP_BEREICH_ZEIT:
                if(view.getId() == R.id.ZW_wert_1)
                    mCallbacks.onZusatzfeldOpenPicker(feld, Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON);
                else  if(view.getId() == R.id.ZW_wert_2)
                    mCallbacks.onZusatzfeldOpenPicker(feld, Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS);
                break;
            case IZusatzfeld.TYP_AUSWAHL_TEXT:
            case IZusatzfeld.TYP_AUSWAHL_ZAHL:
            case IZusatzfeld.TYP_AUSWAHL_ZEIT:
                mCallbacks.onZusatzfeldOpenPicker(feld, Arbeitsschicht.WERT_ZUSATZ_AUSWAHL);
                break;
        }
    }


    // reagiert auf einen Zusatzeintrag in Textform
    private void onTextClick(final TextView view, final Textfeld feld) {
        final InputMethodManager imm =
                (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        final AutoCompleteTextView mInput = new AutoCompleteTextView(mContext);
        mInput.setText(feld.getStringWert(false));
        mInput.setSelection(0, feld.getStringWert(false).length());
        mInput.setFocusableInTouchMode(true);
        mInput.setInputType(
                InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                | InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_FLAG_MULTI_LINE
        );

        TextFeldTextListe textListe = new TextFeldTextListe(feld.getDefinitionID());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                mContext,android.R.layout.simple_list_item_1,textListe.getArray()
        );
        mInput.setAdapter(adapter);

        //Längenbegrenzung des Inputstrings
        InputFilter[] fa = new InputFilter[1];
        fa[0] = new InputFilter.LengthFilter(ISetup.LAENGE_NOTIZ);
        mInput.setFilters(fa);

        new AlertDialog.Builder(mContext)
                .setTitle(feld.getName())
                .setView(mInput)
                .setPositiveButton(
                        mContext.getString(android.R.string.ok),
                        (dialog, whichButton) -> {
                    if (whichButton == Dialog.BUTTON_POSITIVE) {
                        feld.setWert(mInput.getText().toString());
                        feld.save(false);
                        view.setText(mInput.getText());
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(mInput.getWindowToken(), 0);
                        }
                        mCallbacks.onSchichtChanged(feld.getSchichtId());
                    }
                }).setNegativeButton(
                        mContext.getString(android.R.string.cancel),
                        (dialog, whichButton) -> {
                    // Abbruchknopf gedrückt
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(mInput.getWindowToken(), 0);
                    }
                }).show();

        mInput.requestFocus();
        if (imm != null) {
            imm.toggleSoftInputFromWindow(
                    mInput.getWindowToken(),
                    InputMethodManager.SHOW_FORCED,
                    0
            );
        }
    }


    /*
     * Callback Interfaces
     */
    public interface SchichtListeCallbacks {
        /**
         * Aufgerufen wenn sich Werte der Schicht geändert haben
         */
        void onSchichtChanged(long schichtID);

        // wenn ein Picker geöffnet werden soll
        void onSchichtOpenPicker(Arbeitsschicht schicht, int wert);

        void onZusatzfeldOpenPicker(IZusatzfeld feld, int wert);
    }
}
