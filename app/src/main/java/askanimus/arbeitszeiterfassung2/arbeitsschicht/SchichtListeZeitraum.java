/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsschicht;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.arbeitstag.Arbeitstag;

public class SchichtListeZeitraum {
    private final ArrayList<Schicht> Liste;

    public SchichtListeZeitraum(ArrayList<Arbeitstag> tage) {
        ArrayList<Arbeitsschicht> aSchichten = getSchichtListe(tage);

        Liste = new ArrayList<>();
        boolean istEnthalten = false;
        for (Arbeitsschicht aSchicht : aSchichten) {
            for (Schicht sSchicht : Liste) {
                if (sSchicht.Name.equals(aSchicht.getName())) {
                    istEnthalten = true;
                    if (aSchicht.getAbwesenheit().getKategorie() == Abwesenheit.KAT_ARBEITSZEIT)
                        sSchicht.add(aSchicht.getNetto());
                    break;
                }
            }
            if (!istEnthalten) {
                if (aSchicht.getAbwesenheit().getKategorie() == Abwesenheit.KAT_ARBEITSZEIT)
                    Liste.add(new Schicht(aSchicht.getName(), aSchicht.getNetto()));
            } else
                istEnthalten = false;

        }
    }

    public ArrayList<Schicht> getListe(){
        return Liste;
    }

    public int size(){
        return Liste.size();
    }


    private ArrayList<Arbeitsschicht> getSchichtListe(ArrayList<Arbeitstag> tage) {
        ArrayList<Arbeitsschicht> mSchichten = new ArrayList<>();
         if(tage != null ) {
             for (int i = 0; i < tage.size(); i++) {
                 mSchichten.addAll(tage.get(i).getSchichten());
             }
         }

        return mSchichten;
    }

    // Innere Klasse für eine Schicht
    public static class Schicht {
        protected String Name;
        protected int Anzahl;
        Uhrzeit Minuten;

        Schicht(String name, int minuten) {
            Name = name;
            Minuten = new Uhrzeit(minuten);
            Anzahl = 1;
        }

        void add(int minuten) {
            Minuten.add(minuten);
            Anzahl++;
        }

        public String getName() {
            return Name;
        }

        public Uhrzeit getMinuten() {
            return Minuten;
        }

        public int getAnzahl() {
            return Anzahl;
        }
    }

}
