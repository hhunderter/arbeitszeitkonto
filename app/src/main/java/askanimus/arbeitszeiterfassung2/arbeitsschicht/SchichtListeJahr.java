/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsschicht;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.arbeitsjahr.Arbeitsjahr;
import askanimus.arbeitszeiterfassung2.arbeitsmonat.Arbeitsmonat;

public class SchichtListeJahr {
    private final ArrayList<Schicht> Jahr;

    public SchichtListeJahr(Arbeitsjahr jahr) {
        Jahr = new ArrayList<>();

        int monat = 0;
        for (Arbeitsmonat mMonat :jahr.listMonate) {
            ArrayList<Arbeitsschicht> aSchichten = mMonat.getSchichtListe();

            boolean istEnthalten = false;

            for (Arbeitsschicht aSchicht : aSchichten) {
                for (int i = 0; i < Jahr.size(); i++) {
                    Schicht sSchicht = Jahr.get(i);
                    if (sSchicht.Name.equals(aSchicht.getName())) {
                        istEnthalten = true;
                        if (aSchicht.getAbwesenheit().getKategorie() == Abwesenheit.KAT_ARBEITSZEIT) {
                            sSchicht.add(monat);
                        }
                        break;
                    }
                }
                if (!istEnthalten) {
                    if (aSchicht.getAbwesenheit().getKategorie() == Abwesenheit.KAT_ARBEITSZEIT) {
                        Jahr.add(new Schicht(aSchicht.getName(), jahr.listMonate.size()));
                        Jahr.get(Jahr.size()-1).add(monat);
                    }
                } else
                    istEnthalten = false;

            }
            monat++;
        }
    }

    public ArrayList<Schicht> getJahr() {
        return Jahr;
    }

    public int size(){
        return Jahr.size();
    }

    // Innere Klasse für eine Schicht
    public static class Schicht {
        protected String Name;
        protected int Tage;
        ArrayList<Integer> Monate;

        Schicht(String name, int monate) {
            Name = name;
            Tage = 0;
            Monate = new ArrayList<>();
            for (int i = 0; i < monate; i++) {
                Monate.add(0);
            }
        }

        void add(int monat) {
            Tage ++;
            Monate.set(monat, (Monate.get(monat) + 1));
        }

        public String getName() {
            return Name;
        }

        public ArrayList<Integer> getMonate(){
            return Monate;
        }

        public int getTageJahr() {
            return Tage;
        }

        public int getTageMonat(int monat){
            monat --;
            if(monat < Monate.size())
                return Monate.get(monat);
            else
                return 0;
        }

    }

}
