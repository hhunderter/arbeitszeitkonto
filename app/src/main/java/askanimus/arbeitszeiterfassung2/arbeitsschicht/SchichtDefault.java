/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.arbeitsschicht;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzWertListe;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzfeldDefinitionenListe;

/**
 * @author askanimus@gmail.com on 08.08.15.
 */
public class SchichtDefault {

    // Variable
    private long id = -1;
    private long arbeitsplatzID;

    private String name = "";
    private int von = 0;
    private int bis = 0;
    private int pause = 0;
    private long einsatzort = 0;

    private int position = 0;

    private ZusatzWertListe mZusatzfelder;

    private int status = ISetup.STATUS_AKTIV;


    protected SchichtDefault(long jobID, Cursor daten, ZusatzfeldDefinitionenListe defZusatzfelder) {
        arbeitsplatzID = jobID;

        if (daten != null) {
            id = daten.getLong(daten.getColumnIndex(Datenbank.DB_F_ID));
            name = daten.getString(daten.getColumnIndex(Datenbank.DB_F_NAME));
            von = daten.getInt(daten.getColumnIndex(Datenbank.DB_F_VON));
            bis = daten.getInt(daten.getColumnIndex(Datenbank.DB_F_BIS));
            pause = daten.getInt(daten.getColumnIndex(Datenbank.DB_F_PAUSE));
            einsatzort = daten.getLong(daten.getColumnIndex(Datenbank.DB_F_EORT));
            status = daten.getInt(daten.getColumnIndex(Datenbank.DB_F_STATUS));
            position = daten.getInt(daten.getColumnIndex(Datenbank.DB_F_POSITION));

            mZusatzfelder = new ZusatzWertListe(id, defZusatzfelder, true);
        }
    }

    protected SchichtDefault(long jobID, ContentValues daten, ZusatzfeldDefinitionenListe defZusatzfelder) {
        arbeitsplatzID = jobID;

        if (daten != null) {
            id = daten.getAsLong(Datenbank.DB_F_ID);
            name = daten.getAsString(Datenbank.DB_F_NAME);
            von = daten.getAsInteger(Datenbank.DB_F_VON);
            bis = daten.getAsInteger(Datenbank.DB_F_BIS);
            pause = daten.getAsInteger(Datenbank.DB_F_PAUSE);
            einsatzort = daten.getAsLong(Datenbank.DB_F_EORT);
            status = daten.getAsInteger(Datenbank.DB_F_STATUS);
            position = daten.getAsInteger(Datenbank.DB_F_POSITION);

            mZusatzfelder = new ZusatzWertListe(id, defZusatzfelder, true);
        }

        speichern();
    }


    void klone(long ArbeitsplatzID){
        // Wenn Einsatzorte erfasst werden und in einer definierten Schicht
        // eingestellt wurden, die ID des Einsatzortes auf die der geklonten Einsatzortliste
        // umschreiben
        if(einsatzort > 0) {
            Arbeitsplatz mJob = new Arbeitsplatz(this.arbeitsplatzID);
            Arbeitsplatz mJob_neu = new Arbeitsplatz(ArbeitsplatzID);
            int iEort = mJob.getEinsatzortListe().getAktive().indexOf(mJob.getEinsatzortListe().getOrt(einsatzort));
            if(iEort >= 0)
                einsatzort = mJob_neu.getEinsatzortListe().getAktive().get(iEort).getId();
            else {
                iEort = mJob.getEinsatzortListe().getPassive().indexOf(mJob.getEinsatzortListe().getOrt(einsatzort));
                if(iEort >= 0)
                    einsatzort = mJob_neu.getEinsatzortListe().getPassive().get(iEort).getId();
                else
                    einsatzort = 0;
            }
        }

        this.arbeitsplatzID = ArbeitsplatzID;
        id = -1;
        speichern();
    }


    protected void speichern() {
        SQLiteDatabase mDatenbank = ASetup.mDatenbank;/*stundenDB.getWritableDatabase();*/
        ContentValues werte = new ContentValues();

        werte.put(Datenbank.DB_F_JOB, arbeitsplatzID);
        werte.put(Datenbank.DB_F_NAME, name);
        werte.put(Datenbank.DB_F_VON, von);
        werte.put(Datenbank.DB_F_BIS, bis);
        werte.put(Datenbank.DB_F_PAUSE, pause);
        werte.put(Datenbank.DB_F_EORT, einsatzort);
        werte.put(Datenbank.DB_F_STATUS, status);
        werte.put(Datenbank.DB_F_POSITION, position);

        if(!mDatenbank.isOpen())
            mDatenbank = ASetup.stundenDB.getWritableDatabase();

        if (id <= 0)
            id = mDatenbank.insert(Datenbank.DB_T_SCHICHT_DEFAULT, null, werte);
        else
            mDatenbank.update(Datenbank.DB_T_SCHICHT_DEFAULT, werte, Datenbank.DB_F_ID + "=?", new String[]{Long.toString(id)});

        //mDatenbank.close();
    }

    // Eingaben
    public void setName(String name){
        if(!name.equals(this.name)) {
            this.name = name;
            speichern();
        }
    }

    public void setVon(int von){
        if(von != this.von) {
            this.von = von;
            speichern();
        }
    }

    public void setBis(int bis){
        if(bis != this.bis) {
            this.bis = bis;
            speichern();
        }
    }

    public void setPause(int pause){
        if ( pause != this.pause) {
            this.pause = pause;
            speichern();
        }
    }

    public void setEinsatzOrt(long eort){
        if( eort != einsatzort) {
            einsatzort = eort;
            speichern();
        }
    }

    public void setPosition(int position){
        if(position != this.position){
            this.position = position;
            speichern();
        }
    }

    void setStatus(int status){
        if( status != this.status) {
            this.status = status;
            speichern();
        }
    }
    // Ausgaben
    public String getName(){
        return name;
    }

    public int getVon(){
        return von;
    }

    public int getBis(){
        return bis;
    }

    public int getPause(){
        return pause;
    }

    public long getEinsatzOrt(){
        return einsatzort;
    }

    public long getID(){
        return id;
    }

    public int getStatus(){
        return status;
    }

    public int getPosition(){
        return position;
    }

    public IZusatzfeld getZusatzwert(int index){
        if(index < mZusatzfelder.size())
            return mZusatzfelder.get(index);
        else
            return null;
    }

    public ZusatzWertListe getZusatzfelder(){
        return mZusatzfelder;
    }
}
