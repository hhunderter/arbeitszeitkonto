/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.Zeitraum;

import android.content.Context;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsmonat.Arbeitsmonat;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.export.AExportBasis;
import askanimus.arbeitszeiterfassung2.export.IExport_Basis;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

public class Zeitraum_Monat extends AZeitraum implements IZeitraum {
    Arbeitsmonat mMonat;

    public Zeitraum_Monat(Arbeitsmonat monat){
        mMonat = monat;
        mTage = mMonat.getTagListe();
    }

    @Override
    public Zeitraum_Monat wechselArbeitsplatz(Arbeitsplatz job) {
        mMonat = new Arbeitsmonat(job, mMonat.getJahr(), mMonat.getMonat(), true, false);
        mTage = mMonat.getTagListe();
        return this;
    }
    
    @Override
    public int getIst(){
        return mMonat.getIstNetto();
    }
    
    @Override
    public Arbeitsplatz getArbeitsplatz() {
        return mMonat.getArbeitsplatz();
    }

    @Override
    public Datum getBeginn() {
        return mMonat.getDatumErsterTag();
    }

    @Override
    public Datum getEnde() {
        return mMonat.getDatumLetzterTag();
    }

    @Override
    public int getBrutto() {
        return mMonat.getIstBrutto();
    }

    @Override
    public int getPause() {
        return mMonat.getPause();
    }

    @Override
    public int getIstNetto() {
        return mMonat.getIstNettoMinusUeberstundenpauschale();
    }

    @Override
    public int getSoll() {
        return mMonat.getSollNetto();
    }

    @Override
    public int getDifferenz(){
        return mMonat.getDifferenz();
    }

    @Override
    public int getSaldo() {
        return mMonat.getSaldo();
    }

    @Override
    public float getVerdienst() {
        return mMonat.getVerdienst();
    }

    @Override
    public float getArbeitstage() {
        return mMonat.getSollArbeitsTage(false);
    }

    @Override
    public float getAbwesenheitstage() {
        return mMonat.getSummeAbwesenheitsTage();
    }

    @Override
    public float getSummeAlternativTage(long idAbwesenheit) {
        return mMonat.getSummeAlternativTage(idAbwesenheit);
    }

    @Override
    public int getSummeAlternativMinuten(long idAbwesenheit) {
        return mMonat.getSummeAlternativMinuten(idAbwesenheit);
    }

    @Override
    public String getTitel(Context context) {
        return getBeginn().getString_Monat_Jahr(
                getArbeitsplatz().getMonatsbeginn(),
                true)
                + "\n"
                + getBeginn().getString_Datum_Bereich(
                context,
                0,
                mTage.size()-1,
                Calendar.DAY_OF_MONTH
        );
    }

    @Override
    public String getDateiname(Context context, int varianteRes) {
        SimpleDateFormat fMonatsnummer = new SimpleDateFormat(
                "yyyy-MM",
                Locale.getDefault());
        return context.getString(
                R.string.exp_dateiname_zeitraum,
                AExportBasis.getBasisDateiname(),
                context.getString(R.string.monat),
                fMonatsnummer.format(getBeginn().getTime()),
                varianteRes != 0 ? context.getString(varianteRes) : ""
        );
    }

    @Override
    public boolean isMonat() {
        return true;
    }

    @Override
    public int getSaldoUebertrag() {
        return mMonat.getSaldoVormonat();
    }

    @Override
    public int getAuszahlung() {
        return mMonat.getAuszahlung();
    }

    @Override
    public int getPDFFontSize() {
        return ASetup.mPreferenzen.getInt(ISetup.KEY_EXP_M_FONTSIZE, IExport_Basis.MIN_FONTSIZE)
                + IExport_Basis.MIN_FONTSIZE;
    }
}
