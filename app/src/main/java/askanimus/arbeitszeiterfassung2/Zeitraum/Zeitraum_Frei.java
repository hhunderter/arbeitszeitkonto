/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.Zeitraum;

import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;

import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitstag.Arbeitstag;
import askanimus.arbeitszeiterfassung2.export.AExportBasis;
import askanimus.arbeitszeiterfassung2.export.IExport_Basis;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

public class Zeitraum_Frei extends AZeitraum implements IZeitraum {
    Arbeitsplatz mArbeitsplatz;
    Datum mBeginn;
    Datum mEnde;
    int mBrutto = 0;
    int mPause = 0;
    int mNetto = 0;
    int mSoll = 0;
    float mVerdienst = 0;
    float mArbeitstage = 0;
    float mAbwesenheitstage = 0;


    public Zeitraum_Frei(Arbeitsplatz job, Datum beginn, Datum ende){
        mArbeitsplatz = job;
        mBeginn = beginn;
        mEnde = ende;

        mTage = leseArbeitstage();
    }

    @Override
    public Zeitraum_Frei wechselArbeitsplatz(Arbeitsplatz job) {
        return new Zeitraum_Frei(job, mBeginn, mEnde);
    }

    @Override
    public Arbeitsplatz getArbeitsplatz() {
        return mArbeitsplatz;
    }

    @Override
    public Datum getBeginn() {
        return mBeginn;
    }

    @Override
    public Datum getEnde() {
        return mEnde;
    }

    @Override
    public int getIst() {
        return mNetto;
    }

    @Override
    public int getBrutto() {
        return mBrutto;
    }

    @Override
    public int getPause() {
        return mPause;
    }

    @Override
    public int getIstNetto() {
        return mNetto;
    }

    @Override
    public int getSoll() {
        return mSoll;
    }

    @Override
    public int getDifferenz() {
        return mNetto - mSoll;
    }

    @Override
    public int getSaldo() {
        return mNetto - mSoll;
    }

    @Override
    public float getVerdienst() {
        return mVerdienst;
    }

    @Override
    public float getArbeitstage() {
        return mArbeitstage;
    }

    @Override
    public float getAbwesenheitstage() {
        return mAbwesenheitstage;
    }

    @Override
    public float getSummeAlternativTage(long idAbwesenheit) {
        float u = 0;

        for (Arbeitstag tag: mTage) {
            u += tag.getAlternativTag(idAbwesenheit);
        }
        return u;
    }

    @Override
    public int getSummeAlternativMinuten(long idAbwesenheit) {
        int m = 0;
        for (Arbeitstag tag: mTage) {
            m += tag.getAlternativMinuten(idAbwesenheit);
        }
        return m;
    }

    @Override
    public String getTitel(Context context) {
        return getBeginn().getString_Datum_Bereich(
                context,
                0,
                mTage.size()-1,
                Calendar.DAY_OF_MONTH
        );
    }

    @Override
    public String getDateiname(Context context, int varianteRes) {
        return context.getString(
                    R.string.exp_dateiname_zeitraum,
                    AExportBasis.getBasisDateiname(),
                    context.getString(R.string.zeitraum),
                    getTitel(context),
                    varianteRes != 0 ? context.getString(varianteRes) : ""
            );
    }

    @Override
    public boolean isMonat() {
        return false;
    }

    @Override
    public int getSaldoUebertrag() {
        return 0;
    }

    @Override
    public int getAuszahlung() {
        return 0;
    }

    @Override
    public int getPDFFontSize() {
        return ASetup.mPreferenzen.getInt(ISetup.KEY_EXP_ZR_FONTSIZE, IExport_Basis.MIN_FONTSIZE)
                + IExport_Basis.MIN_FONTSIZE;
    }

    // alle Arbeitstage einer Periode und eines Arbeitsplatzes lesen
    private ArrayList<Arbeitstag> leseArbeitstage(){
        Datum mStartTag = new Datum(mBeginn);
        Datum mEndTag = new Datum(mEnde);
        mEndTag.add(Calendar.DAY_OF_MONTH, 1);
        ArrayList<Arbeitstag> liste = new ArrayList<>();

        if(mStartTag.liegtVor(mArbeitsplatz.getStartDatum()))
            mStartTag.set(mArbeitsplatz.getStartDatum().getTime());

        if(mArbeitsplatz.isEndeAufzeichnung(mEndTag))
            mEndTag.set(mArbeitsplatz.getEndDatum().getTime());

        // alle vorhandenen Tage anlegen
        while(mEndTag.liegtNach(mStartTag)){
            Arbeitstag tag = new Arbeitstag(
                    mStartTag.getCalendar(),
                    mArbeitsplatz,
                    mArbeitsplatz.getSollstundenTag(mStartTag),
                    mArbeitsplatz.getSollstundenTagPauschal(mStartTag.get(Calendar.YEAR), mStartTag.get(Calendar.MONTH)));

            mBrutto += tag.getTagBrutto();
            mPause += tag.getTagPause();
            mNetto += tag.getTagNetto();
            mSoll += tag.getTagSollNetto();
            mVerdienst += tag.getTagVerdienst();
            mArbeitstage += mArbeitsplatz.getArbeitstag(tag.getWochentag());
            mAbwesenheitstage += tag.getAbzugTag();

            liste.add(tag);
            mStartTag.add(Calendar.DAY_OF_MONTH, 1);
        }
        return  liste;
    }
}
