/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.Zeitraum;

import android.content.Context;

import java.util.ArrayList;

import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitstag.Arbeitstag;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzWertListe;

public interface IZeitraum{
    IZeitraum wechselArbeitsplatz(Arbeitsplatz job);
    Arbeitsplatz getArbeitsplatz();
    Datum getBeginn();  // Beginn des Zeitraums
    Datum getEnde();    // Ende des Zeitraums
    int getIst();       // gearbeitete Zeiten (inkl. Ersatzzeiten)
    int getIstNetto();  // wie Ist, aber pauschal abgegoltene Überstunden abgezogen
    int getBrutto();    // Ist inkl. unbezahlte Arbeitspausen
    int getPause();     // Arbeitspausen
    int getSoll();      // Sollstunden
    int getDifferenz(); // Soll - IstNetto
    int getSaldo();     // Differenz + Saldo Vorzeitraum + ausbezahlte Überstunden
    float getVerdienst();
    float getKalendertage();
    float getArbeitstage();
    float getAbwesenheitstage();
    float getSummeAlternativTage(long idAbwesenheit);
    int getSummeAlternativMinuten(long idAbwesenheit);
    ZusatzWertListe getZusatzeintragSummenListe();
    ArrayList<Arbeitstag> getTage();
    String getTitel(Context context);
    String getDateiname(Context context, int varianteRes);
    boolean isMonat();
    int getSaldoUebertrag();
    int getAuszahlung();
    int getEortSchichtNetto(long eortID, String schichtname);

    int getEortTage(long eortID, String name);

    int getPDFFontSize();
}
