/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.Zeitraum;

import android.content.Context;

import java.util.Calendar;

import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitstag.Arbeitstag;
import askanimus.arbeitszeiterfassung2.arbeitswoche.Arbeitswoche;
import askanimus.arbeitszeiterfassung2.export.AExportBasis;
import askanimus.arbeitszeiterfassung2.export.IExport_Basis;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

public class Zeitraum_Woche extends AZeitraum implements IZeitraum {
    Arbeitswoche mWoche;

    public Zeitraum_Woche(Arbeitswoche woche){
        mWoche = woche;
        mTage = mWoche.getTagListe();
    }

    @Override
    public Zeitraum_Woche wechselArbeitsplatz(Arbeitsplatz job) {
        mWoche = new Arbeitswoche(getBeginn().getTimeInMillis(), job);
        mTage = mWoche.getTagListe();
        return this;
    }

    @Override
    public Arbeitsplatz getArbeitsplatz() {
        return mWoche.getArbeitsplatz();
    }

    @Override
    public Datum getBeginn() {
        return mWoche.getDatumErsterTag();
    }

    @Override
    public Datum getEnde() {
        return mWoche.getDatumLetzterTag();
    }

    @Override
    public int getIst() {
        return mWoche.getIst();
    }

    @Override
    public int getBrutto() {
        return mWoche.getBrutto();
    }

    @Override
    public int getPause() {
        return mWoche.getPause();
    }

    @Override
    public int getIstNetto() {
        return mWoche.getIst();
    }

    @Override
    public int getSoll() {
        return mWoche.getSoll();
    }

    @Override
    public int getDifferenz() {
        return mWoche.getIst() - mWoche.getSoll();
    }

    @Override
    public int getSaldo() {
        return mWoche.getIst() - mWoche.getSoll();
    }

    @Override
    public float getVerdienst() {
        return mWoche.getVerdienst();
    }

    @Override
    public float getArbeitstage() {
        return mWoche.getSummeArbeitsTage(false);
    }

    @Override
    public float getAbwesenheitstage() {
        float m = 0;
        for (Arbeitstag t: mWoche.getTagListe()) {
            m += t.getAbzugTag();
        }
        return m;
    }

    @Override
    public float getSummeAlternativTage(long idAbwesenheit) {
        return mWoche.getSummeAlternativTage(idAbwesenheit);
    }

    @Override
    public int getSummeAlternativMinuten(long idAbwesenheit) {
        int w = 0;
        for (Arbeitstag t: mWoche.getTagListe()) {
            w += t.getAlternativMinuten(idAbwesenheit);
        }
        return w;
    }

    @Override
    public String getTitel(Context context) {
        return context.getString(
                R.string.export_woche_nummer,
                getBeginn().get(Calendar.WEEK_OF_YEAR),
                getBeginn().get(Calendar.YEAR))
                + "\n"
                + getBeginn().getString_Datum_Bereich(
                context,
                0,
                mTage.size()-1,
                Calendar.DAY_OF_MONTH
        );
    }

    @Override
    public String getDateiname(Context context, int varianteRes) {
        return context.getString(
                R.string.exp_dateiname_zeitraum,
                AExportBasis.getBasisDateiname(),
                context.getString(R.string.woche),
                getBeginn().get(Calendar.YEAR) + "-" +
                        getBeginn().get(Calendar.WEEK_OF_YEAR),
                varianteRes != 0 ? context.getString(varianteRes) : ""
        );
    }

    @Override
    public boolean isMonat() {
        return false;
    }

    @Override
    public int getSaldoUebertrag() {
        return 0;
    }

    @Override
    public int getAuszahlung() {
        return 0;
    }

    @Override
    public int getPDFFontSize() {
        return ASetup.mPreferenzen.getInt(ISetup.KEY_EXP_W_FONTSIZE, IExport_Basis.MIN_FONTSIZE)
                + IExport_Basis.MIN_FONTSIZE;
    }
}
