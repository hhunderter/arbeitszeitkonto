/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.Zeitraum;

import android.content.Context;

import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsjahr.Arbeitsjahr_summe;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.export.AExportBasis;
import askanimus.arbeitszeiterfassung2.export.IExport_Basis;
import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;

public class Zeitraum_Jahr extends AZeitraum implements IZeitraum {
    private Arbeitsjahr_summe mArbeitsjahr;

    public Zeitraum_Jahr(Arbeitsjahr_summe jahr){
        mArbeitsjahr = jahr;
        mTage = mArbeitsjahr.getTage();
    }

    @Override
    public Zeitraum_Jahr wechselArbeitsplatz(Arbeitsplatz job) {
        mArbeitsjahr = new Arbeitsjahr_summe(mArbeitsjahr.Jahr, job);
        mTage = mArbeitsjahr.getTage();
        return this;
    }

    @Override
    public Arbeitsplatz getArbeitsplatz() {
        return mArbeitsjahr.getArbeitsplatz();
    }

    @Override
    public Datum getBeginn() {
        return mArbeitsjahr.getDatumBeginn();
    }

    @Override
    public Datum getEnde() {
        return mArbeitsjahr.getDatumEnde();
    }

    @Override
    public int getIst() {
        return mArbeitsjahr.getIst();
    }

    @Override
    public int getBrutto() {
        return 0;
    }

    @Override
    public int getPause() {
        return 0;
    }

    @Override
    public int getIstNetto() {
        return mArbeitsjahr.getIst();
    }

    @Override
    public int getSoll() {
        return mArbeitsjahr.getSoll();
    }

    @Override
    public int getDifferenz(){
        return mArbeitsjahr.getDifferenz();
    }

    @Override
    public int getSaldo() {
        return mArbeitsjahr.getSaldo();
    }

    @Override
    public int getSaldoUebertrag() {
        return mArbeitsjahr.getSaldoVorjahr();
    }

    @Override
    public float getVerdienst() {
        return mArbeitsjahr.getVerdienst();
    }

    @Override
    public float getArbeitstage() {
        return mArbeitsjahr.getArbeitstage();
    }

    @Override
    public float getAbwesenheitstage() {
        return mArbeitsjahr.getAbwesenheitstage();
    }

    @Override
    public float getSummeAlternativTage(long idAbwesenheit) {
        return mArbeitsjahr.getSummeAlternativTage(idAbwesenheit);
    }

    @Override
    public int getSummeAlternativMinuten(long idAbwesenheit) {
        return mArbeitsjahr.getSummeAlternativMinuten(idAbwesenheit);
    }

    @Override
    public String getTitel(Context context) {
        return String.valueOf(mArbeitsjahr.Jahr);
    }

    @Override
    public String getDateiname(Context context, int varianteRes) {
        return context.getString(
                R.string.exp_dateiname_zeitraum,
                AExportBasis.getBasisDateiname(),
                context.getString(R.string.jahr),
                String.valueOf(mArbeitsjahr.Jahr),
                varianteRes != 0 ? context.getString(varianteRes) : ""
        );
    }

    @Override
    public boolean isMonat() {
        return false;
    }

    @Override
    public int getAuszahlung() {
        return 0;
    }

    public Arbeitsjahr_summe getArbeitsjahr(){
        return mArbeitsjahr;
    }

    @Override
    public int getPDFFontSize() {
        return ASetup.mPreferenzen.getInt(ISetup.KEY_EXP_J_FONTSIZE, IExport_Basis.MIN_FONTSIZE)
                + IExport_Basis.MIN_FONTSIZE;
    }
}
