/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.codetroopers.betterpickers.numberpicker.NumberPickerBuilder;
import com.codetroopers.betterpickers.numberpicker.NumberPickerDialogFragment;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Objects;

import askanimus.arbeitszeiterfassung2.MinutenInterpretationDialog;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.Datum;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.Uhrzeit;

/**
 * A placeholder fragment containing a simple view.
 */
public class InitAssistentFragmentAufzeichnung extends Fragment
        implements View.OnClickListener,
        AdapterView.OnItemSelectedListener,
        SwitchCompat.OnCheckedChangeListener,
        NumberPickerDialogFragment.NumberPickerDialogHandlerV2,
        CalendarDatePickerDialogFragment.OnDateSetListener {
    private Arbeitsplatz mArbeitsplatz;

    private TextView wBeginn;
    private TextView wEnde;
    private SwitchCompat sEnde;
    private TextView hEnde;
    private TextView wMonatsbeginn;
    private AppCompatSpinner wWochenbeginn;
    private SwitchCompat sSaldoIgnore;
    private TextView hSaldoIgnore;
    private LinearLayout cStartsaldo;
    private TextView wStartsaldo;
    private TextView wUebrstundenPauschal;
    private SwitchCompat sAutoAuszahlung;
    private TextView hAutoAuszahlungAb;
    private TextView wAutoAuszahlungAb;

    private Context mContext;
    private InitAufzeichnungCallbacks mCallback;

    private final int REDRAW_ALL = -1;

    private boolean isAnzeigeDezimal;

    /*
    * Neue Instanz anlegen
     */
    public static InitAssistentFragmentAufzeichnung newInstance() {
        return new InitAssistentFragmentAufzeichnung();
    }

    protected void setup(Arbeitsplatz arbeitsplatz,
                         InitAufzeichnungCallbacks callback) {
        mArbeitsplatz = arbeitsplatz;
        mCallback = callback;
        if(ASetup.zustand ==  ISetup.INIT_ZUSTAND_GELADEN) {
            resume();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getContext();
        return Objects.requireNonNull(inflater).inflate(R.layout.fragment_init_aufzeichnung, container, false);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onResume() {
        super.onResume();
        ASetup.init(mContext, this::resume);
    }

    private void resume() {
        View mView = getView();
        if (mArbeitsplatz != null && mView != null) {
            isAnzeigeDezimal = mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL);

            // Widgeds finden
            wBeginn = mView.findViewById(R.id.I_wert_von);
            wEnde = mView.findViewById(R.id.I_wert_bis);
            sEnde = mView.findViewById(R.id.I_switch_bis);
            hEnde = mView.findViewById(R.id.I_hint_bis);
            wMonatsbeginn = mView.findViewById(R.id.I_wert_monatsbeginn);
            wWochenbeginn = mView.findViewById(R.id.I_spinner_wochenbeginn);
            sSaldoIgnore = mView.findViewById(R.id.I_switch_saldo_ignore);
            hSaldoIgnore = mView.findViewById(R.id.I_hint_saldo_ignore);
            cStartsaldo = mView.findViewById(R.id.I_box_startsaldo);
            wStartsaldo = mView.findViewById(R.id.I_wert_startsaldo);
            wUebrstundenPauschal = mView.findViewById(R.id.I_wert_ueber_pauschal);
            sAutoAuszahlung = mView.findViewById(R.id.I_switch_auto_auszahlung);
            hAutoAuszahlungAb = mView.findViewById(R.id.I_titel_auszahlung_auto_ab);
            wAutoAuszahlungAb = mView.findViewById(R.id.I_wert_auto_auszahlung_ab);

            // Farben setzen
            sEnde.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sEnde.setTrackTintList(mArbeitsplatz.getFarbe_Trak());
            sSaldoIgnore.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sSaldoIgnore.setTrackTintList(mArbeitsplatz.getFarbe_Trak());
            sAutoAuszahlung.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sAutoAuszahlung.setTrackTintList(mArbeitsplatz.getFarbe_Trak());

            // Handler registrieren
            wBeginn.setOnClickListener(this);
            wEnde.setOnClickListener(this);
            sEnde.setOnCheckedChangeListener(this);
            wMonatsbeginn.setOnClickListener(this);
            wWochenbeginn.setOnItemSelectedListener(this);
            sSaldoIgnore.setOnCheckedChangeListener(this);
            wStartsaldo.setOnClickListener(this);
            wUebrstundenPauschal.setOnClickListener(this);
            sAutoAuszahlung.setOnCheckedChangeListener(this);
            wAutoAuszahlungAb.setOnClickListener(this);

            // Seitentitel ausblenden wenn es nicht der Initassistent ist
            if (ASetup.mPreferenzen.contains(ISetup.KEY_INIT_FINISH)) {
                View view = getView();
                if (view != null) {
                    TextView tTitel = view.findViewById(R.id.I_aufzeichnung_titel);
                    tTitel.setVisibility(View.GONE);
                }
            }

            // Werte setzen
            wBeginn.setText(mArbeitsplatz.getStartDatum().getString_Datum(mContext));
            wMonatsbeginn.setText(getString(R.string.hint_monatsbeginn, mArbeitsplatz.getMonatsbeginn()));
            wWochenbeginn.setSelection(mArbeitsplatz.getWochenbeginn() - 1);
            wStartsaldo.setText(
                    new Uhrzeit(mArbeitsplatz.getStartsaldo()).getStundenString(
                            false,
                            mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
                    )
            );

            wUebrstundenPauschal.setText(
                    new Uhrzeit(mArbeitsplatz.getUeberstundenPauschal()).getStundenString(
                            false,
                            mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
                    )
            );

            wAutoAuszahlungAb.setText(
                    new Uhrzeit(mArbeitsplatz.getAutoAuzahlungAb()).getStundenString(
                            false,
                            mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
                    )
            );

            // Schalter setzen
            sEnde.setChecked(mArbeitsplatz.isSetEnde());
            sSaldoIgnore.setChecked(mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_RESET_SALDO));
            sAutoAuszahlung.setChecked(mArbeitsplatz.isAutoAuszahlung());
            UpdateView(REDRAW_ALL);
        }
    }

    @Override
    public void onStop() {
        if(mArbeitsplatz != null) {
            mArbeitsplatz.schreibeJob();
        }
        super.onStop();
    }

    private void UpdateView(int bereich) {
        //if(mArbeitsplatz != null && wBeginn != null) {
        // Auswahl des Aufzeichnungsendes ein- bzw. ausblenden
            if (bereich == REDRAW_ALL || bereich == R.id.I_switch_bis) {
                if (sEnde.isChecked()) {
                    wEnde.setVisibility(View.VISIBLE);
                    hEnde.setText(R.string.ende_job);
                    if (mArbeitsplatz.isSetEnde())
                        wEnde.setText(mArbeitsplatz.getEndDatum().getString_Datum(mContext));
                    else {
                        Datum dEnde = new Datum(ASetup.aktDatum);
                        dEnde.setTag(dEnde.getAktuellMaximum(Calendar.DAY_OF_MONTH));
                        wEnde.setText(dEnde.getString_Datum(mContext));
                    }
                    // wenn das Ende erreicht ist alle Einstellungen deaktivieren
                    if (mArbeitsplatz.isEndeAufzeichnung(ASetup.aktDatum)/*getEndDatum().liegtVor(Einstellungen.aktDatum)*/) {
                        wBeginn.setEnabled(false);
                        wWochenbeginn.setEnabled(false);
                        wMonatsbeginn.setEnabled(false);
                        sSaldoIgnore.setEnabled(false);
                        wStartsaldo.setEnabled(false);
                        wUebrstundenPauschal.setEnabled(false);
                        sAutoAuszahlung.setEnabled(false);
                        wAutoAuszahlungAb.setEnabled(false);
                    } else {
                        wBeginn.setEnabled(true);
                        wWochenbeginn.setEnabled(true);
                        wMonatsbeginn.setEnabled(true);
                        sSaldoIgnore.setEnabled(true);
                        wStartsaldo.setEnabled(true);
                        wUebrstundenPauschal.setEnabled(true);
                        sAutoAuszahlung.setEnabled(true);
                        wAutoAuszahlungAb.setEnabled(true);
                    }
                } else {
                    wEnde.setVisibility(View.GONE);
                    hEnde.setText(R.string.ende_offen);
                    wBeginn.setEnabled(true);
                    wWochenbeginn.setEnabled(true);
                    wMonatsbeginn.setEnabled(true);
                    sSaldoIgnore.setEnabled(true);
                    wStartsaldo.setEnabled(true);
                    wUebrstundenPauschal.setEnabled(true);
                    sAutoAuszahlung.setEnabled(true);
                    wAutoAuszahlungAb.setEnabled(true);
                }
            }

            // Optionales Eingabefeld für den Startsaldo ein oder ausblenden
            if (bereich == REDRAW_ALL || bereich == R.id.I_switch_saldo_ignore) {
                if (sSaldoIgnore.isChecked()) {
                    hSaldoIgnore.setText(R.string.reset_saldo_on);
                    cStartsaldo.setVisibility(View.GONE);
                } else {
                    hSaldoIgnore.setText(R.string.reset_saldo_off);
                    cStartsaldo.setVisibility(View.VISIBLE);
                    wStartsaldo.setText(new Uhrzeit(mArbeitsplatz.getStartsaldo())
                            .getStundenString(
                                    false,
                                    isAnzeigeDezimal)
                    );
                }
            }

            // Optionales Eingabefeld für den Automatische Überstundenauszahlung ein oder ausblenden
            if (bereich == REDRAW_ALL || bereich == R.id.I_switch_auto_auszahlung) {
                if (sAutoAuszahlung.isChecked()) {
                    hAutoAuszahlungAb.setText(R.string.auszahlung_auto_ab);
                    wAutoAuszahlungAb.setVisibility(View.VISIBLE);
                    wAutoAuszahlungAb.setText(new Uhrzeit(mArbeitsplatz.getAutoAuzahlungAb())
                            .getStundenString(
                                    false,
                                    isAnzeigeDezimal)
                    );
                } else {
                    hAutoAuszahlungAb.setText(R.string.auszahlung_auto_no);
                    wAutoAuszahlungAb.setVisibility(View.GONE);
                }
            }
       // }
    }

    @Override
    public void onClick(View v) {
        final Datum mKalender = new Datum(mArbeitsplatz.getWochenbeginn());
        FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();

        int id = v.getId();
        if (id == R.id.I_wert_von) {
            mKalender.set(mArbeitsplatz.getStartDatum().getTime());
            CalendarDatePickerDialogFragment vonKalenderPicker =
                    new CalendarDatePickerDialogFragment()
                            .setOnDateSetListener(this)
                            .setFirstDayOfWeek(mArbeitsplatz.getWochenbeginn())
                            .setPreselectedDate(
                                    mKalender.get(Calendar.YEAR),
                                    mKalender.get(Calendar.MONTH) - 1,
                                    mKalender.get(Calendar.DAY_OF_MONTH));
            if (ASetup.isThemaDunkel)
                vonKalenderPicker.setThemeDark();
            else
                vonKalenderPicker.setThemeLight();

            vonKalenderPicker.show(fragmentManager, getString(R.string.beginn));
        } else if (id == R.id.I_wert_bis) {
            mKalender.set(mArbeitsplatz.getEndDatum().getTime());
            mKalender.setTag(mKalender.getAktuellMaximum(Calendar.DAY_OF_MONTH));
            CalendarDatePickerDialogFragment bisKalenderPicker =
                    new CalendarDatePickerDialogFragment()
                            .setOnDateSetListener(this)
                            .setFirstDayOfWeek(ASetup.aktJob.getWochenbeginn())
                            .setPreselectedDate(
                                    mKalender.get(Calendar.YEAR),
                                    mKalender.get(Calendar.MONTH) - 1,
                                    mKalender.getAktuellMaximum(Calendar.DAY_OF_MONTH));

            mKalender.set(mArbeitsplatz.getStartDatum().getTime());
            mKalender.add(Calendar.DAY_OF_MONTH, 1);
            bisKalenderPicker.setDateRange(
                    new MonthAdapter.CalendarDay(
                            mKalender.get(Calendar.YEAR),
                            mKalender.get(Calendar.MONTH) - 1,
                            mKalender.get(Calendar.DAY_OF_MONTH)),
                    null);
            if (ASetup.isThemaDunkel)
                bisKalenderPicker.setThemeDark();
            else
                bisKalenderPicker.setThemeLight();

            bisKalenderPicker.show(fragmentManager, getString(R.string.ende_titel));
        } else if (id == R.id.I_wert_monatsbeginn) {
            NumberPickerBuilder mMonatsbeginnPicker = new NumberPickerBuilder()
                    .setFragmentManager(fragmentManager)
                    .setStyleResId(ASetup.themePicker)
                    .setMinNumber(BigDecimal.valueOf(1))
                    .setMaxNumber(BigDecimal.valueOf(31))
                    .setPlusMinusVisibility(View.INVISIBLE)
                    .setDecimalVisibility(View.INVISIBLE)
                    .setTargetFragment(this)
                    .setReference(R.id.I_wert_monatsbeginn);
            mMonatsbeginnPicker.show();
        } else if (id == R.id.I_wert_startsaldo) {
            NumberPickerBuilder mSaldoPicker = new NumberPickerBuilder()
                    .setFragmentManager(fragmentManager)
                    .setStyleResId(ASetup.themePicker)
                    .setLabelText(getString(R.string.k_stunde))
                    .setPlusMinusVisibility(View.VISIBLE)
                    .setDecimalVisibility(View.VISIBLE)
                    .setTargetFragment(this)
                    .setReference(R.id.I_wert_startsaldo);
            mSaldoPicker.show();
        } else if (id == R.id.I_wert_ueber_pauschal) {
            NumberPickerBuilder mSaldoPicker = new NumberPickerBuilder()
                    .setFragmentManager(fragmentManager)
                    .setStyleResId(ASetup.themePicker)
                    .setLabelText(getString(R.string.k_stunde))
                    .setPlusMinusVisibility(View.INVISIBLE)
                    .setDecimalVisibility(View.VISIBLE)
                    .setTargetFragment(this)
                    .setReference(R.id.I_wert_ueber_pauschal);
            mSaldoPicker.show();
        } else if (id == R.id.I_wert_auto_auszahlung_ab) {
            NumberPickerBuilder mSaldoPicker = new NumberPickerBuilder()
                    .setFragmentManager(fragmentManager)
                    .setStyleResId(ASetup.themePicker)
                    .setLabelText(getString(R.string.k_stunde))
                    .setPlusMinusVisibility(View.INVISIBLE)
                    .setDecimalVisibility(View.VISIBLE)
                    .setTargetFragment(this)
                    .setReference(R.id.I_wert_auto_auszahlung_ab);
            mSaldoPicker.show();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if (id == R.id.I_switch_saldo_ignore) {
            if (mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_RESET_SALDO) != isChecked) {
                //mCallback.onNeuberechnung();
                mArbeitsplatz.setOption(Arbeitsplatz.OPT_RESET_SALDO, isChecked);
                if (isChecked) {
                    mArbeitsplatz.setStartsaldo(0);
                }
            }
        } else if (id == R.id.I_switch_auto_auszahlung) {
            if (mArbeitsplatz.isAutoAuszahlung() != isChecked) {
                //mCallback.onNeuberechnung();
                mArbeitsplatz.setAutoAuszahlung(isChecked);
                mArbeitsplatz.setAutoAuszahlungAb(0);
            }
        } else if (id == R.id.I_switch_bis) {
            if (isChecked) {
                wEnde.setVisibility(View.VISIBLE);
                if (!mArbeitsplatz.isSetEnde()) {
                    Calendar cEnde = Calendar.getInstance();
                    cEnde.setTime(ASetup.aktDatum.getTime());
                    cEnde.set(Calendar.DAY_OF_MONTH, cEnde.getActualMaximum(Calendar.DAY_OF_MONTH));
                    mArbeitsplatz.setEnddatum(cEnde.getTime());
                }
                // wenn das Ende erreicht ist alle Einstellungendeaktivieren
                mCallback.onEndeAufzeichnungChanged(mArbeitsplatz.isEndeAufzeichnung(ASetup.aktDatum));
            } else {
                mArbeitsplatz.setNoEnde();
                mCallback.onEndeAufzeichnungChanged(false);
            }
        }
        UpdateView(buttonView.getId());
    }

    @Override
    public void onDialogNumberSet(int reference, final BigInteger number, final double decimal, boolean isNegative, BigDecimal fullNumber) {
        if (reference == R.id.I_wert_monatsbeginn) {
            mArbeitsplatz.setMonatsbeginn(number.intValue());
            wMonatsbeginn.setText(getString(R.string.hint_monatsbeginn, mArbeitsplatz.getMonatsbeginn()));
        } else if (reference == R.id.I_wert_startsaldo) {
            new MinutenInterpretationDialog(
                    mContext,
                    mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL),
                    /*number,*/
                    decimal,
                    fullNumber,
                    z -> {
                        mArbeitsplatz.setStartsaldo(z.getAlsMinuten());
                        wStartsaldo.setText(z.getStundenString(
                                true,
                                mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
                        ));
                    }

            );
        } else if (reference == R.id.I_wert_ueber_pauschal) {
            new MinutenInterpretationDialog(
                    mContext,
                    mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL),
                    /*number,*/
                    decimal,
                    fullNumber,
                    z -> {
                        mArbeitsplatz.setUeberstundenPauschal(z.getAlsMinuten());
                        wUebrstundenPauschal.setText(z.getStundenString(
                                true,
                                mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
                        ));
                    }

            );
        } else if (reference == R.id.I_wert_auto_auszahlung_ab) {
            new MinutenInterpretationDialog(
                    mContext,
                    mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL),
                    /*number,*/
                    decimal,
                    fullNumber,
                    z -> {
                        mArbeitsplatz.setAutoAuszahlungAb(z.getAlsMinuten());
                        wAutoAuszahlungAb.setText(z.getStundenString(
                                true,
                                mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
                        ));
                    }

            );
        }

    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        Datum mKalender = new Datum(year, monthOfYear + 1, dayOfMonth, mArbeitsplatz.getWochenbeginn());
        String mDialogTag;

        try{
            mDialogTag = dialog.getTag();

            if (Objects.equals(mDialogTag, getString(R.string.beginn))) {
                wBeginn.setText(mKalender.getString_Datum(mContext));
                mArbeitsplatz.setSartdatum(mKalender.getTime(), ASetup.mPreferenzen.contains(ISetup.KEY_INIT_FINISH));
            } else {
                wEnde.setText(mKalender.getString_Datum(mContext));
                mArbeitsplatz.setEnddatum(mKalender.getTime());
                if (ASetup.letzterAnzeigeTag.liegtNach(mArbeitsplatz.getEndDatum()))
                    ASetup.letzterAnzeigeTag.set(mArbeitsplatz.getEndDatum().getTime());

                // wenn das Ende erreicht ist alle Einstellungendeaktivieren
                mCallback.onEndeAufzeichnungChanged(mArbeitsplatz.isEndeAufzeichnung(ASetup.aktDatum));
                UpdateView(R.id.I_switch_bis);
            }
        }catch (IllegalStateException e){
               e.printStackTrace();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(parent.getId() == R.id.I_spinner_wochenbeginn) {
            TextView mText = (TextView) parent.getChildAt(0);
            if(mText != null)
                mText.setTextColor(ASetup.res.getColor(android.R.color.darker_gray));
            mArbeitsplatz.setWochenbeginn(position +1);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /*
     * Callback Interfaces
     */
    public interface InitAufzeichnungCallbacks {
        /**
         * Aufrufen wenn sich das Aufzeichnungsende verändert
         */
        void onEndeAufzeichnungChanged(Boolean isEnde);

        //void onNeuberechnung();
    }
}

