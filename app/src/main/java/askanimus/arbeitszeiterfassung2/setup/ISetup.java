/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;

import java.util.Calendar;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;

public interface ISetup {
    // Initstatus
    int INIT_ZUSTAND_UNGELADEN = 0;
    int INIT_ZUSTAND_LAEDT = 1;
    int INIT_ZUSTAND_GELADEN = 2;

    // Konstanten
    int Minuten_TAG = 1440;

    // Maximallängen für Strings
    int LAENGE_NAME = 40;
    int LAENGE_EMAIL = 256;
    int LAENGE_MAILTEXT = 1024;
    int LAENGE_ANSCHRIFT = 512;
    int LAENGE_PFAD = 1024;
    int LAENGE_NAME_KURZ = 5;
    int LAENGE_WAEHRUNG_KURZ = 3;
    int LAENGE_EORT = 100;
    int LAENGE_NOTIZ = 512;

    //
    // Tageseinteilung
    //
    int TAG_ARBEITSTAG = 2;
    int TAG_HALBFREI = 1;
    int TAG_RUHETAG = 0;

    //
    // Statuswerte
    //
    int STATUS_GELOESCHT = -1;
    int STATUS_INAKTIV = 0;
    int STATUS_AKTIV = 1;

    //
    // Anzeigeoptioen für allgemeine Einstellungen
    //
    int OPT_ANZ_DEZIMAL = 1; // von den allgem. in die Arbeitsplatzoptionen gewandert
    int OPT_ANZ_ERW_SALDO = 2;
    int OPT_ANZ_UMG_SORT = 4;
    int OPT_ANZ_THEMA_DUNKEL = 8;
    int OPT_ANZ_AKTTAG = 16;

    //
    // Seite die beim Start der Anwendung gezeigt werden soll
    //
    int VIEW_JOB = 0;
    int VIEW_JAHR = 1;
    int VIEW_MONAT = 2;
    int VIEW_WOCHE = 3;
    int VIEW_TAG = 4;
    int VIEW_LETZTER = 5;
    int VIEW_ABOUT = 6;
    int VIEW_CHARTS = 7;

    //
    // Optionen
    //
    int OPT_AUTO_RUHETAG = 1;
    int OPT_SPRACHE_DE = 0;
    int OPT_SPRACHE_EN = 1;
    int OPT_SPRACHE_IT = 2;

    //
    // Erinnerung zur Eingabe
    //
    int ERINNERUNG_TAG = 1;
    int ERINNERUNG_SCHICHT_BEGINN = 2;
    int ERINNERUNG_SCHICHT_ENDE = 3;

    //
    // autom. Datensicherung Sicherungsintervall
    //
    int AUTOBACKUP_NO = -1;
    int AUTOBACKUP_STUNDEN = 0;
    int AUTOBACKUP_TAGE = 1;
    int AUTOBACKUP_WOCHEN = 2;
    int AUTOBACKUP_MONATE = 3;
    int[] AUTOBACKUP_INTERVAL = {Calendar.HOUR, Calendar.DAY_OF_MONTH, Calendar.WEEK_OF_YEAR, Calendar.MONTH};

    //
    // Entscheidung bei Zeiteingabe im Dezimalmodus
    //
    int DEZIMAL_ANTWORT_KEINE = 0;
    int DEZIMAL_ANTWORT_DEZIMAL = 1;
    int DEZIMAL_ANTWORT_UHRZEIT = 2;

    //
    // Sortieroptionen
    //
    int SORT_NO = 0;
    int SORT_AZ = 1;
    int SORT_ZA = 2;
    int SORT_BENUTZT = 3;

    //
    // Requestcodes
    //
    int REQ_FOLDER_PICKER_WRITE_EXPORT = 9999;
    int REQ_FOLDER_PICKER_WRITE_BACKUP = 19999;
    int REQ_FOLDER_PICKER_READ = 999;
    int REQ_MAKE_COPY = 666;
    int REQ_DEMAND_WRITE = 8888;
    int REQ_DEMAND_READ = 888;
    int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST_CODE = 777;
    int REQ_IMAGE_READ = 1966;
    /*
     * Die übergebenen Argumente
     */
    String ARG_MONAT = "monat";
    String ARG_JAHR = "jahr";
    String ARG_TAG = "tag";
    String ARG_DATUM = "datum";
    String ARG_NEWJOB = "neuer_job";
    String ARG_POSITION = "position";
    String ARG_MINUTEN = "minuten";
    String ARG_STUNDEN = "stunden";
    String ARG_WIRKUNG = "wirkung";

    //
    // Schlüssel für Shared Preferences
    //
    String KEY_JOBID = "jobid";            // Nummer des Arbeitsplatzes
    String KEY_EDIT_JOB = "editjob";            // Nummer des Arbeitsplatzes der in den Einstellungen geöffnet ist
    String KEY_ANZEIGE_VIEW = "anzeige_view";     // gewünschte Startseite
    String KEY_ANZEIGE_LETZTER = "anzeige_aktuell";  // letzte angezeigte Seite
    String KEY_ANZEIGE_DATUM = "anzeige_datum";    // letztes angezeigtes Datum
    String KEY_ANZEIGE_ZUKUNFT = Datenbank.DB_F_ANZEIGE_ZUKUNFT;
    String KEY_ANZEIGE_DEZIMAL = "opt_anz_dezimal";
    String KEY_ANZEIGE_ERW_SALDO = "opt_anz_erwsaldo";
    String KEY_ANZEIGE_UMG_SORT = "opt_anz_umgsort";
    String KEY_ANZEIGE_AKTTAG = "opt_anz_akttag";
    String KEY_ANZEIGE_MONAT_COMPACT = "opt_anz_m_compact";
    String KEY_ANZEIGE_WOCHE_COMPACT = "opt_anz_w_compact";
    String KEY_ANZEIGE_JAHR_COMPACT = "opt_anz_j_compact";
    String KEY_ANZEIGE_W_KUERZEL = Datenbank.DB_F_W_KUERZEL;
    //String KEY_ANZEIGE_W_TRENNER = Datenbank.DB_F_W_TRENNER;
    String KEY_ANZEIGE_E_KUERZEL = Datenbank.DB_F_E_KUERZEL;
    String KEY_USERNAME = Datenbank.DB_F_USER;         //Name des Benutzers für die Kopfzeile der Berichte
    String KEY_USERANSCHRIFT = Datenbank.DB_F_ANSCHRIFT;    // Anschrift des Users
    String KEY_BACKUP_DIR = Datenbank.DB_F_BACKUP_DIR;
    String KEY_DATEN_DIR = Datenbank.DB_F_DATEN_DIR;
    String KEY_INIT_FINISH = "init_fin";
    String KEY_INIT_SEITE = "init_seite";
    String KEY_WIDGET_ID = "widgetid";
    String KEY_WIDGET_CONFIG = "widget_conf";
    String KEY_RESUME_VIEW = "resume_view"; // Boolian wird gesetzt wenn aus anderen Intend zurückgekehrt wird

    String KEY_EXP_W_ZEITRAUM = "ex_seite";
    String KEY_EXP_W_SPALTEN = "ex_w_sp";
    String KEY_EXP_W_ZEILEN = "ex_W_ze";
    String KEY_EXP_W_ZUSATZ = "ex_W_zu";
    String KEY_EXP_W_NOTIZ = "ex_W_notiz";
    String KEY_EXP_W_SORT_EORT = "ex_W_sort_eort";
    String KEY_EXP_W_FONTSIZE = "ex_W_fontsize";

    String KEY_EXP_J_TABELLEN = "ex_j_tab";
    String KEY_EXP_J_OPTIONEN = "ex_j_opt";
    String KEY_EXP_J_ZUSATZ = "ex_j_zu";
    String KEY_EXP_J_SORT_EORT = "ex_J_sort_eort";
    String KEY_EXP_J_FONTSIZE = "ex_J_fontsize";

    String KEY_EXP_M_SPALTEN_AUSF = "ex_M_spa_a";
    String KEY_EXP_M_ZEILEN_AUSF = "ex_M_zei_a";
    String KEY_EXP_M_ZUSATZ_AUSF = "ex_M_zu_a";
    String KEY_EXP_M_SPALTEN_KURZ = "ex_M_spa_k";
    String KEY_EXP_M_ZEILEN_KURZ = "ex_M_zei_k";
    String KEY_EXP_M_ZUSATZ_KURZ = "ex_M_zu_k";
    String KEY_EXP_M_SPALTEN_LGAV = "ex_M_spa_l";
    String KEY_EXP_M_ZEILEN_LGAV = "ex_M_zei_l";
    String KEY_EXP_M_ZUSATZ_LGAV = "ex_M_zu_l";
    String KEY_EXP_M_VARIANTE = "ex_M_vari";
    String KEY_EXP_M_NOTIZ = "ex_M_notiz";
    String KEY_EXP_M_SORT_EORT = "ex_M_sort_eort";
    String KEY_EXP_M_FONTSIZE = "ex_M_fontsize";

    String KEY_EXP_EO_SPALTEN = "ex_eo_sp";
    String KEY_EXP_EO_ZEILEN = "ex_eo_ze";
    String KEY_EXP_EO_ZUSATZ = "ex_eo_zu";
    String KEY_EXP_EO_NOTIZ = "ex_eo_notiz";
    String KEY_EXP_EO_ZUSAMMEN = "ex_eo_zusammen";
    String KEY_EXP_EO_FONTSIZE = "ex_eo_fontsize";

    String KEY_EXP_ZR_SPALTEN = "ex_zr_sp";
    String KEY_EXP_ZR_ZEILEN = "ex_zr_ze";
    String KEY_EXP_ZR_ZUSATZ = "ex_zr_zu";
    String KEY_EXP_ZR_NOTIZ = "ex_zr_notiz";
    String KEY_EXP_ZR_FONTSIZE = "ex_zr_fontsize";

    String KEY_EXP_TYP = "ex_typ";
    String KEY_EXP_TYP_WOCHE = "ex_typ_w";
    String KEY_EXP_TYP_MONAT = "ex_typ_m";
    String KEY_EXP_TYP_JAHR = "ex_typ_j";
    String KEY_EXP_TYP_EORT = "ex_typ_eo";
    String KEY_EXP_TYP_ZRAUM = "ex_typ_zr";
    String KEY_EXP_TYP_SONSTIGES = "ex_typ_s";
    String KEY_EXPORT_CSV_TRENNER = "ex_csvtrenner";
    String KEY_ANTWORT_DEZ = "a_dezimal";
    String KEY_ANTWORT_BEENDEN = "a_beenden";
    String KEY_ANTWORT_OEFFNEN = "a_oeffnen";
    String KEY_SPRACHE = Datenbank.DB_F_SPRACHE;
    String KEY_OPT_SPRACHE_CHANGE = "sprange_geaendert";
    String KEY_OPT_ZEITZUSATZ_WIRKUNG = "zeit_wirkung";
    String KEY_SORT_EORTLISTE = "eort_sort";
    String KEY_SORT_AUSWAHLLISTE = "auswahl_sort";
    String KEY_SUCHE_STRING = "suchstring";
    String KEY_HANDBUCH_PFAD = "path_handbuch";
    String KEY_AUTOBACKUP_INTERVAL = Datenbank.DB_F_AUTOBACKUP_INTERVALL;
    String KEY_AUTOBACKUP_SCHRITTE = Datenbank.DB_F_AUTOBACKUP_SCHRITTE;
    String KEY_AUTOBACKUP_ANZAHL = Datenbank.DB_F_AUTOBACKUP_ANZAHL;
    String KEY_AUTOBACKUP_LETZTES = "ab_last";
    String KEY_AUTOBACKUP_NAECHSTES = "ab_next";

    // Zusatz der an den Namen der Schicht angehängt wird
    String NAME_ZUSATZ = "°";


    /*
     * Ab Version 2.01.00 wird, bei Wahl der Urlaubsabrechnung in Stunden,
     * der Urlaubsanspruch und der Resturlaub in Minuten gespeichert
     * beim ersten Start der App nach dem Update bzw. nach dem Wiederherstellen
     * einer alten Datensicherung werden die Werte einmalig umgerechnet
     */
    String KEY_URLAUB_ALS_H_UMGERECHENET = "u_umgerechent";

    String KEY_THEMA_DUNKEL = "thema_dunkel";
    String APP_RESET = "ACTION_APP_RESET";

    String KEY_VERSION_APP = "appversion";

}
