/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;


import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;

import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.datenbank.AUpdateDatenbank;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;

public class InitAssistent extends AppCompatActivity implements View.OnClickListener,
        InitAssistentFragmentAufzeichnung.InitAufzeichnungCallbacks,
        InitAssistentFragmentArbeitsplatz.InitArbeitsplatzCallbacks,
        InitAssistentFragmentMigration.migrationDone,
        ISetup{

    private int mSeite;
    private int mMinSeite = 0;
    private final int mMaxSeite = 8;

    private AppCompatButton mButtonBack;
    private AppCompatButton mButtonNext;
    private Arbeitsplatz mArbeitsplatz = null;

    StorageHelper storageHelperImport;

    StorageHelper storageHelperExport;
    
    StorageHelper storageHelperSicherungen;

    /*
     wird zum anpassen der App Sprache benötigt, wenn diese von der Systemsprache abweicht
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setTheme(ASetup.isThemaDunkel ? R.style.MyFullscreenTheme : R.style.MyFullscreenTheme_Light);
        setTheme(
                PreferenceManager
                        .getDefaultSharedPreferences(getApplicationContext())
                        .getBoolean(ASetup.KEY_THEMA_DUNKEL, false) ?
                        R.style.MyFullscreenTheme :
                        R.style.MyFullscreenTheme_Light
        );
        setContentView(R.layout.activity_init_assistent);
    }


    @Override
    protected void onResume() {
        super.onResume();

        mButtonBack = findViewById(R.id.I_knopf_zurueck);
        mButtonNext = findViewById(R.id.I_knopf_vor);

        // Einstellungen laden bzw. laden abwarten
        ASetup.init(this, this::resume);
    }

    private void resume() {
        Intent intent = getIntent();
        // den zu öffnenden Arbeitsplatz ermitteln
        mArbeitsplatz = ASetup.aktJob;
        // die minimale Seite ermitteln
        mMinSeite =
                ASetup.mPreferenzen.getBoolean(ISetup.KEY_INIT_FINISH, false) ? 1 : 0;

        // welche Seite der Einstellungen ist geöffnet
        mSeite = intent.getIntExtra(
                ISetup.KEY_INIT_SEITE,
                ASetup.mPreferenzen.getInt(
                        ISetup.KEY_INIT_SEITE,
                        mMinSeite)
        );

        mButtonNext.setOnClickListener(this);
        mButtonBack.setOnClickListener(this);
        ViewCompat.setBackgroundTintList(mButtonNext, mArbeitsplatz.getFarbe_Button());
        ViewCompat.setBackgroundTintList(mButtonBack, mArbeitsplatz.getFarbe_Button());

        mButtonBack.setTextColor(mArbeitsplatz.getFarbe_Schrift_Button());
        mButtonNext.setTextColor(mArbeitsplatz.getFarbe_Schrift_Button());

        mButtonBack.setVisibility((mSeite > mMinSeite) ? View.VISIBLE : View.INVISIBLE);
        if (mSeite < mMaxSeite)
            mButtonNext.setText(R.string.button_vor);
        else
            mButtonNext.setText(R.string.button_end);

        neuerInhalt();
    }


    @Override
    public void onClick(View v) {
        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
        if (v == mButtonNext) {
            mSeite++;
        } else if (v == mButtonBack) {
            mSeite--;
        }

        mButtonBack.setVisibility((mSeite > mMinSeite) ? View.VISIBLE : View.INVISIBLE);
        mEdit.putInt(ISetup.KEY_INIT_SEITE, mSeite).apply();

        if (mSeite < mMaxSeite) {
            mButtonNext.setText(R.string.button_vor);
            neuerInhalt();
        } else if (mSeite == mMaxSeite) {
            mButtonNext.setText(R.string.button_end);
            neuerInhalt();
        } else {
            // den Initprozess als beendet markieren
            mEdit.putBoolean(ISetup.KEY_INIT_FINISH, true).apply();

            // Die Monate und Jahre vom Aufzeichnungsbeginn bis heute anlegen und
            // mit Soll, Ist, Saldo, Saldo des Vormonat vorbelegen
            AUpdateDatenbank.updateDatenbank(
                    this,
                    mArbeitsplatz
            );
        }
    }

    @Override
    public void onBackPressed() {
        if (mSeite <= mMinSeite) {
            if(!ASetup.mPreferenzen.getBoolean(ISetup.KEY_INIT_FINISH, false)) {
                mArbeitsplatz.delete();
            }
            finish();
        } else
            onClick(mButtonBack);
    }

    @Override
    protected void onDestroy() {
        if (isChangingConfigurations()) {
            Intent mInitIntent = new Intent();
            mInitIntent.setClass(this, this.getClass());
            mInitIntent.putExtra(ISetup.KEY_EDIT_JOB, mArbeitsplatz.getId());
            mInitIntent.putExtra(ISetup.KEY_INIT_SEITE, mSeite);
            mInitIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mInitIntent);
        } else {
            if (mSeite > mMinSeite) {
                ASetup.mPreferenzen.edit()
                        .putLong(ISetup.KEY_EDIT_JOB, mArbeitsplatz.getId())
                        .apply();
                ASetup.mPreferenzen.edit()
                        .putInt(ISetup.KEY_INIT_SEITE, mSeite)
                        .apply();
                ASetup.zustand = ISetup.INIT_ZUSTAND_UNGELADEN; // neuladen der Einstellungen erzwingen
            } /*else {
                finish();
            }*/
        }
        super.onDestroy();
    }

    // fügt die entsprechende Seite ins Layout ein
    private void neuerInhalt() {
        Fragment mInhalt = null;

        switch (mSeite) {
            case 0:
                if (storageHelperImport == null) {
                    storageHelperImport = new StorageHelper(this);
                }
                InitAssistentFragmentMigration fragmentMigration = new InitAssistentFragmentMigration();
                fragmentMigration.setUp(storageHelperImport, this);
                mInhalt = fragmentMigration;
                break;
            case 1:
                if(storageHelperExport == null) {
                    storageHelperExport = new StorageHelper(this);
                }
                mInhalt = InitAssistentFragmentAllgemein.newInstance();
                ((InitAssistentFragmentAllgemein) mInhalt).setUp(mArbeitsplatz, storageHelperExport);
                break;
            case 2:
                if (storageHelperSicherungen == null) {
                    storageHelperSicherungen = new StorageHelper(this);
                }
                mInhalt = InitAssistentFragmentDatensicherung.newInstance();
                ((InitAssistentFragmentDatensicherung) mInhalt).setUp(mArbeitsplatz, storageHelperSicherungen);
                break;
            case 3:
                mInhalt = InitAssistentFragmentArbeitsplatz.newInstance();
                ((InitAssistentFragmentArbeitsplatz) mInhalt).setup(mArbeitsplatz, this);
                break;
            case 4:
                mInhalt = InitAssistentFragmentAufzeichnung.newInstance();
                ((InitAssistentFragmentAufzeichnung) mInhalt).setup(mArbeitsplatz, this);
                break;
            case 5:
                mInhalt = InitAssistentFragmentZusatzeintraege.newInstance();
                ((InitAssistentFragmentZusatzeintraege) mInhalt).setup(mArbeitsplatz);
                break;
            case 6:
                mInhalt = InitAssistentFragmentArbeitszeit.newInstance();
                ((InitAssistentFragmentArbeitszeit) mInhalt).setup(mArbeitsplatz);
                break;
            case 7:
                mInhalt = InitAssistentFragmentSchicht.newInstance();
                ((InitAssistentFragmentSchicht) mInhalt).setup(mArbeitsplatz);
                break;
            case 8:
                mInhalt = InitAssistentFragmentAbwesenheit.newInstance();
                ((InitAssistentFragmentAbwesenheit) mInhalt).setup(mArbeitsplatz);
                break;
        }
        if (mInhalt != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment, mInhalt)
                    .commitAllowingStateLoss();
        }

    }


    @Override
    public void onEndeAufzeichnungChanged(Boolean isEnde) {
        // im Initassistenten muss nicht reagiert werden,
        // hier sollen ja alle Einstellungen zugänglich sein
    }

    @Override
    public void onSettingChaged(String schluessel, int wert) {
        if (Datenbank.DB_F_FARBE.equals(schluessel)) {
            ViewCompat.setBackgroundTintList(mButtonNext, mArbeitsplatz.getFarbe_Button());
            ViewCompat.setBackgroundTintList(mButtonBack, mArbeitsplatz.getFarbe_Button());

            mButtonBack.setTextColor(mArbeitsplatz.getFarbe_Schrift_Button());
            mButtonNext.setTextColor(mArbeitsplatz.getFarbe_Schrift_Button());
        }
    }


    /*
     * Rückmeldung vom Rechtemanagment nach Rechteanfrage
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
            requestCode = requestCode & 0x0000ffff;
            if (requestCode == REQ_DEMAND_WRITE
                    || requestCode == REQ_DEMAND_READ) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (storageHelperImport != null && requestCode == REQ_DEMAND_READ) {
                        storageHelperImport.setPfad(storageHelperImport.getPfad());
                    }
                    if (storageHelperExport != null && requestCode == REQ_DEMAND_WRITE) {
                        storageHelperExport.setPfad(storageHelperExport.getPfad());
                    }
                    if (storageHelperSicherungen != null && requestCode == REQ_DEMAND_WRITE) {
                        storageHelperSicherungen.setPfad(storageHelperSicherungen.getPfad());
                    }
                } else {
                    // Recht verweigert
                    Toast.makeText(
                            this,
                            getString(R.string.err_keine_berechtigung),
                            Toast.LENGTH_LONG
                    ).show();
                }
            }
        }
    }

    // wird nach Auswahl eines Verzeichnisses aufgerufen
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            Uri treeUri = data.getData();
            requestCode &= 0x0000ffff;
            if (treeUri != null) {
                if (requestCode == REQ_FOLDER_PICKER_WRITE_EXPORT) {
                    getContentResolver().takePersistableUriPermission(
                            treeUri,
                            Intent.FLAG_GRANT_READ_URI_PERMISSION
                                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    if (storageHelperExport != null) {
                        storageHelperExport.setPfad(treeUri.toString());
                    }
                } else if (requestCode == REQ_FOLDER_PICKER_WRITE_BACKUP) {
                    getContentResolver().takePersistableUriPermission(
                            treeUri,
                            Intent.FLAG_GRANT_READ_URI_PERMISSION
                                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    if (storageHelperSicherungen != null) {
                        storageHelperSicherungen.setPfad(treeUri.toString());
                    }
                } else if (requestCode == REQ_FOLDER_PICKER_READ) {
                    getContentResolver()
                            .takePersistableUriPermission(treeUri,
                                    Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    if (storageHelperImport != null) {
                        storageHelperImport.setPfad(treeUri.toString());
                    }
                } else if (requestCode == REQ_IMAGE_READ) {
                    // Unterschriftscan kopieren
                    InputStream in;
                    OutputStream out;
                    try {
                        in = getContentResolver().openInputStream(treeUri);
                        out = getContentResolver().openOutputStream(
                                Uri.fromFile(new File(getFilesDir(), "UnterschriftAN.jpg"))
                        );

                        if (out != null && in != null) {
                            byte[] buffer = new byte[1024];
                            int read;
                            while ((read = in.read(buffer)) != -1) {
                                out.write(buffer, 0, read);
                            }
                            in.close();
                            out.flush();
                            out.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onMigrationDone(Boolean status) {
        if(status) {
            // den Initprozess als beendet markieren
            ASetup.mPreferenzen.edit().putBoolean(ISetup.KEY_INIT_FINISH, true).apply();

            ASetup.zustand = ASetup.INIT_ZUSTAND_UNGELADEN;
            Intent i = getIntent();
            i.putExtra(ISetup.KEY_EDIT_JOB, ASetup.mPreferenzen.getLong(ISetup.KEY_JOBID, 0));
        }
        onResume();
    }
}
