/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentManager;

import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codetroopers.betterpickers.numberpicker.NumberPickerBuilder;
import com.codetroopers.betterpickers.numberpicker.NumberPickerDialogFragment;

import java.math.BigDecimal;
import java.math.BigInteger;

import askanimus.arbeitszeiterfassung2.MinutenInterpretationDialog;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import yuku.ambilwarna.AmbilWarnaDialog;

/**
 * A placeholder fragment containing a simple view.
 */
public class InitAssistentFragmentArbeitsplatz extends Fragment implements View.OnClickListener,
        SwitchCompat.OnCheckedChangeListener,
        NumberPickerDialogFragment.NumberPickerDialogHandlerV2 {
    private Arbeitsplatz mArbeitsplatz;

    private EditText wName;
    private TextView wAnschrift;
    private TextView wEmail;
    private TextView wEmailText;
    private TextView wUrlaubstage;
    private TextView wStarturlaub;
    private TextView hRestNoVerfall;
    private TextView hUrlaubstunden;
    private TextView wFarbe;
    private TextView hZukunft;
    private LinearLayout cMonateZukunft;
    private TextView wMonateZukunft;
    private TextView wStundenlohn;
    private TextView hAnzeigeDezimal;

    private Context mContext;

    private boolean isDezimal;
    boolean isUrlaubInStunden;

    private InitArbeitsplatzCallbacks mCallback;

    /*
    * Neue Instanz anlegen
    */
    public static InitAssistentFragmentArbeitsplatz newInstance() {
        return new InitAssistentFragmentArbeitsplatz();
    }

    public void setup(Arbeitsplatz arbeitsplatz, InitArbeitsplatzCallbacks callback){
        mArbeitsplatz = arbeitsplatz;
        isDezimal = mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL);
        isUrlaubInStunden = mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN);
        mCallback = callback;
        if(ASetup.zustand ==  ISetup.INIT_ZUSTAND_GELADEN) {
            resume();
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getContext();
        return inflater.inflate(R.layout.fragment_init_arbeitsplatz, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ASetup.init(mContext, this::resume);
    }

    private void resume() {
        View mView = getView();
        if (mArbeitsplatz != null && mView != null) {
            // Widgeds finden
            wName = mView.findViewById(R.id.I_arbeitsplatz_wert_name);
            wAnschrift = mView.findViewById(R.id.I_wert_anschrift);
            wEmail = mView.findViewById(R.id.I_wert_email);
            wEmailText = mView.findViewById(R.id.I_wert_emailtext);
            wUrlaubstage = mView.findViewById(R.id.I_wert_urlaubstage);
            wStarturlaub = mView.findViewById(R.id.I_wert_starturlaub);
            SwitchCompat cRestNoVerfall = mView.findViewById(R.id.I_resturlaub_noverfall);
            hRestNoVerfall = mView.findViewById(R.id.I_hint_resturlaub);
            SwitchCompat sUrlaubstunden = mView.findViewById(R.id.I_switch_urlaubstunden);
            hUrlaubstunden = mView.findViewById(R.id.I_hint_urlaubsstunden);
            wFarbe = mView.findViewById(R.id.I_wert_Farbe);
            SwitchCompat sZukunft = mView.findViewById(R.id.I_switch_zukunft);
            hZukunft = mView.findViewById(R.id.I_hint_zukunft);
            cMonateZukunft = mView.findViewById(R.id.I_box_zukunft);
            wMonateZukunft = mView.findViewById(R.id.I_wert_zukunft);
            wStundenlohn = mView.findViewById(R.id.I_wert_stundenlohn);
            SwitchCompat sAnzeigeDezimal = mView.findViewById(R.id.I_switch_dezimal);
            hAnzeigeDezimal = mView.findViewById(R.id.I_hint_dezimal);


            // Farben setzen
            cRestNoVerfall.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            cRestNoVerfall.setTrackTintList(mArbeitsplatz.getFarbe_Trak());
            sUrlaubstunden.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sUrlaubstunden.setTrackTintList(mArbeitsplatz.getFarbe_Trak());
            sZukunft.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sZukunft.setTrackTintList(mArbeitsplatz.getFarbe_Trak());
            sAnzeigeDezimal.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sAnzeigeDezimal.setTrackTintList(mArbeitsplatz.getFarbe_Trak());
            wFarbe.setBackgroundColor(mArbeitsplatz.getFarbe());


            // Handler registrieren
            wFarbe.setOnClickListener(this);
            wName.setOnClickListener(this);
            wAnschrift.setOnClickListener(this);
            wEmail.setOnClickListener(this);
            wEmailText.setOnClickListener(this);
            wUrlaubstage.setOnClickListener(this);
            cRestNoVerfall.setOnCheckedChangeListener(this);
            wStarturlaub.setOnClickListener(this);
            sUrlaubstunden.setOnCheckedChangeListener(this);
            sZukunft.setOnCheckedChangeListener(this);
            wMonateZukunft.setOnClickListener(this);
            wStundenlohn.setOnClickListener(this);
            sAnzeigeDezimal.setOnCheckedChangeListener(this);


            // Seitentitel ausblenden wenn es nicht der Initassistent ist
            if (ASetup.mPreferenzen.contains(ISetup.KEY_INIT_FINISH)) {
                TextView tTitel = requireView()
                        .findViewById(R.id.I_arbeitsplatz_titel);
                tTitel.setVisibility(View.GONE);
            }

            // Werte eintragen
            wName.setText(mArbeitsplatz.getName());
            wAnschrift.setText(mArbeitsplatz.getAnschrift());
            wEmail.setText(mArbeitsplatz.getEmail());
            wEmailText.setText(mArbeitsplatz.getEmailText());
            cRestNoVerfall.setChecked(mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_RESTURLAUB_NO_VERFALL));

            sUrlaubstunden.setChecked(mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN));

            sZukunft.setChecked(mArbeitsplatz.isAnzeigeZukunft());

            wStundenlohn.setText(ASetup.waehrungformat.format(mArbeitsplatz.getStundenlohn()));

            sAnzeigeDezimal.setChecked(mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL));


            // Anzahl Urlaubstunden
            if (isUrlaubInStunden) {
                int minuten = (int) mArbeitsplatz.getSoll_Urlaub();
                wUrlaubstage.setText(new Uhrzeit(minuten)
                        .getStundenString(
                                true,
                                isDezimal));
                minuten = (int) mArbeitsplatz.getStart_Urlaub();
                wStarturlaub.setText(new Uhrzeit(minuten)
                        .getStundenString(
                                true,
                                isDezimal
                        ));
            } else {
                wUrlaubstage.setText(ASetup.tageformat.format(mArbeitsplatz.getSoll_Urlaub()));
                wStarturlaub.setText(ASetup.tageformat.format(mArbeitsplatz.getStart_Urlaub()));
            }


            updateView();
        }
    }

    private void updateView() {
        // Hinweis für Ansicht in dezimalen Minuten
        hAnzeigeDezimal.setText(
                isDezimal
                        ? getString(R.string.hint_dezimal_on)
                        : getString(R.string.hint_dezimal_off));
        // Hinweis auf die Art der Urlaubsvberechnung, Tage oder Stunden
        hUrlaubstunden.setVisibility(isUrlaubInStunden ? View.VISIBLE : View.GONE);

        // Urlaubstage Verfall
        hRestNoVerfall.setText(
                mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_RESTURLAUB_NO_VERFALL) ?
                        R.string.resturlaub_no_verfall_on :
                        R.string.resturlaub_no_verfall_off
        );
        // Anzeige von zukünftigen Tagen
        if (mArbeitsplatz.isAnzeigeZukunft()) {
            hZukunft.setVisibility(View.GONE);
            cMonateZukunft.setVisibility(View.VISIBLE);
            if (mArbeitsplatz.getMonate_Zukunft() > 0) {
                wMonateZukunft.setText(String.valueOf(mArbeitsplatz.getMonate_Zukunft()));
            } else {
                wMonateZukunft.setText("");
            }
        } else {
            hZukunft.setVisibility(View.VISIBLE);
            cMonateZukunft.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStop() {
        if(mArbeitsplatz != null) {
            mArbeitsplatz.schreibeJob();
        }
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        final InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        final FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
        int id = v.getId();
        if (id == R.id.I_wert_Farbe) {
            AmbilWarnaDialog dialog = new AmbilWarnaDialog(mContext,
                    mArbeitsplatz.getFarbe(), false,
                    new AmbilWarnaDialog.OnAmbilWarnaListener() {
                        @Override
                        public void onOk(AmbilWarnaDialog dialog, int color) {
                            // color is the color selected by the user
                            mArbeitsplatz.setFarbe(color);
                            wFarbe.setBackgroundColor(color);
                            mCallback.onSettingChaged(Datenbank.DB_F_FARBE, color);
                        }

                        @Override
                        public void onCancel(AmbilWarnaDialog dialog) {
                            // cancel was selected by the user
                        }
                    });

            dialog.show();
        } else if (id == R.id.I_arbeitsplatz_wert_name) {
            final EditText mInputName = new EditText(mContext);
            mInputName.setInputType(InputType.TYPE_CLASS_TEXT);
            mInputName.setMaxLines(1);
            mInputName.setText(mArbeitsplatz.getName());
            mInputName.setSelection(mInputName.getText().length());
            mInputName.setFocusableInTouchMode(true);
            mInputName.requestFocus();
            mInputName.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            //Längenbegrenzung des Inputstrings
            InputFilter[] fn = new InputFilter[1];
            fn[0] = new InputFilter.LengthFilter(ISetup.LAENGE_NAME);
            mInputName.setFilters(fn);
            new AlertDialog.Builder(mContext)
                    .setTitle(getString(R.string.name_arbeitsplatz))
                    .setView(mInputName)
                    .setPositiveButton(getString(android.R.string.ok), (dialog, whichButton) -> {
                        wName.setText(mInputName.getText().toString());
                        mArbeitsplatz.setName(mInputName.getText().toString());
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(mInputName.getWindowToken(), 0);
                        }
                    }).setNegativeButton(getString(android.R.string.cancel), (dialog, whichButton) -> {
                        // Abbruchknopf gedrückt
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(mInputName.getWindowToken(), 0);
                        }

                    }).show();
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        } else if (id == R.id.I_wert_anschrift) {
            final EditText mInputAnschrift = new EditText(mContext);
            mInputAnschrift.setText(mArbeitsplatz.getAnschrift());
            mInputAnschrift.setSelection(mInputAnschrift.getText().length());
            mInputAnschrift.setFocusableInTouchMode(true);
            mInputAnschrift.requestFocus();
            mInputAnschrift.setInputType(
                    InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                    | InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    | InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS
            );
            //Längenbegrenzung des Inputstrings
            InputFilter[] fa = new InputFilter[1];
            fa[0] = new InputFilter.LengthFilter(ISetup.LAENGE_ANSCHRIFT);
            mInputAnschrift.setFilters(fa);
            new AlertDialog.Builder(mContext)
                    .setTitle(getString(R.string.anschrift))
                    .setView(mInputAnschrift)
                    .setPositiveButton(getString(android.R.string.ok), (dialog, whichButton) -> {
                        wAnschrift.setText(String.valueOf(mInputAnschrift.getText()));
                        mArbeitsplatz.setAnschrift(wAnschrift.getText().toString());
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(mInputAnschrift.getWindowToken(), 0);
                        }
                    }).setNegativeButton(getString(android.R.string.cancel), (dialog, whichButton) -> {
                        // Abbruchknopf gedrückt
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(mInputAnschrift.getWindowToken(), 0);
                        }

                    }).show();
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        } else if (id == R.id.I_wert_email) {
            final EditText mInputEmail = new EditText(mContext);
            mInputEmail.setMaxLines(1);
            mInputEmail.setText(mArbeitsplatz.getEmail());
            mInputEmail.setSelection(mInputEmail.getText().length());
            mInputEmail.setFocusableInTouchMode(true);
            mInputEmail.requestFocus();
            mInputEmail.setInputType(
                    InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                    | InputType.TYPE_CLASS_TEXT

            );
            //Längenbegrenzung des Inputstrings
            InputFilter[] fe = new InputFilter[1];
            fe[0] = new InputFilter.LengthFilter(ISetup.LAENGE_EMAIL);
            mInputEmail.setFilters(fe);
            new AlertDialog.Builder(mContext)
                    .setTitle(getString(R.string.email))
                    .setView(mInputEmail)
                    .setPositiveButton(getString(android.R.string.ok), (dialog, whichButton) -> {
                        wEmail.setText(String.valueOf(mInputEmail.getText()));
                        mArbeitsplatz.setEmail(wEmail.getText().toString());
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(mInputEmail.getWindowToken(), 0);
                        }
                    }).setNegativeButton(getString(android.R.string.cancel), (dialog, whichButton) -> {
                        // Abbruchknopf gedrückt
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(mInputEmail.getWindowToken(), 0);
                        }

                    }).show();
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        } else if (id == R.id.I_wert_emailtext) {
            final EditText mInputEmailText = new EditText(mContext);
            mInputEmailText.setText(mArbeitsplatz.getEmailText());

            mInputEmailText.setSelection(mInputEmailText.getText().length());
            mInputEmailText.setFocusableInTouchMode(true);
            mInputEmailText.requestFocus();
            mInputEmailText.setInputType(
                    InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                            | InputType.TYPE_CLASS_TEXT
                            | InputType.TYPE_TEXT_FLAG_MULTI_LINE
            );

            //Längenbegrenzung des Inputstrings
            InputFilter[] ft = new InputFilter[1];
            ft[0] = new InputFilter.LengthFilter(ISetup.LAENGE_MAILTEXT);
            mInputEmailText.setFilters(ft);
            new AlertDialog.Builder(mContext)
                    .setTitle(getString(R.string.text_email))
                    .setView(mInputEmailText)
                    .setPositiveButton(getString(android.R.string.ok), (dialog, whichButton) -> {
                        wEmailText.setText(String.valueOf(mInputEmailText.getText()));
                        mArbeitsplatz.setMailText(wEmailText.getText().toString());
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(mInputEmailText.getWindowToken(), 0);
                        }
                    }).setNegativeButton(getString(android.R.string.cancel), (dialog, whichButton) -> {
                        // Abbruchknopf gedrückt
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(mInputEmailText.getWindowToken(), 0);
                        }

                    }).show();
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        } else if (id == R.id.I_wert_urlaubstage) {
            NumberPickerBuilder mUrlaubstagePicker = new NumberPickerBuilder()
                    .setFragmentManager(fragmentManager)
                    .setStyleResId(ASetup.themePicker)
                    .setMinNumber(BigDecimal.valueOf(0))
                    .setMaxNumber(BigDecimal.valueOf(366))
                    .setPlusMinusVisibility(View.INVISIBLE)
                    .setDecimalVisibility(View.VISIBLE)
                    .setLabelText(getString(isUrlaubInStunden ? R.string.k_stunde : R.string.tage))
                    .setTargetFragment(this)
                    .setReference(R.id.I_wert_urlaubstage);
            mUrlaubstagePicker.show();
        } else if (id == R.id.I_wert_starturlaub) {
            NumberPickerBuilder mStarturlaubPicker = new NumberPickerBuilder()
                    .setFragmentManager(fragmentManager)
                    .setStyleResId(ASetup.themePicker)
                    .setMinNumber(BigDecimal.valueOf(0))
                    .setMaxNumber(BigDecimal.valueOf(366))
                    .setPlusMinusVisibility(View.INVISIBLE)
                    .setDecimalVisibility(View.VISIBLE)
                    .setLabelText(getString(isUrlaubInStunden ? R.string.k_stunde : R.string.tage))
                    .setTargetFragment(this)
                    .setReference(R.id.I_wert_starturlaub);
            mStarturlaubPicker.show();
        } else if (id == R.id.I_wert_stundenlohn) {
            NumberPickerBuilder mStundenlohnPicker = new NumberPickerBuilder()
                    .setFragmentManager(fragmentManager)
                    .setStyleResId(ASetup.themePicker)
                    .setMinNumber(BigDecimal.valueOf(0))
                    /*.setMaxNumber(BigDecimal.valueOf(366))*/
                    .setLabelText(ASetup.sWaehrung)
                    .setPlusMinusVisibility(View.INVISIBLE)
                    .setDecimalVisibility(View.VISIBLE)
                    .setTargetFragment(this)
                    .setReference(R.id.I_wert_stundenlohn);
            mStundenlohnPicker.show();
        } else if (id == R.id.I_wert_zukunft) {
            NumberPickerBuilder mMonatePicker = new NumberPickerBuilder()
                    .setFragmentManager(fragmentManager)
                    .setStyleResId(ASetup.themePicker)
                    .setMinNumber(BigDecimal.valueOf(0))
                    .setPlusMinusVisibility(View.INVISIBLE)
                    .setDecimalVisibility(View.INVISIBLE)
                    .setTargetFragment(this)
                    .setReference(R.id.I_wert_zukunft);
            mMonatePicker.show();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if (id == R.id.I_switch_zukunft) {
            if (isChecked) {
                if (!mArbeitsplatz.isAnzeigeZukunft())
                    mArbeitsplatz.setAnzeige_Zukunft(0);
            } else {
                mArbeitsplatz.setAnzeige_Zukunft(-1);
            }
        } else if (id == R.id.I_resturlaub_noverfall) {
            mArbeitsplatz.setOption(Arbeitsplatz.OPT_RESTURLAUB_NO_VERFALL, isChecked);
        } else if (id == R.id.I_switch_urlaubstunden) {
            isUrlaubInStunden = isChecked;
            mArbeitsplatz.setUrlaubAlsStunden(isChecked);
            if (isChecked) {
                int minuten = (int) mArbeitsplatz.getSoll_Urlaub();
                wUrlaubstage.setText(new Uhrzeit(minuten)
                        .getStundenString(
                                true,
                                isDezimal));
                minuten = (int) mArbeitsplatz.getStart_Urlaub();
                wStarturlaub.setText(new Uhrzeit(minuten)
                        .getStundenString(
                                true,
                                isDezimal
                        ));
            } else {
                wUrlaubstage.setText(ASetup.tageformat.format(mArbeitsplatz.getSoll_Urlaub()));
                wStarturlaub.setText(ASetup.tageformat.format(mArbeitsplatz.getStart_Urlaub()));
            }

        } else if (id == R.id.I_switch_dezimal) {
            if (isDezimal != isChecked) {
                mArbeitsplatz.setOption(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL, isChecked);
                ASetup.mPreferenzen.edit().remove(ISetup.KEY_ANTWORT_DEZ).commit();
                isDezimal = isChecked;
                if (mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_URLAUB_ALS_STUNDEN)) {
                    int minuten = (int) mArbeitsplatz.getSoll_Urlaub();
                    wUrlaubstage.setText(new Uhrzeit(minuten)
                            .getStundenString(
                                    true,
                                    isDezimal));
                    minuten = (int) mArbeitsplatz.getStart_Urlaub();
                    wStarturlaub.setText(new Uhrzeit(minuten)
                            .getStundenString(
                                    true,
                                    isDezimal
                            ));
                } else {
                    wUrlaubstage.setText(ASetup.tageformat.format(mArbeitsplatz.getSoll_Urlaub()));
                    wStarturlaub.setText(ASetup.tageformat.format(mArbeitsplatz.getStart_Urlaub()));
                }
            }
        }

        updateView();
    }

    @Override
    public void onDialogNumberSet(int reference, final BigInteger number, final double decimal, boolean isNegative, BigDecimal fullNumber) {
        MinutenInterpretationDialog.MinutenInterpretationDialogListener mListener = null;
        if (reference == R.id.I_wert_urlaubstage) {
            if (isUrlaubInStunden) {
                mListener = z -> {
                    mArbeitsplatz.setSoll_Urlaub(z.getAlsMinuten());
                    wUrlaubstage.setText(z.getStundenString(
                            true,
                            isDezimal));
                };
            } else {
                mArbeitsplatz.setSoll_Urlaub(fullNumber.floatValue());
                wUrlaubstage.setText(ASetup.tageformat.format(mArbeitsplatz.getSoll_Urlaub()));
            }
        } else if (reference == R.id.I_wert_starturlaub) {
            if (isUrlaubInStunden) {
                mListener = z -> {
                    mArbeitsplatz.setStart_Urlaub(z.getAlsMinuten());
                    wStarturlaub.setText(z.getStundenString(
                            true,
                            isDezimal));

                };
            } else {
                mArbeitsplatz.setStart_Urlaub(fullNumber.floatValue());
                wStarturlaub.setText(ASetup.tageformat.format(mArbeitsplatz.getStart_Urlaub()));
            }
        } else if (reference == R.id.I_wert_zukunft) {
            mArbeitsplatz.setAnzeige_Zukunft(number.intValue());
            wMonateZukunft.setText(String.valueOf(mArbeitsplatz.getMonate_Zukunft()));
            //mCallback.onNeuberechnung();
        } else if (reference == R.id.I_wert_stundenlohn) {
            mArbeitsplatz.setStundenlohn(fullNumber.floatValue());
            wStundenlohn.setText(ASetup.waehrungformat.format(mArbeitsplatz.getStundenlohn()));
        }

        // wenn Stundenwerte gewählt wurden dann den Interpreterdialog öffnen
        if (mListener != null) {
            new MinutenInterpretationDialog(
                    mContext,
                    mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL),
                    /*number,*/
                    decimal,
                    fullNumber,
                    mListener
            );
        }
    }

    /*
    * Callback Interfaces
    */
    public interface InitArbeitsplatzCallbacks {
        //void onColorChanged(int farbe);
        void onSettingChaged(String schluessel, int wert);
        /*
         * Aufrufen wenn sich das Aufzeichnungsende verändert
         */
        //void onNeuberechnung();
    }
}

