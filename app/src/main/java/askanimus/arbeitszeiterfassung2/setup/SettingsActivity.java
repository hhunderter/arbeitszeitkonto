/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;

import android.app.Activity;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.OneShotPreDrawListener;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import askanimus.arbeitszeiterfassung2.MainActivity;
import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.datenbank.AUpdateDatenbank;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;


public class SettingsActivity extends AppCompatActivity implements ISetup{
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager2 mViewPager;
    private Toolbar mToolbar;
    private TabLayout mTabLayout;

    private Arbeitsplatz mArbeitsplatz = null;

    private int mSeiten;

    StorageHelper storageHelperExport;
    StorageHelper storageHelperSicherungen;

    Activity mActivity = this;

    /*
     wird zum anpassen der App Sprache benötigt, wenn diese von der Systemsprache abweicht
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(
                PreferenceManager
                        .getDefaultSharedPreferences(getApplicationContext())
                        .getBoolean(ASetup.KEY_THEMA_DUNKEL, false) ?
                        R.style.MyFullscreenTheme :
                        R.style.MyFullscreenTheme_Light
        );
        setContentView(R.layout.activity_settings);
        // Anzeigeelemente finden
        mToolbar = findViewById(R.id.S_toolbar);
        mViewPager = findViewById(R.id.S_container);
        mTabLayout = findViewById(R.id.S_tabs);
        setSupportActionBar(mToolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ASetup.init(this, this::resume);
    }

    void resume() {
        Intent intent = getIntent();
        mArbeitsplatz = ASetup.jobListe.getVonID(
                intent.getLongExtra(
                        ISetup.KEY_EDIT_JOB,
                        ASetup.mPreferenzen.getLong(
                                ISetup.KEY_EDIT_JOB,
                                ASetup.aktJob.getId()
                        )));

        if (mArbeitsplatz.isEndeAufzeichnung(ASetup.aktDatum)) {
            mSeiten = 4;
            openHinweis();
        } else
            mSeiten = 8;

        mToolbar.setTitle(ASetup.res.getString(R.string.title_activity_settings));
        mToolbar.setBackgroundColor(mArbeitsplatz.getFarbe());
        mToolbar.setTitleTextColor(mArbeitsplatz.getFarbe_Schrift_Titel());

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter =
                new SectionsPagerAdapter(getSupportFragmentManager(), getLifecycle());

        // Den Viewpager einrichten.
        mViewPager.setAdapter(mSectionsPagerAdapter);

        String[] tabTitel = ASetup.res.getStringArray(R.array.prefs_titel);
        TabLayoutMediator mTabMediator = new TabLayoutMediator(
                mTabLayout,
                mViewPager,
                true,
                true,
                (tab, position) -> tab.setText(tabTitel[position]));
        mTabMediator.attach();

        OneShotPreDrawListener.add(
                mViewPager, () -> mViewPager.setCurrentItem(
                        intent.getIntExtra(
                                ISetup.KEY_INIT_SEITE,
                                ASetup.mPreferenzen.getInt(ISetup.KEY_INIT_SEITE, 0)
                        ),
                        true
                )
        );

        /*mViewPager.setCurrentItem(
                intent.getIntExtra(
                        ISetup.KEY_INIT_SEITE,
                        ASetup.mPreferenzen.getInt(ISetup.KEY_INIT_SEITE, 0)),
                true);*/

        mTabLayout.setSelectedTabIndicatorColor(mArbeitsplatz.getFarbe());

        // alten Zustand der App wieder anzeigen, nach Rückkehr aus den Einstellungen
        ASetup.mPreferenzen.edit().putBoolean(ISetup.KEY_RESUME_VIEW, true).apply();
    }


    @Override
    public void onBackPressed() {
        // alten Zustand der App wieder anzeigen
        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
        mEdit.putBoolean(ISetup.KEY_RESUME_VIEW, true).apply();

        if (mArbeitsplatz.istGeaendert()) {
            mArbeitsplatz.schreibeJob();
            requestBackup();
        }
        // Die Monate und Jahre vom Aufzeichnungsbeginn bis heute anlegen/ aktuallisieren und
        // mit Soll, Ist, Saldo, Saldo des Vormonat vorbelegen
        if (mArbeitsplatz.istNeuberechnung()) {
            mArbeitsplatz.resetNeuberechnung();
            AUpdateDatenbank.updateDatenbank(
                    this,
                    mArbeitsplatz);
        } else {
            Intent mMainIntent = new Intent();
            mMainIntent.setClass(this, MainActivity.class);
            mMainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mMainIntent.setAction(ISetup.APP_RESET);
            finish();
            startActivity(mMainIntent);
        }
    }

    @Override
    protected void onDestroy() {
        if(isChangingConfigurations()) {
            Intent mSettingsIntent = new Intent();
            mSettingsIntent.setClass(this, this.getClass());
            mSettingsIntent.putExtra(ISetup.KEY_EDIT_JOB, mArbeitsplatz.getId());
            mSettingsIntent.putExtra(ISetup.KEY_INIT_SEITE, mViewPager.getCurrentItem());
            mSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mSettingsIntent);
        } else {
            if(mArbeitsplatz != null) {
                ASetup.mPreferenzen.edit()
                        .putLong(ISetup.KEY_EDIT_JOB, mArbeitsplatz.getId())
                        .apply();
            }
            if(mViewPager != null) {
                ASetup.mPreferenzen.edit()
                        .putInt(ISetup.KEY_INIT_SEITE, mViewPager.getCurrentItem())
                        .apply();
            }
            ASetup.zustand= ISetup.INIT_ZUSTAND_UNGELADEN; // neuladen der Einstellungen erzwingen
        }
        super.onDestroy();
    }

    /*
     * Rückmeldung vom Rechtemanagment nach Rechteanfrage
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
            requestCode = requestCode & 0x0000ffff;

            if (requestCode == REQ_DEMAND_WRITE) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (storageHelperExport != null) {
                        storageHelperExport.setPfad(storageHelperExport.getPfad());
                    }
                    if (storageHelperSicherungen != null) {
                        storageHelperSicherungen.setPfad(storageHelperSicherungen.getPfad());
                    }
                } else {
                    // Recht verweigert
                    Toast.makeText(
                            this,
                            getString(R.string.err_keine_berechtigung),
                            Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    // wird nach Auswahl eines Verzeichnisses aufgerufen
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data != null) {
            requestCode &= 0x0000ffff;
            Uri treeUri = data.getData();
            if (treeUri != null) {
                if (requestCode == REQ_FOLDER_PICKER_WRITE_EXPORT) {
                    getContentResolver().takePersistableUriPermission(
                            treeUri,
                            Intent.FLAG_GRANT_READ_URI_PERMISSION
                                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    if (storageHelperExport != null) {
                        storageHelperExport.setPfad(treeUri.toString());
                    }
                } else if (requestCode == REQ_FOLDER_PICKER_WRITE_BACKUP) {
                    getContentResolver().takePersistableUriPermission(
                            treeUri,
                            Intent.FLAG_GRANT_READ_URI_PERMISSION
                                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    if (storageHelperSicherungen != null) {
                        storageHelperSicherungen.setPfad(treeUri.toString());
                    }
                } else if (requestCode == REQ_IMAGE_READ) {
                    // Unterschriftscan kopieren
                    InputStream in;
                    OutputStream out;
                    try {
                        in = getContentResolver().openInputStream(treeUri);
                        out = getContentResolver().openOutputStream(
                                Uri.fromFile(new File(getFilesDir(), "UnterschriftAN.jpg"))
                        );

                        if (out != null && in != null) {
                            byte[] buffer = new byte[1024];
                            int read;
                            while ((read = in.read(buffer)) != -1) {
                                out.write(buffer, 0, read);
                            }
                            in.close();
                            out.flush();
                            out.close();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * A {@link FragmentStateAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    private class SectionsPagerAdapter extends FragmentStateAdapter implements
            InitAssistentFragmentAufzeichnung.InitAufzeichnungCallbacks,
            InitAssistentFragmentArbeitsplatz.InitArbeitsplatzCallbacks{

        SectionsPagerAdapter(FragmentManager fm, Lifecycle l) {
            super(fm, l);
        }


        @NonNull
        @Override
        public Fragment createFragment(int position) {
            Fragment mInhalt;

            switch (position) {
                case 0:
                    if(storageHelperExport == null) {
                        storageHelperExport = new StorageHelper(mActivity);
                    }
                    mInhalt = InitAssistentFragmentAllgemein.newInstance();
                    ((InitAssistentFragmentAllgemein) mInhalt)
                            .setUp(mArbeitsplatz, storageHelperExport);
                    break;
                case 1:
                    if(storageHelperSicherungen == null) {
                        storageHelperSicherungen = new StorageHelper(mActivity);
                    }
                    mInhalt = InitAssistentFragmentDatensicherung.newInstance();
                    ((InitAssistentFragmentDatensicherung) mInhalt)
                            .setUp(mArbeitsplatz, storageHelperSicherungen);
                    break;
                case 2:
                    mInhalt = InitAssistentFragmentArbeitsplatz.newInstance();
                    ((InitAssistentFragmentArbeitsplatz) mInhalt).setup(mArbeitsplatz, this);
                    break;
                case 3:
                    mInhalt = InitAssistentFragmentAufzeichnung.newInstance();
                    ((InitAssistentFragmentAufzeichnung) mInhalt).setup(mArbeitsplatz, this);
                    break;
                case 4:
                    mInhalt = InitAssistentFragmentZusatzeintraege.newInstance();
                    ((InitAssistentFragmentZusatzeintraege) mInhalt).setup(mArbeitsplatz);
                    break;
                case 5:
                    mInhalt = InitAssistentFragmentArbeitszeit.newInstance();
                    ((InitAssistentFragmentArbeitszeit) mInhalt).setup(mArbeitsplatz);
                    break;
                case 6:
                    mInhalt = InitAssistentFragmentSchicht.newInstance();
                    ((InitAssistentFragmentSchicht) mInhalt).setup(mArbeitsplatz);
                    break;
                default:
                    mInhalt = InitAssistentFragmentAbwesenheit.newInstance();
                    ((InitAssistentFragmentAbwesenheit) mInhalt).setup(mArbeitsplatz);
                    break;
            }
            return mInhalt;
        }

        @Override
        public int getItemCount() {
            return mSeiten;
        }

        @Override
        public void onEndeAufzeichnungChanged(Boolean isEnde) {
            if(isEnde ) {
                if(mTabLayout.getTabCount() > 4) {
                    mTabLayout.removeTabAt(6);
                    mTabLayout.removeTabAt(5);
                    mTabLayout.removeTabAt(4);
                    mTabLayout.removeTabAt(3);
                    openHinweis();
                    mSeiten = 4;
                    notifyDataSetChanged();
                }
            } else {
                if(mTabLayout.getTabCount() <= 4) {
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.zusatz_eingaben), 3, false);
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.arbeitszeit), 4, false);
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.schichten), 5, false);
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.abwesenheiten), 6, false);
                    mSeiten = 7;
                    notifyDataSetChanged();
                }
            }
        }

        @Override
        public void onSettingChaged(String schluessel, int wert){
            if (Datenbank.DB_F_FARBE.equals(schluessel)) {
                mToolbar.setBackgroundColor(wert);
                mToolbar.setTitleTextColor(mArbeitsplatz.getFarbe_Schrift_Titel());
                mTabLayout.setSelectedTabIndicatorColor(mArbeitsplatz.getFarbe());
            }
            notifyDataSetChanged();
        }
    }


    protected void openHinweis(){
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.aufzeichnung_ende))
                .setMessage(getString(R.string.dialog_aufzeichn_ende,
                        mArbeitsplatz.getEndDatum().getString_Datum(getApplicationContext())))
                .setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {
                    // nichts tun
                }).show();
    }

    // Backup im Google Konto anfordern
    public void requestBackup() {
        BackupManager bm = new BackupManager(this);
        try {
            bm.dataChanged();
        } catch (NullPointerException ne){
            ne.printStackTrace();
        }
    }
}
