/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;



import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;

import android.widget.TextView;

import com.codetroopers.betterpickers.numberpicker.NumberPickerBuilder;
import com.codetroopers.betterpickers.numberpicker.NumberPickerDialogFragment;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;
import com.codetroopers.betterpickers.timepicker.TimePickerBuilder;
import com.codetroopers.betterpickers.timepicker.TimePickerDialogFragment;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;

import askanimus.arbeitszeiterfassung2.MinutenInterpretationDialog;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitsschicht.Arbeitsschicht;
import askanimus.arbeitszeiterfassung2.einsatzort.Einsatzort;
import askanimus.arbeitszeiterfassung2.einsatzort.EinsatzortAuswahlDialog;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsschicht.SchichtDefault;
import askanimus.arbeitszeiterfassung2.Uhrzeit;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.Bereichsfeld;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;

import static androidx.recyclerview.widget.ItemTouchHelper.ACTION_STATE_DRAG;

/**
 * A placeholder fragment containing a simple view.
 */
public class InitAssistentFragmentSchicht extends Fragment implements
        View.OnClickListener,
        SwitchCompat.OnCheckedChangeListener,
        RadialTimePickerDialogFragment.OnTimeSetListener,
        TimePickerDialogFragment.TimePickerDialogHandler,
        EinsatzortAuswahlDialog.EinsatzortAuswahlDialogCallbacks,
        NumberPickerDialogFragment.NumberPickerDialogHandlerV2,
        SchichtDefinitionViewAdapter.ItemClickListener {

    private Arbeitsplatz mArbeitsplatz;

    private Context mContext;

    private TextView hTeilschicht;
    private SwitchCompat sTeilschicht;
    private TextView tSchichtliste;

    private SchichtDefinitionViewAdapter mSchichtDefinitionViewAdapter;
    private RecyclerView mSchichtListeView;


    // das Zusatzfeld, in welchen ein Picker geöffnet wurde
    private IZusatzfeld mEditZusatzwert;


    /*
     * Neue Instanz anlegen
     */
    public static InitAssistentFragmentSchicht newInstance() {
        return new InitAssistentFragmentSchicht();
    }

    protected void setup(Arbeitsplatz arbeitsplatz) {
        mArbeitsplatz = arbeitsplatz;
        if (ASetup.zustand == ISetup.INIT_ZUSTAND_GELADEN) {
            resume();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getContext();
        View view = inflater.inflate(R.layout.fragment_init_schicht, container, false);
        mSchichtListeView = view.findViewById(R.id.I_schichten_liste);
        mSchichtDefinitionViewAdapter = new SchichtDefinitionViewAdapter();
        GridLayoutManager layoutManger =
        new GridLayoutManager(
                mContext,
                1 );
        mSchichtListeView.setLayoutManager(layoutManger);
        mSchichtListeView.setAdapter(mSchichtDefinitionViewAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ASetup.init(mContext, this::resume);
    }

    private void resume() {
        View mView = getView();
        if (mArbeitsplatz != null && mView != null) {
            // Anzeigeelemente finden
            hTeilschicht = mView.findViewById(R.id.I_hint_teilschicht);
            sTeilschicht = mView.findViewById(R.id.I_switch_teilschicht);
            tSchichtliste = mView.findViewById(R.id.I_schichten_listtitel);

            ImageView bAdd = mView.findViewById(R.id.I_add_schicht);

            // Seitentitel ausblenden wenn es nicht der Initassistent ist
            if (ASetup.mPreferenzen.contains(ISetup.KEY_INIT_FINISH)) {
                TextView tTitel = mView
                        .findViewById(R.id.I_schichten_titel);
                tTitel.setVisibility(View.GONE);
            }

            // Knopffarben
            sTeilschicht.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sTeilschicht.setTrackTintList(mArbeitsplatz.getFarbe_Trak());

            // Handler und Adapter definieren
            bAdd.setOnClickListener(this);
            sTeilschicht.setOnCheckedChangeListener(this);

            // Die Schichtliste anzeigen
            setSchichtListAdapter();

            // Werte vorbelegen
            sTeilschicht.setChecked(mArbeitsplatz.isTeilschicht());

            updateView();
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private void setSchichtListAdapter() {
        mSchichtDefinitionViewAdapter.setUp(
                mContext,
                mArbeitsplatz,
                this
        );
        mSchichtDefinitionViewAdapter.notifyDataSetChanged();

        ArrayList<SchichtDefault> liste = mArbeitsplatz.getDefaultSchichten().getAktive();
        ItemTouchHelper tHelper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(
                        ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                        0) {

                    @Override
                    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
                        super.onSelectedChanged(viewHolder, actionState);
                        if (actionState == ACTION_STATE_DRAG && viewHolder != null) {
                            ((SchichtDefinitionViewAdapter.ViewHolder) viewHolder).mErweitert.setVisibility(View.GONE);
                            ((SchichtDefinitionViewAdapter.ViewHolder) viewHolder)
                                    .iArrow.setImageDrawable(ResourcesCompat.getDrawable(
                                    ASetup.res,
                                    R.drawable.arrow_down,
                                    mContext.getTheme())
                            );
                            viewHolder.itemView.setAlpha(0.5f);
                        }
                    }

                    @Override
                    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                        super.clearView(recyclerView, viewHolder);
                        viewHolder.itemView.setAlpha(1);
                    }

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder quelle, @NonNull RecyclerView.ViewHolder ziel) {

                        int fromPosition = quelle.getAdapterPosition();
                        int toPosition = ziel.getAdapterPosition();
                        int size = mSchichtDefinitionViewAdapter.getItemCount();

                        if (fromPosition < size && toPosition < size) {

                            if (fromPosition < toPosition) {
                                for (int i = fromPosition; i < toPosition; i++) {
                                    Collections.swap(liste, i, i + 1);
                                }
                            } else {
                                for (int i = fromPosition; i > toPosition; i--) {
                                    Collections.swap(liste, i, i - 1);
                                }
                            }

                            mSchichtDefinitionViewAdapter.notifyItemMoved(fromPosition, toPosition);

                            return true;
                        }
                        return false;
                    }

                    @Override
                    public void onMoved(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, int fromPos, @NonNull RecyclerView.ViewHolder target, int toPos, int x, int y) {
                        super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);

                        for (int i = 0; i < liste.size(); i++) {
                            SchichtDefault schicht = liste.get(i);
                            schicht.setPosition(i);
                        }
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                    }
                });

        tHelper.attachToRecyclerView(mSchichtListeView);
    }

    private void updateView() {
        if (sTeilschicht.isChecked()) {
            hTeilschicht.setText(R.string.hint_teilschichten);
            tSchichtliste.setText(R.string.titel_schichtliste_teil);
        } else {
            hTeilschicht.setText(R.string.hint_vollschichten);
            tSchichtliste.setText(R.string.titel_schichtliste_voll);
        }
    }

    @Override
    public void onStop() {
        if (mArbeitsplatz != null) {
            mArbeitsplatz.schreibeJob();
        }
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        int position = mArbeitsplatz.getDefaultSchichten().getSizeAktive();
        mArbeitsplatz.getDefaultSchichten().add(
                mArbeitsplatz.isTeilschicht(),
                position
        );
        mArbeitsplatz.setSchichtzahl(position +1);
        mSchichtDefinitionViewAdapter.notifyItemInserted(position);
        mSchichtDefinitionViewAdapter.openNew();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mArbeitsplatz.setIsTeilschicht(isChecked);
        updateView();
    }

    @Override
    public void onSchichtDelete(int index) {
        SchichtDefault sd = mArbeitsplatz.getDefaultSchichten().getAktive(index);
        new AlertDialog.Builder(mContext)
                .setTitle(mContext.getString(R.string.dialog_delete, sd.getName()))
                .setMessage(mContext.getString(R.string.dialog_delete_frage, sd.getName()))
                .setPositiveButton(mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                    // Zusatzfeld löschen
                    mArbeitsplatz.getDefaultSchichten().delete(index);
                    mSchichtDefinitionViewAdapter.notifyItemRemoved(index);
                    mSchichtDefinitionViewAdapter.notifyItemChanged(0);
                })
                .setNegativeButton(mContext.getString(android.R.string.cancel), (dialog, whichButton) -> {
                    // Do nothing.
                }).show();
    }

    @Override
    public void onExpand(int position) {
       mSchichtListeView.smoothScrollToPosition(position +1);
    }

    @Override
    public void onSchichtOpenPicker(int wert) {
        final InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        final FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();

        SchichtDefault sd = mSchichtDefinitionViewAdapter.getOpenItem();
        if (sd != null) {
            switch (wert) {
                case Arbeitsschicht.WERT_NAME_SCHICHT:
                    final EditText input = new EditText(mContext);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                    input.setLines(1);
                    input.setText(sd.getName());
                    input.setSelection(input.getText().length());
                    input.setFocusableInTouchMode(true);
                    input.requestFocus();
                    input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
                    if (imm != null) {
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    }

                    //Längenbegrenzung des Inputstrings
                    InputFilter[] fa = new InputFilter[1];
                    fa[0] = new InputFilter.LengthFilter(ISetup.LAENGE_NAME);
                    input.setFilters(fa);

                    new AlertDialog.Builder(mContext)
                            .setTitle(mContext.getString(R.string.bezeichnung))
                            .setView(input)
                            .setPositiveButton(mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                                sd.setName(input.getText().toString());
                                mSchichtDefinitionViewAdapter.notifyItemChanged(sd.getPosition());
                                if (imm != null) {
                                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                                }
                            }).setNegativeButton(mContext.getString(android.R.string.cancel), (dialog, whichButton) -> {
                        // Abbruchknopf gedrückt
                        if (imm != null) {
                            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                        }
                    }).show();
                    break;
                case Arbeitsschicht.WERT_VON:
                    Uhrzeit mZeitVon = new Uhrzeit(sd.getVon());
                    RadialTimePickerDialogFragment vonPickerDialog =
                            new RadialTimePickerDialogFragment()
                                    .setOnTimeSetListener(this)
                                    .setStartTime(
                                            mZeitVon.getStunden(),
                                            mZeitVon.getMinuten());
                    if (ASetup.isThemaDunkel)
                        vonPickerDialog.setThemeDark();
                    else
                        vonPickerDialog.setThemeLight();
                    vonPickerDialog.show(
                            fragmentManager,
                            String.valueOf(Arbeitsschicht.WERT_VON));
                    break;
                case Arbeitsschicht.WERT_BIS:
                    Uhrzeit mZeitBis = new Uhrzeit(sd.getBis());
                    RadialTimePickerDialogFragment bisPickerDialog =
                            new RadialTimePickerDialogFragment()
                                    .setOnTimeSetListener(this)
                                    .setStartTime(
                                            mZeitBis.getStunden(),
                                            mZeitBis.getMinuten());
                    if (ASetup.isThemaDunkel)
                        bisPickerDialog.setThemeDark();
                    else
                        bisPickerDialog.setThemeLight();
                    bisPickerDialog.show(
                            fragmentManager,
                            String.valueOf(Arbeitsschicht.WERT_BIS));
                    break;
                case Arbeitsschicht.WERT_PAUSE:
                    if (mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)) {
                        NumberPickerBuilder pausePicker = new NumberPickerBuilder()
                                .setFragmentManager(fragmentManager)
                                .setStyleResId(ASetup.themePicker)
                                .setMinNumber(BigDecimal.valueOf(0))
                                .setLabelText(getString(R.string.k_stunde))
                                .setPlusMinusVisibility(View.INVISIBLE)
                                .setDecimalVisibility(View.VISIBLE)
                                .setReference(wert)
                                .setTargetFragment(this);
                        pausePicker.show();
                    } else {
                        TimePickerBuilder tpb = new TimePickerBuilder()
                                .setFragmentManager(fragmentManager)
                                .setTargetFragment(this)
                                .setReference(wert)
                                .setStyleResId(ASetup.themePicker)
                                .addTimePickerDialogHandler(this);
                        tpb.show();
                    }
                    break;
                case Arbeitsschicht.WERT_EORT:
                    new EinsatzortAuswahlDialog(mContext, this).open();
                    break;
            }
        }
    }

    @Override
    public void onZusatzfeldOpenPicker(IZusatzfeld feld, int wert) {
        final FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
        switch (wert) {
            case Arbeitsschicht.WERT_ZUSATZ_ZAHL:
            case Arbeitsschicht.WERT_ZUSATZ_ZAHL_VON:
            case Arbeitsschicht.WERT_ZUSATZ_ZAHL_BIS:
                mEditZusatzwert = feld;
                NumberPickerBuilder nPicker = new NumberPickerBuilder()
                        .setFragmentManager(fragmentManager)
                        .setStyleResId(ASetup.themePicker)
                        .setMinNumber(BigDecimal.valueOf(0))
                        .setLabelText(feld.getEinheit())
                        .setPlusMinusVisibility(View.INVISIBLE)
                        .setDecimalVisibility(View.VISIBLE)
                        .setReference(wert)
                        .setTargetFragment(this);
                nPicker.show();
                break;
            case Arbeitsschicht.WERT_ZUSATZ_ZEIT:
                mEditZusatzwert = feld;
                if (ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)) {
                    NumberPickerBuilder zPicker = new NumberPickerBuilder()
                            .setFragmentManager(fragmentManager)
                            .setStyleResId(ASetup.themePicker)
                            .setMinNumber(BigDecimal.valueOf(0))
                            .setMaxNumber(BigDecimal.valueOf(24))
                            .setLabelText(feld.getEinheit())
                            .setPlusMinusVisibility(View.INVISIBLE)
                            .setDecimalVisibility(View.VISIBLE)
                            .setReference(wert)
                            .setTargetFragment(this);
                    zPicker.show();
                } else {
                    TimePickerBuilder zPicker = new TimePickerBuilder()
                            .setFragmentManager(fragmentManager)
                            .setTargetFragment(this)
                            .setReference(wert)
                            .setStyleResId(ASetup.themePicker)
                            .addTimePickerDialogHandler(this);
                    zPicker.show();
                }
                break;
            case Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON:
            case Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS:
                final int w = (wert == Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS) ? 1 : 0;
                mEditZusatzwert = feld;
                Uhrzeit mZeit = new Uhrzeit((int) feld.get()[w].getWert());
                RadialTimePickerDialogFragment zusatzPickerDialog =
                        new RadialTimePickerDialogFragment()
                                .setOnTimeSetListener(this)
                                .setStartTime(
                                        mZeit.getStunden(),
                                        mZeit.getMinuten());
                if (ASetup.isThemaDunkel)
                    zusatzPickerDialog.setThemeDark();
                else
                    zusatzPickerDialog.setThemeLight();
                zusatzPickerDialog.show(fragmentManager, String.valueOf(wert));
                break;
            default:
                final InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                final EditText mInput = new EditText(mContext);
                mInput.setText(feld.getStringWert(false));
                mInput.setSelection(mInput.getText().length());
                mInput.setFocusableInTouchMode(true);
                mInput.requestFocus();
                mInput.setInputType(
                        InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                                | InputType.TYPE_CLASS_TEXT
                                | InputType.TYPE_TEXT_FLAG_MULTI_LINE
                );
                //Längenbegrenzung des Inputstrings
                InputFilter[] fa = new InputFilter[1];
                fa[0] = new InputFilter.LengthFilter(ISetup.LAENGE_NOTIZ);
                mInput.setFilters(fa);
                new AlertDialog.Builder(mContext)
                        .setTitle(feld.getName())
                        .setView(mInput)
                        .setPositiveButton(mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                            if (whichButton == Dialog.BUTTON_POSITIVE) {
                                feld.setWert(mInput.getText().toString());
                                feld.save(true);
                                if (imm != null) {
                                    imm.hideSoftInputFromWindow(mInput.getWindowToken(), 0);
                                }
                                mSchichtDefinitionViewAdapter.notifyItemChanged(mSchichtDefinitionViewAdapter.getOpenPosition());
                            }
                        }).setNegativeButton(mContext.getString(android.R.string.cancel), (dialog, whichButton) -> {
                            // Abbruchknopf gedrückt
                            if (imm != null) {
                                imm.hideSoftInputFromWindow(mInput.getWindowToken(), 0);
                            }

                        }).show();
                if (imm != null) {
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                }
        }
    }

    @Override
    public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
        String dTag = dialog.getTag();
        SchichtDefault sd = mSchichtDefinitionViewAdapter.getOpenItem();
        if (dTag != null && sd != null) {
            int reference = Integer.parseInt(dTag);
            switch (reference) {
                case Arbeitsschicht.WERT_VON:
                        sd.setVon(Uhrzeit.makeMinuten(hourOfDay, minute));
                        mSchichtDefinitionViewAdapter.notifyItemChanged(sd.getPosition());
                    break;
                case Arbeitsschicht.WERT_BIS:
                        sd.setBis(Uhrzeit.makeMinuten(hourOfDay, minute));
                        mSchichtDefinitionViewAdapter.notifyItemChanged(sd.getPosition());
                    break;
                case Arbeitsschicht.WERT_PAUSE:
                        sd.setPause(Uhrzeit.makeMinuten(hourOfDay, minute));
                        mSchichtDefinitionViewAdapter.notifyItemChanged(sd.getPosition());
                    break;
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT:
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON:
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS:
                    if (mEditZusatzwert != null) {
                        int wert = (reference == Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS) ? 1 : 0;
                        mEditZusatzwert.get()[wert].setWert(Uhrzeit.makeMinuten(hourOfDay, minute));
                        if (mEditZusatzwert.getDatenTyp() == IZusatzfeld.TYP_BEREICH_ZEIT) {
                            ((Bereichsfeld) mEditZusatzwert).setNotSave();
                        }
                        mEditZusatzwert.save(true);
                        mSchichtDefinitionViewAdapter.notifyItemChanged(mSchichtDefinitionViewAdapter.getOpenPosition());
                        mEditZusatzwert = null;
                    }
            }
        }
    }


    @Override
    public void onDialogTimeSet(int reference, int hourOfDay, int minute) {
        SchichtDefault sd = mSchichtDefinitionViewAdapter.getOpenItem();
        if (sd != null) {
            switch (reference) {
                case Arbeitsschicht.WERT_PAUSE:
                        sd.setPause(Uhrzeit.makeMinuten(hourOfDay, minute));
                        mSchichtDefinitionViewAdapter.notifyItemChanged(sd.getPosition());
                    break;
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT:
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON:
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS:
                    if (mEditZusatzwert != null) {
                        int wert = (reference == Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS) ? 1 : 0;
                        mEditZusatzwert.get()[wert].setWert(Uhrzeit.makeMinuten(hourOfDay, minute));
                        if (mEditZusatzwert.getDatenTyp() == IZusatzfeld.TYP_BEREICH_ZEIT) {
                            ((Bereichsfeld) mEditZusatzwert).setNotSave();
                        }
                        mEditZusatzwert.save(true);
                        mSchichtDefinitionViewAdapter.notifyItemChanged(sd.getPosition());
                        mEditZusatzwert = null;
                    }
            }
        }
    }

    @Override
    public void onDialogNumberSet(final int reference,
                                  final BigInteger number,
                                  final double decimal,
                                  boolean isNegative,
                                  BigDecimal fullNumber) {
        MinutenInterpretationDialog.MinutenInterpretationDialogListener mListener = null;
        SchichtDefault sd = mSchichtDefinitionViewAdapter.getOpenItem();
        if (sd != null) {
            switch (reference) {
                case Arbeitsschicht.WERT_PAUSE:
                        mListener = z -> {
                            sd.setPause(z.getAlsMinuten());
                            mSchichtDefinitionViewAdapter.notifyItemChanged(sd.getPosition());
                        };
                    break;
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT:
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT_VON:
                case Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS:
                    if (mEditZusatzwert != null) {
                        final int wert = (reference == Arbeitsschicht.WERT_ZUSATZ_ZEIT_BIS) ? 1 : 0;
                        mListener = z -> {
                            mEditZusatzwert.get()[wert].setWert(z.getAlsMinuten());
                            mEditZusatzwert.save(true);
                            mSchichtDefinitionViewAdapter.notifyItemChanged(sd.getPosition());
                            mEditZusatzwert = null;
                        };
                    }
                    break;
                case Arbeitsschicht.WERT_ZUSATZ_ZAHL:
                case Arbeitsschicht.WERT_ZUSATZ_ZAHL_VON:
                case Arbeitsschicht.WERT_ZUSATZ_ZAHL_BIS:
                    if (mEditZusatzwert != null) {
                        int wert = (reference == Arbeitsschicht.WERT_ZUSATZ_ZAHL_BIS) ? 1 : 0;
                        mEditZusatzwert.get()[wert].setWert(fullNumber.floatValue());
                        if (mEditZusatzwert.getDatenTyp() == IZusatzfeld.TYP_BEREICH_ZAHL) {
                            ((Bereichsfeld) mEditZusatzwert).setNotSave();
                        }
                        mEditZusatzwert.save(true);
                        mSchichtDefinitionViewAdapter.notifyItemChanged(sd.getPosition());
                        mEditZusatzwert = null;
                    }
                    break;
            }
        }

        if(mListener != null){
            new MinutenInterpretationDialog(
                    mContext,
                    ASetup.aktJob.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL),
                    /*number,*/
                    decimal,
                    fullNumber,
                    mListener
            );
        }
    }


    @Override
    public void onEinsatzortSet(Einsatzort eort) {
        SchichtDefault sd = mSchichtDefinitionViewAdapter.getOpenItem();
        if(sd != null) {
            if (eort == null) {
                sd.setEinsatzOrt(0);
            } else {
                sd.setEinsatzOrt(eort.getId());
            }
            mSchichtDefinitionViewAdapter.notifyItemChanged(sd.getPosition());
        }
    }
}
