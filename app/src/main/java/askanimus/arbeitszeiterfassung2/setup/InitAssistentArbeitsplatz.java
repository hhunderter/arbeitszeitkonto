/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;

import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.view.View;
import androidx.appcompat.widget.AppCompatButton;

import askanimus.arbeitszeiterfassung2.MainActivity;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.datenbank.AUpdateDatenbank;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;

public class InitAssistentArbeitsplatz extends AppCompatActivity implements View.OnClickListener,
        InitAssistentFragmentAufzeichnung.InitAufzeichnungCallbacks,
        InitAssistentFragmentArbeitsplatz.InitArbeitsplatzCallbacks{

    private int mSeite;
    private final int mMaxSeite = 5;
    private Arbeitsplatz mArbeitsplatz;

    /*
     *  Die Einstellungen und globalen Werte der App
     */

    private AppCompatButton mButtonBack;
    private AppCompatButton mButtonNext;

    /*
     wird zum anpassen der App Sprache benötigt, wenn diese von der Systemsprache abweicht
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(
                PreferenceManager
                        .getDefaultSharedPreferences(getApplicationContext())
                        .getBoolean(ASetup.KEY_THEMA_DUNKEL, false) ?
                        R.style.MyFullscreenTheme :
                        R.style.MyFullscreenTheme_Light
        );
        setContentView(R.layout.activity_init_assistent);
    }

    @Override
    public void onResume() {
        super.onResume();

        mButtonBack = findViewById(R.id.I_knopf_zurueck);
        mButtonNext = findViewById(R.id.I_knopf_vor);

        ASetup.init(this, this::resume);
    }

    private void resume(){
        Intent intent = getIntent();
        mArbeitsplatz = ASetup.jobListe.getVonID(
                intent.getLongExtra(
                        ISetup.KEY_EDIT_JOB,
                        ASetup.mPreferenzen.getLong(
                                ISetup.KEY_EDIT_JOB,
                                ASetup.aktJob.getId()
                        )));

        mButtonBack.setVisibility((mSeite > 0)?View.VISIBLE:View.INVISIBLE);
        if (mSeite < mMaxSeite)
            mButtonNext.setText(R.string.button_vor);
        else
            mButtonNext.setText(R.string.button_end);


        mButtonNext.setOnClickListener(this);
        mButtonBack.setOnClickListener(this);

        ViewCompat.setBackgroundTintList(mButtonNext, mArbeitsplatz.getFarbe_Button());
        ViewCompat.setBackgroundTintList(mButtonBack, mArbeitsplatz.getFarbe_Button());

        mButtonBack.setTextColor(mArbeitsplatz.getFarbe_Schrift_Button());
        mButtonNext.setTextColor(mArbeitsplatz.getFarbe_Schrift_Button());

        mSeite = intent.getIntExtra(
                ISetup.KEY_INIT_SEITE,
                ASetup.mPreferenzen.getInt(
                        ISetup.KEY_INIT_SEITE,
                        0)
        );

        if(mSeite > mMaxSeite)
            mSeite = 0;

        neuerInhalt();
    }

    @Override
    public void onClick(View v) {
        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
        if (v == mButtonNext) {
            mSeite++;
        } else if (v == mButtonBack) {
            mSeite--;
        }

        mButtonBack.setVisibility((mSeite > 0)?View.VISIBLE:View.INVISIBLE);
        mEdit.putInt(ISetup.KEY_INIT_SEITE, mSeite).apply();

        if (mSeite < mMaxSeite) {
            mButtonNext.setText(R.string.button_vor);
            neuerInhalt();
        } else if (mSeite == mMaxSeite) {
            mButtonNext.setText(R.string.button_end);
            neuerInhalt();
        } else {
            requestBackup();
            // diesen arbeitsplatz als aktuellen markieren
            mEdit.putLong(ISetup.KEY_JOBID, mArbeitsplatz.getId()).apply();
            // alten Zustand der App wieder anzeigen
            mEdit.putBoolean(ISetup.KEY_RESUME_VIEW, true).apply();

            // Die Monate und Jahre vom Aufzeichnungsbeginn bis heute anlegen und
            // mit Soll, Ist, Saldo, Saldo des Vormonat vorbelegen
            AUpdateDatenbank.updateDatenbank(
                    this,
                    mArbeitsplatz);
        }
    }

    @Override
    public void onBackPressed() {
        if (mSeite == 0) {
            ASetup.jobListe.remove(mArbeitsplatz.getId());
            Intent mMainIntent = new Intent();
            mMainIntent.setClass(this, MainActivity.class);
            mMainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mMainIntent);
            finish();
        } else
            onClick(mButtonBack);

    }

    @Override
    protected void onDestroy() {
        if(isChangingConfigurations()) {
            Intent mInitIntent = new Intent();
            mInitIntent.setClass(this, this.getClass());
            mInitIntent.putExtra(ISetup.KEY_EDIT_JOB, mArbeitsplatz.getId());
            mInitIntent.putExtra(ISetup.KEY_INIT_SEITE, mSeite);
            mInitIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mInitIntent);
        }else {
            ASetup.mPreferenzen.edit()
                    .putLong(ISetup.KEY_EDIT_JOB, mArbeitsplatz.getId())
                    .apply();
            ASetup.mPreferenzen.edit()
                    .putInt(ISetup.KEY_INIT_SEITE, mSeite)
                    .apply();
            ASetup.zustand= ISetup.INIT_ZUSTAND_UNGELADEN; // neuladen der Einstellungen erzwingen
        }
        super.onDestroy();
    }


    // fügt die entsprechende Seite ins Layout ein
    private void neuerInhalt(){
        Fragment mInhalt = null;

        switch (mSeite) {
            case 0:
                mInhalt = InitAssistentFragmentArbeitsplatz.newInstance();
                ((InitAssistentFragmentArbeitsplatz)mInhalt).setup(mArbeitsplatz, this);
                break;
            case 1:
                mInhalt = InitAssistentFragmentAufzeichnung.newInstance();
                ((InitAssistentFragmentAufzeichnung)mInhalt).setup(mArbeitsplatz, this );
                break;
            case 2:
                mInhalt = InitAssistentFragmentZusatzeintraege.newInstance();
                ((InitAssistentFragmentZusatzeintraege)mInhalt).setup(mArbeitsplatz);
                break;
            case 3:
                mInhalt = InitAssistentFragmentArbeitszeit.newInstance();
                ((InitAssistentFragmentArbeitszeit)mInhalt).setup(mArbeitsplatz);
                break;
            case 4:
                mInhalt = InitAssistentFragmentSchicht.newInstance();
                ((InitAssistentFragmentSchicht)mInhalt).setup(mArbeitsplatz);
                break;
            case 5:
                mInhalt = InitAssistentFragmentAbwesenheit.newInstance();
                ((InitAssistentFragmentAbwesenheit)mInhalt).setup(mArbeitsplatz);
                break;
        }
        if(mInhalt != null){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment, mInhalt)
                    .commitAllowingStateLoss();
        }

    }

    // Backup im Google Konto anfordern
    public void requestBackup() {
        BackupManager bm = new BackupManager(this);
        try {
            bm.dataChanged();
        } catch (NullPointerException ne){
            ne.printStackTrace();
        }
    }

    @Override
    public void onEndeAufzeichnungChanged(Boolean isEnde) {
        // im Initassistenten muss nicht reagiert werden,
        // hier sollen ja alle Einstellungen zugänglich sein
    }

    @Override
    public void onSettingChaged(String schluessel, int wert) {
        if (Datenbank.DB_F_FARBE.equals(schluessel)) {
        ViewCompat.setBackgroundTintList(mButtonNext, mArbeitsplatz.getFarbe_Button());
        ViewCompat.setBackgroundTintList(mButtonBack, mArbeitsplatz.getFarbe_Button());

        mButtonBack.setTextColor(mArbeitsplatz.getFarbe_Schrift_Button());
        mButtonNext.setTextColor(mArbeitsplatz.getFarbe_Schrift_Button());
        }
    }
}
