/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.codetroopers.betterpickers.numberpicker.NumberPickerBuilder;
import com.codetroopers.betterpickers.numberpicker.NumberPickerDialogFragment;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.datensicherung.AAutoBackup;

/**
 * A placeholder fragment containing a simple view.
 */
public class InitAssistentFragmentDatensicherung
        extends Fragment
        implements ISetup,
        View.OnClickListener,
            SwitchCompat.OnCheckedChangeListener,
            AdapterView.OnItemSelectedListener,
        NumberPickerDialogFragment.NumberPickerDialogHandlerV2 {
    private Context mContext;

    private Arbeitsplatz mArbeitsplatz;

    private TextView wPfad;
    private RelativeLayout cAutoSicherung;
    private TextView wSchritte;
    private Spinner sIntervall;
    private TextView wAnzahlAlte;

    private StorageHelper mStorageHelper;
    private boolean sicherungenAuto;
    private int sicherungenIntervall;
    private int sicherungenSchritte;
    private int sicherungenAnzahlAlte;
    private boolean isGeaendert = false;


    /*
     * Neue Instanz anlegen
     */
    public static InitAssistentFragmentDatensicherung newInstance() {
        return new InitAssistentFragmentDatensicherung();
    }

    public void setUp(@NonNull Arbeitsplatz arbeitsplatz, @NonNull StorageHelper storageHelper) {
        mArbeitsplatz = arbeitsplatz;
        mStorageHelper = storageHelper;

        if (ASetup.zustand == ISetup.INIT_ZUSTAND_GELADEN) {
            resume();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getContext();
        return inflater.inflate(R.layout.fragment_init_datensicherung, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ASetup.init(mContext, this::resume);
    }

    private void resume() {
        View mView = getView();
        if (mStorageHelper != null && mView != null) {
            // Anzeigeelemente suchen
            wPfad = mView.findViewById(R.id.I_sicherung_wert_datenpfad);
            SwitchCompat sAutoSicherung = mView.findViewById(R.id.I_sicherung_switch_auto);
            cAutoSicherung = mView.findViewById(R.id.I_sicherung_container_auto);
            wSchritte = mView.findViewById(R.id.I_sicherung_schritte);
            sIntervall = mView.findViewById(R.id.I_sicherung_intervall);
            wAnzahlAlte = mView.findViewById(R.id.I_sicherung_anzahl_alte);


            // Seitentitel ausblenden wenn es nicht der Initassistent ist
            if (ASetup.mPreferenzen.contains(ISetup.KEY_INIT_FINISH)) {
                TextView tTitel = mView.findViewById(R.id.I_sicherung_titel);
                tTitel.setVisibility(View.GONE);
            }

            // Switches
            sAutoSicherung.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sAutoSicherung.setTrackTintList(mArbeitsplatz.getFarbe_Trak());

            // Klickhandler registrieren
            wPfad.setOnClickListener(this);
            sAutoSicherung.setOnCheckedChangeListener(this);
            wSchritte.setOnClickListener(this);
            sIntervall.setOnItemSelectedListener(this);
            wAnzahlAlte.setOnClickListener(this);
            sicherungenIntervall = ASetup.mPreferenzen.getInt(KEY_AUTOBACKUP_INTERVAL, AUTOBACKUP_NO);
            sicherungenSchritte = ASetup.mPreferenzen.getInt(KEY_AUTOBACKUP_SCHRITTE, 0);
            sicherungenAnzahlAlte = ASetup.mPreferenzen.getInt(KEY_AUTOBACKUP_ANZAHL, 0);
            // sind autom. Sicherungen eingeschaltet?
            sicherungenAuto = (sicherungenIntervall > AUTOBACKUP_NO);
            sAutoSicherung.setChecked(sicherungenAuto);

            // den Pfad für Sicherungen prüfen
            if (mStorageHelper.getPfad() == null) {
                String ExportPfad = Environment.getExternalStorageDirectory().getAbsolutePath()
                        + File.separator
                        + getString(R.string.app_verzeichnis)
                        + File.separator
                        + getString(R.string.app_verzeichnis_backup);
                ExportPfad = ASetup.mPreferenzen.getString(ASetup.KEY_BACKUP_DIR, ExportPfad);
                mStorageHelper.setUp(
                        ExportPfad,
                        Datenbank.DB_F_BACKUP_DIR,
                        ASetup.KEY_BACKUP_DIR,
                        true,
                        REQ_FOLDER_PICKER_WRITE_BACKUP
                );
            }

            updateView();
        }
    }

    private void updateView() {
        String sicherungenPfad = mStorageHelper.getPfad();
        if (sicherungenPfad != null && !sicherungenPfad.isEmpty()) {
            wPfad.setText(mStorageHelper.getPfadSubtree());
        }

        if (sicherungenAuto) {
            cAutoSicherung.setVisibility(View.VISIBLE);
            wSchritte.setText(String.valueOf(sicherungenSchritte));
            wAnzahlAlte.setText(String.valueOf(sicherungenAnzahlAlte));
            sIntervall.setSelection(sicherungenIntervall);
        } else {
            cAutoSicherung.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        final FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
        int id = v.getId();
        if (id == R.id.I_sicherung_wert_datenpfad) {
            // die einzelnen Speicher Volumes abfragen
            mStorageHelper.waehlePfad();
        } else if (id == R.id.I_sicherung_schritte) {
            int schritte;
            int idEinheit;
            switch (sicherungenIntervall) {
                case AUTOBACKUP_STUNDEN:
                    schritte = 24;
                    idEinheit = R.string.stunden;
                    break;
                case AUTOBACKUP_TAGE:
                    schritte = 356;
                    idEinheit = R.string.tage;
                    break;
                case AUTOBACKUP_WOCHEN:
                    schritte = 52;
                    idEinheit = R.string.wochen;
                    break;
                default:
                    schritte = 12;
                    idEinheit = R.string.monate;
            }
            NumberPickerBuilder mSchrittePicker = new NumberPickerBuilder()
                    .setFragmentManager(fragmentManager)
                    .setStyleResId(ASetup.themePicker)
                    .setMinNumber(BigDecimal.valueOf(1))
                    .setMaxNumber(BigDecimal.valueOf(schritte))
                    .setPlusMinusVisibility(View.INVISIBLE)
                    .setDecimalVisibility(View.INVISIBLE)
                    .setLabelText(getString(idEinheit))
                    .setTargetFragment(this)
                    .setReference(R.id.I_sicherung_schritte);
            mSchrittePicker.show();
        } else if (id == R.id.I_sicherung_anzahl_alte) {
            NumberPickerBuilder mAltePicker = new NumberPickerBuilder()
                    .setFragmentManager(fragmentManager)
                    .setStyleResId(ASetup.themePicker)
                    .setMinNumber(BigDecimal.valueOf(1))
                    .setPlusMinusVisibility(View.INVISIBLE)
                    .setDecimalVisibility(View.INVISIBLE)
                    .setLabelText(getString(R.string.backup))
                    .setTargetFragment(this)
                    .setReference(R.id.I_sicherung_anzahl_alte);
            mAltePicker.show();
        }
    }
        
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int id = buttonView.getId();
        if (id == R.id.I_sicherung_switch_auto) {
            isGeaendert = sicherungenAuto != isChecked;
            sicherungenAuto = isChecked;
            if (sicherungenSchritte == 0) {
                sicherungenSchritte = 1;
                sicherungenIntervall = AUTOBACKUP_WOCHEN;
                sicherungenAnzahlAlte = 5;
            }
        }
        updateView();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(sicherungenIntervall != position) {
            isGeaendert = true;
            sicherungenIntervall = position;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDialogNumberSet(int reference, BigInteger number, double decimal, boolean isNegative, BigDecimal fullNumber) {
        if (reference == R.id.I_sicherung_schritte) {
            isGeaendert = sicherungenSchritte != number.intValue();
            sicherungenSchritte = number.intValue();
            wSchritte.setText(String.valueOf(sicherungenSchritte));
        } else if (reference == R.id.I_sicherung_anzahl_alte) {
            isGeaendert = sicherungenAnzahlAlte != number.intValue();
            sicherungenAnzahlAlte = number.intValue();
            wAnzahlAlte.setText(String.valueOf(sicherungenAnzahlAlte));
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mArbeitsplatz != null && isGeaendert) {
            SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
            SQLiteDatabase mDatenbank = ASetup.mDatenbank;

            ContentValues mWerte = new ContentValues();

            if (sicherungenAuto) {
                mEdit.putInt(ISetup.KEY_AUTOBACKUP_INTERVAL, sicherungenIntervall);
                mEdit.putInt(ISetup.KEY_AUTOBACKUP_SCHRITTE, sicherungenSchritte);
                mEdit.putInt(ISetup.KEY_AUTOBACKUP_ANZAHL, sicherungenAnzahlAlte);

                mWerte.put(Datenbank.DB_F_AUTOBACKUP_INTERVALL, sicherungenIntervall);
                mWerte.put(Datenbank.DB_F_AUTOBACKUP_SCHRITTE, sicherungenSchritte);
                mWerte.put(Datenbank.DB_F_AUTOBACKUP_ANZAHL, sicherungenAnzahlAlte);
            } else {
                mEdit.putInt(ISetup.KEY_AUTOBACKUP_INTERVAL, AUTOBACKUP_NO);
                mEdit.putInt(ISetup.KEY_AUTOBACKUP_SCHRITTE, 0);
                mEdit.putInt(ISetup.KEY_AUTOBACKUP_ANZAHL, 0);

                mWerte.put(Datenbank.DB_F_AUTOBACKUP_INTERVALL, AUTOBACKUP_NO);
                mWerte.put(Datenbank.DB_F_AUTOBACKUP_SCHRITTE, 0);
                mWerte.put(Datenbank.DB_F_AUTOBACKUP_ANZAHL, 0);
            }

            // Einstellungen in der Datenbank sichern
            if (!mDatenbank.isOpen())
                mDatenbank = ASetup.stundenDB.getWritableDatabase();

            mDatenbank.update(
                    Datenbank.DB_T_SETTINGS,
                    mWerte,
                    Datenbank.DB_F_ID + "=?",
                    new String[]{Long.toString(1)});

            // Einstellungen in den Preferenzen sichern
            mEdit.apply();

            // den Alarmtimer neu setzen
            AAutoBackup.init(mContext, sicherungenIntervall);
        }
    }

}
