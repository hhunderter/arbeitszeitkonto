/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import askanimus.arbeitszeiterfassung2.abwesenheiten.Abwesenheit;
import askanimus.arbeitszeiterfassung2.abwesenheiten.AbwesenheitListe;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.R;

import static androidx.recyclerview.widget.ItemTouchHelper.ACTION_STATE_DRAG;

/**
 * A placeholder fragment containing a simple view.
 */
public class InitAssistentFragmentAbwesenheit
        extends Fragment
        implements View.OnClickListener,
        AbwesenheitDefinitionViewAdapter.ItemClickListener {
    private AbwesenheitListe Abwesenheiten;
    private Context mContext;
    private Arbeitsplatz mArbeitsplatz;

    private AbwesenheitDefinitionViewAdapter mAbwesenheitDefinitionViewAdapter;
    private RecyclerView mAbwesenheitListeView;


    /*
    * Neue Instanz anlegen
    */
   public static InitAssistentFragmentAbwesenheit newInstance() {

        return new InitAssistentFragmentAbwesenheit();
    }

    protected void setup(Arbeitsplatz arbeitsplatz){
        mArbeitsplatz = arbeitsplatz;
        Abwesenheiten = mArbeitsplatz.getAbwesenheiten();
        if(ASetup.zustand ==  ISetup.INIT_ZUSTAND_GELADEN) {
            resume();
        }
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       super.onCreateView(inflater, container, savedInstanceState);

        mContext = getContext();
        View view = inflater.inflate(R.layout.fragment_init_abwesenheit, container, false);
        mAbwesenheitListeView = view.findViewById(R.id.I_abwesenheit_liste);

        mAbwesenheitDefinitionViewAdapter =
                new AbwesenheitDefinitionViewAdapter();
        GridLayoutManager layoutManger =
        new GridLayoutManager(
                getContext(),
                1 );
        mAbwesenheitListeView.setLayoutManager(layoutManger);
        mAbwesenheitListeView.setAdapter(mAbwesenheitDefinitionViewAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ASetup.init(mContext, this::resume);
    }

    private void resume(){
        View mView = getView();
        if(mArbeitsplatz != null && mView != null) {
            // Seitentitel ausblenden wenn es nicht der Initassistent ist
            if (ASetup.mPreferenzen.contains(ISetup.KEY_INIT_FINISH)) {
                TextView tTitel = mView.findViewById(R.id.I_abwesenheit_titel);
                tTitel.setVisibility(View.GONE);
            }

            if (Abwesenheiten != null) {
                ImageView bAdd = mView.findViewById(R.id.I_add_abwesenheit);
                bAdd.setOnClickListener(this);

            //mAbwesenheitListeView = mView.findViewById(R.id.I_abwesenheit_liste);
            // Die Schichtliste anzeigen
            setAbwesenheitListAdapter();
            }
        }
    }

    private void setAbwesenheitListAdapter() {
        mAbwesenheitDefinitionViewAdapter.setUp(
                mContext,
                mArbeitsplatz,
                this
        );
        mAbwesenheitDefinitionViewAdapter.notifyDataSetChanged();

        ArrayList<Abwesenheit> liste = mArbeitsplatz.getAbwesenheiten().getListeAktive();

        ItemTouchHelper tHelper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(
                        ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                        0) {

                    @Override
                    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                        if(mAbwesenheitDefinitionViewAdapter.isMenuOpen((AbwesenheitDefinitionViewAdapter.ViewHolder) viewHolder)){
                           return makeMovementFlags(0,0);
                        } else {
                            final int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                            final int swipeFlags = 0;
                            return makeMovementFlags(dragFlags, swipeFlags);
                        }
                    }


                    @Override
                    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
                        super.onSelectedChanged(viewHolder, actionState);
                        if (actionState == ACTION_STATE_DRAG && viewHolder != null) {
                            if (!mAbwesenheitDefinitionViewAdapter.isMenuOpen((AbwesenheitDefinitionViewAdapter.ViewHolder) viewHolder)) {
                                mAbwesenheitDefinitionViewAdapter.closeOpenItem();
                                viewHolder.itemView.setAlpha(0.5f);
                            }
                        }
                    }

                    @Override
                    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                        super.clearView(recyclerView, viewHolder);
                        viewHolder.itemView.setAlpha(1);
                    }

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder quelle, @NonNull RecyclerView.ViewHolder ziel) {
                        int fromPosition = quelle.getAdapterPosition();
                        int toPosition = ziel.getAdapterPosition();
                        int size = mAbwesenheitDefinitionViewAdapter.getItemCount();

                        if (fromPosition < size
                                && toPosition < size
                                && fromPosition > Abwesenheit.RUHETAG
                                && toPosition > Abwesenheit.RUHETAG) {

                            if (fromPosition < toPosition) {
                                for (int i = fromPosition; i < toPosition; i++) {
                                    Collections.swap(liste, i, i + 1);
                                }
                            } else {
                                for (int i = fromPosition; i > toPosition; i--) {
                                    Collections.swap(liste, i, i - 1);
                                }
                            }
                            mAbwesenheitDefinitionViewAdapter.notifyItemMoved(fromPosition, toPosition);
                            return true;
                        }
                        return false;
                    }

                    @Override
                    public void onMoved(
                            @NonNull RecyclerView recyclerView,
                            @NonNull RecyclerView.ViewHolder viewHolder,
                            int fromPos,
                            @NonNull RecyclerView.ViewHolder target,
                            int toPos, int x, int y) {
                        super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);

                        for (int i = 0; i < liste.size(); i++) {
                            Abwesenheit abw = liste.get(i);
                            abw.setPosition(i);
                        }
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                    }

                });
        tHelper.attachToRecyclerView(mAbwesenheitListeView);
    }


    @Override
    public void onAbwesenheitDelete(int index) {
        Abwesenheit abw = mArbeitsplatz.getAbwesenheiten().getAktive(index);
        new AlertDialog.Builder(mContext)
                .setTitle(mContext.getString(R.string.dialog_delete, abw.getName()))
                .setMessage(mContext.getString(R.string.dialog_delete_frage, abw.getName()))
                .setPositiveButton(mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                    // Zusatzfeld löschen
                    mArbeitsplatz.getAbwesenheiten().delete(index);
                    mAbwesenheitDefinitionViewAdapter.notifyItemRemoved(index);
                })
                .setNegativeButton(mContext.getString(android.R.string.cancel), (dialog, whichButton) -> {
                    // Do nothing.
                }).show();
    }

    @Override
    public void onExpand(int position) {
       mAbwesenheitListeView.smoothScrollToPosition(position +1);
    }

    @Override
    public void onClick(View v) {
        int position = mArbeitsplatz.getAbwesenheiten().sizeAktive();
        Abwesenheiten.add();
        mAbwesenheitDefinitionViewAdapter.notifyItemInserted(position);
        mAbwesenheitDefinitionViewAdapter.openNew();
    }

}
