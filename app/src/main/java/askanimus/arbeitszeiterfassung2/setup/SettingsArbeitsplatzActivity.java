/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;

import android.app.backup.BackupManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import androidx.annotation.NonNull;
import androidx.core.view.OneShotPreDrawListener;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import askanimus.arbeitszeiterfassung2.MainActivity;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.datenbank.AUpdateDatenbank;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;


public class SettingsArbeitsplatzActivity extends AppCompatActivity{
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager2 mViewPager;

    private TabLayout mTabLayout;
    private Toolbar mToolbar;
    private Arbeitsplatz mArbeitsplatz = null;
    private int mSeiten;

    /*
     wird zum anpassen der App Sprache benötigt, wenn diese von der Systemsprache abweicht
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(
                PreferenceManager
                        .getDefaultSharedPreferences(getApplicationContext())
                        .getBoolean(ASetup.KEY_THEMA_DUNKEL, false) ?
                        R.style.MyFullscreenTheme :
                        R.style.MyFullscreenTheme_Light
        );
        setContentView(R.layout.activity_settings);

        //Anzeigeelemente finden
        mToolbar = findViewById(R.id.S_toolbar);
        mViewPager = findViewById(R.id.S_container);
        mTabLayout = findViewById(R.id.S_tabs);
        setSupportActionBar(mToolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();

        ASetup.init(this, this::resume);
    }

    void resume() {
        Intent intent = getIntent();
        mArbeitsplatz = ASetup.jobListe.getVonID(
                intent.getLongExtra(
                        ISetup.KEY_EDIT_JOB,
                        ASetup.mPreferenzen.getLong(
                                ISetup.KEY_EDIT_JOB,
                                ASetup.aktJob.getId()
                        ))
        );

        if (mArbeitsplatz.isEndeAufzeichnung(ASetup.aktDatum)) {
            mSeiten = 2;
            openHinweis();
        } else
            mSeiten = 6;

        mToolbar.setBackgroundColor(mArbeitsplatz.getFarbe());
        mToolbar.setTitleTextColor(mArbeitsplatz.getFarbe_Schrift_Titel());

        // Den Adapter erzeugen
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), getLifecycle());
        mViewPager.setAdapter(mSectionsPagerAdapter);

        String[] tabTitel = ASetup.res.getStringArray(R.array.prefs_titel);
        TabLayoutMediator mTabMediator = new TabLayoutMediator(
                mTabLayout,
                mViewPager,
                true,
                true,
                (tab, position) -> tab.setText(tabTitel[position + 2]));
        mTabMediator.attach();

        OneShotPreDrawListener.add(
                mViewPager, () -> mViewPager.setCurrentItem(
                        intent.getIntExtra(
                                ISetup.KEY_INIT_SEITE,
                                0),
                        true
                )
        );

        /*mViewPager.setCurrentItem(
                intent.getIntExtra(
                        ISetup.KEY_INIT_SEITE,
                        0),
                true);*/

        mTabLayout.setSelectedTabIndicatorColor(mArbeitsplatz.getFarbe());
    }

    @Override
    public void onBackPressed() {
        // alten Zustand der App wieder anzeigen
        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
        mEdit.putBoolean(ISetup.KEY_RESUME_VIEW, true).apply();

        if (mArbeitsplatz.istGeaendert()) {
            mArbeitsplatz.schreibeJob();
            requestBackup();
        }
        // Die Monate und Jahre vom Aufzeichnungsbeginn bis heute anlegen/ aktuallisieren und
        // mit Soll, Ist, Saldo, Saldo des Vormonat vorbelegen
        if (mArbeitsplatz.istNeuberechnung()) {
            mArbeitsplatz.resetNeuberechnung();
            AUpdateDatenbank.updateDatenbank(
                    this,
                    mArbeitsplatz);
        } else {
            Intent mMainIntent = new Intent();
            mMainIntent.setClass(this, MainActivity.class);
            mMainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mMainIntent.setAction(ASetup.APP_RESET);
            startActivity(mMainIntent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        if(isChangingConfigurations()) {
            Intent mSettingsIntent = new Intent();
            mSettingsIntent.setClass(this, this.getClass());
            mSettingsIntent.putExtra(ISetup.KEY_EDIT_JOB, mArbeitsplatz.getId());
            mSettingsIntent.putExtra(ISetup.KEY_INIT_SEITE, mViewPager.getCurrentItem());
            mSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mSettingsIntent);
        } else {
            ASetup.zustand= ISetup.INIT_ZUSTAND_UNGELADEN; // neuladen der Einstellungen erzwingen
            ASetup.mPreferenzen.edit()
                    .putLong(ISetup.KEY_EDIT_JOB, mArbeitsplatz.getId())
                    .apply();
            ASetup.mPreferenzen.edit()
                    .putInt(ISetup.KEY_INIT_SEITE, mViewPager.getCurrentItem())
                    .apply();
        }
        super.onDestroy();
    }

    /**
     * A {@link FragmentStateAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStateAdapter implements
            InitAssistentFragmentAufzeichnung.InitAufzeichnungCallbacks,
            InitAssistentFragmentArbeitsplatz.InitArbeitsplatzCallbacks{

        SectionsPagerAdapter(FragmentManager fm, Lifecycle l) {
            super(fm, l);
        }

        @NonNull
        @Override
        public Fragment createFragment(int position) {
            Fragment mInhalt;

            switch (position) {
                case 0:
                    mInhalt = InitAssistentFragmentArbeitsplatz.newInstance();
                    ((InitAssistentFragmentArbeitsplatz) mInhalt).setup(mArbeitsplatz, this);
                    break;
                case 1:
                    mInhalt = InitAssistentFragmentAufzeichnung.newInstance();
                    ((InitAssistentFragmentAufzeichnung) mInhalt).setup(mArbeitsplatz, this);
                    break;
                case 2:
                    mInhalt = InitAssistentFragmentZusatzeintraege.newInstance();
                    ((InitAssistentFragmentZusatzeintraege) mInhalt).setup(mArbeitsplatz);
                    break;
                case 3:
                    mInhalt = InitAssistentFragmentArbeitszeit.newInstance();
                    ((InitAssistentFragmentArbeitszeit) mInhalt).setup(mArbeitsplatz);
                    break;
                case 4:
                    mInhalt = InitAssistentFragmentSchicht.newInstance();
                    ((InitAssistentFragmentSchicht) mInhalt).setup(mArbeitsplatz);
                    break;
                default:
                    mInhalt = InitAssistentFragmentAbwesenheit.newInstance();
                    ((InitAssistentFragmentAbwesenheit) mInhalt).setup(mArbeitsplatz);
                    break;
            }
            return mInhalt;
        }

        @Override
        public int getItemCount() {
            return mSeiten;
        }

        @Override
        public void onEndeAufzeichnungChanged(Boolean isEnde) {
            if(isEnde) {
                if(mTabLayout.getTabCount() > 2) {
                    mSeiten = 2;
                    mTabLayout.removeTabAt(5);
                    mTabLayout.removeTabAt(4);
                    mTabLayout.removeTabAt(3);
                    mTabLayout.removeTabAt(2);
                    openHinweis();
                    notifyDataSetChanged();
                }
            } else {
                if(mTabLayout.getTabCount() <= 2) {
                    mSeiten = 5;
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.zusatz_eingaben), 2, false);
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.arbeitszeit), 3, false);
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.schichten), 4, false);
                    mTabLayout.addTab(mTabLayout.newTab().setText(R.string.abwesenheiten), 5, false);
                    notifyDataSetChanged();
                }
            }
        }

        @Override
        public void onSettingChaged(String schluessel, int wert){
            if (Datenbank.DB_F_FARBE.equals(schluessel)) {
                mToolbar.setBackgroundColor(wert);
                mToolbar.setTitleTextColor(mArbeitsplatz.getFarbe_Schrift_Titel());
                mTabLayout.setSelectedTabIndicatorColor(mArbeitsplatz.getFarbe());
            }
            mSectionsPagerAdapter.notifyDataSetChanged();
        }
    }




    protected void openHinweis(){
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.aufzeichnung_ende))
                .setMessage(getString(R.string.dialog_aufzeichn_ende,
                        mArbeitsplatz.getEndDatum().getString_Datum(getBaseContext())))
                .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // nichts tun
                    }

                }).show();
    }


    // Backup im Google Konto anfordern
    public void requestBackup() {
        BackupManager bm = new BackupManager(this);
        try {
            bm.dataChanged();
        } catch (NullPointerException ne){
            ne.printStackTrace();
        }
    }
}
