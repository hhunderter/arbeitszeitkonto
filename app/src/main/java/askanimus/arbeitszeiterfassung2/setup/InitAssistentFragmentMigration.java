/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;

import static askanimus.arbeitszeiterfassung2.setup.ISetup.REQ_FOLDER_PICKER_READ;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import askanimus.arbeitszeiterfassung2.MainActivity;
import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.ArbeitsplatzListe;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank_Migrate;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank_toMigrate;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.datensicherung.Datenbank_Backup;
import askanimus.arbeitszeiterfassung2.datensicherung.Datensicherung_ViewAdapter;

/**
 * A placeholder fragment containing a simple view.
 */
public class InitAssistentFragmentMigration
        extends Fragment implements Datensicherung_ViewAdapter.ItemClickListener {

    //private final static String ARG_PFAD = "imp_pfad";

    private StorageHelper mStorageHelper = null;

    private ArrayList<String> listeSicherungen;

    private Context mContext;

    //Der Rückrufpunkt
    migrationDone mCallback;

    /*
     * Neue Instanz anlegen
     */
    public static InitAssistentFragmentMigration newInstance() {
        return new InitAssistentFragmentMigration();
    }

    public void setUp(StorageHelper storageHelper, migrationDone callback) {
        mStorageHelper = storageHelper;
        mCallback = callback;
        if (ASetup.zustand == ISetup.INIT_ZUSTAND_GELADEN) {
            resume();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getContext();
        return inflater.inflate(R.layout.fragment_init_migration, container, false);
    }


    @Override
    public void onResume() {
        super.onResume();
        ASetup.init(mContext, this::resume);
    }

    private void resume() {
        View v = getView();

        if (mStorageHelper != null && v != null) {
            RecyclerView listView = v.findViewById(R.id.DU_liste_sicherungen);

            Button buttonSuche = v.findViewById(R.id.DU_button_suche);
            ViewCompat.setBackgroundTintList(buttonSuche, ASetup.aktJob.getFarbe_Button());
            buttonSuche.setTextColor(ASetup.aktJob.getFarbe_Schrift_Button());
            buttonSuche.setOnClickListener(v1 -> mStorageHelper.waehlePfad());

            if (mStorageHelper.getPfad() == null) {
                String mPfad = Environment.getExternalStorageDirectory().toString();
                mPfad += File.separator + ASetup.res.getString(R.string.app_verzeichnis_toMigrate);
                mPfad += File.separator + getString(R.string.app_verzeichnis_backup);

                mStorageHelper.setUp(
                        mPfad,
                        null,
                        null,
                        false,
                        REQ_FOLDER_PICKER_READ/*,
                        this::setUpListe*/
                );
            }

            if (mStorageHelper.isReadable()) {
                // Die Liste der Datensicherungen erzeugen, wenn sie nicht schon angelegt wurde
                if (listeSicherungen == null) {
                    //listeSicherungen = new ArrayList<>();
                    Datensicherung_ViewAdapter myAdapter = new Datensicherung_ViewAdapter();
                    //myAdapter.setUp(listeSicherungen, this);
                    listeSicherungen = myAdapter.setUp(mContext, mStorageHelper, this, false);
                    GridLayoutManager gLayoutManager = new GridLayoutManager(
                            mContext,
                            1);
                    listView.setLayoutManager(gLayoutManager);
                    listView.setAdapter(myAdapter);
                }
            }

            AppCompatButton bWiederherstellen = v.findViewById(R.id.DU_button_restore);
            ViewCompat.setBackgroundTintList(bWiederherstellen, ASetup.aktJob.getFarbe_Button());
            bWiederherstellen.setTextColor(ASetup.aktJob.getFarbe_Schrift_Button());
        }
    }

    @Override
    public void onSicherungClick(int position, int action, View view) {
        String fileName = listeSicherungen.get(position);
        AlertDialog.Builder restorDialog = new AlertDialog.Builder(mContext);
        restorDialog.setMessage(getString(R.string.sich_frage_restore, fileName))
                .setIcon(R.mipmap.ic_launcher_foreground)
                .setTitle(R.string.sich_restore)
                .setPositiveButton(R.string.ja, (dialog, id) -> {
                    // Daten wiederherstellen
                    if (fileName.endsWith(".xml")) {
                        restoreTask(fileName);
                    } else {
                        importTask(fileName);
                    }

                })
                .setNegativeButton(R.string.nein, (dialog, id) -> {
                    // nichts tun
                });
        // den Dialog erzeugen und anzeigen
        restorDialog.create().show();

    }


    // wird nach erfolgreicher Migration aufgerufen
    public interface migrationDone {
        void onMigrationDone(Boolean status);
    }


    /*
     * Wiederherstellung der Sicherungen im Hintergrund
     */
    private void restoreTask(String pfad) {
        final ProgressDialog mDialog = new ProgressDialog(mContext);

        // Fortschritsdialog öffnen
        mDialog.setIndeterminate(true);
        mDialog.setIndeterminateDrawable(
                ResourcesCompat.getDrawable(
                        getResources(),
                        R.drawable.progress_dialog_anim,
                        mContext.getTheme()));
        mDialog.setMessage(getString(R.string.progress_restore));
        mDialog.setCancelable(false);
        mDialog.show();

        Handler mHandler = new Handler();
        new Thread(() -> {
            boolean mStatus = false;
            SQLiteDatabase mDatenbank = ASetup.mDatenbank;
            Datenbank_Backup mBackup = new Datenbank_Backup(mContext, mDatenbank, mStorageHelper);
            try {
                mBackup.restore(pfad);
                mStatus = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Erfolgsmeldung ausgeben
            final boolean fStatus = mStatus;
            mHandler.post(() -> {
                // Fortschrittsdialog schliessen
                mDialog.dismiss();
                reset(fStatus, true);
            });
        }).start();
    }

    /*
     * Import Task - importiert Sicherungen aus "Arbeitszeiterfassung"
     */
    private void importTask(String filename) {
        final ProgressDialog mDialog = new ProgressDialog(mContext);

        // Fortschritsdialog öffnen
        mDialog.setIndeterminate(true);
        mDialog.setIndeterminateDrawable(
                ResourcesCompat.getDrawable(
                        getResources(),
                        R.drawable.progress_dialog_anim,
                        Objects.requireNonNull(mContext).getTheme()));
        mDialog.setMessage(getString(R.string.mig_prog_import));
        mDialog.setCancelable(false);
        mDialog.show();

        Handler mHandler = new Handler();
        new Thread(() -> {
            boolean mStatus = false;
            SQLiteDatabase importDB = new Datenbank_toMigrate(mContext).getWritableDatabase();
            Datenbank_Migrate mMigradeDB = new Datenbank_Migrate(importDB, mStorageHelper, mContext);
            try {
                // Datenbank neu anlegen
                mMigradeDB.resetDB();

                // Einstellungen einlesen und Migration beginnen
                mMigradeDB.einlesen(filename);
                mHandler.post(() -> mDialog.setMessage(ASetup.res.getString(R.string.mig_prog_einstellungen)));
                mMigradeDB.Migrate_Einstellungen();
                mHandler.post(() -> mDialog.setMessage(ASetup.res.getString(R.string.mig_prog_arbeitsplatz)));
                mMigradeDB.Migrate_Arbeitsplatz();
                mHandler.post(() -> mDialog.setMessage(ASetup.res.getString(R.string.mig_prog_eort)));
                mMigradeDB.Migrate_Eorte();
                mHandler.post(() -> mDialog.setMessage(ASetup.res.getString(R.string.mig_prog_zeit)));
                mMigradeDB.Migrate_Zeiten();
                mMigradeDB.Delete_AlteDB();

                mStatus = true;
            } catch (Exception e) {
                e.printStackTrace();
            }


            final boolean fStatus = mStatus;
            mHandler.post(() -> {
                // Fortschrittsdialog schliessen
                mDialog.dismiss();
                reset(fStatus, false);
            });
        }).start();
    }

    @SuppressLint("Range")
    private void reset(boolean status, boolean isKonto) {
        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
        // den Initprozess als beendet markieren
        mEdit.putBoolean(ISetup.KEY_INIT_FINISH, true).apply();

        if (isKonto) {
            // Allgemeine Einstellungen wiederherstellen
            String mSQL;
            Cursor mResult;

            mSQL = "SELECT * FROM " + Datenbank.DB_T_SETTINGS +
                    " WHERE " + Datenbank.DB_F_ID + "=1 LIMIT 1 ";

            mResult = ASetup.mDatenbank.rawQuery(mSQL, null);

            if (mResult.getCount() > 0) {
                int mOptionen;
                mResult.moveToFirst();

                mEdit.putLong(ISetup.KEY_JOBID, mResult.getLong(mResult.getColumnIndex(Datenbank.DB_F_JOB))).commit();
                mEdit.putString(ISetup.KEY_USERNAME, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_USER)));
                mEdit.putString(ISetup.KEY_USERANSCHRIFT, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_ANSCHRIFT)));
                mEdit.putString(ISetup.KEY_ANZEIGE_W_KUERZEL, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_W_KUERZEL)));
                //mEdit.putString(ISetup.KEY_ANZEIGE_W_TRENNER, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_W_TRENNER)));
                mEdit.putString(ISetup.KEY_ANZEIGE_E_KUERZEL, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_E_KUERZEL)));
                mEdit.putInt(ISetup.KEY_ANZEIGE_VIEW, mResult.getInt(mResult.getColumnIndex(Datenbank.DB_F_VIEW)));
                mEdit.putString(ISetup.KEY_DATEN_DIR, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_DATEN_DIR)));
                if (mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_BACKUP_DIR)) == null)
                    mEdit.putString(ISetup.KEY_BACKUP_DIR, mResult.getString(mResult.getColumnIndex(Datenbank.DB_F_BACKUP_DIR)));

                mOptionen = mResult.getInt(mResult.getColumnIndex(Datenbank.DB_F_OPTIONEN));

                // die Option "dezimale Minutenanzeige" ist seit V 1.02.94 in die Arbeitsplatzeinstellungen gewandert
                if (mResult.getInt(mResult.getColumnIndex(Datenbank.DB_F_VERSION)) < 10294) {
                    boolean dezimal = ((mOptionen & ISetup.OPT_ANZ_DEZIMAL) != 0);
                    //ArbeitsplatzListe aListe = new ArbeitsplatzListe(null);
                    ArbeitsplatzListe aListe = ASetup.jobListe;
                    for (Arbeitsplatz a : aListe.getListe()) {
                        a.setOption(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL, dezimal);
                        a.schreibeJob();
                    }
                }
                mEdit.putBoolean(ISetup.KEY_ANZEIGE_ERW_SALDO, ((mOptionen & ISetup.OPT_ANZ_ERW_SALDO) != 0));
                mEdit.putBoolean(ISetup.KEY_ANZEIGE_UMG_SORT, ((mOptionen & ISetup.OPT_ANZ_UMG_SORT) != 0));
                mEdit.putBoolean(ISetup.KEY_THEMA_DUNKEL, ((mOptionen & ISetup.OPT_ANZ_THEMA_DUNKEL) != 0));

                mEdit.apply();

                if (!mResult.isNull(mResult.getColumnIndex(Datenbank.DB_F_SPRACHE))) {
                    String s = LocaleHelper.getLanguage(mContext);

                    switch (s) {
                        case "it":
                            LocaleHelper.setLocale(mContext.getApplicationContext(), "it", Locale.getDefault().getCountry());
                            break;
                        case "de":
                            LocaleHelper.setLocale(mContext.getApplicationContext(), "de", Locale.getDefault().getCountry());
                            break;
                        default:
                            LocaleHelper.setLocale(mContext.getApplicationContext(), "en", Locale.getDefault().getCountry());
                    }
                }

            }
            mResult.close();

            // Toast ausgeben
            Toast toast = Toast.makeText(
                    mContext,
                    status ?
                            getString(R.string.restore_toast_erfolg) :
                            getString(R.string.restore_toast_misserfolg),
                    Toast.LENGTH_LONG);
            toast.show();

            ASetup.zustand = ISetup.INIT_ZUSTAND_UNGELADEN;
            // alten Zustand der App wieder anzeigen
            mEdit.putBoolean(ISetup.KEY_RESUME_VIEW, true).apply();

            Intent mMainIntent = new Intent();
            mMainIntent.setClass(mContext, MainActivity.class);
            mMainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mMainIntent);
        } else {
            mCallback.onMigrationDone(status);
        }
    }
}
