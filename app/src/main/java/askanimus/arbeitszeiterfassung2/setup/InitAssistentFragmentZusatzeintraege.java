/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;

import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.Fragment_Dialog_Zusatzfeld_Edit;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.IZusatzfeld;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzfeldDefinition;
import askanimus.arbeitszeiterfassung2.zusatzeintrag.ZusatzfeldDefinitionViewAdapter;

import static androidx.recyclerview.widget.ItemTouchHelper.ACTION_STATE_DRAG;

/**
 * A placeholder fragment containing a simple view.
 */
public class InitAssistentFragmentZusatzeintraege extends Fragment
        implements
        SwitchCompat.OnCheckedChangeListener,
        ZusatzfeldDefinitionViewAdapter.ItemClickListener,
        Fragment_Dialog_Zusatzfeld_Edit.EditZusatzfeldDialogListener, View.OnClickListener {

    private Arbeitsplatz mArbeitsplatz;

    private Context mContext;

    // die Zusatzfelder
    private RecyclerView gZusatzwerte;
    private GridLayoutManager gLayoutManager;
    private ZusatzfeldDefinitionViewAdapter mZusatzfeldViewAdapter;

    /*
     * Neue Instanz anlegen
    */
    public static InitAssistentFragmentZusatzeintraege newInstance() {
        return new InitAssistentFragmentZusatzeintraege();
    }

    protected void setup(Arbeitsplatz arbeitsplatz) {
        mArbeitsplatz = arbeitsplatz;
        if(ASetup.zustand ==  ISetup.INIT_ZUSTAND_GELADEN) {
            resume();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getContext();
        View view = inflater.inflate(R.layout.fragment_init_zusatzfelder, container, false);
        gZusatzwerte = view.findViewById(R.id.I_liste_zusatzwerte);
        mZusatzfeldViewAdapter = new ZusatzfeldDefinitionViewAdapter();
        gLayoutManager = new GridLayoutManager(
                mContext,
                IZusatzfeld.MAX_COLUM);
        gZusatzwerte.setLayoutManager(gLayoutManager);
        gZusatzwerte.setAdapter(mZusatzfeldViewAdapter);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        ASetup.init(mContext, this::resume);
    }

    private void resume(){
        // Widgeds finden
        View mView = getView();
        if(mArbeitsplatz != null && mView != null) {
            SwitchCompat sEinsatzort = mView.findViewById(R.id.I_switch_eort);
            sEinsatzort.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sEinsatzort.setTrackTintList(mArbeitsplatz.getFarbe_Trak());

            ImageView bAdd = mView.findViewById(R.id.I_add_eintrag);

            // Handler registrieren
            sEinsatzort.setOnCheckedChangeListener(this);
            bAdd.setOnClickListener(this);

            // Seitentitel ausblenden wenn es nicht der Initassistent ist
            if (ASetup.mPreferenzen.contains(ISetup.KEY_INIT_FINISH)) {
                TextView tTitel = mView.findViewById(R.id.I_zusatzeintraege_titel);
                tTitel.setVisibility(View.GONE);
            }

            // Werte setzen
            sEinsatzort.setChecked(mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_WERT_EORT));

            // Zusatzwerte anzeigen
            setZusatzfeldViewAdapter();
        }
    }

    @Override
    public void onStop() {
        if(mArbeitsplatz != null) {
            mArbeitsplatz.schreibeJob();
        }
        super.onStop();
    }


    @SuppressLint("NotifyDataSetChanged")
    private void setZusatzfeldViewAdapter() {
        mZusatzfeldViewAdapter.setUp(mArbeitsplatz, this);
        gLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position < mArbeitsplatz.getZusatzfeldListe().size())
                    return mArbeitsplatz.getZusatzfeldListe().get(position).getColums();
                else
                    return IZusatzfeld.MAX_COLUM;
            }
        });
        mZusatzfeldViewAdapter.notifyDataSetChanged();


        //if(newTouchhelper) {
        ItemTouchHelper tHelper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(
                        ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT,
                        0) {

                    @Override
                    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
                        super.onSelectedChanged(viewHolder, actionState);
                        if (actionState == ACTION_STATE_DRAG && viewHolder != null) {
                            viewHolder.itemView.setAlpha(0.5f);
                        }
                    }

                    @Override
                    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
                        super.clearView(recyclerView, viewHolder);
                        viewHolder.itemView.setAlpha(1);
                    }


                    @Override
                    public void onMoved(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, int fromPos, @NonNull RecyclerView.ViewHolder target, int toPos, int x, int y) {
                        super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);

                        for (int i = 0; i < mArbeitsplatz.getZusatzfeldListe().size(); i++) {
                            mArbeitsplatz.getZusatzfeldListe().get(i).setPosition(i);
                            mArbeitsplatz.getZusatzfeldListe().get(i).speichern();
                        }
                    }

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder quelle, @NonNull RecyclerView.ViewHolder ziel) {

                        int fromPosition = quelle.getAdapterPosition();
                        int toPosition = ziel.getAdapterPosition();

                        if (fromPosition < mArbeitsplatz.getZusatzfeldListe().size() &&
                                toPosition < mArbeitsplatz.getZusatzfeldListe().size()) {

                            if (fromPosition < toPosition) {
                                for (int i = fromPosition; i < toPosition; i++) {
                                    Collections.swap(mArbeitsplatz.getZusatzfeldListe().getListe(), i, i + 1);
                                }
                            } else {
                                for (int i = fromPosition; i > toPosition; i--) {
                                    Collections.swap(mArbeitsplatz.getZusatzfeldListe().getListe(), i, i - 1);
                                }
                            }

                            mZusatzfeldViewAdapter.notifyItemMoved(fromPosition, toPosition);

                            return true;
                        }
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                    }
                });
        tHelper.attachToRecyclerView(gZusatzwerte);
        //}
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.I_switch_eort) {
            mArbeitsplatz.setOption(Arbeitsplatz.OPT_WERT_EORT, isChecked);
        }
    }


    @Override
    public void onClick(View v) {
        final FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
        // Dialog zum anlegen eines neuen Zusatzfeldes öffnen
        ZusatzfeldDefinition mZusatzFeld = new ZusatzfeldDefinition(
                mArbeitsplatz.getId(),
                getString(R.string.notiz),
                IZusatzfeld.TYP_TEXT,
                "",
                IZusatzfeld.NEUTRAL,
                mArbeitsplatz.getZusatzfeldListe().size(),
                IZusatzfeld.MAX_COLUM,
                mArbeitsplatz.isOptionSet(Arbeitsplatz.OPT_ANZEIGE_DEZIMAL)
        );
        // Zusatzfeld zum bearbeiten öffnen
        Fragment_Dialog_Zusatzfeld_Edit mDialog = new Fragment_Dialog_Zusatzfeld_Edit();
        mDialog.setup(mZusatzFeld, this, true);
        mDialog.show(fragmentManager, "EditZusatzfeldDialog");

    }


    @Override
    public void onZusatzWertDefinitionClick(final ZusatzfeldDefinition feld, boolean delete) {
        final FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
        if(feld != null) {
            // Dialog zum Bearbeiten öffnen oder löschen
            if (delete) {
                new AlertDialog.Builder(mContext)
                        .setTitle(mContext.getString(R.string.dialog_delete, feld.getName()))
                        .setMessage(mContext.getString(R.string.dialog_delete_frage_zusatzfeld, feld.getName()))
                        .setPositiveButton(mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                            // Zusatzfeld löschen
                            mArbeitsplatz.getZusatzfeldListe().deleteFeld(feld);
                            mZusatzfeldViewAdapter.notifyItemRemoved(feld.getPosition());
                        })
                        .setNegativeButton(mContext.getString(android.R.string.cancel), (dialog, whichButton) -> {
                            // Do nothing.
                        }).show();
            } else {
                // Zusatzfeld bearbeiten
                Fragment_Dialog_Zusatzfeld_Edit mDialog = new Fragment_Dialog_Zusatzfeld_Edit();
                mDialog.setup(feld, this, false);
                mDialog.show(fragmentManager, "EditZusatzfeldDialog");
            }
        }
    }

    @Override
    public void onEditZusatzwertPositiveClick(ZusatzfeldDefinition zusatzFeld, boolean isNeu) {
        if (isNeu) {
            mArbeitsplatz.getZusatzfeldListe().add(zusatzFeld);
            mZusatzfeldViewAdapter.notifyItemInserted(mArbeitsplatz.getZusatzfeldListe().size()-1);
        } else {
            mZusatzfeldViewAdapter.notifyItemChanged(zusatzFeld.getPosition());
        }
    }

    @Override
    public void onEditZusatzwertNegativeClick() {
        // tut nichts
    }
}

