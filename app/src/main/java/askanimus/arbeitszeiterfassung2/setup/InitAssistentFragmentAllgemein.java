/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2.setup;

import static askanimus.arbeitszeiterfassung2.setup.ISetup.REQ_FOLDER_PICKER_WRITE_EXPORT;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.database.sqlite.SQLiteDatabase;
import androidx.annotation.NonNull;

import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.widget.CompoundButtonCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.SwitchCompat;

import android.os.Environment;
import android.text.InputFilter;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.util.Locale;
import java.util.Objects;

import askanimus.arbeitszeiterfassung2.StorageHelper;
import askanimus.arbeitszeiterfassung2.arbeitsplatz.Arbeitsplatz;
import askanimus.arbeitszeiterfassung2.BuildConfig;
import askanimus.arbeitszeiterfassung2.R;
import askanimus.arbeitszeiterfassung2.datenbank.Datenbank;

/**
 * A placeholder fragment containing a simple view.
 */
public class InitAssistentFragmentAllgemein
        extends Fragment
        implements View.OnClickListener,
            SwitchCompat.OnCheckedChangeListener,
            RadioGroup.OnCheckedChangeListener,
            AdapterView.OnItemSelectedListener {
    private Arbeitsplatz mArbeitsplatz;
    private boolean isGeaendert = false;

    private TextView wName;
    private TextView wAnschrift;
    private TextView wDatenpfad;
    private TextView wWaehrung;
    //private TextView wDezimaltrenner;
    private TextView wEinheitEntfernung;
    private SwitchCompat sSaldoErweitert;
    private TextView hSaldoErweitert;
    private SwitchCompat sSortUmgekehrt;
    private TextView hSortUmgekehrt;
    private SwitchCompat sViewAktuellerTag;
    private TextView hViewAktuellerTag;
    private SwitchCompat sThemaDunkel;
    private TextView hThemaDunkel;
    private ImageView iUnterschrift;
    private ImageView iLoescheUnterschrift;

    private int mOptionen;
    private int mAnzeige;
    private int mSprache = -1;
    private Context mContext;
    String ExportPfad = "";
    private StorageHelper mStorageHelper;

    /*
     * Neue Instanz anlegen
     */
    public static InitAssistentFragmentAllgemein newInstance() {
        return new InitAssistentFragmentAllgemein();
    }

    public void setUp(@NonNull Arbeitsplatz arbeitsplatz, @NonNull StorageHelper storageHelper) {
        mArbeitsplatz = arbeitsplatz;
        mStorageHelper = storageHelper;

        if(ASetup.zustand == ISetup.INIT_ZUSTAND_GELADEN) {
            resume();
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getContext();
        return inflater.inflate(R.layout.fragment_init_allgemein, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        ASetup.init(mContext, this::resume);
    }

    private void resume() {
        View mView = getView();
        if (mStorageHelper != null && mView != null) {
            // Anzeigeelemente suchen
            wName = mView.findViewById(R.id.I_allgemein_wert_name);
            Spinner sSprache = mView.findViewById(R.id.I_allgemein_spinner_sprache);
            wAnschrift = mView.findViewById(R.id.I_allgemein_anschrift);
            wDatenpfad = mView.findViewById(R.id.I_allgemein_wert_datenpfad);
            wWaehrung = mView.findViewById(R.id.I_wert_waehrung);
            //wDezimaltrenner = mView.findViewById(R.id.I_wert_trenner);
            wEinheitEntfernung = mView.findViewById(R.id.I_wert_entfernung);
            sSaldoErweitert = mView.findViewById(R.id.I_switch_erwsaldo);
            hSaldoErweitert = mView.findViewById(R.id.I_hint_erwsaldo);
            sSortUmgekehrt = mView.findViewById(R.id.I_switch_sort);
            hSortUmgekehrt = mView.findViewById(R.id.I_hint_sort);
            sViewAktuellerTag = mView.findViewById(R.id.I_switch_akttagview);
            hViewAktuellerTag = mView.findViewById(R.id.I_hint_akttagview);
            RadioGroup rgAnsicht = mView.findViewById(R.id.I_allgemein_gruppe_ansicht);
            sThemaDunkel = mView.findViewById(R.id.I_switch_thema);
            hThemaDunkel = mView.findViewById(R.id.I_hint_thema);
            iUnterschrift = mView.findViewById(R.id.I_allgemein_image_unterschrift);
            iLoescheUnterschrift = mView.findViewById(R.id.I_allgemein_loesche_unterschrift);

            // Seitentitel ausblenden wenn es nicht der Initassistent ist
            if (ASetup.mPreferenzen.contains(ISetup.KEY_INIT_FINISH)) {
                TextView tTitel = mView.findViewById(R.id.I_allgemein_titel);
                tTitel.setVisibility(View.GONE);
            }
            // Farbgebung der Knöpfe anpassen
            // Radiobutton
            for (View v : rgAnsicht.getTouchables()) {
                CompoundButtonCompat.setButtonTintList(
                        ((AppCompatRadioButton) v),
                        mArbeitsplatz.getFarbe_Radio());
            }

            // Switches
            sSaldoErweitert.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sSaldoErweitert.setTrackTintList(mArbeitsplatz.getFarbe_Trak());
            sSortUmgekehrt.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sSortUmgekehrt.setTrackTintList(mArbeitsplatz.getFarbe_Trak());
            sViewAktuellerTag.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sViewAktuellerTag.setTrackTintList(mArbeitsplatz.getFarbe_Trak());
            sThemaDunkel.setThumbTintList(mArbeitsplatz.getFarbe_Thumb());
            sThemaDunkel.setTrackTintList(mArbeitsplatz.getFarbe_Trak());

            // Klickhandler registrieren
            sSprache.setOnItemSelectedListener(this);
            sSaldoErweitert.setOnCheckedChangeListener(this);
            sViewAktuellerTag.setOnCheckedChangeListener(this);
            sSortUmgekehrt.setOnCheckedChangeListener(this);
            sThemaDunkel.setOnCheckedChangeListener(this);
            wName.setOnClickListener(this);
            wAnschrift.setOnClickListener(this);
            wWaehrung.setOnClickListener(this);
            //wDezimaltrenner.setOnClickListener(this);
            wEinheitEntfernung.setOnClickListener(this);
            rgAnsicht.setOnCheckedChangeListener(this);

            wDatenpfad.setOnClickListener(this);

            iUnterschrift.setOnClickListener(this);

            // wenn die App zum ersten Mal gestartet wird existieren noch keine Einstellungen
            // also auch wenn die Defaulteinstellungen nicht geändert wurden, diese in die Datenbank schreiben
            if(!ASetup.mPreferenzen.contains(ISetup.KEY_USERNAME)){
                isGeaendert = true;
            }

            // gespeicherte Einstellungen lesen
            wName.setText(ASetup.mPreferenzen.getString(ISetup.KEY_USERNAME, getString(R.string.default_user)));
            wAnschrift.setText(ASetup.mPreferenzen.getString(ISetup.KEY_USERANSCHRIFT, ""));
            wWaehrung.setText(ASetup.mPreferenzen.getString(ISetup.KEY_ANZEIGE_W_KUERZEL, ASetup.sWaehrung));
            //wDezimaltrenner.setText(ASetup.mPreferenzen.getString(ISetup.KEY_ANZEIGE_W_TRENNER, ASetup.dTrenner));
            wEinheitEntfernung.setText(ASetup.mPreferenzen.getString(ISetup.KEY_ANZEIGE_E_KUERZEL, ASetup.sEntfernung));
            mAnzeige = ASetup.mPreferenzen.getInt(ISetup.KEY_ANZEIGE_VIEW, ISetup.VIEW_TAG);

            String s = LocaleHelper.getLanguage(getActivity());
            switch (s) {
                case "it":
                    mSprache = ISetup.OPT_SPRACHE_IT;
                    break;
                case "de":
                    mSprache = ISetup.OPT_SPRACHE_DE;
                    break;
                default:
                    mSprache = ISetup.OPT_SPRACHE_EN;
            }
            mSprache = ASetup.mPreferenzen.getInt(ISetup.KEY_SPRACHE, mSprache);

            mOptionen = 0;
            if(ASetup.mPreferenzen.getBoolean(ISetup.KEY_ANZEIGE_ERW_SALDO, true)){
                mOptionen |= ISetup.OPT_ANZ_ERW_SALDO;
            }
            if(ASetup.mPreferenzen.getBoolean(ISetup.KEY_ANZEIGE_UMG_SORT, false)){
                mOptionen |= ISetup.OPT_ANZ_UMG_SORT;
            }
            if(ASetup.mPreferenzen.getBoolean(ISetup.KEY_ANZEIGE_AKTTAG, true)){
                mOptionen |= ISetup.OPT_ANZ_AKTTAG;
            }
            if(ASetup.mPreferenzen.getBoolean(ISetup.KEY_THEMA_DUNKEL, false)){
                mOptionen |= ISetup.OPT_ANZ_THEMA_DUNKEL;
            }

            if(mStorageHelper.getPfad() == null) {
                String ExportPfad = Environment.getExternalStorageDirectory().getAbsolutePath()
                        + File.separator
                        + ASetup.res.getString(R.string.app_verzeichnis);
                ExportPfad = ASetup.mPreferenzen.getString(ASetup.KEY_DATEN_DIR, ExportPfad);
                mStorageHelper.setUp(
                        ExportPfad,
                        Datenbank.DB_F_DATEN_DIR,
                        ASetup.KEY_DATEN_DIR,
                        true,
                        REQ_FOLDER_PICKER_WRITE_EXPORT
                );
            }

            // Die Auswahl der bevorzugten Ansicht
            switch (mAnzeige) {
                case ISetup.VIEW_WOCHE:
                    //mButton = (AppCompatRadioButton) mView.findViewById(R.id.I_allgemein_button_woche);
                    rgAnsicht.check(R.id.I_allgemein_button_woche);
                    break;
                case ISetup.VIEW_MONAT:
                    //mButton = (AppCompatRadioButton) mView.findViewById(R.id.I_allgemein_button_monat);
                    rgAnsicht.check(R.id.I_allgemein_button_monat);
                    break;
                case ISetup.VIEW_JAHR:
                    //mButton = (AppCompatRadioButton) mView.findViewById(R.id.I_allgemein_button_jahr);
                    rgAnsicht.check(R.id.I_allgemein_button_jahr);
                    break;
                case ISetup.VIEW_JOB:
                    //mButton = (AppCompatRadioButton) mView.findViewById(R.id.I_allgemein_button_job);
                    rgAnsicht.check(R.id.I_allgemein_button_job);
                    break;
                case ISetup.VIEW_LETZTER:
                    //mButton = (AppCompatRadioButton) mView.findViewById(R.id.I_allgemein_button_letzte);
                    rgAnsicht.check(R.id.I_allgemein_button_letzte);
                    break;
                default:
                    //mButton = (AppCompatRadioButton) mView.findViewById(R.id.I_allgemein_button_tag);
                    rgAnsicht.check(R.id.I_allgemein_button_tag);
            }

            // die einzelnen Anzeigeotionen
            sSaldoErweitert.setChecked((mOptionen & ISetup.OPT_ANZ_ERW_SALDO) != 0);
            sSortUmgekehrt.setChecked((mOptionen & ISetup.OPT_ANZ_UMG_SORT) != 0);
            sViewAktuellerTag.setChecked((mOptionen & ISetup.OPT_ANZ_AKTTAG) != 0);
            sThemaDunkel.setChecked((mOptionen & ISetup.OPT_ANZ_THEMA_DUNKEL) != 0);

            // die App Sprache
            sSprache.setSelection(mSprache);

            // Hinweis, dass nach der Umstellung der Sprache noch einige Dinge per Hand umgestellt werden müssen
            if (ASetup.mPreferenzen.contains(ISetup.KEY_INIT_FINISH) &&
                    ASetup.mPreferenzen.getBoolean(ISetup.KEY_OPT_SPRACHE_CHANGE, false)) {
                ASetup.mPreferenzen.edit().putBoolean(ISetup.KEY_OPT_SPRACHE_CHANGE, false).apply();


                AlertDialog.Builder hinweisDialog = new AlertDialog.Builder(requireActivity());
                hinweisDialog.setMessage(getString(R.string.language_change))
                        .setIcon(R.mipmap.ic_launcher_foreground)
                        .setTitle(R.string.hinweis)
                        .setNeutralButton(android.R.string.ok, (dialog, id1) -> {
                            // nichts tun
                        });
                // den Dialog erzeugen und anzeigen
                hinweisDialog.create().show();

                // die Sprache wurde geändert aber diese Änderung noch nicht gespeichert
                isGeaendert = true;
            }
            updateView();
        }
    }

    private void updateView() {
        if(mStorageHelper != null) {
            ExportPfad = mStorageHelper.getPfad();
            if (ExportPfad != null && !ExportPfad.isEmpty()) {
                wDatenpfad.setText(mStorageHelper.getPfadSubtree());
            }
        }

        hSaldoErweitert.setText(
                (sSaldoErweitert.isChecked()
                        ? getString(R.string.hint_erweitert_on)
                        : getString(R.string.hint_erweitert_off))
        );

        hSortUmgekehrt.setText(
                (sSortUmgekehrt.isChecked()
                        ? getString(R.string.taglist_sort_on)
                        : getString(R.string.taglist_sort_off))
        );

        hViewAktuellerTag.setText(
                (sViewAktuellerTag.isChecked() ?
                        getString(R.string.hint_akttag_erweitert_on) :
                        getString(R.string.hint_akttag_erweitert_off))
        );

        hThemaDunkel.setText(
                (sThemaDunkel.isChecked()
                        ? getString(R.string.hint_theme_dark_on)
                        : getString(R.string.hint_theme_dark_off))
        );

        File iFile = new File(mContext.getFilesDir(), "UnterschriftAN.jpg");
        if(iFile.exists()){
            iUnterschrift.setImageDrawable(Drawable.createFromPath(iFile.toString()));
            iLoescheUnterschrift.setVisibility(View.VISIBLE);
            iLoescheUnterschrift.setOnClickListener(this);
        } else {
            iUnterschrift.setImageDrawable(
                    AppCompatResources.getDrawable(mContext, R.drawable.unterschrift_leer)
            );
            iLoescheUnterschrift.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        final InputMethodManager imm = (InputMethodManager) mContext.getSystemService(android.content.Context.INPUT_METHOD_SERVICE);
        final AlertDialog.Builder mDialog = new AlertDialog.Builder(mContext);
        final EditText mInput = new EditText(getActivity());
        boolean mOpen = false;

        mInput.setInputType(InputType.TYPE_CLASS_TEXT);

        int id = v.getId();
        if (id == R.id.I_wert_waehrung) {
            mOpen = true;
            mInput.setText(wWaehrung.getText());
            mInput.setSelection(mInput.getText().length());
            mInput.setMaxLines(1);
            mInput.setFocusableInTouchMode(true);
            mInput.requestFocus();
            mInput.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
            //Längenbegrenzung des Inputstrings
            InputFilter[] fw = new InputFilter[1];
            fw[0] = new InputFilter.LengthFilter(ISetup.LAENGE_WAEHRUNG_KURZ);
            mInput.setFilters(fw);
            mDialog.setTitle(R.string.titel_waehrung_kurz);
        } /*else if (id == R.id.I_wert_trenner) {
            mOpen = true;
            mInput.setText(wDezimaltrenner.getText());
            mInput.setSelection(mInput.getText().length());
            mInput.setMaxLines(1);
            mInput.setFocusableInTouchMode(true);
            mInput.requestFocus();
            //Längenbegrenzung des Inputstrings
            InputFilter[] ft = new InputFilter[1];
            ft[0] = new InputFilter.LengthFilter(1);
            mInput.setFilters(ft);
            //mInput.setSelection(0, mInput.getText().length());
            mDialog.setTitle(R.string.titel_trenner);
        }*/ else if (id == R.id.I_wert_entfernung) {
            mOpen = true;
            mInput.setText(wEinheitEntfernung.getText());
            mInput.setSelection(mInput.getText().length());
            mInput.setMaxLines(1);
            mInput.setFocusableInTouchMode(true);
            mInput.requestFocus();
            //Längenbegrenzung des Inputstrings
            InputFilter[] fe = new InputFilter[1];
            fe[0] = new InputFilter.LengthFilter(ISetup.LAENGE_WAEHRUNG_KURZ);
            mInput.setFilters(fe);
            //mInput.setSelection(0, mInput.getText().length());
            mDialog.setTitle(R.string.titel_entfernung);
        } else if (id == R.id.I_allgemein_wert_name) {
            mOpen = true;
            mInput.setText(wName.getText());
            mInput.setSelection(mInput.getText().length());
            mInput.setMaxLines(1);
            mInput.setFocusableInTouchMode(true);
            mInput.requestFocus();
            mInput.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            //Längenbegrenzung des Inputstrings
            InputFilter[] fn = new InputFilter[1];
            fn[0] = new InputFilter.LengthFilter(ISetup.LAENGE_NAME);
            mInput.setFilters(fn);
            //mInput.setSelection(0, mInput.getText().length());
            mDialog.setTitle(R.string.name_user);
        } else if (id == R.id.I_allgemein_anschrift) {
            mOpen = true;
            mInput.setText(wAnschrift.getText());
            mInput.setSelection(mInput.getText().length());
            mInput.setFocusableInTouchMode(true);
            mInput.requestFocus();
            mInput.setInputType(
                    InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                    | InputType.TYPE_CLASS_TEXT
                    | InputType.TYPE_TEXT_FLAG_MULTI_LINE
                    | InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS
            );
            //Längenbegrenzung des Inputstrings
            InputFilter[] fa = new InputFilter[1];
            fa[0] = new InputFilter.LengthFilter(ISetup.LAENGE_ANSCHRIFT);
            mInput.setFilters(fa);
            mInput.setSelection(mInput.getText().length());
            mDialog.setTitle(R.string.anschrift);
        } else if (id == R.id.I_allgemein_wert_datenpfad) {
            isGeaendert = true;
            // einen neuen Ordner für das Datenverzeichnis suchen/anlegen
            mStorageHelper.waehlePfad();
        } else if(id == R.id.I_allgemein_image_unterschrift){
            // Bild mit eingescanter Unterschrift suchen
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.setType("image/jpeg");
            Activity activity = getActivity();
            if(activity != null) {
                activity.startActivityForResult(intent, ISetup.REQ_IMAGE_READ);
            }
        } else if(id == R.id.I_allgemein_loesche_unterschrift) {
            File iFile = new File(mContext.getFilesDir(), "UnterschriftAN.jpg");
            if (iFile.exists()) {
                String s = mContext.getString(R.string.unterschrift_an);
                new AlertDialog.Builder(mContext)
                        .setTitle(mContext.getString(R.string.dialog_delete, s))
                        .setMessage(mContext.getString(R.string.dialog_delete_frage, s))
                        .setPositiveButton(mContext.getString(android.R.string.ok), (dialog, whichButton) -> {
                            // Unterschrift löschen
                            if(iFile.delete()){
                                updateView();
                            }
                        })
                        .setNegativeButton(mContext.getString(android.R.string.cancel), (dialog, whichButton) -> {
                            // Do nothing.
                        }).show();
            }
        }


        // Dialog öffnen
        if (mOpen) {
            final TextView mView = (TextView) v;
            if (imm != null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
            mDialog.setView(mInput);
            mDialog.setPositiveButton(getString(android.R.string.ok), (dialog, whichButton) -> {
                isGeaendert = !mInput.getText().equals(mView.getText());
                if(isGeaendert){
                    mView.setText(mInput.getText());
                }
                Objects.requireNonNull(imm).hideSoftInputFromWindow(mInput.getWindowToken(), 0);
            });
            mDialog.setNegativeButton(getString(android.R.string.cancel), (dialog, whichButton) -> {
                // Abbruchknopf gedrückt
                Objects.requireNonNull(imm).hideSoftInputFromWindow(mInput.getWindowToken(), 0);

            });
            mDialog.show();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int mSchalter = 0;
        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
        int id = buttonView.getId();
        if (id == R.id.I_switch_erwsaldo) {
            mSchalter = ISetup.OPT_ANZ_ERW_SALDO;
            mEdit.putBoolean(ISetup.KEY_ANZEIGE_ERW_SALDO, isChecked);
        } else if (id == R.id.I_switch_sort) {
            mSchalter = ISetup.OPT_ANZ_UMG_SORT;
            //hSortUmgekehrt.setText((isChecked ? getString(R.string.taglist_sort_on) : getString(R.string.taglist_sort_off)));
            mEdit.putBoolean(ISetup.KEY_ANZEIGE_UMG_SORT, isChecked);
        } else if (id == R.id.I_switch_akttagview) {
            mSchalter = ISetup.OPT_ANZ_AKTTAG;
            mEdit.putBoolean(ISetup.KEY_ANZEIGE_AKTTAG, isChecked);
        } else if (id == R.id.I_switch_thema) {
            if (ASetup.isThemaDunkel != isChecked) {
                isGeaendert = true;
                mSchalter = ISetup.OPT_ANZ_THEMA_DUNKEL;
                mEdit.putBoolean(ISetup.KEY_THEMA_DUNKEL, isChecked).apply();
                ASetup.isThemaDunkel = isChecked;
                ASetup.setFarben();
                mArbeitsplatz.setFarbe(mArbeitsplatz.getFarbe());
                ASetup.mPreferenzen.edit()
                        .putLong(ISetup.KEY_EDIT_JOB, mArbeitsplatz.getId())
                        .apply();
                restart();
            }
        }
        mEdit.apply();

        int optionenAlt = mOptionen;
        mOptionen = isChecked ? mOptionen | mSchalter : mOptionen & ~mSchalter;
        isGeaendert = optionenAlt != mOptionen;

        updateView();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int anzeigeAlte = mAnzeige;
        if (group.getId() == R.id.I_allgemein_gruppe_ansicht) {
            if (checkedId == R.id.I_allgemein_button_tag) {
                mAnzeige = ISetup.VIEW_TAG;
            } else if (checkedId == R.id.I_allgemein_button_woche) {
                mAnzeige = ISetup.VIEW_WOCHE;
            } else if (checkedId == R.id.I_allgemein_button_monat) {
                mAnzeige = ISetup.VIEW_MONAT;
            } else if (checkedId == R.id.I_allgemein_button_jahr) {
                mAnzeige = ISetup.VIEW_JAHR;
            } else if (checkedId == R.id.I_allgemein_button_job) {
                mAnzeige = ISetup.VIEW_JOB;
            } else if (checkedId == R.id.I_allgemein_button_letzte) {
                mAnzeige = ISetup.VIEW_LETZTER;
            }
        }
        if(anzeigeAlte != mAnzeige) {
            // wenn Sich die bevorzugte Ansicht geändert hat, dann nicht in die alte Ansicht,
            // sondern in die neue bevorzugte Ansicht zurück springen
            ASetup.mPreferenzen.edit().putBoolean(ISetup.KEY_RESUME_VIEW, false).apply();

            isGeaendert = true;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
        if (mSprache != position) {
            mSprache = position;
            switch (mSprache) {
                case ISetup.OPT_SPRACHE_DE:
                    LocaleHelper.setLocale(mContext, "de", Locale.getDefault().getCountry());
                    break;
                case ISetup.OPT_SPRACHE_IT:
                    LocaleHelper.setLocale(mContext, "it", Locale.getDefault().getCountry());
                    break;
                default:
                    LocaleHelper.setLocale(mContext, "en", Locale.getDefault().getCountry());
            }
            mEdit.putInt(ISetup.KEY_SPRACHE, mSprache).apply();
            mEdit.putBoolean(ISetup.KEY_OPT_SPRACHE_CHANGE, true).apply();
            restart();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    @Override
    public void onStop() {
        super.onStop();
        if (isGeaendert && mArbeitsplatz != null && wName != null) {
            SQLiteDatabase mDatenbank = ASetup.mDatenbank;/*stundenDB.getWritableDatabase();*/

            ContentValues mWerte = new ContentValues();

            mWerte.put(Datenbank.DB_F_JOB, ASetup.aktJob.getId());
            mWerte.put(Datenbank.DB_F_SPRACHE, mSprache);
            mWerte.put(Datenbank.DB_F_USER, wName.getText().toString());
            mWerte.put(Datenbank.DB_F_ANSCHRIFT, wAnschrift.getText().toString());
            mWerte.put(Datenbank.DB_F_W_KUERZEL, wWaehrung.getText().toString());
            //mWerte.put(Datenbank.DB_F_W_TRENNER, wDezimaltrenner.getText().toString());
            mWerte.put(Datenbank.DB_F_E_KUERZEL, wEinheitEntfernung.getText().toString());
            mWerte.put(Datenbank.DB_F_OPTIONEN, mOptionen);
            mWerte.put(Datenbank.DB_F_VIEW, mAnzeige);
            //mWerte.put(Datenbank.DB_F_DATEN_DIR, mStorageHelper.getPfad()/*ExportPfad*/);
            mWerte.put(Datenbank.DB_F_VERSION, BuildConfig.VERSION_CODE);


            if (!mDatenbank.isOpen())
                mDatenbank = ASetup.stundenDB.getWritableDatabase();

            mDatenbank.update(
                    Datenbank.DB_T_SETTINGS,
                    mWerte,
                    Datenbank.DB_F_ID + "=?",
                    new String[]{Long.toString(1)}
            );

            SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();

            mEdit.putString(ISetup.KEY_USERNAME, wName.getText().toString());
            mEdit.putString(ISetup.KEY_USERANSCHRIFT, wAnschrift.getText().toString());
            mEdit.putString(ISetup.KEY_ANZEIGE_W_KUERZEL, wWaehrung.getText().toString());
            //mEdit.putString(ISetup.KEY_ANZEIGE_W_TRENNER, wDezimaltrenner.getText().toString());
            mEdit.putString(ISetup.KEY_ANZEIGE_E_KUERZEL, wEinheitEntfernung.getText().toString());
            mEdit.putInt(ISetup.KEY_ANZEIGE_VIEW, mAnzeige);
            mEdit.putBoolean(ISetup.KEY_THEMA_DUNKEL, sThemaDunkel.isChecked());
            mEdit.putInt(ISetup.KEY_SPRACHE, mSprache);

            mEdit.apply();
        }
    }


    // Einstellungen neu starten wenn sich die Sprache geändert hat
    private void restart() {
        Intent mSettingsIntent = new Intent();
        if (ASetup.mPreferenzen.contains(ASetup.KEY_INIT_FINISH)) {
            // die Ersteinrichtung wurde beendet, der Arbeitsplatz bleibt bestehen
            // einige Texte müssen dadurch vom User angepasst/übersetzt werden
            mSettingsIntent.setClass(mContext, SettingsActivity.class);
            mSettingsIntent.putExtra(ISetup.KEY_EDIT_JOB, mArbeitsplatz.getId());
            mSettingsIntent.putExtra(ISetup.KEY_INIT_SEITE, 0);
        } else {
            // es ist die Ersteinrichtung, der momentane Arbeitsplatz wird gelöscht und neu angelegt
            // damit alle Texte übersetzt werden
            mSettingsIntent.putExtra(ISetup.KEY_INIT_SEITE, 0);
            if (ASetup.mPreferenzen.getBoolean(ISetup.KEY_OPT_SPRACHE_CHANGE, false)) {
                ASetup.mPreferenzen.edit().putBoolean(ISetup.KEY_OPT_SPRACHE_CHANGE, false).apply();
                mArbeitsplatz.delete();
                ASetup.zustand = ISetup.INIT_ZUSTAND_UNGELADEN;
                mSettingsIntent.putExtra(ISetup.KEY_EDIT_JOB, 0);
            }
            mSettingsIntent.setClass(mContext, InitAssistent.class);
            mSettingsIntent.putExtra(ISetup.KEY_INIT_SEITE, 1);
        }
        mSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mSettingsIntent);
    }
}
