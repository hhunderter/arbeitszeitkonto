/* Copyright 2014-2019 askanimus@gmail.com */

/* This File is part of "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).  If not, see <http://www.gnu.org/licenses/>.
 *
 * Diese Datei ist Teil von "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2).
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) ist Freie Software: Sie können es unter den Bedingungen
 * der GNU General Public License, wie von der Free Software Foundation,
 * Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
 * veröffentlichten Version, weiter verteilen und/oder modifizieren.
 *
 * "Arbeitszeitkonto" (askanimus.arbeitszeiterfassung2) wird in der Hoffnung, dass es nützlich sein wird, aber
 * OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 * Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 * Siehe die GNU General Public License für weitere Details.
 *
 * Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 * Programm erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
 */
package askanimus.arbeitszeiterfassung2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterViewAnimator;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Locale;

import askanimus.arbeitszeiterfassung2.setup.ASetup;
import askanimus.arbeitszeiterfassung2.setup.ISetup;


public class MinutenInterpretationDialog {
    private Uhrzeit zeit;

    public MinutenInterpretationDialog(
            Context ctx,
            Boolean isDezimal,
            /*BigInteger stunden,*/
            double minuten,
            BigDecimal uhrzeit,
            MinutenInterpretationDialogListener listener
    ) {
        // der Stundenanteil mit Vorzeichen
        final int iStunden = uhrzeit.intValue();
        // die Minuten mit Vorzeichen
        final int iMinuten = (int)Math.round(minuten * 100);
        // die Minuten ohne Vorzeichen
        int mMinuten = iMinuten < 0 ? -iMinuten : iMinuten;

        final int dAntwort = ASetup.mPreferenzen.getInt(ISetup.KEY_ANTWORT_DEZ, ISetup.DEZIMAL_ANTWORT_KEINE);

        if ((mMinuten > 0 && mMinuten <= 59) && dAntwort == ISetup.DEZIMAL_ANTWORT_KEINE) {
            if (ctx != null) {
                zeit = new Uhrzeit(uhrzeit.floatValue());
                final View vFrage = LayoutInflater
                        .from(ctx)
                        .inflate(R.layout.fragment_dialog_frage_zeit, null);
                AlertDialog.Builder aFrage = new AlertDialog.Builder(ctx);
                aFrage.setView(vFrage);

                // den Button mit den umgerechneten Normalminuten erzeugen
                aFrage.setPositiveButton(
                        zeit.getStundenString(false, isDezimal),
                        (dialog, which) -> {
                            // Antwort des Users merken
                            antwortMerken(vFrage, ISetup.DEZIMAL_ANTWORT_UHRZEIT);
                            // interpretierte Zeit zurück geben
                            listener.onZeitSet(zeit);
                        });


                // den Button mit den Industrieminuten
                String buttonText;
                if(iStunden == 0 && iMinuten < 0){
                   buttonText = String.format(Locale.getDefault(),"-%d:%02d",iStunden,mMinuten);
                } else {
                    buttonText = String.format(Locale.getDefault(),"%d:%02d",iStunden,mMinuten);
                }
                aFrage.setNegativeButton(
                        buttonText,
                        (dialog, which) -> {
                            // die eingeebenen Minuten nicht umrechnen
                            zeit.set(iStunden, iMinuten);
                            // Antwort des Users merken
                            antwortMerken(vFrage, ISetup.DEZIMAL_ANTWORT_DEZIMAL);
                            // interpretierte Zeit zurück geben
                            listener.onZeitSet(zeit);
                        });
                aFrage.create().show();
            }
        } else {
            if (dAntwort == ISetup.DEZIMAL_ANTWORT_DEZIMAL) {
                zeit = new Uhrzeit(iStunden, iMinuten/*stunden.intValue(), (int)Math.round(minuten * 100)*/);
            } else {
                zeit = new Uhrzeit(uhrzeit.floatValue());
            }
            // interpretierte Zeit zurück geben
            listener.onZeitSet(zeit);
        }
    }

    private void antwortMerken(View dialog, int antwort) {
        // Antwort des Users merken
        AppCompatCheckBox mMerken = dialog.findViewById(R.id.F_button_merken);
        if (mMerken.isChecked()) {
            SharedPreferences.Editor mEdit = ASetup.mPreferenzen.edit();
            mEdit.putInt(ISetup.KEY_ANTWORT_DEZ, antwort).apply();
        }
    }

    // das Callback Interface
    public interface MinutenInterpretationDialogListener {
        void onZeitSet(Uhrzeit z);
    }
}
